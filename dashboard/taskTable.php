      
 <?php
include ('../config/phpConfig.php');
    //fetch table rows from mysql db
    $sql = "SELECT serial_reference, tag_reference, last_updated, last_updated_by FROM kuk.inventory_master where tag_reference like '%REP%' AND last_updated < DATE_SUB(NOW(), INTERVAL 1 HOUR);";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = $row;
    }

    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>