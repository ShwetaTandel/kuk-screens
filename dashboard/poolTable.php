      
 <?php
include ('../config/phpConfig.php');
    //fetch table rows from mysql db
    $sql = "SELECT inventory_master.location_code, COUNT(inventory_master.id) as Ammout, MIN(inventory_master.date_created) AS date_created, inventory_master.part_number, inventory_master.serial_reference,  round(inventory_qty/1000) as QTY FROM kuk.inventory_master, kuk.location where inventory_master.current_location_id = location.id AND inventory_master.location_code LIKE 'DOC%' AND inventory_master.date_created <= date_sub(now(), interval 4 hour) group by location_code";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = $row;
    }

    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>