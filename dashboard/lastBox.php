 <?php
 //database
include ('../config/phpConfig.php');


    //fetch table rows from mysql db
    $sql = "select count(*) as count from inventory_master where part_number in
		    (select part_number from inventory_master where location_code = 'STKCHK')
			and part_number not in (select part_number from inventory_master where location_code != 'STKCHK')";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
      $emparray[] = $row;
    }

    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>

