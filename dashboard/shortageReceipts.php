      
 <?php
include ('../config/phpConfig.php');
    //fetch table rows from mysql db
    $sql = "SELECT distinct(receipt_header.customer_reference) AS customer_reference, TIMESTAMPDIFF(MINUTE,receipt_header.date_created,now()) as timediff FROM kuk.receipt_header LEFT JOIN kuk.receipt_body on receipt_header.id=receipt_body.receipt_header_id where  receipt_body.shortage = true and receipt_header.document_status_id =1 ";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = $row;
    }

    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>