
// get day and week All Locations
$.ajax({
    url: '../dashboard/locationsCounted.php',
    type: 'GET',
    data: {var : 'day'},
    dataType: "json",
    success: function (data) {
        document.getElementById('counted').innerHTML = data[0].count;
    }
});
$.ajax({
    url: '../dashboard/locationsCounted.php',
    type: 'GET',
    data: {var : 'week'},
    dataType: "json",
    success: function (data) {
        document.getElementById('weekCount').innerHTML = data[0].count;
    }
});
//-----------------------------------------------------------------------

//Get daily F6 box count 
$.ajax({
    url: '../dashboard/f6Box.php',
    type: 'GET',
    dataType: "json",
    success: function (data) {
        document.getElementById('f6Box').innerHTML = data[0].count;
    }
});
//---------------------------------------------------------------------


//Get No header recept 
/*$.ajax({
    url: '../dashboard/recNoHead.php',
    type: 'GET',
    dataType: "json",
    success: function (data) {
        document.getElementById('recCheck').innerHTML = data[0].count;
        
        if(data[0].count > 200){
                            var d = document.getElementById("recBox");
                d.className += " pulse";
        }
    }
});*/
//---------------------------------------------------------------------
//get day and week number of serials not existent 
$.ajax({
    url: '../dashboard/notExist.php',
    type: 'GET',
    data: {var : 'day'},
    dataType: "json",
    success: function (data) {
        if (data[0].count > 0) {
            document.getElementById('notExist').innerHTML = data[0].count;
        } else {
            document.getElementById('notExist').innerHTML = 0;
        }
    }
});
$.ajax({
    url: '../dashboard/notExist.php',
    type: 'GET',
    data: {var : 'week'},
    dataType: "json",
    success: function (data) {
        if (data[0].count > 0) {
            document.getElementById('weekNotExist').innerHTML = data[0].count;
        } else {
            document.getElementById('weekNotExist').innerHTML = 0;
        }
    }
});
//--------------------------------------------------------------------------

//Get day and week number of accurate locations
$.ajax({
    url: '../dashboard/locationsAccurate.php',
    type: 'GET',
    data: {var : 'day'},
    dataType: "json",
    success: function (data) {
        document.getElementById('accurate').innerHTML = data[0].count;

        var acc = document.getElementById('accurate').innerHTML;
        var count = document.getElementById('counted').innerHTML;
        document.getElementById('totalPer').innerHTML = Math.round(acc / count * 100);

        if (acc === 0 && count === 0) {
            document.getElementById('totalPer').innerHTML = " "
        }

        var quickInt = setInterval(function () {
            fixPercent();
        }, 900);
        setInterval(function () {
            if (document.getElementById('totalPer').innerHTML < 40) {
                var d = document.getElementById("cc-box");
                d.className += " pulse";
            }

            clearInterval(quickInt);
        }, 1000);
    }
});
$.ajax({
    url: '../dashboard/locationsAccurate.php',
    type: 'GET',
    data: {var : 'week'},
    dataType: "json",
    success: function (data) {
        document.getElementById('weekAccurate').innerHTML = data[0].count;

        var acc = document.getElementById('weekAccurate').innerHTML;
        var count = document.getElementById('weekCount').innerHTML;
        document.getElementById('WeekTotalPer').innerHTML = Math.round(acc / count * 100);

        var quickInt = setInterval(function () {
            fixPercent2();
        }, 900);
        setInterval(function () {
            if (document.getElementById('WeekTotalPer').innerHTML < 40) {
                var d = document.getElementById("cc-box2");
                d.className += " pulse";
            }

            clearInterval(quickInt);
        }, 1000);
    }
});
//--------------------------------------------------------------------------------


//Get day and week count all wrong locations
$.ajax({
    url: '../dashboard/wrongLoc.php',
    type: 'GET',
    data: {var : 'day'},
    dataType: "json",
    success: function (data) {
        if (data[0].count > 0) {
            document.getElementById('wrongLoc').innerHTML = data[0].count;
        } else {
            document.getElementById('wrongLoc').innerHTML = 0;
        }
    }
});
$.ajax({
    url: '../dashboard/wrongLoc.php',
    type: 'GET',
    data: {var : 'week'},
    dataType: "json",
    success: function (data) {
        if (data[0].count > 0) {
            document.getElementById('weekWrongLoc').innerHTML = data[0].count;
        } else {
            document.getElementById('weekWrongLoc').innerHTML = 0;
        }
    }
});
//-------------------------------------------------------------------------


//Get day and week count all missing serials
$.ajax({
    url: '../dashboard/missingSerial.php',
    type: 'GET',
    data: {var : 'day'},
    dataType: "json",
    success: function (data) {
        document.getElementById('missing').innerHTML = data[0].count;
    }
});
$.ajax({
    url: '../dashboard/missingSerial.php',
    type: 'GET',
    data: {var : 'week'},
    dataType: "json",
    success: function (data) {
        document.getElementById('weekMissing').innerHTML = data[0].count;
    }
});
//-------------------------------------------------------------------------

//Get last box count 
$.ajax({
    url: '../dashboard/lastBox.php',
    type: 'GET',
    dataType: "json",
    success: function (data) {
        document.getElementById('lastBox').innerHTML = data[0].count;
    }
});
//---------------------------------------------------------------------
//Get last box count 
$.ajax({
    url: '../dashboard/stkCheck.php',
    type: 'GET',
    dataType: "json",
    success: function (data) {
        document.getElementById('stkCheck').innerHTML = data[0].count;
    }
});
//---------------------------------------------------------------------


//-----------------------------------------------------------------------
//Open Recipts
$.ajax({
    url: '../dashboard/recOpen.php',
    type: 'GET',
    dataType: "json",
    success: function (data) {
        document.getElementById('recOpen').innerHTML = data[0].count;
        
        if(data[0].count > 100){
                            var d = document.getElementById("recBoxBottom");
                d.className += " pulse";
        }
    }
});

//--------------------------------------------------------------------------  
//Full no Invy
$.ajax({
    url: '../dashboard/noInv.php',
    type: 'GET',
    dataType: "json",
    success: function (data) {
        document.getElementById('noInv').innerHTML = data[0].count;
        
        if(data[0].count > 24){
                            var d = document.getElementById("recBoxRight");
                d.className += " pulse";
        }
    }
});