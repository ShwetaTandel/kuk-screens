      
 <?php
include ('../config/phpConfig.php');
    //fetch table rows from mysql db
    $sql = "SELECT count(*) as number_of_records, sum(picked = 1) as picked, sum(picked  = 0) as open, sum(date_add(expected_delivery, interval 15 minute)  < current_timestamp() and picked = 0) as overdue FROM document_header where bwlvs = 913 AND date_created > current_date();";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = $row;
    }

    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>