<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Zone Type Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="modal fade" id="mCreateNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New Zone Type</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Zone Type Code:</label>
                                    <div class="controls">
                                        <input type="text" name="nZoneTypeCode" id="nZoneTypeCode" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Zone Type Description:</label>
                                    <div class="controls">
                                        <input type="text" name="nZoneTypDescription" id="nZoneTypeDescription" class="form-control" >
                                    </div>
                                </div>

                            </div>
                           
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="newZoneTypeButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confCreate" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Zone Type Added</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >New Zone Type Added</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="mEditZoneType" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New Zone Type</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <input type="text" name="eID" id="eID" class="form-control" style="display: none;">
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Zone Type Code:</label>
                                    <div class="controls">
                                        <input type="text" name="eZoneTypeCode" id="eZoneTypeCode" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Zone Type Description:</label>
                                    <div class="controls">
                                        <input type="text" name="eZoneTypeDescription" id="eZoneTypeDescription" class="form-control" >
                                    </div>
                             
                            </div>
                          
                              
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-danger"  style="float: left;" id="bDelete">Delete</button>
                        <button type="button" class="btn btn-success" id="editZoneTypeButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Zone Type Edited</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Zone Type Edited</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confCreate" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Aisle Added</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >New Aisle Added</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-sm" id="confEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Aisle Edited</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Aisle Edited</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confDelete" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete: <strong><span id="delMessage2">*</span></strong></h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Are you sure you want to delete <strong><span id="delMessage">*</span></strong></h4>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="conDel">Yes</button>
                        <button type="button" class="btn btn-danger"  data-dismiss="modal" >No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="doneDel" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Zone Type Deleted</h4>
                    </div>

                    <div class="modal-footer" >
                       <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="errorDel" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Zone Type unable to be deleted</h4>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>

            <?php
            include('../common/topNav.php');
            include('../common/sideBar.php');
            ?>

        <!-- Page Content  -->
        <div id="content">
            <br>
            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th>Zone Type Code</th>
                        <th>Description</th>
                        <th>Date Created</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th></th>
                </thead>
                <tfoot>
                    <tr>
                        <th>Zone Type Code</th>
                        <th>Description</th>
                        <th>Date Created</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th></th>
                </tfoot>
            </table>

            <input type="Button"  id="bCreateNew" class="btn btn-warning" value="Create New"/>
            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

        </div>
        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->

<script>


    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }


    $(document).ready(function () {
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>';

        var table = $('#example').DataTable({
            ajax: {"url": "../tableData/zoneTypeTable.php", "dataSrc": ""},
            columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bEdit' class='btn btn-warning' value='Edit'/>"
                }
            ],
            buttons: [
                {extend: 'excel', filename: 'zone_type', title: 'Zone Type'}
            ],
            columns: [
                {data: "location_sub_type_code"},
                {data: "location_sub_type_description"},
                {data: "date_created"},
                {data: "last_updated"},
                {data: "last_updated_by"},
                {data: ""}
            ],
            order: [[0, 'asc']]
        });
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });

     $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });


        $("#bCreateNew").unbind().on("click", function () {
            $('#mCreateNew').modal('show');
        });
        $("#newZoneTypeButton").unbind().on("click", function () {

            //  var newType = document.getElementById('nChargeType').value;
            var newCode = document.getElementById('nZoneTypeCode').value;
            var newDesc = document.getElementById('nZoneTypeDescription').value;
           // var newPltZone = document.getElementById('nPltZone').checked ? true:false;

                   if(newCode.length === 0 ){
                alert('Zone Type Code Cannot Be Blank');
                return;
            }
            
            $('#mCreateNew').modal('hide');

            var obj = {"zoneTypeCode": newCode, "zoneTypeDescription": newDesc, "createdBy": currentUser, "updatedBy": currentUser};
            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            console.log(filter);


            $.ajax({
                url: callPostService + filter  + "&function=createZoneType" + screen,
                type: 'GET',
                success: function (response, textstatus) {
                    if(response === 'OK - true'){
                    $('#confCreate').modal('show');
                }else{
                    alert('Unable to Add');
                }
                }
            });
        });
        $('#example tbody').unbind().on('click', '#bEdit', function () {

            $('#mEditZoneType').modal('show');
            var data = table.row($(this).parents('tr')).data();
            document.getElementById('eID').value = data.id;
            document.getElementById('eZoneTypeCode').value = data.location_sub_type_code;
            document.getElementById('eZoneTypeDescription').value = data.location_sub_type_description;
         //   document.getElementById('ePltZone').checked = (data.plt_zone === '1');
        });
 
        $("#editZoneTypeButton").unbind().on("click", function () {

            var newID = document.getElementById('eID').value;
            var newCode = document.getElementById('eZoneTypeCode').value;
            var newDesc = document.getElementById('eZoneTypeDescription').value;
        //    var newPltZone = document.getElementById('ePltZone').checked ? 1:0;
            
            if(newCode.length === 0 ){
                alert('Zone Type Code Cannot Be Blank');
                return;
            }
            

            $('#mEditZoneType').modal('hide');

            var obj = {"id": newID, "zoneTypeCode": newCode, "zoneTypeDescription": newDesc, "updatedBy": currentUser};
            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            console.log(filter);


            $.ajax({
                url: callPostService + filter  + "&function=editZoneType" + screen,
                type: 'GET',
                success: function (response, textstatus) {
                    if(response === 'OK - true'){
                    $('#confEdit').modal('show');
                }else{
                    alert('Unable to Edit');
                }
                }
                
            });
        });
        $("#bDelete").unbind().on("click", function () {

            $('#confDelete').modal('show');
            document.getElementById("delMessage").innerHTML = document.getElementById('eZoneTypeCode').value;
            document.getElementById("delMessage2").innerHTML = document.getElementById('eZoneTypeCode').value;

        });

        $("#conDel").unbind().on("click", function () {
            var zoneID = document.getElementById('eID').value;
            //   alert('DELETE: ' + rowID);

            var filter = 'zoneTypeId=' + zoneID;

            $.ajax({
                url: callGetService + filter + "&function=deleteZoneType"  + screen,
                type: 'GET',
                success: function (response, textstatus) {
                    if (response === 'true') {
                        $('#mEditZoneType').modal('hide');
                        $('#confDelete').modal('hide');
                        $('#doneDel').modal('show');
                        
                    } else {
                        $('#mEditZoneType').modal('hide');
                        $('#confDelete').modal('hide');
                        $('#errorDel').modal('show');
                        
                    }
                }
            });
        });
    });

</script>
</body>
</html>



