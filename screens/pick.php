<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Pick Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>

        <div class="modal fade bd-example-modal-sm" id="mDoneUnAllo" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Order Unallocation</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong>Order has been Unallocated</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="bdone" onclick="$('#example').DataTable().ajax.reload(null, false);" data-dismiss="modal" >Ok</button>

                    </div>
                </div>
            </div>
        </div>
         <div class="modal fade" id="mCloseTrain" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Close Train</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Train Location:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="nTrainLocation">                                                                  
                                            <?php
                                            include ('../config/phpConfig.php');
                                            // Check connection
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'select distinct(inventory_master.location_code) from '. $mDbName . '.inventory_master, '. $mDbName . '.location, '. $mDbName . '.inventory_status  where current_location_id = location.id and inventory_master.inventory_status_id = inventory_status.id and inventory_status_code= "PKD" and location.location_code = "TRAIN"');
                                            echo "<option value=\"$value\"></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['location_code'] . '">' . $row['location_code'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="newButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="mSummR" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title label" style="font-size:120%; color:#323232;" id="exampleModalLabel">Print Options</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" align="center">
                        <input type="text" id="printHelp" style="display: none;">
                        <button style="width: 200px" class='btn btn-info' id="summThree" type='button'>Print Train Labels</button><br><br>
                        <button style="width: 200px" class='btn btn-primary' id="summFour" type='button'>All Pick Details</button><br><br>
                        <button style="width: 200px" class='btn btn-primary' id="summFive" type='button'>Pick Shortages</button><br><br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="printClose" class="btn btn-default" data-dismiss="modal" >Done</button>
                    </div>
                </div>
            </div>
        </div>        
        
         <div class="modal fade" id="mShowZebraPrinters" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title label" style="font-size:120%; color:#323232;" id="exampleModalLabel">Print Options</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" align="center">
                        <input type="hidden" id="pickdetailid"/>
                  
                         <?php
                                include ('../config/phpConfig.php');
                                if (mysqli_connect_errno()) {
                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                }
                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.printer_mapping where printer_code like "ZEB%"');
                               
                                while ($row = mysqli_fetch_array($result)) {
                                    echo '  <button class="dropdown-item btn-success" name="picklabelprint" id="'.$row['printer_address'].'" >'.$row['printer_code'].'</button>';
                                }
                                echo '';
                                mysqli_close($con);
                                ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="printClose" class="btn btn-default" data-dismiss="modal" >Done</button>
                    </div>
                </div>
            </div>
        </div>        

        <?php
        include('../common/topNav.php');
        include('../common/sideBar.php');
        ?>
        <!-- Page Content  -->
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>Document Reference</th>
                         <th>Pick Type </th>
                        <th class="yes">Document Status</th>
                        <th>Order Reference</th>
                         <th>Machine Model</th>
                          <th>BAAN Serial</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th>Options</th>
                </thead>
                <tfoot>
                    <tr>
                        <th></th>
                        <th>Document Reference</th>
                        <th>Pick Type </th>
                        <th>Document Status</th>
                           <th>Order Reference</th>
                         <th>Machine Model</th>
                          <th>BAAN Serial</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th>Options</th>
                </tfoot>
            </table>

            <input type="Button" id="bCloseTrain" class="btn btn-warning" value="Close Train"/>
            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

        </div>

        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->

<script>
    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }
    function formatBody(d) {
        // `d` is the original data object for the row
        return '<table id="pBody" class="compact" border="0" style="padding-left:20px;  width:90%;">' +
                '<thead>' +
                '<th></th>' +
                '<th>Part Number</th>' +
                   '<th>Issue Location</th>' +
                '<th>Expected Qty</th>' +
                '<th>Allocated Qty</th>' +
                '<th>Transacted Qty</th>' +
                '<th>Shortage Qty</th>' +
                '<th>Processed</th>' +
                '<th>Pick Task</th>' +
                '<th>Last Updated By</th>' +
                '<th>Last Updated Date</th>' +
                '</thead>' +
                '</table>';
    }
    function formatDetail(d) {
        // `d` is the original data object for the row
        return '<table id="pDetail" class="compact" border="0" style="padding-left:20px; width:90%;" >' +
                '<thead>' +
                '<th>Part Number</th>' +
                '<th>Serial Reference</th>' +
                '<th>Location Code</th>' +
                '<th>Allocated Qty</th>' +
                '<th>Scanned Qty</th>' +
                '<th>Serial Reference Scanned</th>' +
                '<th>Line No</th>' +
                '<th>Processed</th>' +
                '<th>Last Updated Date</th>' +
                '<th>Last Updated by</th>' +
                '<th>Options</th>'+
                '</thead>' +
                '</table>';
    }

    $(document).ready(function () {
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>';
       

        

        var table = $('#example').DataTable({
            ajax: {"url": "../tableData/pickTableFromView.php"},
            
            serverSide : true,
            dom: 'Blfrtipb',
            columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bOptions' class='btn btn-primary' value='Options'/> <input type='Button' id='bUnallocate' class='btn btn-danger' value='Unallocate'/>"
                } ,
                {
                    "width": "150",
                    "targets": [  4, 5, 6 /* Just include all visible columns here */ ]
                }

            ],
            buttons: [
                {extend: 'excel', filename: 'Pick', title: 'Pick'}
            ],
            columns: [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {data: "document_reference"},
                {data: "pick_type"},
                {data: "status"},
                  {data: "order_reference"},
                    {data: "machine_model"},
                      {data: "baan_serial"},
                {data: "last_updated_date"},
                {data: "last_updated_by"},
                {data: ""}
            ],
            order: [[1, 'desc']]
        });
         $('#example tfoot th').each( function (i) {
        if(i!==6 && i!==0 && i!==11 && i!==9){
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        }
        } );
        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change clear', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
                }
            } );
        } );
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });
        $('#bCloseTrain').click(function () {
            $('#mCloseTrain').modal('show');
        });
        
        
         $('#newButton').click(function () {
            var trainLocation = document.getElementById('nTrainLocation').value
            if(trainLocation.length <= 0 ) {
                alert('Train can not be blank');
                return;
            }

            $('#mCloseTrain').modal('hide');
            var values = trainLocation.split(" - ");

            var obj = 'trainLocation=' + encodeURIComponent(trainLocation)  + '|AND|userId=' + currentUser ;
//+filter+ "&function=closeTrain"+pick
            var filter = obj;
               $.ajax({
                    url: '../action/checkCloseTrain.php',
                    type: 'GET',
                    data: {pickRef: values[0]},
                    success: function (result) {
                        var data = JSON.parse(result);
                        if(parseInt(data) > 0){
                            if(confirm("The Pick Reference has open picks. Do you want go ahead and close the train?")){
                                $.ajax({
                                  url: callGetService ,
                                   data: {function: "closeTrain", filter: filter, "connection":pickServiceUrl},
                                   type: 'GET',
                                   success: function (response,textstatus) {  //
                                       if (response.substring(0, 5) == 'FAIL-') {
                                           alert('ERROR\n\ Unable to Close train')
                                           console.log(response)
                                       }else {
                                          alert("Train closed successfully");
                                          location.reload();
                                       }
                                   }
                                });  
                            }
                        }
                    }
                });


        
        });
        
        
        

        $('#example tbody').on('click', '#bUnallocate', function () {
            var data = table.row($(this).parents('tr')).data();
            var docRef = data.document_reference;
            var filter = 'documentReference=' + docRef + '|AND|userId=' + currentUser;
            if(confirm("Are you sure you want to UnAllocate "+docRef+ " ?")){
            $.ajax({
                
                url: callGetService +filter+ "&function=unAllocate"+pick,
                type: 'POST',
                success: function (response, textstatus) {

                    if (response === "true") {
                        $('#mDoneUnAllo').modal('show');
                    } else {
                        alert('Cannot un-allocate');
                        console.log(response);
                    }
                }
            });
        }
        
        });



        $('#example tbody').on('click', '#bOptions', function () 
		{
//			console.log("TEST2");
            data = table.row($(this).parents('tr')).data();
            $('#mSummR').modal('show');
            document.getElementById('printHelp').value = data.document_reference;    
    
            $('#summThree').click(function () {
                window.open("../reports/trainTrolleyLabel.php?pickreference=" + document.getElementById('printHelp').value);
                $('#example').DataTable().ajax.reload(null, false);
            });
 
            $('#summFour').click(function () {
                window.open("../reports/pickdetails.php?pickreference=" + document.getElementById('printHelp').value);
                $('#example').DataTable().ajax.reload(null, false);
                console.log("button works");
            });
            
            $('#summFive').click(function () {
                window.open("../reports/pickshortagedetails.php?pickreference=" + document.getElementById('printHelp').value);
                $('#example').DataTable().ajax.reload(null, false);
                console.log("button works");
            });
            
        });
//             $('#example_filter label input').on("focus", function (event) {
//            //console.log('Focus')
//            $('#example').DataTable().ajax.reload(null, false);
//
//        });


        $('#example tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = table.row(tr).data();


            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                if (table.row('.shown').length) {
                    $('.details-control', table.row('.shown').node()).click();
                }

                var rowID = data.id;
                row.child(formatBody(row.data())).show();
                tr.addClass('shown');
                pickBodyTable(rowID);
            }
        });

        function pickBodyTable(rowID) {
            var table = $('#pBody').DataTable({
                ajax: {"url": "../tableData/pickBodyTable.php", "data": {rowID: rowID}, "dataSrc": ""},
                searching: false,
                select: {
                    style: 'os',
                    selector: 'td:not(:first-child)'

                },
                paging: false,
                info: false,
                columns: [
                    {
                        "className": 'table-controls',
                        "orderable": false,
                        "data": null,
                        "defaultContent": ''
                    },
                    {data: "part_number"},
                    {data: "issue_location"},
                    {data: "qty_expected"},
                    {data: "qty_allocated"},
                    {data: "qty_transacted"},
                    {data: "qty_shortage"},
                    {data: "processed",
                        render: function (data, type, row) {
                            return (data === '1') ? '<span style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color: red;" class="glyphicon glyphicon-remove"></span>';
                        }
                    },
                    {data: "pick_task"},
                    {data: "last_updated_by"},
                    {data: "last_updated_date"}


                ],
                order: [[1, 'asc']]
            });


            $('#pBody tbody').on('click', 'td.table-controls', function () {
                var tr = $(this).closest('tr');
                var row = table.row(tr);
                var data = table.row(tr).data();


                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    // Open this row
                    if (table.row('.shown').length) {
                        $('.table-controls', table.row('.shown').node()).click();
                    }

                    var rowID = data.id;
                    console.log(rowID)
                    row.child(formatDetail(row.data())).show();
                    tr.addClass('shown');
                    pickDetailTable(rowID);
                }
            });
        }
        function pickDetailTable(rowID) {
            var table = $('#pDetail').DataTable({
                ajax: {"url": "../tableData/pickDetailTable.php", "data": {rowID: rowID}, "dataSrc": ""},
                searching: false,
                select: {
                    style: 'os',
                    selector: 'td:not(:first-child)'

                },
                paging: false,
                info: false,
                columnDefs: [{
                        targets: -1,
                        data: null,
                        orderable: false,
                        defaultContent: "<input type='Button' id='bPrintLabel' class='btn btn-danger' value='Print Label'/>"
                    }

                ],
                "createdRow": function (row, data, dataIndex) {
                    if(data.blocked!==null && data.blocked===1){
                    	 $(row).css('background-color', '#CD5C5C');
                     }
             
                 },
              
                columns: [
                    {data: "part_number"},
                    {data: "serial_reference"},
                    {data: "location_code"},
                    {data: "qty_allocateds"},
                    {data: "qty_scanneds"},
                    {data: "serial_reference_scanned"},
                    {data: "line_nos"},
                    {data: "processed",
                        render: function (data, type, row) {
                            return (data === '1') ? '<span style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color: red;" class="glyphicon glyphicon-remove"></span>';
                        }
                    },
                    {data: "last_updated_by"},
                    {data: "last_updated_date"},
                     {data: ""}


                ],
                order: [[0, 'asc']]
            });
             $('#pDetail tbody').on('click', '#bPrintLabel', function () {
                var data = table.row($(this).parents('tr')).data();
                $('#mShowZebraPrinters').modal('show');
                document.getElementById('pickdetailid').value = data.id;
               
         });
          $('button[name="picklabelprint"]').unbind().click(function () {
             var printer = this.id;
              $.ajax({
                    url: "../action/printLabels.php?function=picklabelzebra&filter=pickdetailid=" + document.getElementById('pickdetailid').value  + "|AND|printer="+printer,
                    type: 'GET',
                    success: function (response, textstatus) {
                        if (response.startsWith('OK')) {
                            alert("The print labels are successfully printed.");
                            $('#mShowZebraPrinters').modal('hide');
                        } else {
                            alert(response);
                        }
                    }
                });
                table.ajax.reload(null, false);
         
         });
        }
        

    });

</script>
</body>
</html>



