<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Location Type Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="modal fade " id="mCreateNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New location Type</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-2 col-lg-6">
                                <div class="form-group">
                                    <label class="input-group-text">Location Type Code:</label>
                                    <div class="controls">
                                        <input type="text" name="nLocationTypeCode" id="nLocationTypeCode" class="form-control" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-lg-6">
                                <div class="form-group">
                                    <label class="input-group-text">Location Type Description:</label>
                                    <div class="form-controls">
                                        <input type="text" name="nLocationTypDescription" id="nLocationTypeDescription" class="form-control" >
                                    </div>
                                </div>
                            </div>



                        </div>
                        <div class="row">
                            <div class="col-md-2 col-lg-6">
                                <div class="form-group">
                                    <label class="input-group-text">Is Pallet Zone:</label>
                                    <div class="form-controls">
                                        <input type="checkbox" name="nPltZone" id="nPltZone" class="input" onchange="enableDropZone('n')">
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-2 col-lg-6">
                                <div class="form-group">
                                    <label class="input-group-text">Drop Zone:</label>
                                    <div class="form-controls">
                                        <select class="form-control" id="nDropZoneId" disabled="true">
                                            <?php
                                            include ('../config/phpConfig.php');
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.location where location_type_id in (select id from location_type where location_type_code = "DZ")');
                                            echo "<option value='-1'></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['id'] . '">' . $row['location_code'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>

                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 col-lg-4">
                                <div class="form-group">
                                    <label class="input-group-text">Is Train:</label>
                                    <div class="controls">
                                        <input type="checkbox" name="nIsTrain" id="nIsTrain" class="input" >
                                    </div>
                                </div>
                            </div>
                             <div class="col-md-2 col-lg-4">
                                <div class="control-group">
                                    <label class="input-group-text">Is Pickable:</label>
                                    <div class="controls">
                                        <input type="checkbox" name="nIsPickable" id="nIsPickable" class="input" >
                                    </div>
                                </div>
                            </div>
                             <div class="col-md-2 col-lg-4">
                                <div class="control-group">
                                    <label class="input-group-text">No Serial Scan:</label>
                                    <div class="controls">
                                        <input type="checkbox" name="nNoSerialScan" id="nNoSerialScan" class="input" >
                                    </div>
                                </div>
                            </div>
                        </div>
                          <div class="row">
                         
                            <div class="col-md-2 col-lg-4">
                                <div class="control-group">
                                    <label class="input-group-text">Pick Time: (MM:SS)</label>
                                    <div class="controls">
                                        <input type="number" name="nPickTimeMin" id="nPickTimeMin" class="input" min="0" max="60" value="0"/>
                                        <input type="number" name="nPickTimeSecs" id="nPickTimeSecs" class="input" min="0" max="60" value ="0"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- </div>-->
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="newLocationTypeButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confCreate" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">location Type Added</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >New location Type Added</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="mEditLocationType" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Location Type</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <input type="text" name="eID" id="eID" class="form-control" style="display: none;">
                            <h2 style="text-align: center;"> <span id="noticeMes"></span></h2>

                        </div>
                        <div class="row">
                            <div class="col-md-2 col-lg-6">
                                <div class="form-group">
                                    <label class="input-group-text">Location Type Code:</label>
                                    <div class="form-controls">
                                        <input type="text" name="eLocationTypeCode" id="eLocationTypeCode" class="form-control" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-lg-6">
                                <div class="form-group">
                                    <label class="input-group-text">Location Type Description:</label>
                                    <div class="form-controls">
                                        <input type="text" name="eLocationDescription" id="eLocationTypeDescription" class="form-control" >
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-2 col-lg-6">
                                <div class="form-group">
                                    <label class="input-group-text">Is Pallet Zone:</label>
                                    <div class="form-controls">
                                        <input type="checkbox" name="nPltZone" id="ePltZone" class="input" onchange="enableDropZone('e')">
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-2 col-lg-6">
                                <div class="form-group">
                                    <label class="input-group-text">Drop Zone:</label>
                                    <div class="form-controls">
                                        <select class="form-control" id="eDropZoneId" disabled="true">
                                            <?php
                                            include ('../config/phpConfig.php');
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.location where location_type_id in (select id from location_type where location_type_code = "DZ")');
                                            echo "<option value='-1'></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['id'] . '">' . $row['location_code'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>

                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 col-lg-4">
                                <div class="control-group">
                                    <label class="input-group-text">Is Train:</label>
                                    <div class="controls">
                                        <input type="checkbox" name="eIsTrain" id="eIsTrain" class="input" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-lg-4">
                                <div class="control-group">
                                    <label class="input-group-text">Is Pickable:</label>
                                    <div class="controls">
                                        <input type="checkbox" name="eIsPickable" id="eIsPickable" class="input" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-lg-4">
                                <div class="control-group">
                                    <label class="input-group-text">No Serial Scan:</label>
                                    <div class="controls">
                                        <input type="checkbox" name="eNoSerialScan" id="eNoSerialScan" class="input" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                         
                            <div class="col-md-2 col-lg-4">
                                <div class="control-group">
                                    <label class="input-group-text">Pick Time: (MM:SS)</label>
                                    <div class="controls">
                                        <input type="number" name="ePickTimeMin" id="ePickTimeMin" class="input" min="0" max="60" />
                                        <input type="number" name="ePickTimeSecs" id="ePickTimeSecs" class="input" min="0" max="60" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-danger"  style="float: left;" id="bDelete">Delete</button>
                        <button type="button" class="btn btn-success" id="editLocationTypeButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-sm" id="confEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Location Type Edited</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Location Type Edited</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confDelete" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete: <strong><span id="delMessage2">*</span></strong></h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Are you sure you want to delete <strong><span id="delMessage">*</span></strong></h4>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="conDel">Yes</button>
                        <button type="button" class="btn btn-danger"  data-dismiss="modal" >No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="doneDel" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Location Type Deleted</h4>
                    </div>

                    <div class="modal-footer" >
                        `                  <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="errorDel" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Location Type unable to be deleted - linked to a location</h4>
                    </div>

                    <div class="modal-footer" >
                        `                  <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>








<?php
include('../common/topNav.php');
include('../common/sideBar.php');
?>
        <!-- Page Content  -->
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th>Location Type Code</th>
                        <th>Description</th>
                        <th>Is Train</th>
                        <th>Is Pallet Zone</th>
                        <th>Is Pickable</th>
                        <th>No Serial Scan</th>
                        <th>Created By</th>
                        <th>Date Created</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th></th>
                </thead>
                <tfoot>
                    <tr>
                        <th>Location Type Code</th>
                        <th>Description</th>
                       <th>Is Train</th>
                        <th>Is Pallet Zone</th>
                        <th>Is Pickable</th>
                        <th>No Serial Scan</th>
                        <th>Created By</th>
                        <th>Date Created</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th></th>
                </tfoot>
            </table>

            <input type="Button"  id="bCreateNew" class="btn btn-warning" value="Create New"/>
            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

        </div>

        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->

<script>

    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }
    function enableDropZone(flag){
            
               if(document.getElementById(flag+'PltZone').checked){
                    document.getElementById(flag+'DropZoneId').disabled = false;
               }else{
                   document.getElementById(flag+'DropZoneId').value = -1;
                   document.getElementById(flag+'DropZoneId').disabled = true;
               }
            
    }
    $(document).ready(function () {
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>'
        
        
        var table = $('#example').DataTable({
            ajax: {"url": "../tableData/locationTypeTable.php", "dataSrc": ""},
            columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bEdit' class='btn btn-warning' value='Edit'/>"
                }
            ],
            buttons: [
                {extend: 'excel', filename: 'zone_type', title: 'Zone Type'}
            ],
            columns: [
                {data: "location_type_code"},
                {data: "location_type_description"},
                {data: "is_train",
                    render: function (data, type, row) {
                        return (data === "1") ? '<span data-order="1" style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color:red" data-order="0" class="glyphicon glyphicon-remove"></span>';
                    }, },
                {data: "plt_zone",
                    render: function (data, type, row) {
                        return (data === "1") ? '<span data-order="1" style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color:red" data-order="0" class="glyphicon glyphicon-remove"></span>';
                    }, },

                {data: "is_pickable",
                    render: function (data, type, row) {
                        return (data === "1") ? '<span data-order="1" style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color:red" data-order="0" class="glyphicon glyphicon-remove"></span>';
                    }, },
                {data: "no_serial_scan",
                    render: function (data, type, row) {
                        return (data === "1") ? '<span data-order="1" style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color:red" data-order="0" class="glyphicon glyphicon-remove"></span>';
                    }, },
                {data: "created_by"},
                {data: "date_created"},
                {data: "last_updated"},
                {data: "last_updated_by"},
                {data: ""}
            ],
            order: [[0, 'asc']]
        });
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });


        $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });

        $("#bCreateNew").on("click", function () {
            $('#mCreateNew').modal('show');
        });
        $("#newLocationTypeButton").on("click", function () {

            //  var newType = document.getElementById('nChargeType').value;
            var newCode = document.getElementById('nLocationTypeCode').value;
            var newDesc = document.getElementById('nLocationTypeDescription').value;
            var newIsTrain = document.getElementById('nIsTrain').checked;
            var newIsPickable = document.getElementById('nIsPickable').checked;
            var newPltZone = document.getElementById('nPltZone').checked ;
            var newDropZoneId = document.getElementById('nDropZoneId').value ;
            var newNoSerialScan = document.getElementById('nNoSerialScan').checked ;
              var pickTimeInSecs = Math.floor( document.getElementById('nPickTimeMin').value * 60) + Math.floor(document.getElementById('nPickTimeSecs').value);
            if(newPltZone === true  && newDropZoneId === '-1'){
                alert('Please select a Drop Zone if Pallet Zone is selected.');
                return;
                
            }



            if (newCode.length === 0) {
                alert('Location Type Code Cannot Be Blank');
                return;
            }


            $('#mCreateNew').modal('hide');



            var obj = {"locationTypeCode": newCode, "locationTypeDescription": newDesc,"isTrain":newIsTrain, "pltZone":newPltZone,"dropZoneId":newDropZoneId,
                      "isPickable":newIsPickable,"createdBy": currentUser, "updatedBy": currentUser, "noSerialScan":newNoSerialScan, "pickTime":pickTimeInSecs};
            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            console.log(filter)


            $.ajax({
                
                url: callPostService + filter + "&function=createLocationType" + "&connection=" + screen,
                type: 'GET',
                success: function (response, textstatus) {
                    $('#confCreate').modal('show');
                }
            });
        });
        $('#example tbody').on('click', '#bEdit', function () {

            $('#mEditLocationType').modal('show');
            var data = table.row($(this).parents('tr')).data();
            if (data.location_area === 'Work') {
                document.getElementById('noticeMes').style.display = "inline";
                document.getElementById('noticeMes').innerHTML = "Unable to Edit";
                document.getElementById('eLocationTypeCode').disabled = true;
                document.getElementById('eIsTrain').checked  = (data.is_train === '1') ? true:false;
                document.getElementById('eIsTrain').disabled = true;
                document.getElementById('eLocationTypeDescription').disabled = true;
                document.getElementById('eLocationTypeCode').value = data.location_type_code;
                document.getElementById('eLocationTypeDescription').value = data.location_type_description;
                document.getElementById('ePltZone').checked  = (data.plt_zone === '1') ? true:false;
                document.getElementById('ePltZone').disabled = true;
                document.getElementById('eDropZoneId').disabled = true;
                document.getElementById('eIsPickable').disabled = true;
                document.getElementById('eIsPickable').checked  = (data.is_pickable === '1') ? true:false;
                document.getElementById('eNoSerialScan').disabled = true;
                document.getElementById('eNoSerialScan').checked  = (data.no_serial_scan === '1') ? true:false;
                document.getElementById('ePickTimeMin').value = Math.floor(data.pick_smv / 60);
                 document.getElementById('ePickTimeSecs').value = Math.floor(data.pick_smv % 60);

                

            } else {
                document.getElementById('noticeMes').style.display = "none";
                document.getElementById('noticeMes').innerHTML = "";
                document.getElementById('eLocationTypeCode').disabled = false;
                document.getElementById('eLocationTypeDescription').disabled = false;
                document.getElementById('eIsTrain').disabled = false;
                document.getElementById('eIsTrain').checked  = (data.is_train === '1') ? true:false;
                document.getElementById('eIsPickable').disabled = false;
                document.getElementById('eIsPickable').checked  = (data.is_pickable === '1') ? true:false;
                document.getElementById('ePltZone').disabled = false;
                
                document.getElementById('ePltZone').checked  = (data.plt_zone === '1') ? true:false;
                
                if(data.plt_zone === '1'){
                    document.getElementById('eDropZoneId').disabled = false;
                    document.getElementById('eDropZoneId').value = data.drop_zone_location_id;
                }else{
                    document.getElementById('eDropZoneId').value = -1;
                    document.getElementById('eDropZoneId').disabled = true;
                }
                document.getElementById('eID').value = data.id;
                document.getElementById('eLocationTypeCode').value = data.location_type_code;
                document.getElementById('eLocationTypeDescription').value = data.location_type_description;
                
                 document.getElementById('eNoSerialScan').disabled = false;
                document.getElementById('eNoSerialScan').checked  = (data.no_serial_scan === '1') ? true:false;
                     document.getElementById('ePickTimeMin').value = Math.floor(data.pick_smv / 60);
                 document.getElementById('ePickTimeSecs').value = Math.floor(data.pick_smv % 60);
            }

        });

        $("#editLocationTypeButton").on("click", function () {

            var newID = document.getElementById('eID').value;
            var newCode = document.getElementById('eLocationTypeCode').value;
            var newDesc = document.getElementById('eLocationTypeDescription').value;
            var newIsTrain = document.getElementById('eIsTrain').checked ;
            var newPltZone = document.getElementById('ePltZone').checked ;
            var newDropZoneId = document.getElementById('eDropZoneId').value ;
            var newIsPickable = document.getElementById('eIsPickable').checked;
            var newNoSerialScan = document.getElementById('eNoSerialScan').checked ;
            var pickTimeInSecs = Math.floor( document.getElementById('ePickTimeMin').value * 60) + parseInt(document.getElementById('ePickTimeSecs').value);
            if(newPltZone === true  && newDropZoneId === '-1'){
                alert('Please select a Drop Zone if Pallet Zone is selected.');
                return;
                
            }

            if (newCode.length === 0) {
                alert('Location Type Code Cannot Be Blank');
                return;
            }


            $('#mEditLocationType').modal('hide');

            var obj = {"id": newID, "locationTypeCode": newCode, "locationTypeDescription": newDesc, "isTrain":newIsTrain,"pltZone":newPltZone,"isPickable":newIsPickable, "dropZoneId":newDropZoneId,"updatedBy": currentUser, "noSerialScan":newNoSerialScan, "pickTime":pickTimeInSecs};
            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            console.log(filter)


            $.ajax({
               
               url: callPostService + filter + "&function=editLocationType" + "&connection=" + screen,
               
                type: 'GET',
                success: function (response, textstatus) {
                    $('#confEdit').modal('show');
                }
            });
        });

        $("#bDelete").on("click", function () {

            $('#confDelete').modal('show');
            document.getElementById("delMessage").innerHTML = document.getElementById('eLocationTypeCode').value
            document.getElementById("delMessage2").innerHTML = document.getElementById('eLocationTypeCode').value
        });

        $("#conDel").on("click", function () {
            var locationTypeID = document.getElementById('eID').value;
            //   alert('DELETE: ' + rowID);

            var filter = 'locationTypeId=' + locationTypeID;

            $.ajax({
                url: callGetService + filter+  "&function=deleteLocationType" + screen,
                type: 'GET',
                success: function (response, textstatus) {
                    if (response === "true") {
                         $('#mEditLocationType').modal('hide');
                        $('#confDelete').modal('hide');
                        $('#doneDel').modal('show');
                    } else {
                       $('#mEditLocationType').modal('hide');
                        $('#confDelete').modal('hide');
                        $('#errorDel').modal('show');
                    }


                }
            });
        });



    });

</script>
</body>
</html>



