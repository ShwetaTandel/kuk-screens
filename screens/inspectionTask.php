<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Inspection Task Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="modal fade bd-example-modal-sm" id="mConfEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Order Edited</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton2" data-dismiss="modal" onClick="$('#docDetail').DataTable().ajax.reload(null, false);" >Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-sm" id="mEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Order</h5>
                    </div>
                    <br>
                    <div align="center">
                        <p style="color: black">Line Number: <strong><span id="lineNum">#</span></strong></p>
                        <p style="color: black">Part Number: <strong><span id="partNum">#</span></strong></p>
                        <p style="color: black">Vehicle Number: <strong><span id="vehNum">#</span></strong></p>
                        <input type="hidden" id='eStatus' name='eStatus'/>
                    </div>
                    <hr>
                    <div class="col-xs-12">
                        <div class="control-group" id="eExp">
                            <p style="display: none;"><span id="eId"></span></p>
                            <label class="input-group-text">Expected Qty:</label>
                            <div class="controls">
                                <input type="number" name="eExpected" id="eExpected" class="form-control" disabled>
                            </div>
                        </div>
                        <div class="control-group" id="eTran">
                            <label class="input-group-text">Transacted Qty:</label>
                            <div class="controls">
                                <input type="number" name="eTransacted" id="eTransacted" class="form-control" >
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-danger" id="confUnblock" data-dismiss="modal">Unblock</button>
                        <button type="button" class="btn btn-danger" id="confBlock" data-dismiss="modal" >Block</button>
                        <button type="button" class="btn btn-success" id="confEdit" data-dismiss="modal" >Save</button>
                        <button type="button" class="btn btn-danger" id="cancelEdit" data-dismiss="modal" >Cancel</button>
                    </div>
                </div>
            </div>
        </div>

       
     
     

        <?php
        include('../common/topNav.php');
        include('../common/sideBar.php');
        ?>
        <!-- Page Content  -->
        <div id="content">
            <br>
            <p>Red : BLOCKED</p>
            <table id="example" class="compact stripe hover row-border" >
                <thead>
                    <tr>
                        <th></th>
                        <th>Document Reference</th>
                        <th>Customer Reference</th>

                        <th>Expected Delivery Time</th>


                        <th class="yes">Status</th>

                        <th>Date Created</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>

                </thead>
                <tfoot>
                    <tr>
                        <th></th>
                        <th>Document Reference</th>
                        <th>Customer Reference</th>
                        <th>Expected Delivery Time</th>
                           <th>Status</th>

                        <th>Date Created</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>

                </tfoot>
            </table>

          <!--  <input type="Button" id="bCreateNew" class="btn btn-warning" value="Create New"/>-->
            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>
            

        </div>

        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->

<script>
    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }

    function formatDetail(d) {
        // `d` is the original data object for the row
        return '<table id="docDetail" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                '<thead>' +
                '<th>Line Number</th>' +
                '<th>Part number</th>' +
                '<th>Qty Expected</th>' +
                '<th>Qty Transacted</th>' +
                '<th>Difference</th>' +
                '<th>Vendor Code</th>' +
                '<th>Last Updated By</th>' +
                '<th>Date Created</th>' +
                '<th>Action</th>' +
                '</thead>' +
                '</table>';
    }


    $(document).ready(function () {
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>';
        var rowID;
        var selected = [];
        var table = $('#example').DataTable({
            // serverSide: true, // enables server-side processing
            ajax: {"url": "../tableData/inspectionTaskHeaderTable.php"},
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: 'Export To Excel',
                    exportOptions: {
                        modifier: {
                            page: 'current'
                        }
                    }
                }
            ],
            // dom: 'Blrtipb',
            columnDefs: [
                {
                    targets: 0,
                    className: 'details-control',
                    orderable: false,
                    data: null,
                    defaultContent: ''
                },
                {
                    targets: [5],
                    orderable: false

                }
            ],
            initComplete: function () {
                this.api().columns().every(function (i) {
                    var column = this;
                    if ($(column.header()).hasClass('yes')) {
                        var select = $('<select><option id="ssearch" value=""></option></select>')
                                .appendTo($(column.header()))
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                            );
                                    column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                            //.search(val ? val : '', true, false)
                                            .draw();
                                });
                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                    }


                });
            },
            //what data will be in each column these are done by selected the same of the data
            //from the php data that has been asked for by thr ajax
            columns: [
                // this first column is the column we uses to click for the drop to the body 
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {data: "document_reference"},
                {data: "customer_reference"},
                {data: "expected_delivery_time"},
                {data: "document_status_code"},
                
                {data: "date_created"},
                {data: "last_updated_date"},
                {data: "last_updated_by"}
              
            ],
            order: [[7, 'desc']]
        });
        /*$('#example tfoot th').each(function (i) {
         if (i == 1) {
         var title = $(this).text();
         $(this).html('<input style="width: 100%; text-align: center" type="text" placeholder="Search" />');
         }
         });
         table.columns().every(function () {
         var that = this;
         $('input', this.footer()).on('keyup change clear', function () {
         if (that.search() !== this.value) {
         that
         .search(this.value)
         .draw();
         }
         });
         });
         
         */

        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });
        $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });

       
  


       
        $('#example tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = table.row(tr).data();


            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                //close other open row before opening  a new row
                if (table.row('.shown').length) {
                    $('.details-control', table.row('.shown').node()).click();
                }
                var rowID = data.id;
                row.child(formatDetail(row.data())).show();
                tr.addClass('shown');
                orderDetailTable(rowID);
            }

        });




        function orderDetailTable(rowID) {
            var table = $('#docDetail').DataTable({
                ajax: {"url": "../tableData/inspectionTaskBodyTable.php", "data": {rowID: rowID}, "dataSrc": ""},
                searching: false,
                paging: false,
                info: false,
                "columnDefs": [{
                        targets: -1,
                        data: null,
                        defaultContent: "<input type='Button' id='bEdit' class='btn btn-info' value='Edit'/>"
                    }

                ],
                "createdRow": function (row, data, dataIndex) {
                    if (data.blocked != null && data.blocked == 1) {
                        $(row).css('background-color', '#CD5C5C');
                    }

                },
                columns: [
                    {data: "line_no"},
                    {data: "part_number"},
                    {data: "qty_expected"},
                    {data: "qty_transacted"},
                    {data: "difference"},
                    {data: "vendor_code"},
                    {data: "last_updated_by"},
                    {data: "date_created"},
                    {data: ""}


                ],
                order: [[0, 'asc']]
            });




            $('#docDetail tbody').unbind().on('click', '#bEdit', function () {
                var data = table.row($(this).parents('tr')).data();

                $('#mEdit').modal('show');
                //console.log(data.qty_transacted)
                document.getElementById('eId').innerHTML = data.id;
                document.getElementById('lineNum').innerHTML = data.tapos;
                document.getElementById('partNum').innerHTML = data.matnr;
                document.getElementById('vehNum').innerHTML = data.wempf;
                document.getElementById('eExpected').value = data.qty_expected / 1000;
                document.getElementById('eTransacted').value = data.qty_transacted / 1000;
                document.getElementById('eStatus').value = data.document_status_code;
                if (data.blocked === '1') {
                    document.getElementById('confUnblock').style.display = 'block';
                    document.getElementById('confBlock').style.display = 'none';
                } else {
                    document.getElementById('confUnblock').style.display = 'none';
                    document.getElementById('confBlock').style.display = 'block';

                }


            });

            $('#confEdit').unbind().on('click', function () {
                var eId = document.getElementById('eId').innerHTML;
                var newTranQ = document.getElementById('eTransacted').value;


                $('#mEditRow').modal('hide');

                var obj = {"docDetailId": eId, "transactedQty": newTranQ, "updatedBy": currentUser};
                var newEditjson = JSON.stringify(obj);
                var filter = newEditjson;


                $.ajax({
                    url: callPostService + filter + "&function=editDocumentDetailQty" + screen,
                    type: 'GET',
                    success: function (response, textstatus) {
                        if (response === 'OK - true') {
                            $('#mConfEdit').modal('show');
                        } else {
                            alert(response);
                        }

                    }
                });

            });

            $('#confBlock').unbind().on('click', function () {

                var status = document.getElementById('eStatus').value;
                console.log(status);
                if (status == null || status == 'OPEN') {


                    var eId = document.getElementById('eId').innerHTML;

                    $.ajax({
                        url: '../action/isDocDetailAllocated.php',
                        data: {eId: eId},
                        type: 'GET',
                        success: function (response, textstatus) {
                            if (response[11] > 0) {
                                alert("Detail allocated");
                            } else {
                                $('#blockCheck').modal('show');
                            }
                        }
                    });
                } else {
                    alert("Document is not OPEN ");
                    return
                }

            });

            $('#confUnblock').unbind().on('click', function () {

                var r = confirm("Unblock the Document Detail!");
                var isBlock = 'false';
                if (r == true) {

                    var eId = document.getElementById('eId').innerHTML;
                    var isBlock = false;
                    var obj = {"docDetailId": eId, "isBlock": isBlock, "updatedBy": currentUser};
                    var newEditjson = JSON.stringify(obj);
                    var filter = newEditjson;

                    $.ajax({
                        url: callGetService + filter + "&function=unblockdocumentdetail" + screen,
                        type: 'GET',
                        success: function (response, textstatus) {
                        }
                    });
                } else {
                    txt = "You pressed Cancel!";
                }

            });

            $('#bBlockYes').unbind().on('click', function () {
                var eId = document.getElementById('eId').innerHTML;
                var eBlockedComment = document.getElementById('eblockedComment').value;
                var isBlock = 'true';

                if (eBlockedComment.length <= 0) {
                    alert('please Enter a Comment');
                    return;
                }


                var obj = {"docDetailId": eId, "blockedComment": eBlockedComment, "isBlock": isBlock, "updatedBy": currentUser};
                var newEditjson = JSON.stringify(obj);
                var filter = newEditjson;

                $.ajax({
                    url: callPostService + filter + "&function=blockdocumentdetail" + screen,
                    type: 'GET',
                    success: function (response, textstatus) {
                        if (response === 'OK - true') {
                            $('#blockCheck').modal('hide');
                        } else {
                            alert(response);
                        }
                    }
                });

            });
        }

    });

</script>
</body>
</html>



