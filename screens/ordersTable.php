<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Orders Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="modal fade bd-example-modal-sm" id="mConfEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Order Edited</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton2" data-dismiss="modal" onClick="$('#docDetail').DataTable().ajax.reload(null, false);" >Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-sm" id="mEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Order</h5>
                    </div>
                    <br>
                    <div align="center">
                        <p style="color: black">Line Number: <strong><span id="lineNum">#</span></strong></p>
                        <p style="color: black">Part Number: <strong><span id="partNum">#</span></strong></p>
                        <p style="color: black">Vehicle Number: <strong><span id="vehNum">#</span></strong></p>
                        <input type="hidden" id='eStatus' name='eStatus'/>
                    </div>
                    <hr>
                    <div class="col-xs-12">
                        <div class="control-group" id="eExp">
                            <p style="display: none;"><span id="eId"></span></p>
                            <label class="input-group-text">Expected Qty:</label>
                            <div class="controls">
                                <input type="number" name="eExpected" id="eExpected" class="form-control" disabled>
                            </div>
                        </div>
                        <div class="control-group" id="eTran">
                            <label class="input-group-text">Transacted Qty:</label>
                            <div class="controls">
                                <input type="number" name="eTransacted" id="eTransacted" class="form-control" >
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-danger" id="confUnblock" data-dismiss="modal">Unblock</button>
                        <button type="button" class="btn btn-danger" id="confBlock" data-dismiss="modal" >Block</button>
                        <button type="button" class="btn btn-success" id="confEdit" data-dismiss="modal" >Save</button>
                        <button type="button" class="btn btn-danger" id="cancelEdit" data-dismiss="modal" >Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-sm" id="mSap" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Transfer To Sap</h5>
                    </div>
                    <br>
                    <div align="center">
                        <p style="display: none;"><span id="orderRef"></span></p>
                        Are you sure you want to transfer <strong > <span id="helpSap"></span></strong> to SAP
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="sapYes" data-dismiss="modal" >Yes</button>
                        <button type="button" class="btn btn-danger" id="sapNo" data-dismiss="modal" >No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="doneSap" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Transfer To SAP</h5>
                    </div>
                    <br>
                    <div align="center">
                        Order Transfered To Sap
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-info" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);" >Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="errorSap" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Transfer To SAP</h5>
                    </div>
                    <br>
                    <div align="center">
                        Error on Transfer
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-info" data-dismiss="modal" >Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="mAllo" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Allocate</h5>
                    </div>
                    <br>
                    <div align="center">
                        <p style="display: none"><span id="corNum"></span></p>
                        Are you sure you want to Allocate <strong ><span id="helpAllo"></span></strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="corYes" data-dismiss="modal" >Yes</button>
                        <button type="button" class="btn btn-danger" id="corNo" data-dismiss="modal" >No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="doneAllocate" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Allocate</h5>
                    </div>
                    <br>
                    <div align="center">
                        Order Allocated
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-info" data-dismiss="modal" onClick="location.reload();" >Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="errorAllocate" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Allocate</h5>
                    </div>
                    <br>
                    <div align="center">
                        Error on Allocation
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-info" data-dismiss="modal" onClick="location.reload();">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="mTransfer" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Transfer</h5>
                    </div>
                    <br>
                    <div align="center">
                        <p style="display: none"><span id="corNumTran"></span></p>
                        Are you sure you want to Transfer <strong ><span id="helpTran"></span></strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="corYesTran" data-dismiss="modal" >Yes</button>
                        <button type="button" class="btn btn-danger" id="corNoTran" data-dismiss="modal" >No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="doneTransfer" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Transfer</h5>
                    </div>
                    <br>
                    <div align="center">
                        Order Transmitted
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-info" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);" >Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="errorTransfer" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Transfer</h5>
                    </div>
                    <br>
                    <div align="center">
                        Error on Transfer
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-info" data-dismiss="modal" >Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="blockCheck" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Block</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong > Comment </strong>
                        <select class="form-control" type="text" id="eblockedComment"> 
                            <option>Stock discrepancy</option> 
                            <option>Parts still under inspection</option> 
                            <option>Supplier issue</option> 
                            <option>Parts in Oxford</option>
                            <option>RRMC request</option>  
                        </select>
                    </div>
                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="bBlockYes" >Yes</button>
                        <button type="button" class="btn btn-success" id="bBlockNo" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">No</button>
                    </div>
                </div>
            </div>
        </div>





        <?php
        include('../common/topNav.php');
        include('../common/sideBar.php');
        ?>
        <!-- Page Content  -->
        <div id="content">
            <br>
            <p>Red : BLOCKED</p>
            <table id="example" class="compact stripe hover row-border" >
                <thead>
                    <tr>
                        <th></th>
                        <th>Document Reference <input type="text" placeholder="Search Document Ref" /></th>
                        <th>Customer Reference<input type="text" placeholder="Search Cust Ref" /></th>

                        <th>Expected Delivery Time</th>
                        <th>Order Type<input type="text" placeholder="Search Order Type" /></th>

                        <th>Status<input type="text" placeholder="Search Status" /></th>
                        <th></th>
                        <th>Pick Reference<input type="text" placeholder="Search Pick Ref" /></th>
                        <th>Machine Model</th>
                        <th>BAAN Serial</th>
                        <th>Date Created</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th>Options</th>
                        <th>Action</th>

                </thead>
                <tfoot>
                    <tr>
                        <th></th>
                        <th>Document Reference<input type="text" placeholder="Search Document Ref" /></th>
                        <th>Customer Reference <input type="text" placeholder="Search Cust Ref" /></th>
                        <th>Expected Delivery Time</th>
                        <th><input type="text" placeholder="Search Order Type" /></th>
                        <th><input type="text" placeholder="Search Status" /></th>
                        <th></th>
                        <th><input type="text" placeholder="Search Pick Ref" /></th>
                        <th>Machine Model</th>
                        <th>BAAN Serial</th>
                        <th>Date Created</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th>Options</th>
                        <th>Action</th>

                </tfoot>
            </table>

          <!--  <input type="Button" id="bCreateNew" class="btn btn-warning" value="Create New"/>-->
            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>
            <input type="Button" id="bProcessSelected" class="btn btn-danger" value="Bulk Allocate Selected Orders"/>

        </div>

        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->

<script>
    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }

    function formatDetail(d) {
        // `d` is the original data object for the row
        return '<table id="docDetail" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                '<thead>' +
                '<th>Line Number</th>' +
                '<th>Part number</th>' +
                '<th>Qty Expected</th>' +
                '<th>Qty Transacted</th>' +
                '<th>Difference</th>' +
                '<th>Zone Destination</th>' +
                '<th>Last Updated By</th>' +
                '<th>Date Created</th>' +
                '<th>Action</th>' +
                '</thead>' +
                '</table>';
    }


    $(document).ready(function () {
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>';
        var rowID;
        var selected = [];
        var orderTypes = [];
        var table = $('#example').DataTable({
            // serverSide: true, // enables server-side processing
            ajax: {"url": "../tableData/orderHeaderTable.php"},
            iDisplayLength: 20,
            "processing": true,
            "serverSide": true,
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: 'Export To Excel',
                    exportOptions: {
                        modifier: {
                            page: 'current'
                        }
                    }
                }
            ],
            // dom: 'Blrtipb',
            columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bAllocate' class='btn btn-success' value='Allocate'/><input type='Button' id='bTransfer' class='btn btn-warning' value='Transmit'/>"
                },
                {
                    targets: -2,
                    data: null,
                    defaultContent: "<input type='checkbox' id='bProcess' class='btn btn-danger' value=''/>"
                },
                {
                    targets: 0,
                    className: 'details-control',
                    orderable: false,
                    data: null,
                    defaultContent: ''
                },
                {
                    targets: 6,
                    visible: false
                },
                {
                    targets: [4,5,8,9,12,13 ],
                    orderable: false

                }
            ],
            //what data will be in each column these are done by selected the same of the data
            //from the php data that has been asked for by thr ajax
            columns: [
                // this first column is the column we uses to click for the drop to the body 
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {data: "order_reference"},
                {data: "customer_reference"},
                {data: "expected_delivery_time",
                     render: function (data, type, row) {
                        return data.substring(0,16);
                    }},
                {data: "order_type"},
                {data: "document_status_code"},
                 {data: "document_status_id"},
                {data: "pick_reference"},
                {data: "baan_serial"},
                {data: "machine_model"},
                {data: "date_created"},
                {data: "last_updated_date"},
                {data: "last_updated_by"},
                {data: "",
                    render: function (data, type, row) {
                        return (row.document_status_code !== 'OPEN') ? '<span style="color: grey;" class="glyphicon glyphicon-stop"></span>' : '<input type="checkbox" id="bProcess" class="btn btn-danger" value=""/>';
                    }},
                {data: ""}
            ],
            order: [[6, 'asc'], [10, 'desc']]
        });

        table.columns().every(function () {
            var that = this;
            $('input', this.footer()).on('keyup change', function (e) {
                if (that.search() !== this.value)
                    that.search(this.value);
                // if serverside draw() only on enter
                if (e.keyCode === 13)
                    that.draw();
            });
        });
        table.columns().every(function () {
            var that = this;
            $('input', this.header()).on('keyup change', function (e) {
                if (that.search() !== this.value)
                    that.search(this.value);
                // if serverside draw() only on enter
                if (e.keyCode === 13)
                    that.draw();
            });
        });
       

        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });
        $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });

        $('#example tbody').unbind().on('click', '#bTransfer', function () {
            var data = table.row($(this).parents('tr')).data();
            var corNum = data['order_reference'];
            if (data['document_status_code'] === 'TRANSMITTED') {
                alert('Already Transmitted');
                return;
            } else if (data['document_status_code'] !== 'PICKED') {
                alert('Order has to be Picked in order to be transmitted');
                return;
            }else {
                $('#mTransfer').modal('show');
                document.getElementById('helpTran').innerHTML = data['customer_reference']
                document.getElementById('corNumTran').innerHTML = corNum;
            }

        });
        $('#corYesTran').unbind().on('click', function () {
            var corNum = document.getElementById('corNumTran').innerHTML;

            var filter = 'documentReference=' + corNum + '|AND|userId=' + currentUser;
            console.log(filter);
            $.ajax({
                url: callGetService + filter + "&function=transmitPickMessage" + pick,
                type: 'GET',
                success: function (response, textstatus) {
                    if (response === 'true') {
                        $('#mTransfer').modal('hide');
                        $('#doneTransfer').modal('show');
                    } else {
                        $('#mTransfer').modal('hide');
                        $('#errorTransfer').modal('show');

                    }


                }
            });

        });


        $('#example tbody').on('click', '#bProcess', function () {
            var data = table.row($(this).parents('tr')).data();

            var id = data.order_reference;
            var orderType = data.order_type;
            var index = $.inArray(id, selected);
            if (index === -1) {
                selected.push(id);
            } else {
                selected.splice(index, 1);
            }
            var indType = $.inArray(orderType, orderTypes);
            if (this.checked) {
                orderTypes.push(orderType);
            } else {
                orderTypes.splice(indType, 1);
            }

        });

        $('#example tbody').on('click', '#bAllocate', function () {
            //TODO - Do we have to check for pending status????
            var data = table.row($(this).parents('tr')).data();
            var corNum = data.order_reference;

            //TODO - Do we have to check for pending status????
            if (data.document_status_code !== 'ALLOCATED') {
                $('#mAllo').modal('show');
                document.getElementById('helpAllo').innerHTML = corNum;
                document.getElementById('corNum').innerHTML = corNum;
            } else {
                alert('The selected order is already allocated');
            }

        });
        $('#bProcessSelected').on('click', function () {
            if (selected.length === 0) {
                alert("Please select some rows for processing.")
                return;
            } else {
                var message = "Are you sure you want to allocate the selected orders(" + selected + ")?";
                if (new Set(orderTypes).size > 1) {
                    message = "Are you sure you want to create one pick run for mixed order types from selected orders(" + selected + ")?"
                }
                if (confirm(message)) {

                    var obj = {"docRef": selected, "userId": currentUser};
                    var filter = JSON.stringify(obj);
                    allocateOrders(filter);

                }
            }

        });
        $('#corYes').unbind().on('click', function () {
            var corNum = document.getElementById('corNum').innerHTML;
            var docRef = [];
            docRef.push(corNum);

            var obj = {"docRef": docRef, "userId": currentUser};
            var filter = JSON.stringify(obj);
            allocateOrders(filter);

        });

        function allocateOrders(filter) {
            console.log(filter);
            $.ajax({
                url: callPostService + filter + "&function=allocateOrders" + pick,
                type: 'POST',
                success: function (response, textstatus) {
                    if (response === 'OK - true') {
                        $('#mAllo').modal('hide');
                        $('#doneAllocate').modal('show');

                    } else {
                        $('#mAllo').modal('hide');
                        $('#errorAllocate').modal('show');
                    }
                    
                    
                }
            });

        }


        $('#example tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = table.row(tr).data();


            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                //close other open row before opening  a new row
                if (table.row('.shown').length) {
                    $('.details-control', table.row('.shown').node()).click();
                }
                var rowID = data.id;
                row.child(formatDetail(row.data())).show();
                tr.addClass('shown');
                orderDetailTable(rowID);
            }

        });




        function orderDetailTable(rowID) {
            var table = $('#docDetail').DataTable({
                ajax: {"url": "../tableData/orderBodyTable.php", "data": {rowID: rowID}, "dataSrc": ""},
                searching: false,
                paging: false,
                info: false,
                "columnDefs": [{
                        targets: -1,
                        data: null,
                        defaultContent: "<input type='Button' id='bEdit' class='btn btn-info' value='Edit'/>"
                    }

                ],
                "createdRow": function (row, data, dataIndex) {
                    if (data.blocked != null && data.blocked == 1) {
                        $(row).css('background-color', '#CD5C5C');
                    }

                },
                columns: [
                    {data: "line_no"},
                    {data: "part_number"},
                    {data: "qty_expected"},
                    {data: "qty_transacted"},
                    {data: "difference"},
                    {data: "zone_destination"},
                    {data: "last_updated_by"},
                    {data: "date_created"},
                    {data: ""}


                ],
                order: [[0, 'asc']]
            });




            $('#docDetail tbody').unbind().on('click', '#bEdit', function () {
                var data = table.row($(this).parents('tr')).data();

                $('#mEdit').modal('show');
                //console.log(data.qty_transacted)
                document.getElementById('eId').innerHTML = data.id;
                document.getElementById('lineNum').innerHTML = data.tapos;
                document.getElementById('partNum').innerHTML = data.matnr;
                document.getElementById('vehNum').innerHTML = data.wempf;
                document.getElementById('eExpected').value = data.qty_expected / 1000;
                document.getElementById('eTransacted').value = data.qty_transacted / 1000;
                document.getElementById('eStatus').value = data.document_status_code;
                if (data.blocked === '1') {
                    document.getElementById('confUnblock').style.display = 'block';
                    document.getElementById('confBlock').style.display = 'none';
                } else {
                    document.getElementById('confUnblock').style.display = 'none';
                    document.getElementById('confBlock').style.display = 'block';

                }


            });

            $('#confEdit').unbind().on('click', function () {
                var eId = document.getElementById('eId').innerHTML;
                var newTranQ = document.getElementById('eTransacted').value;


                $('#mEditRow').modal('hide');

                var obj = {"docDetailId": eId, "transactedQty": newTranQ, "updatedBy": currentUser};
                var newEditjson = JSON.stringify(obj);
                var filter = newEditjson;


                $.ajax({
                    url: callPostService + filter + "&function=editDocumentDetailQty" + screen,
                    type: 'GET',
                    success: function (response, textstatus) {
                        if (response === 'OK - true') {
                            $('#mConfEdit').modal('show');
                        } else {
                            alert(response);
                        }

                    }
                });

            });

            $('#confBlock').unbind().on('click', function () {

                var status = document.getElementById('eStatus').value;
                console.log(status);
                if (status == null || status == 'OPEN') {


                    var eId = document.getElementById('eId').innerHTML;

                    $.ajax({
                        url: '../action/isDocDetailAllocated.php',
                        data: {eId: eId},
                        type: 'GET',
                        success: function (response, textstatus) {
                            if (response[11] > 0) {
                                alert("Detail allocated");
                            } else {
                                $('#blockCheck').modal('show');
                            }
                        }
                    });
                } else {
                    alert("Document is not OPEN ");
                    return
                }

            });

            $('#confUnblock').unbind().on('click', function () {

                var r = confirm("Unblock the Document Detail!");
                var isBlock = 'false';
                if (r == true) {

                    var eId = document.getElementById('eId').innerHTML;
                    var isBlock = false;
                    var obj = {"docDetailId": eId, "isBlock": isBlock, "updatedBy": currentUser};
                    var newEditjson = JSON.stringify(obj);
                    var filter = newEditjson;

                    $.ajax({
                        url: callGetService + filter + "&function=unblockdocumentdetail" + screen,
                        type: 'GET',
                        success: function (response, textstatus) {
                        }
                    });
                } else {
                    txt = "You pressed Cancel!";
                }

            });

            $('#bBlockYes').unbind().on('click', function () {
                var eId = document.getElementById('eId').innerHTML;
                var eBlockedComment = document.getElementById('eblockedComment').value;
                var isBlock = 'true';

                if (eBlockedComment.length <= 0) {
                    alert('please Enter a Comment');
                    return;
                }


                var obj = {"docDetailId": eId, "blockedComment": eBlockedComment, "isBlock": isBlock, "updatedBy": currentUser};
                var newEditjson = JSON.stringify(obj);
                var filter = newEditjson;

                $.ajax({
                    url: callPostService + filter + "&function=blockdocumentdetail" + screen,
                    type: 'GET',
                    success: function (response, textstatus) {
                        if (response === 'OK - true') {
                            $('#blockCheck').modal('hide');
                        } else {
                            alert(response);
                        }
                    }
                });

            });
        }

    });

</script>
</body>
</html>



