<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Filling Status Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>

        <div class="modal fade" id="mCreateNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New Filling Status</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Filling Code:</label>
                                    <div class="controls">
                                        <input type="text" name="nFillingCode" id="nFillingCode" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Filling Description:</label>
                                    <div class="controls">
                                        <input type="text" name="nFillingDescription" id="nFillingDescription" class="form-control" >
                                    </div>
                                </div>
                                <hr>

                                <div class="control-group" align="center" style="alignment-adjust: central">
                                    <div class="row" >
                                        <div class="col-xs-3">

                                        </div>

                                        <div class="col-xs-3">
                                            <label class="input-group-text">Pick:</label>
                                            <div class="controls">
                                                <input type="checkbox" name="autoRec" id="nPick" class="input">
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="input-group-text">Putaway:</label>
                                            <div class="controls">
                                                <input type="checkbox" name="autoPick" id="nPutaway" class="input">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="nSave">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confCreate" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">New Filling Status</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >New Filling Status Added</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" onClick="$('#example').DataTable().ajax.reload(null, false);" data-dismiss="modal">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="mEditStatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New Filling Status</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Filling Code:</label>
                                    <div class="controls">
                                        <input type="text" name="eID" id="eID" class="form-control" style="display: none;">
                                        <input type="text" name="eFillingCode" id="eFillingCode" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Filling Description:</label>
                                    <div class="controls">
                                        <input type="text" name="eDescription" id="eDescription" class="form-control" >
                                    </div>
                                </div>
                                <hr>

                                <div class="control-group" align="center" style="alignment-adjust: central">
                                    <div class="row" >
                                        <div class="col-xs-3">

                                        </div>

                                        <div class="col-xs-3">
                                            <label class="input-group-text">Pick:</label>
                                            <div class="controls">
                                                <input type="checkbox" name="ePick" id="ePick" class="input">
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="input-group-text">Putaway:</label>
                                            <div class="controls">
                                                <input type="checkbox" name="ePutaway" id="ePutaway" class="input">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="eSave">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Filling Status</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Edit Filling Status Added</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" onClick="$('#example').DataTable().ajax.reload(null, false);" data-dismiss="modal">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <?php
        include('../common/topNav.php');
        include('../common/sideBar.php');
        ?>
        <!-- Page Content  -->
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th>Filling Code</th>
                        <th>Description</th>
                        <th>Pick</th>
                        <th>Putaway</th>
                        <th>Created By</th>
                        <th>Last Updated By</th>
                        <th>last Updated Date</th>
                        <th>Actions</th>
                </thead>
                <tfoot>
                    <tr>
                        <th>Filling Code</th>
                        <th>Description</th>
                        <th>Pick</th>
                        <th>Putaway</th>
                        <th>Created By</th>
                        <th>Last Updated By</th>
                        <th>last Updated Date</th>
                        <th>Actions</th>
                </tfoot>
            </table>

            <input type="Button" id="bCreateNew" class="btn btn-warning" value="Create New"/>
            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

        </div>

        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->

<script>

    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }

    $(document).ready(function () {
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>'


        var table = $('#example').DataTable({
            ajax: {"url": "../tableData/fillingStatusTable.php", "dataSrc": ""},
            columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bEdit' class='btn btn-warning' value='Edit'/>"
                }
            ],
            buttons: [
                {extend: 'excel',
                    filename: 'filling_status',
                    title: 'Filling Status',
                    exportOptions: {
                        columns: ':visible',
                        orthogonal: {
                            display: ':null'
                        }
                    }}
            ],
            columns: [
                {data: "filling_code"},
                {data: "filling_description"},
                {data: "pick",
                    render: function (data, type, row) {
                        return (data == true) ? '<span data-order="1" style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color:red" data-order="0" class="glyphicon glyphicon-remove"></span>';
                    }
                },
                {data: "putaway",
                    render: function (data, type, row) {
                        return (data == true) ? '<span data-order="1" style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color:red" data-order="0" class="glyphicon glyphicon-remove"></span>';
                    }
                },
                {data: "created_by"},
                {data: "last_updated_by"},
                {data: "last_updated_date"},
                {data: ""}
            ],
            order: [[0, 'asc']],
        });
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });
             $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });


        $('#mCreateNew').on('hidden.bs.modal', function (e) {
            $(this)
                    .find("input,textarea,select")
                    .val('')
                    .end()
                    .find("input[type=checkbox], input[type=radio]")
                    .prop("checked", "")
                    .end();
        });


        $('#example tbody').on('click', '#bEdit', function () {

            $('#mEditStatus').modal('show');
            var data = table.row($(this).parents('tr')).data();
             document.getElementById('eID').value = data.id;
            document.getElementById('eFillingCode').value = data.filling_code;
            document.getElementById('eDescription').value = data.filling_description;
            document.getElementById('ePick').value = data.pick;
            document.getElementById('ePutaway').value = data.putaway;
            if (data.pick === '1') {
                $('#ePick').prop('checked', true);
            } else {
                $('#ePick').prop('checked', false);
            }
            if (data.putaway === '1') {
                $('#ePutaway').prop('checked', true);
            } else {
                $('#ePutaway').prop('checked', false);
            }
        });
        
        $('#eSave').unbind().on('click', function() {
           
           var eId = document.getElementById('eID').value;
           var eCode = document.getElementById('eFillingCode').value;
           var eDesc = document.getElementById('eDescription').value;
           var ePick;
           var ePut;
            if ($('#ePick').is(':checked')) {
                ePick = 1;
            } else {
                ePick = 0;
            }
            if ($('#ePutaway').is(':checked')) {
                ePut = 1;
            } else {
                ePut = 0;
            }
            if(eCode.length === 0 ){
                alert('Filling Code Cannot Be Blank');
                return;
            }
          $('#mEditStatus').modal('hide');
             
            var obj = {"fillingStatusId":eId ,"fillingCode": eCode, "fillingDescription": eDesc, "pick": ePick, "putaway": ePut, "userId": currentUser};
            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            console.log(filter);
           
               $.ajax({
                url: callPostService+filter + "&function=editLocationFillingStatus"+ screen,
                type: 'GET',
                success: function (response, textstatus) {
                    if(response === 'OK - true'){
                    $('#confEdit').modal('show');
                }else {
                    alert(response);
                }
                }
            });
           
            
        });
             
        $("#bCreateNew").on("click", function () {
            $('#mCreateNew').modal('show');
        });


        $("#nSave").on("click", function () {

            //  var newType = document.getElementById('nChargeType').value;
            var newCode = document.getElementById('nFillingCode').value;
            var newDesc = document.getElementById('nFillingDescription').value;
            var nPick = $('#nPick');
            var nPutaway = $('#nPutaway');
            if (nPick.is(':checked')) {
                nPick = 1;
            } else {
                nPick = 0;
            }
            if (nPutaway.is(':checked')) {
                nPutaway = 1;
            } else {
                nPutaway = 0;
            }
                   if(newCode.length === 0 ){
                alert('Filling Code Cannot Be Blank');
                return;
            }

            $('#mCreateNew').modal('hide');

            var obj = {"fillingCode": newCode, "fillingDescription": newDesc, "pick": nPick, "putaway": nPutaway, "userId": currentUser};
            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            console.log(filter);


            $.ajax({
                url: callPostService + filter+ "&function=createLocationFillingStatus" + screen,
                type: 'GET',
                success: function (response, textstatus) {
                    if(response === 'OK - true'){
                    $('#confCreate').modal('show');
                }else{
                    alert(response);
                }
                }
            });
        });



    });

</script>
</body>
</html>


