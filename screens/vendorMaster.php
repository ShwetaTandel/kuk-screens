<?php
// Modified - 2019-12-19 - Added relative address for PHPValidate/validaterdt.php
header('Cache-Control: no-cache, no-store, must-revalidate');   
header('Expires: 0');
// Header to allow Cross-Server AJAX Connection
header('Access-Control-Allow-Origin: *'); 
header('Expires: 0');

include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Vendor Master Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <script src="../js/libs/mask/jquery.mask.js" type="text/javascript"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>

    <body>
        <!-- conf new Vendor -->
        <div class="modal fade bd-example-modal-sm" id="confNewVendor" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">New Vendor</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >New Vendor Added</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" data-dismiss="modal">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- conf Edit Vendor -->
        <div class="modal fade bd-example-modal-sm" id="confEditVendor" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Vendor</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Vendor has been Changed</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- create new modal-->
        <div class="modal fade right" id="createNew" role="dialog" aria-labelledby="viewEdit" aria-hidden="true" >
            <div class="modal-dialog modal-fluid" role="document" style="width: 1000px">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalPreviewLabel">Vendor Master</h5>
                    </div>
                    <div class="modal-body">
                        <div class=" col-md-12">
                            <div class="row">
                                <!-- Half of the modal-body div-->
                                <div class="col-xs-6">
                                    <div class="control-group">
                                        <label class="input-group-text">Vendor Code:</label>
                                        <div class="controls">
                                            <input type="text" name="vendorCode"  style="text-transform:uppercase" id="nvendorCode" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="input-group-text">Vendor Name:</label>
                                        <div class="controls">
                                            <input type="text" name="vendorName" id="nvendorName" class="form-control">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="input-group-text">Contact Name:</label>
                                        <div class="controls">
                                            <input type="text" name="contactName" id="ncontactName" class="form-control">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="input-group-text">Contact Email:</label>
                                        <div class="controls">
                                            <input type="text" name="contactEmail" id="ncontactEmail" class="form-control">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="input-group-text">Contact Tel:</label>
                                        <div class="controls">
                                            <input type="text" name="contactTel" id="ncontactTel" class="form-control phone">
                                        </div>
                                    </div>
                                </div>
                                <!-- Other half of the modal-body div-->
                                <div class="col-xs-6">
                                    <div class="control-group">
                                        <label class="input-group-text">Street:</label>
                                        <div class="controls">
                                            <input type="text" name="street" id="nstreet" class="form-control">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="input-group-text">Town:</label>
                                        <div class="controls">
                                            <input type="text" name="Town" id="ntown" class="form-control">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="input-group-text">Country:</label>
                                        <div class="controls">
                                            <input type="text" name="country" id="ncountry" class="form-control">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="input-group-text">Post Code:</label>
                                        <div class="controls">
                                            <input type="text" name="postCode" id="npostCode" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div id="newdivBottom">
                                    <div class="col-xs-4">
                                        <label class="input-group-text">Effective From:</label>
                                        <div class="controls">
                                            <input type="text" name="effectFrom" id="neffectFrom" class="form-control date_time"  placeholder="YYYY-MM-DD HH:MM:SS">
                                        </div>   
                                    </div>
                                    <div class="col-xs-4">
                                        <label class="input-group-text">Effective To:</label>
                                        <div class="controls">
                                            <input type="text" name="effectTo" id="neffectTo" class="form-control date_time"  placeholder="YYYY-MM-DD HH:MM:SS">
                                        </div> 
                                    </div>

                                    <br>
                                    <div class="row">

                                    </div>
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <label class="input-group-text">Auto Recieve:</label>
                                            <div class="controls">
                                                <input type="checkbox" name="autoRec" id="nautoRec" class="input">
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="input-group-text">Auto Pick:</label>
                                            <div class="controls">
                                                <input type="checkbox" name="autoPick" id="nautoPick" class="input">
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="input-group-text">Decant:</label>
                                            <div class="controls">
                                                <input type="checkbox" name="decant" id="ndecant" class="input">
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="input-group-text">inspect:</label>
                                            <div class="controls">
                                                <input type="checkbox" name="inspect" id="ninspect" class="input">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="newdivBottom2" style="display: none;">
                                
                                <br>
                                <br>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="button" id="newModalBack" class="btn btn-primary">Back</button>
                        <button type="button" id="newModalNext" class="btn btn-primary">Next</button>
                        <button type="button" id="saveNew" class="btn btn-success">Create</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Edit modal-->
        <div class="modal fade right" id="viewEdit" role="dialog" aria-labelledby="viewEdit" aria-hidden="true"  >
            <div class="modal-dialog modal-fluid" role="document" style="width: 1000px">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalPreviewLabel">Vendor Master</h5>
                    </div>
                    <div class="modal-body">
                        <div class=" col-md-12">
                            <div class="row">
                                <!-- Half of the modal-body div-->
                                <div class="col-xs-6">
                                    <div class="control-group">
                                        <label class="input-group-text">Vendor Code:</label>
                                        <div class="controls">
                                            <input type="text" id="versionNum" style="display: none"/>
                                            <input type="text" name="vendorCode" id="vendorCode" class="form-control" disabled>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="input-group-text">Vendor Name:</label>
                                        <div class="controls">
                                            <input type="text" name="vendorName" id="vendorName" class="form-control">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="input-group-text">Contact Name:</label>
                                        <div class="controls">
                                            <input type="text" name="contactName" id="contactName" class="form-control">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="input-group-text">Contact Email:</label>
                                        <div class="controls">
                                            <input type="text" name="contactEmail" id="contactEmail" class="form-control">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="input-group-text">Contact Tel:</label>
                                        <div class="controls">
                                            <input type="text" name="contactTel" id="contactTel" class="form-control phone">
                                        </div>
                                    </div>
                                </div>
                                <!-- Other half of the modal-body div-->
                                <div class="col-xs-6">
                                    <div class="control-group">
                                        <label class="input-group-text">Street:</label>
                                        <div class="controls">
                                            <input type="text" name="street" id="street" class="form-control">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="input-group-text">Town:</label>
                                        <div class="controls">
                                            <input type="text" name="Town" id="town" class="form-control">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="input-group-text">Country:</label>
                                        <div class="controls">
                                            <input type="text" name="country" id="country" class="form-control">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="input-group-text">Post Code:</label>
                                        <div class="controls">
                                            <input type="text" name="postCode" id="postCode" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div id="divBottom">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <label class="input-group-text">Date Created:</label>
                                        <div class="controls">
                                            <input type="text" name="dateCreaed" id="dateCreated" class="form-control" disabled>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <label class="input-group-text">Effective From:</label>
                                        <div class="controls">
                                            <input type="text" name="effectFrom" id="effectFrom" class="form-control date_time" placeholder="YYYY-MM-DD HH:MM:SS">
                                        </div>   
                                    </div>
                                    <div class="col-xs-4">
                                        <label class="input-group-text">Effective To:</label>
                                        <div class="controls">
                                            <input type="text" name="effectTo" id="effectTo" class="form-control date_time"  placeholder="YYYY-MM-DD HH:MM:SS">
                                        </div> 
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <label class="input-group-text">Last Updated:</label>
                                        <div class="controls">
                                            <input type="text" name="lastUpdate" id="lastUpdated" class="form-control" disabled>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <label class="input-group-text">Last Updated By:</label>
                                        <div class="controls">
                                            <input type="text" name="lastUpBy" id="lastUpBy" class="form-control" disabled>
                                        </div>   
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Auto Recieve:</label>
                                        <div class="controls">
                                            <input type="checkbox" name="autoRec" id="autoRec" class="input">
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Auto Pick:</label>
                                        <div class="controls">
                                            <input type="checkbox" name="autoPick" id="autoPick" class="input">
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Decant:</label>
                                        <div class="controls">
                                            <input type="checkbox" name="decant" id="decant" class="input">
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <label class="input-group-text">inspect:</label>
                                        <div class="controls">
                                            <input type="checkbox" name="inspect" id="inspect" class="input">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divBottom2" style="display: none;">
                                
                                <br>
                                <br>
                                </div>

                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="row">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            <button type="button" id="modalBack" class="btn btn-primary">Back</button>
                            <button type="button" id="modalNext" class="btn btn-primary">Next</button>
                            <button type="button" id="saveEdit" class="btn btn-success">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        include('../common/topNav.php');
        include('../common/sideBar.php');
        ?>
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th>Vendor Code</th>
                        <th>Vendor Name</th>
                        <th>Effective From</th>
                        <th>Effective To</th>
                        <th>Auto Receive</th>
                        <th>Auto Pick</th>
                        <th>Decant</th>
                        <th>Inspect</th>
                        <th></th>
                </thead>
                <tfoot>
                    <tr>
                        <th>Vendor Code</th>
                        <th>Vendor Name</th>
                        <th>Effective From</th>
                        <th>Effective To</th>
                        <th>Auto Receive</th>
                        <th>Auto Pick</th>
                        <th>Decant</th>
                        <th>Inspect</th>
                        <th></th>
                </tfoot>
            </table>


            <input type="Button" id="bNew"  class="btn btn-warning" value="Create New"/>
            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

        </div>

        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->

<script>
    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }

    $(document).ready(function ()
    {
        $('.date_time').mask('0000-00-00 00:00:00');
        $('.phone').mask('000000000000000');
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>'


        var table = $('#example').DataTable({
            ajax: {"url": "../tableData/vendorMasterTable.php", "dataSrc": ""},
            columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bEdit' class='btn btn-warning' value='View/Edit'/>"
                }
            ],
            buttons: [
                {extend: 'excel', filename: 'Vendor_Master', title: 'Vendor Master'}
            ],
            columns: [
                {data: "vendor_reference_code"},
                {data: "vendor_name"},
                {data: "effective_from"},
                {data: "effective_to"},
                {data: "auto_receive",
                    render: function (data, type, row) {
                        return (data == true) ? '<span style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color: red;" class="glyphicon glyphicon-remove"></span>';
                    }
                },
                {data: "auto_pick",
                    render: function (data, type, row) {
                        return (data == true) ? '<span style="color: green;" class="glyphicon glyphicon-ok"></span>' : '<span style="color: red;" class="glyphicon glyphicon-remove"></span>';
                    }
                },
                {data: "decant",
                    render: function (data, type, row) {
                        return (data == true) ? '<span style="color: green;" class="glyphicon glyphicon-ok"></span>' : '<span style="color: red;" class="glyphicon glyphicon-remove"></span>';
                    }
                },
                {data: "inspect",
                    render: function (data, type, row) {
                        return (data == true) ? '<span style="color: green;" class="glyphicon glyphicon-ok"></span>' : '<span style="color: red;" class="glyphicon glyphicon-remove"></span>';
                    }
                },
                {data: ""}
            ],
            order: [[0, 'asc']]
        });
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });
        $("#bNew").on("click", function () {

            $('#createNew').modal('show');
            document.getElementById('newdivBottom').style.display = "block";
            document.getElementById('newdivBottom2').style.display = "none";
            document.getElementById('newModalNext').style.display = "inline";
            document.getElementById('newModalBack').style.display = "none";
        });
        $("#saveNew").on("click", function () {
            var newVendorCode = document.getElementById('nvendorCode').value;
            var newVendorName = document.getElementById('nvendorName').value;
            var newContactName = document.getElementById('ncontactName').value;
            var newContactEmail = document.getElementById('ncontactEmail').value;
            var newContactTel = document.getElementById('ncontactTel').value;
            var newStreet = document.getElementById('nstreet').value;
            var newTown = document.getElementById('ntown').value;
            var newCountry = document.getElementById('ncountry').value;
            var newPostCode = document.getElementById('npostCode').value;
            var newEffectFrom = document.getElementById('neffectFrom').value;
            var newEffectTo = document.getElementById('neffectTo').value;
            var newAutoReceive = $('#nautoRec');
            var newAutoPick = $('#nautoPick');
            var newDecant = $('#ndecant');
            var newInspect = $('#ninspect');
            var newCheckMon = $('#newCheckMon');
            var newCheckTues = $('#newCheckTues');
            var newCheckWed = $('#newCheckWed');
            var newCheckThurs = $('#newCheckThurs');
            var newCheckFri = $('#newCheckFri');
            var newCheckSat = $('#newCheckSat');
            var newCheckSun = $('#newCheckSun');
            if (newAutoReceive.is(':checked')) {
                newAutoReceive = 1;
            } else {
                newAutoReceive = 0;
            }
            if (newAutoPick.is(':checked')) {
                newAutoPick = 1;
            } else {
                newAutoPick = 0;
            }
            if (newDecant.is(':checked')) {
                newDecant = 1;
            } else {
                newDecant = 0;
            }
            if (newInspect.is(':checked')) {
                newInspect = 1;
            } else {
                newInspect = 0;

            }
          

            if (newCheckMon.is(':checked')) {
                newCheckMon = "1";
            } else {
                newCheckMon = "0";
            }
            if (newCheckTues.is(':checked')) {
                newCheckTues = "1";
            } else {
                newCheckTues = "0";
            }
            if (newCheckWed.is(':checked')) {
                newCheckWed = "1";
            } else {
                newCheckWed = "0";
            }
            if (newCheckThurs.is(':checked')) {
                newCheckThurs = "1";
            } else {
                newCheckThurs = "0";
            }
            if (newCheckFri.is(':checked')) {
                newCheckFri = "1";
            } else {
                newCheckFri = "0";
            }
            if (newCheckSat.is(':checked')) {
                newCheckSat = "1";
            } else {
                newCheckSat = "0";
            }
            if (newCheckSun.is(':checked')) {
                newCheckSun = "1";
            } else {
                newCheckSun = "0";
            }

           // var newChargeDay = newCheckMon + newCheckTues + newCheckWed + newCheckThurs + newCheckFri + newCheckSat + newCheckSun;

//            var values = $('#newRepCharge').val();
//
//            if (values !== null) {
//                var reportCharge = values.join(","); // there is a break after comma
//            }


            if (newVendorCode === "") {
                alert('Please Enter a Vendor Code');
            } else {

                if (checkDate(newEffectFrom)) {
                    if (checkDate(newEffectTo)) {
                        var obj = {"vendorName": newVendorName, "vendorCode": newVendorCode, "effectiveFrom": newEffectFrom, "effectiveTo": newEffectTo,
                            "contactName": newContactName, "contactTel": newContactTel, "contactEmail": newContactEmail, "street": newStreet,
                            "town": newTown, "country": newCountry, "postCode": newPostCode, "autoReceive": newAutoReceive, "autoPick": newAutoPick,
                            "decant": newDecant, "inspect": newInspect, "vatPercentage": "", "vatNumber": ""
                            };


                        var newVendorjson = JSON.stringify(obj);
                        var filter = newVendorjson;
			
                        //console.log(filter);
                        $.ajax({
                            url: callPostService+ filter + "&function=createVendor"  + screen,
                            type: 'POST',
                            success: function (response, textstatus) {

                                if (response === 'OK - true') {
                                    $('#example').DataTable().ajax.reload(null, false);
                                    $('#confNewVendor').modal('show');
                                    $('#createNew').modal('hide');
                                } else {
                                    alert(response);
                                }

                            }

                        });
                    } else {
                        alert('Effect To Invalid');
                    }

                } else {
                    alert('Effect From Invalid');
                }


            }


        });

        $('#createNew').on('hidden.bs.modal', function (e) {
            $(this)
                    .find("input,textarea,select")
                    .val('')
                    .end()
                    .find("input[type=checkbox], input[type=radio]")
                    .prop("checked", "")
                    .end();
        });

        $("#modalNext").on("click", function () {
            document.getElementById('divBottom').style.display = "none";
            document.getElementById('modalBack').style.display = "inline";
            document.getElementById('modalNext').style.display = "none";
            document.getElementById('divBottom2').style.display = "block";
            document.getElementById('vendorName').disabled = true;
            document.getElementById('contactName').disabled = true;
            document.getElementById('contactEmail').disabled = true;
            document.getElementById('contactTel').disabled = true;
            document.getElementById('street').disabled = true;
            document.getElementById('town').disabled = true;
            document.getElementById('country').disabled = true;
            document.getElementById('postCode').disabled = true;
        });
        $("#modalBack").on("click", function () {

            document.getElementById('vendorName').disabled = false;
            document.getElementById('contactName').disabled = false;
            document.getElementById('contactEmail').disabled = false;
            document.getElementById('contactTel').disabled = false;
            document.getElementById('street').disabled = false;
            document.getElementById('town').disabled = false;
            document.getElementById('country').disabled = false;
            document.getElementById('postCode').disabled = false;
            document.getElementById('divBottom').style.display = "block";
            document.getElementById('divBottom2').style.display = "none";
            document.getElementById('modalNext').style.display = "inline";
            document.getElementById('modalBack').style.display = "none";
        });

        $("#newModalNext").on("click", function () {
            document.getElementById('newdivBottom').style.display = "none";
            document.getElementById('newModalBack').style.display = "inline";
            document.getElementById('newModalNext').style.display = "none";
            document.getElementById('newdivBottom2').style.display = "block";
        });
        $("#newModalBack").on("click", function () {
            document.getElementById('newdivBottom').style.display = "block";
            document.getElementById('newdivBottom2').style.display = "none";
            document.getElementById('newModalNext').style.display = "inline";
            document.getElementById('newModalBack').style.display = "none";
        });

        $('#example tbody').on('click', '#bEdit', function () {

            var data = table.row($(this).parents('tr')).data();
            document.getElementById('vendorName').disabled = false;
            document.getElementById('contactName').disabled = false;
            document.getElementById('contactEmail').disabled = false;
            document.getElementById('contactTel').disabled = false;
            document.getElementById('street').disabled = false;
            document.getElementById('town').disabled = false;
            document.getElementById('country').disabled = false;
            document.getElementById('postCode').disabled = false;
            document.getElementById('divBottom').style.display = "block";
            document.getElementById('divBottom2').style.display = "none";
            document.getElementById('modalBack').style.display = "none";
            document.getElementById('modalNext').style.display = "inline";
            document.getElementById('vendorCode').value = data.vendor_reference_code;
            document.getElementById('vendorName').value = data.vendor_name;
            document.getElementById('effectFrom').value = data.effective_from;
            document.getElementById('effectTo').value = data.effective_to;
            document.getElementById('contactName').value = data.recipient;
            document.getElementById('contactEmail').value = data.contact_email;
            document.getElementById('contactTel').value = data.contact_tel;
            document.getElementById('street').value = data.street;
            document.getElementById('town').value = data.postal_town;
            document.getElementById('country').value = data.country_name;
            document.getElementById('postCode').value = data.post_code;
            document.getElementById('dateCreated').value = data.date_created;
            document.getElementById('lastUpdated').value = data.last_updated;
            document.getElementById('lastUpBy').value = data.last_updated_by;
            document.getElementById('versionNum').value = data.version;
            if (data.auto_receive === '1') {
                $('#autoRec').prop('checked', true);
            } else {
                $('#autoRec').prop('checked', false);
            }
            if (data.auto_pick === '1') {
                $('#autoPick').prop('checked', true);
            } else {
                $('#autoPick').prop('checked', false);
            }
            if (data.decant === '1') {
                $('#decant').prop('checked', true);
            } else {
                $('#decant').prop('checked', false);
            }
            if (data.inspect === '1') {
                $('#inspect').prop('checked', true);
            } else {
                $('#inspect').prop('checked', false);
            }

            $('#viewEdit').modal('show');
        });
             $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });

        $("#saveEdit").on("click", function () {
            var eVenCode = document.getElementById('vendorCode').value;
            var eVenName = document.getElementById('vendorName').value;
            var eEffFrom = document.getElementById('effectFrom').value;
            var eEffTo = document.getElementById('effectTo').value;
            var eConName = document.getElementById('contactName').value;
            var eConEmail = document.getElementById('contactEmail').value;
            var eConTel = document.getElementById('contactTel').value;
            var eStreet = document.getElementById('street').value;
            var eTown = document.getElementById('town').value;
            var eCountry = document.getElementById('country').value;
            var ePostCode = document.getElementById('postCode').value;
            var version = document.getElementById('versionNum').value;
            var eAutoReceive = $('#autoRec');
            var eAutoPick = $('#autoPick');
            var eDecant = $('#decant');
            var eInspect = $('#inspect');
            if (eAutoReceive.is(':checked')) {
                eAutoReceive = 1;
            } else {
                eAutoReceive = 0;
            }
            if (eAutoPick.is(':checked')) {
                eAutoPick = 1;
            } else {
                eAutoPick = 0;
            }
            if (eDecant.is(':checked')) {
                eDecant = 1;
            } else {
                eDecant = 0;
            }
            if (eInspect.is(':checked')) {
                eInspect = 1;
            } else {
                eInspect = 0;
            }
           if (checkDate(eEffFrom)) {
                if (checkDate(eEffTo)) {
                    var obj = {"vendorName": eVenName, "vendorCode": eVenCode, "effectiveFrom": eEffFrom, "effectiveTo": eEffTo,
                        "contactName": eConName, "contactTel": eConTel, "contactEmail": eConEmail, "street": eStreet,
                        "town": eTown, "country": eCountry, "postCode": ePostCode, "autoReceive": eAutoReceive, "autoPick": eAutoPick,
                        "decant": eDecant, "inspect": eInspect, "vatPercentage": "", "vatNumber": "",   "version": version
                    };

                    var newVendorjson = JSON.stringify(obj);
                    var filter = newVendorjson;

                    console.log(filter);
                    $.ajax({
                        url: callPostService+ filter + "&function=editVendor"  + screen,
                        type: 'POST',
                        success: function (response, textstatus) {

                            if (response === 'OK - true') {
                                $('#example').DataTable().ajax.reload(null, false);
                                $('#confEditVendor').modal('show');
                                $('#viewEdit').modal('hide');
                            } else {
                                alert(response);
                            }

                        }

                    });
                } else {
                    alert('Effect To Invalid');
                }

            } else {
                alert('Effect From Invalid');
            }
        });

    });



    function checkDate(form)
    {
        var iso8601 = form,
                userDate = new Date(iso8601),
                today = new Date(),
                dateTime,
                date,
                time,
                value;

// check is valid date
        if (isNaN(userDate)) {
            alert("invalid Date");
        }


// check the string specifically matches "yyyy-mm-dd hh:mm:ss" and is valid
        function isGregorianLeapYear(year) {
            return year % 400 === 0 || year % 100 !== 0 && year % 4 === 0;
        }

        function daysInGregorianMonth(year, month) {
            var days;

            if (month === 2) {
                days = 28;
                if (isGregorianLeapYear(year)) {
                    days += 1;
                }
            } else {
                days = 31 - ((month - 1) % 7 % 2);
            }

            return days;
        }

        if (typeof iso8601 !== "string") {
            alert("not an iso8601 string");
        } else {
            dateTime = iso8601.split(" ");
            if (dateTime.length !== 2) {
                alert("missing date or time element");
                return;
            } else {
                date = dateTime[0].split("-");
                if (date.length !== 3) {
                    alert("incorrect number of date elements");
                    return;
                } else {
                    value = +date[0];
                    if (date[0].length !== 4 || value < -9999 || value > 9999) {
                        alert("year value is incorrect");
                        return;
                    }

                    value = +date[1];
                    if (date[1].length !== 2 || value < 1 || value > 12) {
                        alert("month value is incorrect");
                        return;
                    }

                    value = +date[2];
                    if (date[2].length !== 2 || value < 1 || value > daysInGregorianMonth(+date[0], +date[1])) {
                        alert("day value is incorrect");
                        return;
                    }
                }

                time = dateTime[1].split(":");
                if (time.length !== 3) {
                    alert("incorrect number of time elements");
                    return;
                } else {
                    value = +time[0];
                    if (time[0].length !== 2 || value < 0 || value > 23) {
                        alert("hour value is incorrect");
                        return;
                    }

                    value = +time[1];
                    if (time[1].length !== 2 || value < 0 || value > 59) {
                        alert("minute value is incorrect");
                        return;
                    }

                    value = +time[2];
                    if (time[2].length !== 2 || value < 0 || value > 59) {
                        alert("second value is incorrect");
                        return;
                    }
                }
            }
        }

        return true;
    }


</script>
</body>
</html>
