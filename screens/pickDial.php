<?php
include '../config/logCheck.php';
?>
<html>
    <?php
    $toDate = '';
    if (isset($_GET['to'])) {
        $toDate = $_GET['to'];
    }
    $fromDate = '';
    if (isset($_GET['from'])) {
        $fromDate = $_GET['from'];
    }
    ?>
    <style>
        .table-cell-total{
            background-color: lightgrey;
        }
    </style>
    <head>
        <title>Pick Dial</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="refresh" content="600">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <!--  <link href="../css/dashboardStyle.css" rel="stylesheet" type="text/css"/>-->
        <script src="../config/screenConfig.js" type="text/javascript"></script>  
        <!--script src="../dashboard/dashAjax.js" type="text/javascript"></script>-->
    </head>
    <body>
        <div class="row">
            <nav class="navbar navbar-brand">
                <div class="container-flud">    
                    <ul class="nav navbar-nav">
                        <li><a href="dashboard.php">Dashboard</a></li>
                        <li><a href="mainMenu.php">VIMS</a></li>
                    </ul>
                </div>
            </nav>
        </div>
        <div>
            <ul class="nav nav-tabs" role="tablist"  id="myTabs">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab"  id="pickDialTab" href="#pickDial" onclick="getData()">Pick Dial</a>
                </li>


            </ul>
        </div>
        <hr/>

        <div class="tab-content">
            <div id="pickDial" class="container-fluid tab-pane show">
                <h2 align="center" id="title">PICK STATUS SCREEN</h2>

                <div class="container-fluid">

                    <table id="example" class="compact stripe hover cell-border"  width="100%">
                    </table>
                </div>


            </div>

        </div>
        
        <div class="row">
            <div class="col-lg-12">
            <button class="btn btn-primary" type="button" onclick ="$('#dateRangeModal').modal('show')">Time By Area (Date Range)</button>
              </div>
        </div>

        <!----Modal box--->
        <div class="modal fade" id="commentsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Comments</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-lg-12">
                                <div class="control-group">
                                    <label class="input-group-text">Comments:</label>
                                    <div class="controls">
                                        <textarea rows="2" class="form-control" id="comments" maxlength="250" ></textarea>
                                        <input type="hidden" name="pickId" id="pickId" class="form-control" >
                                    </div>
                                </div>

                                <hr>


                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="saveComments">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        
         <div class="modal fade" id="dateRangeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Select a Date Range</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-lg-12">
                                <div class="control-group">
                                    <label class="input-group-text">From Date:</label>
                                    <div class="controls">
                                        <input type="date" name="fromDate" id="fromDate" class="form-control"  >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">To Date:</label>
                                    <div class="controls">
                                        <input type="date" name="toDate" id="toDate" class="form-control"  >
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="saveDateRangeReport">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <script>


            function getData(cb_func) {
                var url = "../tableData/pickDialData.php?filter=data";
                var toDate = '<?php echo $toDate;?>';
                 var fromDate = '<?php echo $fromDate;?>';
                if(toDate!== '' && fromDate !== ''){
                    url = url + '&toDate='+toDate+ '&fromDate='+fromDate; 
                }
                $.ajax({
                    url: url,
                    success: cb_func
                });
            }
            $(document).ready(function () {
               // $('#pickDialTab').trigger('click');
                getData(function (data) {
                    var columns = [];
                    data = JSON.parse(data);
                    columnNames = data.columns;
                    for (var i in columnNames) {
                        if (columnNames[i] === 'Total') {
                            columns.push({data: columnNames[i],
                                title: columnNames[i], className: "table-cell-total"});
                        } else {
                            columns.push({data: columnNames[i],
                                title: columnNames[i]});
                        }
                    }

                    var pickTable = $('#example').DataTable({
                        dom: 'Bfrltip',
                        //orderCellsTop: true,
                        data: data.data,
                        columns: columns,
                        initComplete: function () {
                            this.api().columns().every(function (i) {
                                var column = this;
                                if (i === 2) {
                                    var select = $('<select><option id="ssearch" value=""></option></select>')
                                            .appendTo($(column.header()))
                                            .on('change', function () {
                                                var val = $.fn.dataTable.util.escapeRegex(
                                                        $(this).val()
                                                        );
                                                column
                                                        .search(val ? '^' + val + '$' : '', true, false)
                                                        //.search(val ? val : '', true, false)
                                                        .draw();
                                            });
                                    column.data().unique().sort().each(function (d, j) {
                                        select.append('<option value="' + d + '">' + d + '</option>');
                                    });
                                }


                            });
                        }, 
                        "paging": false,
                        "order": [],
                        "columnDefs": [
                            {
                                "targets": [0],
                                "visible": false,
                                "searchable": false,
                                  "orderable":false
                            },
                            {
                                "targets": "_all",
                                "createdCell": function (td, cellData, rowData, row, col) {
                                    if (cellData === '0') {
                                        $(td).css('background-color', '#dff5d3')
                                    }
                                    
                                }
                            },
                                {
                                     "targets": "_all",
                                  "orderable":false
                                }
                        ],
                           "createdRow": function (row, data, dataIndex) {
                                if ( data.Comments === 'Tasks By Area') {
                                    $(row).css('background-color', 'yellow');
                                }else if ( data.Comments === 'Time By Area(hours)') {
                                    $(row).css('color', 'red');
                                }
                    }
                    });

                    
                    $('#example tbody').on('click', 'td', function () {
                        var tr = $(this).closest('tr');
                        if ($(this).index() === 5) {
                            var data = pickTable.row(tr).data();
                            $('#commentsModal').modal('show');
                            document.getElementById('pickId').value = data.Run;
                        }
                    });

                    $('#saveComments').on('click', function () {
                        var comments = document.getElementById('comments').value;
                        var pickId = document.getElementById('pickId').value;
                        if (comments !== '') {
                            $.ajax({
                                url: "../tableData/pickDialData.php?filter=comments&id=" + pickId + "&comments=" + comments,
                                type: 'GET',
                                success: function (response) {
                                    if (response === "OK") {
                                        location.href = "pickDial.php";
                                    }
                                    else {
                                        alert("Something went wrong please try again.")
                                    }
                                }
                            });
                        } else {
                            alert("Please enter some comments");
                        }
                    });
                    
                    
                        $('#saveDateRangeReport').on('click', function () {
                        var fromDate = document.getElementById('fromDate').value;
                        var toDate = document.getElementById('toDate').value;
                        if (toDate !== '' && fromDate !== '') {
                            location.href = "../screens/pickDial.php?from=" + fromDate + "&to=" + toDate;
                          
                        } else {
                            alert("Please enter Date range");
                        }
                    });
                });



            });
        </script>
    </body>

</html>