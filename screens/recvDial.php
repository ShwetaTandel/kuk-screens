<?php
include '../config/logCheck.php';
?>
<html>
    <?php
    ?>
    <style>
    </style>
    <head>
        <title>Receiving Dial</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="refresh" content="600">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <link href="../css/dashboardStyle.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>  
        <!--script src="../dashboard/dashAjax.js" type="text/javascript"></script>-->
    </head>
    <body>
        <div class="row">
            <nav class="navbar navbar-brand">
                <div class="container-flud">    
                    <ul class="nav navbar-nav">
                        <li><a href="dashboard.php">Dashboard</a></li>
                        <li><a href="mainMenu.php">VIMS</a></li>
                    </ul>
                </div>
            </nav>
        </div>
        <div>
            <ul class="nav nav-tabs" role="tablist"  id="myTabs">
                <li class="nav-item">
                    <a class="nav-link " data-toggle="tab"  id="recvDialTab" href="#recvDial" onclick="showRecvDial()">Receiving Dial</a>
                </li>
            </ul>
        </div>
        <hr/>

        <div class="tab-content">
            <div id="recvDial" class="container-fluid tab-pane">
                <h2 align="center" id="title">RECEIVING DIAL</h2>
                <div class="col-lg-4">
                    <div class="well">
                        <strong onclick="reloadNotConfirmedTable()">NOT CONFIRMED</strong>
                        <table id="notConfirmedTable" class="table table-bordered" style="font-family:arial; font-size: 20px">
                            <tr>
                                <th>GRN</th>
                                <th>Time passed(mins)</th>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="well">
                        <strong onclick="reloadNotCleared()">CHECKPOOL NOT CLEARED</strong>
                        <table id="notClearedTable" class="table table-bordered" style="font-family:arial; font-size: 20px">
                            <tr>
                                <th>GRN</th>
                                <th>Quantity</th>
                                <th>Part</th>
                                <th>Serial</th>
                            </tr>
                        </table>
                    </div>
                </div>
               

            </div>
        </div>
        <script>


            var notConfirmedUrl = "../tableData/recvDialData.php?data=notConfirmed";
            var notConfirmedTable = document.getElementById("notConfirmedTable");
            var notClearUrl = "../tableData/recvDialData.php?data=notCleared";
            var notClearedTable = document.getElementById("notClearedTable");
            var currHrs = new Date().getHours();
            var currMins = new Date().getMinutes();
            var currTimeInMins = parseInt(currHrs) * 60 + parseInt(currMins);
            $('#recvDialTab').trigger('click');

            function reloadNotConfirmedTable() {
                while (notConfirmedTable.rows.length > 1) {
                    notConfirmedTable.deleteRow(1);
                }
                $.ajax({
                    url: notConfirmedUrl,
                    type: 'GET',
                    success: function (data) {
                        var json = JSON.parse(data);
                        //var json = [{"customerReference": "ORDR1", "timediff": "30"}];
                        var tr;

                        for (var i = 0; i < json.length; i++) {
                            
                            var expTimeInMins = json[i].timeDiff;
                            tr = $('<tr/>');
                            if (expTimeInMins >= 60 && expTimeInMins <= 120) {
                                tr.css({"background-color": "#f0b851"})
                            }else if (expTimeInMins >= 120) {
                                tr.css({"background-color": "#eb3f21"})
                            }
                            tr.append("<td>" + json[i].customerReference + "</td>");
                            tr.append("<td>" + json[i].timeDiff + "</td>");
                            $('#notConfirmedTable').append(tr);
                        }
                    }
                });
            }
            function showRecvDial() {
                reloadNotConfirmedTable();
            reloadNotCleared();
              //  reloadTrailerTable();

            }
            
            function reloadNotCleared() {
                    while (notClearedTable.rows.length > 1) {
                        notClearedTable.deleteRow(1);
                    }
                    $.ajax({
                    url: notClearUrl,
                    type: 'GET',
                        success: function (data) {
                            var json = JSON.parse(data);
                            // var json = [{"number_of_picks": "127", "picked": "52", "open":"75","overdue":"1"}];
                            var tr;

                            for (var i = 0; i < json.length; i++) {
                                var expTimeInMins = json[i].timeDiff;
                                tr = $('<tr/>');
                                if (expTimeInMins >= 60 && expTimeInMins <= 120) {
                                    tr.css({"background-color": "#f0b851"})
                                }else if (expTimeInMins >= 120) {
                                    tr.css({"background-color": "#eb3f21"})
                                }
                                tr.append("<td>" + json[i].customer_reference + "</td>");
                                tr.append("<td>" + json[i].available_qty + "</td>");
                                tr.append("<td>" + json[i].part_number + "</td>");
                                tr.append("<td>" + json[i].serial_reference + "</td>");
                                $('#notClearedTable').append(tr);
                            }
                        }
                    });             
            }

        

        </script>
    </body>

</html>