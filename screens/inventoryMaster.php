<?php
include '../config/logCheck.php';
include ('../config/phpConfig.php');
?>
<html>
    <head>
        <title>Inventory Master Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="expires" content="timestamp">
        <meta http-equiv="cache-control" content="no-cache" />
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>																									  																				
	<link rel="stylesheet" type="text/css" href="../dataTables/datatables.min.css"/>
        <script type="text/javascript" src="../dataTables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <style>
            body 
            {
                position: relative;
                background-color: #CFD8DC;
                    width : 98%;
            }
            body,html 
            { 
                    height: 100%; 
                    width: 100%
            }
            .nav .open > a, 
            .nav .open > a:hover, 
            .nav .open > a:focus {background-color: transparent;}

            table.dataTable 
            {
            /* table-layout: fixed;  */
            }

            /* Ensure that the demo table scrolls */
            th, td 
            { 
                    white-space: nowrap; 
            }
            div.dataTables_wrapper 
            {
                    left: 0px;
                    width: 98%;
                    margin: 2px auto;
            }
            div.container 
            {
                    left: 0px;
            }
            /*-------------------------------*/
            /*           Wrappers            */
            /*-------------------------------*/

            .wrapper 
            {
            /*	width: 50px;  !*/
                padding-left: 0;
            }

            .dropdown-header {
                text-align: center;
                font-size: 1em;
                color: #ddd;
                background:#212531;
                background: linear-gradient(to right bottom, #2f3441 50%, #212531 50%);
            }

            .dropdown-menu.show {
                top: 0;
            }
	</style>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
        </head>
        <body>
	    <div class="modal fade" id="mPrintPart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Print Odette Label</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Ran Order:</label>
                                    <div class="controls">
                                        <input type="text"  name="pRanOrder" id="pRanOrder" class="form-control" required>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Part Number:</label>
                                    <div class="controls">
                                        <input type="text" name="pPartNumber" id="pPartNumber" class="form-control" required>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Serial Number:</label>
                                    <div class="controls">
                                        <input type="text" name="pSerialNumber" id="pSerialNumber" class="form-control" required>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Quantity:</label>
                                    <div class="controls">
                                        <input type="number" name="pQuantity" id="pQuantity" class="form-control" required>
                                    </div>
                                </div>
                               <div class="control-group">
                                    <label class="input-group-text">Printer:</label>
                                    <select class="form-control" type="text" id="printerIP">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
                                        // Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM printer_mapping where printer_code like "receiving%"');
                                        echo "<option value=\"$value\"></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['printer_code'] . '">' . $row['printer_code'] .'</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="printPartButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="mSaOut" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Sa-Out</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Part Number:</label>
                                    <div class="controls">
										<input type="hidden" name="saoId" id="saoId">
                                        <input type="text" name="saoPart" id="saoPart" class="form-control" disabled >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Serial Reference:</label>
                                    <div class="controls">
                                        <input type="text" name="saoSerial" id="saoSerial" class="form-control" disabled >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Quantity:</label>
                                    <div class="controls">
                                        <input type="number" name="saoQty" id="saoQty"  style="text-transform:uppercase" class="form-control" min="0" >
										<input type="hidden" name="conversionFactor" id="conversionFactor">
                                        <input type="hidden" name="saoProductType" id="saoProductType">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Reason Code:</label>
                                    <select class="form-control" type="text" id="saoReason">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
                                        // Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT reason_code,reason_code_description FROM reason_code');
                                        echo "<option value=\"$value\"></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['reason_code_description'] . '">' . $row['reason_code'] . ' -- ' . $row['reason_code_description'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Explanation:</label>
                                    <input class="form-control" type="text" id="saoComment" onkeypress="if (event.key.replace(/[^\w\-. ]/g,'')=='') event.preventDefault();"/>   
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row" align="center">
                                <!-- Half of the modal-body div-->
                                <div class="controls">
                                    <label class="input-group-text">Customer Charge:</label>  
									<input type="checkbox" name="eCustomerCharge" id="eCustomerCharge" class="input"></input>
									<label style="color: white">............</label>  
                                    <label class="input-group-text">Notify KUK:</label>   
									<input type="checkbox" name="eNotifyNMUK" id="eNotifyNMUK" class="input"></input>
                                </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="bSaOutSubmit">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="mSaIn" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Sa-In</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Ran/Order:</label>
                                    <div class="controls">
                                        <input type="text"  style="text-transform:uppercase" name="saiRanOrder" id="saiRanOrder" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Part Number:</label>
                                    <input class="form-control" type="text" id="saiPartNumber" onchange="partHelp()"/>                                                                  
                                    <input style="display: none;" id="locTypeId"/>
                                    <input style="display: none;" id="zoneTypeId"/>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Serial Reference:</label>
                                    <div class="controls">
                                        <?php
                                        include('../config/phpConfig.php');
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.company');
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<input id="minLength" style="display: none;" value="' . $row['min_serial_number_length'] . '" />';
                                            echo '<input type="text" maxlength="' . $row['max_serial_number_length'] . '"  style="text-transform:uppercase" name="saiSerial" id="saiSerial" class="form-control" >';
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Quantity:</label>
                                    <div class="controls">
                                        <input type="number" name="saoQty" id="saiQty" class="form-control" min="0" >
                                        <input type="number" id="qtyHelp" class="form-control" style="display: none;">
										<input type="hidden" name="conversionFactor" id="conversionFactor">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">To Location:</label>
                                    <input class="form-control" type="text" id="saiToLoc" onchange="locHelp()"/>   
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Reason Code:</label>
                                    <select class="form-control" type="text" id="saiReason">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
                                        // Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT reason_code,reason_code_description FROM reason_code');
                                        echo "<option value=\"$value\"></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['reason_code_description'] . '">' . $row['reason_code'] . ' -- ' . $row['reason_code_description'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
								<div class="control-group">
                                    <label class="input-group-text">Product Type:</label>
                                    <select class="form-control" type="text" id="saiProductType">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
                                        // Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM product_type;');
                                        echo "<option value=\"$value\"></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['id'] . '">' . $row['product_type_code'] . ' -- ' . $row['product_type_description'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Explanation:</label>
                                    <input class="form-control" type="text" id="saiComment"  onkeypress="if (event.key.replace(/[^\w\-. ]/g,'')=='') event.preventDefault();" />   
                                </div>
                            </div>
                        </div>
                    </div>
		    <div class="row" align="center">
                                <!-- Half of the modal-body div-->
                                <div class="controls">
                                  
                                    <label class="input-group-text">Notify KUK:</label>   
									<input type="checkbox" name="saInNotifyKUK" id="saInNotifyKUK" class="input"></input>
                                </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="bDoneSaIn">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-sm" id="saOutSent" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">SA-OUT</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >SA-Out Complete</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton2" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="saInSent" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">SA-IN</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >SA-In Complete</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton2" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="saInCheck" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">SA-IN Check</h5>
                    </div>
                    <br>
                    <div align="center">
                        <input type="hidden" value='' id='filter'/>
                        <strong >This part number and serial exist in inventory do you want to add to it?</strong>
                    </div>
                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="bSaYes" data-dismiss="modal">Yes</button>
                        <button type="button" class="btn btn-success" id="bSaNo" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade right" id="mSplitPiece" role="dialog" aria-labelledby="viewSplitPiece" aria-hidden="true" >
            <div class="modal-dialog modal-fluid" role="document" style="width: 1000px">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalPreviewLabel">Split Piece</h5>
                    </div>
                    <div class="modal-body">
                        <div class=" col-md-12">
                            <div id="nPage1">
                                <div class="row" align="center" style="vertical-align:left">
                                    <!-- Half of the modal-body div-->
                                    <p style="display: none;"><span id="eId"></span></p>
                                    <h2>Serial <strong id="eSerialSplitPiec">#</strong>  will be split into the following </h2> 
                                    <h2>Serial <strong id="eSerialSplitPiec1">#</strong> Qty: <strong id="eQtySplitPiec1">#</strong></h2>
				    <h2>---- New Serial ----</h2>
                                    <h2>Serial <strong id="eSerialSplitPiec2">#</strong> Qty: 
                                    <input type="number" id="eQtySplitPiec2" min="1" max="2""/></h2> 
                                </div>
                            </div>
                            <div class="modal-footer">
                                <h2 align="left">Are you sure you want to split this case?</h2>
                                <button type="button" id="bSaveSplitPiece" class="btn btn-success">Yes</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade right" id="mEdit" role="dialog" aria-labelledby="viewEdit" aria-hidden="true" >
            <div class="modal-dialog modal-fluid" role="document" style="width: 1000px">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalPreviewLabel">Edit Inventory</h5>
                    </div>
                    <div class="modal-body">
                        <div class=" col-md-12">
                            <div id="nPage1">
                                <div class="row" align="center" style="vertical-align:left">
                                    <!-- Half of the modal-body div-->
                                    <p style="display: none;"><span id="eId"></span></p>
                                    <h2>Part Number: <strong id="ePart">#</strong></h2>
                                    <h2>Serial Reference: <strong id="eSerial">#</strong></h2>
                                    <h2>Qty: <strong id="eQty">#</strong></h2>
                                    <h2>Inventory Status: <strong id="eStatus">#</strong></h2>
                                    <!-- Other half of the modal-body div-->
                                </div>
                            </div>
                            <hr>
                            <div class="row" align="center">
                              <div class="controls" align="center" style="width: 350px">
                                    <label style="alignment-adjust: central" class="input-group-text">Inventory Status:</label>
                                    <select class="form-control" type="text" id="eStatusEdit" align="center">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
                                        // Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.inventory_status WHERE NOT (inventory_status_code = "PKD") AND NOT (inventory_status_code = "RCV");');
                                        echo "<option value='0' selected></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['inventory_status_code'] . '">' . $row['inventory_status_code'] . ' - ' . $row['inventory_status_description'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                    <label class="input-group-text">To Location:</label>
                                    <input class="form-control" type="text" id="eToLocEdit"/> 
                                    <label class="input-group-text">Expiry Date:</label>
                                    <input type="date" name="eExpiryDate" id="eExpiryDate" class="form-control date">
                                    <div class="control-group" style="display: none">
                                        <label class="input-group-text">Product Type:</label>
                                        <select class="form-control" type="text" id="eProductType">                                                                  
                                            <?php
                                            include ('../config/phpConfig.php');
                                            // Check connection
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.product_type;');
                                            echo "<option value=\"$value\"></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['id'] . '">' . $row['product_type_code'] . ' -- ' . $row['product_type_description'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>									
                                </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row" align="center">
                                <!-- Half of the modal-body div-->
                                <div class="controls">
                                    <label class="input-group-text">Decant:</label>  
                                    <input type="checkbox" name="eDecant" id="eDecant" class="input"></input>
                                    <label style="color: white">............</label>  
                                    <label class="input-group-text">Inspect:</label>   
                                    <input type="checkbox" name="eInspect" id="eInspect" class="input"></input>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" id="bSplitPiece" class="btn btn-success">Split Piece</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                <button type="button" id="bSaveEdit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Inventory Edit</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Inventory has been Changed</strong>
                    </div>
                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="toNums" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">To Numbers</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong ><span id="testNums"></span></strong>
                    </div>
                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <?php
        include('../common/topNavMenu.php');
 //       include('../common/sideBar.php');
        ?>

     
        <div>
            <label style="position:absolute;top:7%;left:85%;font-size: calc(0.5vw + 0.5vh + .02vmin);">Search:</label>
		<input id="search" style="position:absolute;top:7%;left:90%;width:10%;font-size: calc(0.5vw + 0.5vh + .02vmin)" type="text"></input>
                <br>
				<table id="example" class="compact stripe hover row-border" style="font-size: calc(0.5vw + 0.5vh + .05vmin)">
					<thead>
						<tr>
							<th>id</th>
							<!--<th style="width:5%">Vendor</th>-->
							<th style="width:10%">Part</th>
							<th>Serial</th>
							<th>Ran/Order</th>
							<th>Inventory</th>
							<th>Allocated</th>
                                                        <th>Picked</th>
                                                        <th>Hold</th>
							<th>Status</th>
							<th>Location</th>
							<th>Pallet</th>
							<!--<th>Product Type</th>-->
							<th>Decant</th>
							<th>Inspect</th>
							<th>Date Created</th>

							<th>Action</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>id</th>
							<!--<th><input id="search1" name="colSearch" style="width: 100%; text-align: center" type="text"></input></th>-->
							<th><input id="search2" name="colSearch" style="width: 100%; text-align: center" type="text"></input></th>
							<th><input id="search3" name="colSearch" style="width: 100%; text-align: center" type="text"></input></th> 
							<th><input id="search4" name="colSearch" style="width: 100%; text-align: center" type="text"></input></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
							<th><input id="search5" name="colSearch" style="width: 100%; text-align: center" type="text"></input></th>
							<th><input id="search6" name="colSearch" style="width: 100%; text-align: center" type="text"></input></th>
							<th><input id="search7" name="colSearch" style="width: 100%; text-align: center" type="text"></input></th>
							<!--<th><input id="search8" name="colSearch" style="width: 100%; text-align: center" type="text"></input></th>-->
                                                        <th></th>
                                                        <th></th>
							<th><input id="search9" name="colSearch" style="width: 100%; text-align: center" type="text"></input></th>
                                                        <th></th>
						</tr>
					</tfoot>
				</table>
				<input type="Button" id="bSaIn" class="btn btn-warning" value="SA_IN"/>
				<input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

	</div>
        <div id="filterTxt" type="hidden"></div>
        <div id="orderTxt" type="hidden"></div>
   <script>
    var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>'
    var tabStart = 0;
    var limit = tabStart;
   
    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }

    function locHelp() {
        var toLoc = document.getElementById('saiToLoc').value;
        $.ajax({
            url: '../action/locHelp.php',
            type: 'GET',
            data: {toLoc: toLoc},
            success: function (response, textstatus) {
                var myObj = JSON.parse(response);
                var num = myObj[0].num;
                if (num === '0') {
                    alert('Location Does Not Exist');
                    document.getElementById('saiToLoc').value = "";
					  $('#saiToLoc').focus();
                } else {
                    return;
                }
            }
        });
    }
    
    function partHelp() {
        var partNumber = document.getElementById('saiPartNumber').value;
        $.ajax({
            url: '../action/partHelp.php',
            type: 'GET',
            data: {partNumber: partNumber},
            success: function (response, textstatus) {
                var myObj = JSON.parse(response);
                var num = myObj[0].num;
                if (num === '0') {
                    alert('Part Does Not Exist');
                    document.getElementById('saiPartNumber').value = "";
					  $('#saiPartNumber').focus();
                } else {
                    return;
                }
            }
        });
    }

    function QtySplitPieceHelp() {
        var qty = document.getElementById('QtySplitPiece2').value;
        $.ajax({
//            url: '../action/partHelp.php',
			url: '../action/updateInventory.php?function=splitinventory',
            type: 'GET',
            data: {partNumber: partNumber},
            success: function (response, textstatus) {
                var myObj = JSON.parse(response);
                var num = myObj[0].num;
                if (num === '0') {
                    alert('Part Does Not Exist');
                    document.getElementById('saiPartNumber').value = "";
                } else {
                    return;
                }
            }
        });
    }

    $(document).ready(function () {
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>';
        var table = $('#example').DataTable({
            "lengthMenu": [[10, 15, 20, 50, 100, 500, 1000], [10, 15, 20, 50, 100, 500, 1000]],
            "processing": true,
            "serverSide": true,
            "searching": false,
             "rowId": "id",
            "order": [],
            ajax: {
                    "url": "../tableData/inventoryMasterTable.php",
                    type: "POST",
                    // Apply Filter
                    data: function ( d ) {
                            d.filter = $("#filterTxt").val();
//					if ($("#orderTxt").val != "")
//						d.filter = $("#filterTxt").val()+"ORDER BY "+$("#orderTxt").val();
                            d.sort = $("#orderTxt").val();
                    }
            },
            //setting the default page length 
            iDisplayLength: 15,
	    buttons: ['excel'],		
            columns: [
                {data: "id"},
		//{data: "vendor_reference_code","width":"5%"},
                {data: "part_number","width":"10%"},
                {data: "serial_reference"},
                {data: "ran_or_order"},
                {data: "inventory_qty",
                    render: function (data, type, row) {
                        return  parseInt(data) / row.conversion_factor;
                    }},
                {data: "allocated_qty",
                    render: function (data, type, row) {
                        return '<div id="popOver" data-toggle="popover"  >' + parseInt(data) / row.conversion_factor + '</div>';
                    }
                },
                 {data: "picked_qty",
                     render: function (data, type, row) {
                        return '<div id="popOver" data-toggle="popover"  >' + parseInt(data) / row.conversion_factor + '</div>';
                    }
                    
                },
                 {data: "hold_qty",
                    render: function (data, type, row) {
                        return '<div id="popOver" data-toggle="popover"  >' + parseInt(data) / row.conversion_factor + '</div>';
                    }
                },
                {data: "inventory_status_code","width":"5%"},
                {data: "location_code"},
                {data: "tag_reference"},
		//{data: "product_type_code"},
		{data: "requires_decant",
                    render: function (data, type, row) {
                        return (data == 1) ? '<span style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color: red;" class="glyphicon glyphicon-remove"></span>';
                    }
                },
		{data: "requires_inspection",
                    render: function (data, type, row) {
                        return (data == 1) ? '<span style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color: red;" class="glyphicon glyphicon-remove"></span>';
                    }
                },
                {data: "date_created"},
//              {data: "conversion_factor"}, !-->
                {data: "","width":"10%"}
            ],
	    columnDefs: [
		{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bEdit' class='btn btn-warning' value='Edit'/>  <input type='Button' id='bSaOut' class='btn btn-warning' value='SA_OUT'/><input type='Button' id='bPrint' class='btn btn-warning' value='Print'/>"
                }, 
				{
                    targets: [12,11],
                    orderable: false
                },
                {
                    targets: [0],
                    visible: false
                }
            ],	
            "infoCallback": function( settings, start, end, max, total, pre ) 
			{
				return "Entries "+start +" to "+ end;
			}
        });


        $("#exportExcel").on("click", function () {
            excelData();
        });
        $("#search").keyup(function (e){
            applyFilter();
        });
        $("[name='colSearch']").keyup(function (e){
            if (e.which === 13){
                applyFilter();
            }
        });
		
        $('#example_filter label input').on("focus", function (event) {
            $('#example').DataTable().ajax.reload(null, false);
        });
        
        $('#mSaOut').on('hidden.bs.modal', function (e) {
            $(this)
                    .find("input,textarea,select,number")
                    .val('')
                    .end()
                    .find("input[type=checkbox], input[type=radio]")
                    .prop("checked", "")
                    .end();
        });
        $('#mSaIn').on('hidden.bs.modal', function (e) {
            $(this)
                    .find("input,textarea,select")
                    .val('')
                    .end()
                    .find("input[type=checkbox], input[type=radio]")
                    .prop("checked", "")
                    .end();
        });
        $('#example tbody').unbind().on('click', '#popOver', function () {

            var data = table.row($(this).parents('tr')).data();
            var invID = data.id;
            $.ajax({
                url: '../action/getTo.php',
                type: 'GET',
                data: {invID: invID},
                success: function (result) {
                    var results = JSON.parse(result);
                    var fullArray = [];
                    var arr = Object.keys(results).map(function (k) {
                        return results[k];
                    });
                    for (var i = 0; i < arr.length; i++) {
                        fullArray.push(arr[i].document_reference);

                    }
                    $('#toNums').modal('show');
                    document.getElementById('testNums').innerHTML = fullArray.join('<br>')
                }
            });
        });

        $('#example tbody').on('click', '#bSaOut', function () {
            $('#mSaOut').modal('show');
            var data = table.row($(this).parents('tr')).data();
            document.getElementById('saoId').value = data.id;
            document.getElementById('saoPart').value = data.part_number;
            document.getElementById('saoSerial').value = data.serial_reference;
            document.getElementById('saoQty').value = data.available_qty / data.conversion_factor;
            document.getElementById('qtyHelp').value = data.available_qty / data.conversion_factor;
            document.getElementById('saoReason').value;
            document.getElementById('saoComment').value;
			document.getElementById('conversionFactor').value = data.conversion_factor;
           // document.getElementById('productType').value = data.product_type_code;

            $('#saoQty').unbind().on('change', function () {

                var thisOne = document.getElementById('saoQty').value;
                var thatOne = document.getElementById('qtyHelp').value;
//                 alert("Current: "+ thisOne + " Inventory: "+ thatOne);
//                 alert(thisOne > thatOne)
                if (parseInt(thisOne) > parseInt(thatOne)) {
                    alert('Quantity can not exceed the available inventory quantity of: ' + thatOne);
                    document.getElementById('saoQty').value = thatOne;
                }
            });
        });

       $("#bSaOutSubmit").on("click", function () {

            var partNumber = document.getElementById('saoPart').value;
            var serialReference = document.getElementById('saoSerial').value;
            var invyQty = document.getElementById('saoQty').value * document.getElementById('conversionFactor').value;
            var reasonCode = document.getElementById('saoReason').value;
            var outComment = document.getElementById('saoComment').value;

            var obj = {"partNumber": partNumber, "serialReference": serialReference, "qty": invyQty, "reasonCode": reasonCode, "comment": outComment ,"createdBy": currentUser};
            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            console.log(filter);
            
            $.ajax({
                url: callPostService +filter+ "&function=saOut" + screen,
                type: 'GET',
                success: function (response, textstatus) {
                    if (response === 'OK - true') {
                        $('#saOutSent').modal('show');
                    } else {
                        alert(response);
                    }
                }
            });
            $('#mSaOut').modal('hide');
        });

        $("#bSaIn").on("click", function () {
            $('#mSaIn').modal('show');
        });
		
        $('#example tbody').on('click', '#bPrint', function () {

            var data = table.row($(this).parents('tr')).data();
            document.getElementById('pRanOrder').value = data.ran_or_order;
            document.getElementById('pPartNumber').value = data.part_number;
            document.getElementById('pSerialNumber').value = data.serial_reference;
            document.getElementById('pQuantity').value = data.inventory_qty;
            $('#mPrintPart').modal('show');

        });
        $("#printPartButton").on("click", function () {
            var ranOrder = document.getElementById('pRanOrder').value;
            var partNumber = document.getElementById('pPartNumber').value;
            var snp = document.getElementById('pQuantity').value;
            var serialReference = document.getElementById('pSerialNumber').value;
            var printer = document.getElementById('printerIP').value;
            
            if(printer === ''){
                alert("Please select a printer");
                return;
            }
            
            var filter = "ranorder=" + ranOrder + "|AND|partnumber=" + partNumber + "|AND|snp=" + snp + 
                         "|AND|number=" + 1 + "|AND|serialreference=" + serialReference + "|AND|currentUser="+currentUser+"|AND|printer="+printer;
            console.log(filter);
             $.ajax({
                    url: "../action/printLabels.php?function=partlabelpdf&filter="+filter,
                    type: 'GET',
                    success: function (response, textstatus) {
                        if (response === 'OK  -partLabelPdf') {
                            alert("The odette labels are successfully printed.");
                                $('#mPrintPart').modal('hide');
                        } else {
                            alert(response);
                        }
                    }
                        });

           // window.open(recReport + "report.php?"+connections+"&function=partlabelpdf&filter=ranorder=" + ranOrder + "|AND|partnumber=" + partNumber + "|AND|snp=" + snp + "|AND|number=" + 1 + "|AND|serialreference=" + serialReference );
         });

       $("#bDoneSaIn").on("click", function () {
            var minLength = document.getElementById("minLength").value.replace(/\s/g, '');
            var ranOrder = document.getElementById("saiRanOrder").value.replace(/\s/g, '');
            var partNumber = document.getElementById("saiPartNumber").value.replace(/\s/g, '');
            var serialReference = document.getElementById("saiSerial").value.replace(/\s/g, '');
            var sQty = document.getElementById("saiQty").value.replace(/\s/g, '');
            var toLoc = document.getElementById("saiToLoc").value.replace(/\s/g, '');
            var reasonCode = document.getElementById("saiReason").value.replace(/\s/g, '');
            var newComment = document.getElementById("saiComment").value;
            var sendMsg = document.getElementById("saInNotifyKUK").checked;
            console.log(reasonCode)
            if (partNumber.length <= 0) {
                alert('please Enter a Part');
                return;
            }

            //console.log(minLength);
            if (serialReference.length < minLength) {
                alert('Serial incorrect length');
                return;
            }

            if (sQty.length <= 0) {
                alert('please Enter a Qty');
                return;
            }
            if (toLoc.length <= 0) {
                alert('please Enter a To Location');
                return;
            }
            if (reasonCode.length <= 0) {
                alert('please Enter a reason code');
                return;
            }
            if (serialReference.length <= 0) {
                alert('please Enter a Serial');
                return;
            }
            var saInJson = '';
            if (ranOrder.length > 0) {
                var obj = {"ranOrder": ranOrder, "partNumber": partNumber, "serialReference": serialReference, "qty": sQty, "reasonCode": reasonCode, "comment": newComment, "toLocationCode": toLoc, "createdBy": currentUser, "sendMsg":sendMsg};
                console.log(obj);
                saInJson = JSON.stringify(obj);
            } else {
                var obj = {"partNumber": partNumber, "serialReference": serialReference, "qty": sQty, "reasonCode": reasonCode, "comment": newComment, "toLocationCode": toLoc, "createdBy": currentUser, "sendMsg":sendMsg};
                saInJson = JSON.stringify(obj);
            }
            var filter = saInJson;

            var valFilter = 'serialReference=' + serialReference + '|AND|partNumber=' + partNumber + '|AND|locationCode=' + toLoc;
              var parts = [];
                parts.push(partNumber);
                var obj = {"parts": parts, "userId": currentUser};
                var allocReq = JSON.stringify(obj);
            $.ajax({
                
                url: callGetService +valFilter+ "&function=saInValidation" + screen,
                type: 'GET',
                success: function (response) {
                    if (response === 'true' || response === 'validated' || response === 'OK') {
                        $.ajax({
                            url: '../action/validatePartSerial.php',
                            data: {partNumber: partNumber,
                                serialReference: serialReference},
                            type: 'GET',
                            success: function (response, textstatus) {
                                if (response[11] === "1") {
                                    $('#saInCheck').modal('show');
                                    document.getElementById('filter').value = filter;
                                } else {
                                    $.ajax({
                                        url: callPostService +filter+ "&function=saIn" + screen,
                                        type: 'GET',
                                        success: function (response, textstatus) {
                                            if (response === 'OK - true') {
                                                callAllocateParts(allocReq);
                                                $('#mSaIn').modal('hide');
                                                $('#saInSent').modal('show');
                                            } else {
                                                alert(response);
                                            }
                                        }
                                    });
                                }
                            }
                        });
                        
                    } else {
                        alert(response);
                    }
                }
            });
             $('#mSaIn').modal('hide');
        });
          function callAllocateParts(filter){
                 $.ajax({
                url: callPostService + filter + "&function=allocateForParts" + pick,
                type: 'POST',
                success: function (response, textstatus) {
                }
            });
            }
        $("#bSaYes").on("click", function () {
            
            var filter = document.getElementById('filter').value;
            
            $.ajax({
                
                url: callPostService +filter+ "&function=saIn" + screen,
                type: 'GET',
                success: function (response, textstatus) {
                    if (response === 'OK - true') {
                        $('#mSaIn').modal('hide');
                        $('#saInSent').modal('show');
                    } else {
                        alert(response);
                    }
                }
            });
        });

        $('#example tbody').on('click', '#bEdit', function () 
		{
//            $('#mEdit').modal('show');
            var data = table.row($(this).parents('tr')).data();
            document.getElementById('eId').innerHTML = data.id;
            document.getElementById('ePart').innerHTML = data.part_number;
            document.getElementById('eSerial').innerHTML = data.serial_reference;
            document.getElementById('eQty').innerHTML = data.inventory_qty / data.conversion_factor;
            document.getElementById('eStatus').innerHTML = data.inventory_status_code;
            document.getElementById('eStatusEdit').value = data.inventory_status_code;
            document.getElementById('eToLocEdit').value = data.location_code;
			if (data.expiry_date != null)
				document.getElementById('eExpiryDate').value = data.expiry_date.substr(0,10);
			else
				document.getElementById('eExpiryDate').value = "2035-12-31";
            if (data.requires_decant == 1) 
			{
                $('#eDecant').prop('checked', true);
            } 
			else 
			{
                $('#eDecant').prop('checked', false);
            }
            if (data.requires_inspection == 1) 
			{
                $('#eInspect').prop('checked', true);
            } 
			else 
			{
                $('#eInspect').prop('checked', false);
            }
			$('#mEdit').modal('show');
        });

        $("#bSplitPiece").on("click", function () {

            var partNum = document.getElementById('ePart').innerHTML;
            var serialRef = document.getElementById('eSerial').innerHTML;
            var qty1 =  document.getElementById('eQty').innerHTML;
            if(document.getElementById('eStatus').innerHTML  === 'OK'){
                $('#mEdit').modal('hide');
                document.getElementById('eSerialSplitPiec').innerHTML = serialRef;
                document.getElementById('eSerialSplitPiec1').innerHTML = serialRef;
                document.getElementById('eSerialSplitPiec2').innerHTML = serialRef+"SPL";
                document.getElementById('eQtySplitPiec1').innerHTML = qty1;
                $('#mSplitPiece').modal('show');
            }

        });


        $("#bSaveSplitPiece").on("click", function () {

            var partNum = document.getElementById('ePart').innerHTML;
            var toLocation = document.getElementById('eToLocEdit').value;
            var serial1 = document.getElementById('eSerialSplitPiec1').innerHTML;
            var serial2 = document.getElementById('eSerialSplitPiec2').innerHTML;
            var qty1 = document.getElementById('eQtySplitPiec1').innerHTML;
            var qty = document.getElementById('eQtySplitPiec2').value;
            
          
            if(parseInt(qty) >= parseInt(qty1) || parseInt(qty) <= 0 ){
                alert("Please Enter correct qty");
                return;
            }
//       	    var txt;
//            if (confirm("PRINT SERIAL LABEL?")) {
//        		window.open('../action/splitSerialPDF.php?splitSerial='+serial2, '_blank');
//            } else {
//                 	  txt = "You pressed Cancel!";
//            }

            
           var obj = {"partNumber": partNum, "serial1": serial1, "serial2": serial2, "qty": qty,"createdBy": currentUser,"toLocationCode": toLocation};
            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            
        	$.ajax({
                
                url: callPostService + filter + "&function=splitPiece" + screen,
                type: 'GET',
                success: function (response, textstatus) {
                    if (response === 'OK - true') {
                        $('#mSplitPiece').modal('hide');
                        alert("Serial Split Complete");
                    } else {
                        alert(response);
                    }
                }
            });
        });

        $("#bSaveEdit").on("click", function () {

            var id = document.getElementById('eId').innerHTML;
            var partNum = document.getElementById('ePart').innerHTML;
            var serialRef = document.getElementById('eSerial').innerHTML;
            var statusCo = document.getElementById('eStatusEdit').value;
            var toLoc = document.getElementById('eToLocEdit').value;
            console.log(statusCo);
            if (statusCo === '0') {
                alert('Status cannot be blank');
                return;
            }
            var inspectBox = $('#eInspect');
            var decantBox = $('#eDecant');

            if (inspectBox.is(':checked')) {
                inspectBox = true;
            } else {
                inspectBox = false;
            }
            if (decantBox.is(':checked')) {
                decantBox = true;
            } else {
                decantBox = false;
            }
            //validate location
            $.ajax({
                url: '../action/locHelp.php',
                type: 'GET',
                data: {toLoc: toLoc},
                success: function (response, textstatus) {
                    var myObj = JSON.parse(response);
                    var num = myObj[0].num;
                    if (num === '0') {
                        alert('Location Does Not Exist');
                        return;
                    } else {
                        //validate muiltipart location
                        var valFilter = 'serialReference=' + serialRef + '|AND|partNumber=' + partNum + '|AND|locationCode=' + toLoc;
                        $.ajax({
                            url: callGetService +valFilter+ "&function=saInValidation"  + screen,
                            type: 'GET',
                            success: function (response) {
                                if(response === 'OK'){
                                    var obj = {"partNumber": partNum, "serialNumber": serialRef, "statusCode": statusCo, "decant": decantBox, "inspect": inspectBox, "currentUser": currentUser, "locationCode":toLoc};
                                    var newEditjson = JSON.stringify(obj);
                                    var filter = newEditjson;
                                    console.log(filter);

                                    $.ajax({
                                        url: callPostService + filter +"&function=editInventory" +  screen,
                                        type: 'GET',
                                        success: function (response, textstatus) {
                                            if (response === 'OK - false') {
                                                alert('Cannot move the Part without serial.');
                                            } else {
                                                $('#mEdit').modal('hide');
                                                $('#confEdit').modal('show');
                                            }
                                        }
                                    });
                                }else{
                                    alert(response);
                                }
                            }
                        });
                    }
                }
            });
        });
    });

function excelData()
{	
	
        var txtHost="<?php echo $mHost ?>";
        var txtUser="<?php echo $mDbUser ?>";
        var txtPwd="<?php echo $mDbPassword ?>";
        var txtDbase="<?php echo $mDbName ?>";
        var txtTable="inventory_master";

	$("#Loading").show();
	var table = $('#dropdown_table').val();
	var txtFilter = $('#filterTxt').val();
	excelWindow = window.open("","Excel","width=500,height=100");
	excelWindow.document.write("<p>Please Wait...</p>");
	var url = ("/cgi-bin/tableprintxl.py"+
		"?filter="+txtFilter+
		"&table=inventory_master"+
		"&host="+txtHost+
		"&user="+txtUser+
		"&password="+txtPwd+
		"&database="+txtDbase);	
	$.ajax({
                url:url,
		cache: true,
		success:function(data){
//			alert("OK  - "+data);
			$("#Loading").hide();
//			window.open(data);
//			window.open("/vimsreports/");
			return excelWindow.location = data;
		},
		error:function(jqXHR,data){
			alert("Error- "+jqXHR.status)
			$("#Loading").hide();
		}
	});
}

function applyFilter()
{
	var filter = "";
	var order = "";
	if ($('#search').val() != "")
	{
		filter = "CONCAT_WS('', vendor.vendor_reference_code,inventory_master.part_number,inventory_master.serial_reference,inventory_master.ran_or_order,inventory_master.location_code,inventory_master.tag_reference,product_type.product_type_code,inventory_status.inventory_status_code) LIKE ('%"+$('#search').val()+"%')";
		$('#filterTxt').val(filter);
		$('#orderTxt').val(order);
		$('#example').DataTable().ajax.reload();
		return;
	}
	/*if ($('#search1').val() != "")
		if (filter.length > 0)
			filter = filter + " AND vendor.vendor_reference_code='"+$('#search1').val()+"'";
		else
			filter = "vendor.vendor_reference_code='"+$('#search1').val()+"'";*/
         if ($('#search2').val() != "")
		if (filter.length > 0)
			filter = filter + " AND inventory_master.part_number='"+$('#search2').val()+"'";
		else
			filter = "inventory_master.part_number='"+$('#search2').val()+"'";
	if ($('#search3').val() != "")
		if (filter.length > 0)
			filter = filter + " AND inventory_master.serial_reference LIKE('"+$('#search3').val()+"%')";
		else
			filter = "inventory_master.serial_reference LIKE('"+$('#search3').val()+"%')";
	if ($('#search4').val() != "")
		if (filter.length > 0)
			filter = filter + " AND inventory_master.ran_or_order='"+$('#search4').val()+"'";
		else
			filter = "inventory_master.ran_or_order='"+$('#search4').val()+"'";
	if ($('#search5').val() != "")
		if (filter.length > 0)
			filter = filter + " AND inventory_status.inventory_status_code='"+$('#search5').val()+"'";
		else
			filter = "inventory_status.inventory_status_code='"+$('#search5').val()+"'";
	if ($('#search6').val() != "")
		if (filter.length > 0)
			filter = filter + " AND inventory_master.location_code='"+$('#search6').val()+"'";
		else
			filter = "inventory_master.location_code='"+$('#search6').val()+"'";
	if ($('#search7').val() != "")
		if (filter.length > 0)
			filter = filter + " AND inventory_master.tag_reference='"+$('#search7').val()+"'";
		else
			filter = "inventory_master.tag_reference='"+$('#search7').val()+"'";
   /* if ($('#search8').val() != "")
		if (filter.length > 0)
			filter = filter + " AND product_type.product_type_code='"+$('#search8').val()+"'";
		else
			filter = "product_type.product_type_code='"+$('#search8').val()+"'";*/
	if ($('#search9').val() != "")
		if (filter.length > 0)
			filter = filter + " AND substr(inventory_master.date_created,1,"+$('#search9').val().length+") ='"+$('#search9').val()+"'";
		else
			filter = "substr(inventory_master.date_created,1,"+$('#search9').val().length+")='"+$('#search9').val()+"'";
	$('#filterTxt').val(filter);
	$('#orderTxt').val(order);
//		alert("TEST1"+filter);
	$('#example').DataTable().ajax.reload();
}
</script>
</body>
</html>
