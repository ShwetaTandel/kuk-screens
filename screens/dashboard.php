<!DOCTYPE html>
<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Dashboard</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <link href="../css/dashboardStyle.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>  
        <script src="../dashboard/dashAjax.js" type="text/javascript"></script>
        <style>

        </style>

    </head>

    <body>



        <div id="div1" class="outline">
            <input id="back" class="btn btn-success back" type="button" value="Vims" onclick="gotoVims()"/>
            <h2 onclick="openPi()" align="center">PI – Inventory Accuracy</h2>
            <div style="float: left;">

                <div  class = "container">

                    <div id = "tl-box" class = "box top-row">
                        <p>Locations Counted: <br><span id="counted" style="font-weight: bold; font-size: medium">#</span></p>
                    </div>
                    <div id = "tr-box" class = "box top-row">
                        <p>Locations Accurate: <br><span id="accurate" style="font-weight: bold; font-size: medium">#</span></p>
                    </div>
                    <div id = "bl-box" class = "box bottom-row">
                        <p>Serial Not Exist: <br><span id="notExist"  style="font-weight: bold; font-size: medium">#</span></p>
                    </div>
                    <div id = "br-box" class = "box bottom-row">
                        <p>Serials Wrong Location: <br><span id="wrongLoc"  style="font-weight: bold; font-size: medium">#</span></p>
                    </div>
                    <div id = "cc-box" class = "box">
                        <p>Current Days Accuracy <br><span id="totalPer" onclick="fixPercent()"  style="font-weight: bold; font-size: medium">#</span><span>%</span></p>
                    </div>
                </div>



                <div class="alertOne" style="margin-left: 10px; margin-top: 20px; position: inherit">
                    <p style="color: white; text-align: center; margin-top: 5px; font-size: medium;">Missing Serial: <span id="missing" style="font-weight: bold">#</span</p>
                </div>
            </div>

            <div style="float: left;">
                <div  class = "container">
                    <div id = "tl-box2" class = "box2 top-row">
                        <p>Locations Counted: <br><span id="weekCount" style="font-weight: bold; font-size: medium">#</span></p>
                    </div>
                    <div id = "tr-box2" class = "box2 top-row">
                        <p>Locations Accurate: <br><span id="weekAccurate" style="font-weight: bold; font-size: medium">#</span></p>
                    </div>
                    <div id = "bl-box2" class = "box2 bottom-row">
                        <p>Serial Not Exist: <br><span id="weekNotExist"  style="font-weight: bold; font-size: medium">#</span></p>
                    </div>
                    <div id = "br-box2" class = "box2 bottom-row">
                        <p>Serials Wrong Location: <br><span id="weekWrongLoc"  style="font-weight: bold; font-size: medium">#</span></p>
                    </div>
                    <div id = "cc-box2" class = "box2">
                        <p>Current Weeks Accuracy <br><span id="WeekTotalPer" onclick="fixPercent2()"  style="font-weight: bold; font-size: medium">#</span><span>%</span></p>
                    </div>

                </div>



                <div class="alertOne pulse" style="margin-left: 10px; margin-top: 20px; position: inherit">
                    <p style="color: white; text-align: center; margin-top: 5px; font-size: medium; ">Missing Serial: <span style="font-weight: bold" id="weekMissing">#</span</p>
                </div>

            </div>
            <div  style="float: left" id="topBox">
                <p style="padding-top: 18px;  font-size: 14px; ">Total Serials Currently Missing (Loc STKCHK) <br><span id="stkCheck" style="font-weight: bold;">#</span></p>
            </div>
            <br><br><br><br><br><br><br>
            <div style="float: left" id="bottomBox">
                <p style=" padding-top: 18px;  text-align: center; font-size: 25px;">Last Box <br><span id="lastBox" style="font-weight: bold;">#</span></p>
            </div>

            <div  id="circle" class="topcorner" >
                <p style="font-size: medium">Today’s F6  boxes: <br><span id="f6Box"  style="font-weight: bold; font-size: large">#</span></p>
            </div>

            <textarea id="piComments" style="margin-top: 10px" ></textarea>

        </div>

        <div id="div2" class="outline">
            <input  id="recvDial" class="btn btn-success back" type="button" value="Receiving Dial" onclick="gotoRecvDial()"/>
            <h2 onclick="openRec()" align="center">Receiving & Put Away</h2>
            <div class="row">
            <!--    <div style="margin-top: 15px" class="col-sm-7">   
                    <div style="margin-left: 15px" class="well">
                        <table id="poolTable" class="compact stripe hover row-border" style="width: auto">
                            <thead>
                                <tr>
                                    <th>Receiving Location</th>
                                    <th>Cases >4 Hours</th>
                                    <th>Oldest Box</th>
                                    <th>Part Number</th>
                                    <th>Serial</th>
                                    <th>Qty</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- <div  class="col-sm-2"> 
                     <div id="recBox">
                         <p style="padding-top: 18px;  font-size: 14px; ">Number of receipt with No Haulier <br><span id="recCheck" style="font-weight: bold;">#</span></p>
                     </div>
                 </div>-->
                <div style="margin-top: 15px" class="col-sm-3">
                    <div style="margin-left: 15px" class="well">
                        <h4  align="center">Shortages</h4>
                        <table id="shortageTable" class="compact stripe hover row-border" style="width: auto">
                            <thead>
                                <tr>
                                    <th>GRN</th>
                                    
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                 <div class="col-sm-2"> 
                    <div id="recBoxRight">
                        <p style="padding-top: 10px;  font-size: 14px; ">Locations full no Inventory <br><span id="noInv" style="font-weight: bold;">#</span><br><button onclick="emptyLocation()" class="btn btn-primary">Empty</button></p>
                    </div>
                </div>
                <div style="padding-top: 10px" class="col-sm-3"> 
                    <div id="recBoxBottom">
                        <p style="padding-top: 18px;  font-size: 14px; ">Number of open receipts  <br><span id="recOpen" style="font-weight: bold;">#</span></p>
                    </div>
                </div>
            </div>
          

            <textarea id="recComments" style="margin-top: 10px" ></textarea>

        </div>



    

    <div id="div3" class="outline">

        <input  id="pickDial" class="btn btn-success back" type="button" value="Pick Dial" onclick="gotoPickDial()"/>
        <h2 onclick="openPick()" align="center">Pick & Despatch</h2>

        <div class="col-lg-6">
            <div class="well">
                <h4  align="center">Emergency Picks</h4>
                <table id="emergDay" class="compact stripe hover row-border" style="width:100%">
                    <thead>
                        <tr>
                            <th>Total Number Of Orders</th>
                            <th>Picked</th>
                            <th>Open</th>
                            <th>Overdue</th>
                        </tr>
                    </thead>
                </table>
                <table id="emergOver" class="compact stripe hover row-border" style="width:100%">
                    <thead>
                        <tr>
                            <th>Reference Number</th>
                            <th>Time Due</th>
                        </tr>
                    </thead>
                </table>
            </div>          
        </div>
        <div class="col-sm-3">
            <div  class="well">
                <table id="blockedTanumTable" class="compact stripe hover row-border" style="width:auto">
                    <thead>
                        <tr>
                            <th>Blocked TOs</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>

        <textarea id="pickComments" style="margin-top: 10px" ></textarea>
    </div>


    <div id="div4" class="outline">
        <h2 onclick="openReplen()" align="center">Replen</h2>

        <div class="col-sm-3">
            <div class="well">

                <table id="replenTable" class="compact stripe hover row-border" style="width: auto">
                    <thead>
                        <tr>
                            <th>Pick Group</th>
                            <th>Open Tasks</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="col-lg-6">
            <div  class="well">
                <table id="taskTable" class="compact stripe hover row-border" style="width: auto">
                    <thead>
                        <tr>
                            <th>Serial</th>
                            <th>Pallet</th>
                            <th>Last Updated</th>
                            <th>Last Updated By</th>
                        </tr>
                    </thead>
                </table>
            </div>

        </div>

        <textarea id="replenComments" style="margin-top: 10px" ></textarea>
    </div>



    <script>
        function gotoVims() {
            window.open("mainMenu.php", "_self");
        }
        function gotoPickDial() {
            window.open("pickDial.php", "_self");
        }
        function gotoRecvDial() {
            window.open("recvDial.php", "_self");
        }
        function fixPercent() {
            var acc = document.getElementById('accurate').innerHTML;
            var count = document.getElementById('counted').innerHTML;
            document.getElementById('totalPer').innerHTML = Math.round(acc / count * 100);
        }
        function fixPercent2() {
            var acc = document.getElementById('weekAccurate').innerHTML;
            var count = document.getElementById('weekCount').innerHTML;
            document.getElementById('WeekTotalPer').innerHTML = Math.round(acc / count * 100);
        }
        function openReplen() {
            window.open('replenTask.php', '_self')
        }
        function openRec() {
            window.open('recTable.php', '_self')
        }
        function openPick() {
            window.open('pick.php', '_self')
        }
        function openPi() {
            window.open('piTable.php', '_self')
        }

        function emptyLocation() {
            $.ajax({
                url: '../dashboard/emptyLocation.php',
                type: 'GET',
                success: function (result) {
                    alert(result)

                    $.ajax({
                        url: '../dashboard/noInv.php',
                        type: 'GET',
                        dataType: "json",
                        success: function (data) {
                            document.getElementById('noInv').innerHTML = data[0].count;

                            if (data[0].count > 24) {
                                var d = document.getElementById("recBoxRight");
                                d.className += " pulse";
                            }
                        }
                    });


                }
            });

        }

        $('#piComments').blur(function () {
            var newComment = $('#piComments').val();
            $.ajax({
                url: "../dashboard/updateCommentPi.php",
                data: {'newComment': newComment},
                type: "GET",
                success: function (result) {
                    console.log(result)
                }
            });
            $("#piComments").focusin(function () {
                $.ajax({
                    url: '../dashboard/getCommentsPi.php',
                    type: 'GET',
                    success: function (result) {
                        var data = JSON.parse(result);
                        document.getElementById('piComments').innerHTML = data[0].day_comments;
                    }
                });
            });
        });
        $('#pickComments').blur(function () {
            var newComment = $('#pickComments').val();
            $.ajax({
                url: "../dashboard/updateCommentPick.php",
                data: {'newComment': newComment},
                type: "GET",
                success: function (result) {
                    console.log(result)
                }
            });
            $("#pickComments").focusin(function () {
                $.ajax({
                    url: '../dashboard/getCommentsPick.php',
                    type: 'GET',
                    success: function (result) {
                        var data = JSON.parse(result);
                        document.getElementById('pickComments').innerHTML = data[0].day_comments;
                    }
                });
            });
        });
        $('#replenComments').blur(function () {
            var newComment = $('#replenComments').val();
            $.ajax({
                url: "../dashboard/updateCommentReplen.php",
                data: {'newComment': newComment},
                type: "GET",
                success: function (result) {
                    console.log(result)
                }
            });
            $("#replenComments").focusin(function () {
                $.ajax({
                    url: '../dashboard/getCommentsReplen.php',
                    type: 'GET',
                    success: function (result) {
                        var data = JSON.parse(result);
                        document.getElementById('replenComments').innerHTML = data[0].day_comments;
                    }
                });
            });
        });
        $('#recComments').blur(function () {
            var newComment = $('#recComments').val();
            $.ajax({
                url: "../dashboard/updateCommentRec.php",
                data: {'newComment': newComment},
                type: "GET",
                success: function (result) {
                    console.log(result)
                }
            });
            $("#piComments").focusin(function () {
                $.ajax({
                    url: '../dashboard/getCommentsRec.php',
                    type: 'GET',
                    success: function (result) {
                        var data = JSON.parse(result);
                        document.getElementById('recComments').innerHTML = data[0].day_comments;
                    }
                });
            });
        });

        $(document).ready(function () {
            var replenTable = $('#replenTable').DataTable({
                ajax: {"url": "../dashboard/replenTable.php", "dataSrc": ""},
                paging: false,
                searching: false,
                bInfo: false,
                ordering: false,
                scrollY: "300px",
                scrollCollapse: true,
                columns: [
                    {data: "pick_group_code"},
                    {data: "open_task"},
                ],
                order: [[0, 'asc']]
            });
             var shortageTable = $('#shortageTable').DataTable({
                ajax: {"url": "../dashboard/shortageReceipts.php", "dataSrc": ""},
                paging: false,
                searching: false,
                bInfo: false,
                ordering: false,
                scrollY: "300px",
                scrollCollapse: true,
                columns: [
                    {data: "customer_reference"}
                    
                ],
                 "createdRow": function (row, data, dataIndex) {
                if (data.timediff > 45) {
                    $(row).css('background-color', 'red');
                } else if (data.timediff < 45 &&data.timediff > 30 ){
                    //console.log(data.Closed)
                    $(row).css('background-color', '#f5ca53');
                }
                },
                order: [[0, 'asc']]
            });
            var poolTable = $('#poolTable').DataTable({
                ajax: {"url": "../dashboard/poolTable.php", "dataSrc": ""},
                paging: false,
                searching: false,
                bInfo: false,
                ordering: false,
                scrollY: "150px",
                scrollCollapse: true,
                columns: [
                    {data: "location_code"},
                    {data: "Ammout"},
                    {data: "date_created"},
                    {data: "part_number"},
                    {data: "serial_reference"},
                    {data: "QTY"}
                ],
                order: [[0, 'asc']]
            });
            var taskTable = $('#taskTable').DataTable({
                ajax: {"url": "../dashboard/taskTable.php", "dataSrc": ""},
                paging: false,
                searching: false,
                bInfo: false,
                ordering: false,
                scrollY: "300px",
                scrollCollapse: true,
                columns: [
                    {data: "serial_reference"},
                    {data: "tag_reference"},
                    {data: "last_updated"},
                    {data: "last_updated_by"}
                ],
                order: [[0, 'asc']]
            });
            
          


        });



    </script>



</body>
</html>