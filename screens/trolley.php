<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Trolley Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="modal fade" id="mCreateNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New Trolley</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Trolley Code:</label>
                                    <div class="controls">
                                        <input type="text" name="nTrolleyCode" id="nTrolleyCode" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Trolley Description:</label>
                                    <div class="controls">
                                        <input type="text" name="nTrolleyDescription" id="nTrolleyDescription" class="form-control" >
                                    </div>
                                </div>
                                 <div class="control-group">
                                            <label class="input-group-text">Train:</label>
                                            <select class="form-control" type="text" id="nTrainId">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
// Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.train;');
                                                echo "<option value='-1'></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['train'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>

                            </div>
                           
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="newTrolleyButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confCreate" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Trolley Added</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >New Trolley Added</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="mEditTrolley" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New Trolley</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <input type="text" name="eID" id="eID" class="form-control" style="display: none;">
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Trolley Code:</label>
                                    <div class="controls">
                                        <input type="text" name="eTrolleyCode" id="eTrolleyCode" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Trolley Description:</label>
                                    <div class="controls">
                                        <input type="text" name="eTrolleyDescription" id="eTrolleyDescription" class="form-control" >
                                    </div>
                             
                            </div>
                                 <div class="control-group">
                                            <label class="input-group-text">Train:</label>
                                            <select class="form-control" type="text" id="eTrainId">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
// Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.train;');
                                                echo "<option value='-1'></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['train'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                          
                              
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-danger"  style="float: left;" id="bDelete">Delete</button>
                        <button type="button" class="btn btn-success" id="editTrolleyButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Trolley Edited</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Trolley Edited</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confCreate" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Aisle Added</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >New Aisle Added</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-sm" id="confEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Aisle Edited</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Aisle Edited</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confDelete" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete: <strong><span id="delMessage2">*</span></strong></h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Are you sure you want to delete <strong><span id="delMessage">*</span></strong></h4>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="conDel">Yes</button>
                        <button type="button" class="btn btn-danger"  data-dismiss="modal" >No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="doneDel" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Trolley Deleted</h4>
                    </div>

                    <div class="modal-footer" >
                       <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="errorDel" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Trolley unable to be deleted</h4>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>

            <?php
            include('../common/topNav.php');
            include('../common/sideBar.php');
            ?>

        <!-- Page Content  -->
        <div id="content">
            <br>
            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th>Trolley Code</th>
                        <th>Description</th>
                        <th>Train</th>
                        <th>Date Created</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th></th>
                </thead>
                <tfoot>
                    <tr>
                        <th>Trolley Code</th>
                        <th>Description</th>
                        <th>Train</th>
                        <th>Date Created</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th></th>
                </tfoot>
            </table>

            <input type="Button"  id="bCreateNew" class="btn btn-warning" value="Create New"/>
            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

        </div>
        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->

<script>


    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }


    $(document).ready(function () {
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>';

        var table = $('#example').DataTable({
            ajax: {"url": "../tableData/TrolleyTable.php", "dataSrc": ""},
            columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bEdit' class='btn btn-warning' value='Edit'/>"
                }
            ],
            buttons: [
                {extend: 'excel', filename: 'trolley_type', title: 'Trolley'}
            ],
            columns: [
                {data: "trolley"},
                {data: "description"},
                {data: "train"},
                {data: "date_created"},
                {data: "last_updated"},
                {data: "last_updated_by"},
                {data: ""}
            ],
            order: [[0, 'asc']]
        });
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });

     $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });


        $("#bCreateNew").unbind().on("click", function () {
            $('#mCreateNew').modal('show');
        });
        $("#newTrolleyButton").unbind().on("click", function () {

            //  var newType = document.getElementById('nChargeType').value;
            var newCode = document.getElementById('nTrolleyCode').value;
            var newDesc = document.getElementById('nTrolleyDescription').value;
             var newTrainId = document.getElementById('nTrainId').value;
           // var newPlttrolley = document.getElementById('nPlttrolley').checked ? true:false;

                   if(newCode.length === 0 ){
                alert('Trolley Code Cannot Be Blank');
                return;
            }
             if(newTrainId === '-1' ){
                alert('Train Cannot Be Blank');
                return;
            }
            
            
            $('#mCreateNew').modal('hide');

            var obj = {"trolleyCode": newCode, "trolleyDescription": newDesc, "createdBy": currentUser, "updatedBy": currentUser,"trainId":newTrainId};
            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            console.log(filter);


            $.ajax({
                url: callPostService + filter  + "&function=createTrolley" + screen,
                type: 'GET',
                success: function (response, textstatus) {
                    if(response === 'OK - true'){
                    $('#confCreate').modal('show');
                }else{
                    alert('Unable to Add');
                }
                }
            });
        });
        $('#example tbody').unbind().on('click', '#bEdit', function () {

            $('#mEditTrolley').modal('show');
            var data = table.row($(this).parents('tr')).data();
            document.getElementById('eID').value = data.id;
            document.getElementById('eTrolleyCode').value = data.trolley;
            document.getElementById('eTrolleyDescription').value = data.description;
            document.getElementById('eTrainId').value = data.train_id
         //   document.getElementById('ePlttrolley').checked = (data.plt_trolley === '1');
        });
 
        $("#editTrolleyButton").unbind().on("click", function () {

            var newID = document.getElementById('eID').value;
            var newCode = document.getElementById('eTrolleyCode').value;
            var newDesc = document.getElementById('eTrolleyDescription').value;
            var newTrainId = document.getElementById('eTrainId').value;
        //    var newPlttrolley = document.getElementById('ePlttrolley').checked ? 1:0;
            
            if(newCode.length === 0 ){
                alert('Trolley Code Cannot Be Blank');
                return;
            }
            if(newTrainId === '-1' ){
                alert('Train Cannot Be Blank');
                return;
            }
            

            $('#mEditTrolley').modal('hide');

            var obj = {"id": newID, "trolleyCode": newCode, "trolleyDescription": newDesc, "updatedBy": currentUser, "trainId":newTrainId};
            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            console.log(filter);


            $.ajax({
                url: callPostService + filter  + "&function=editTrolley" + screen,
                type: 'GET',
                success: function (response, textstatus) {
                    if(response === 'OK - true'){
                    $('#confEdit').modal('show');
                }else{
                    alert('Unable to Edit');
                }
                }
                
            });
        });
        $("#bDelete").unbind().on("click", function () {

            $('#confDelete').modal('show');
            document.getElementById("delMessage").innerHTML = document.getElementById('eTrolleyCode').value;
            document.getElementById("delMessage2").innerHTML = document.getElementById('eTrolleyCode').value;

        });

        $("#conDel").unbind().on("click", function () {
            var trolleyID = document.getElementById('eID').value;
            //   alert('DELETE: ' + rowID);

            var filter = 'trolleyId=' + trolleyID;

            $.ajax({
                url: callGetService + filter + "&function=deleteTrolley"  + screen,
                type: 'GET',
                success: function (response, textstatus) {
                    if (response === 'true') {
                        $('#mEditTrolley').modal('hide');
                        $('#confDelete').modal('hide');
                        $('#doneDel').modal('show');
                        
                    } else {
                        $('#mEditTrolley').modal('hide');
                        $('#confDelete').modal('hide');
                        $('#errorDel').modal('show');
                        
                    }
                }
            });
        });
    });

</script>
</body>
</html>



