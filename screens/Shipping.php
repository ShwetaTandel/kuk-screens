
<?php
include '../config/logCheck.php';
?>

<html>
    <head>
        <title>Shipping Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>        
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        
       <!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
        <script src="../js/libs/mask/jquery.mask.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="modal fade" id="mNewShip" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Trailer Location:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="nTraLoc">                                                                  
                                            <?php
                                            include ('../config/phpConfig.php');
                                            // Check connection
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.Location where location_code like "%van%";');
                                            echo "<option value=\"$value\"></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['location_code'] . '">' . $row['location_code'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="newButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confCreate" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Creation Success</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Creation Successful</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="pOpt" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Print Reports</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" align="center">
                        <input type="text" id="printHelp" style="display: none;">
                        <button style="width: 200px"  class='btn btn-danger' id="sumReport" type='button'>Scan Report Summary</button><br><br>
                        <button style="width: 200px" class='btn btn-warning' id="detailReport" type='button'>Scan Report Detail</button><br><br>
                        <button style="width: 200px" class='btn btn-success' id="excelReport" type='button'>Excel Detail Report</button><br><br>
                        <button style="width: 200px" class='btn btn-info' id="manReport" type='button'>Manifest Summary</button>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="printClose" class="btn btn-secondary" data-dismiss="modal" >Done</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade right" id="mEdit" role="dialog" aria-labelledby="viewEdit" aria-hidden="true" >
            <div class="modal-dialog modal-fluid" role="document" style="width: 1000px">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalPreviewLabel">Edit Manifest</h5>
                    </div>
                    <div class="modal-body">
                        <div class=" col-md-12">
                            <div id="nPage1">
                                <div class="row" align="center" style="vertical-align:left">
                                    <!-- Half of the modal-body div-->
                                    <p style="display: none;"><span id="eId"></span></p>
                                    <h2>Manifest Number: <strong id="eMan">#</strong></h2>
                                    <h2>Trailer Reference: <strong id="eTrail">#</strong></h2>
                                    <!-- Other half of the modal-body div-->
                                </div>
                            </div>
                            <hr>
                            <div class="row" align="center">
                                <div class="form-group" style="width: 350px">
                                    <label class="control-label col-sm-2">Time Slot:</label> 
                                    <div class="col-sm-10">
                                        <input class="form-control date_time" id="eTimeSlot" placeholder="YYYY-MM-DD HH:MM:SS" />
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                <button type="button" id="bSaveEdit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Success</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Edit Successful</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>

<?php
include('../common/topNav.php');
include('../common/sideBar.php');
?>
        <!-- Page Content  -->
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" >
                <thead>
                    <tr>
                        <th></th>
                        <th>Document Reference</th>
                        <th>Trailer Reference</th>
                        <th>Expected Delivery</th>
                        <th>Time Slot</th>
                        <th>Created By</th>
                        <th>Date Created</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th>Report Options</th>
                        <th>Action</th>
                </thead>
                <tfoot>
                    <tr>
                        <th></th>
                        <th>Document Reference</th>
                        <th>Trailer Reference</th>
                        <th>Expected Delivery</th>
                        <th>Time Slot</th>
                        <th>Created By</th>
                        <th>Date Created</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th>Report Options</th>                        
                        <th>Action</th>
                </tfoot>
            </table>

            <input type="Button" id="bCreateNew" class="btn btn-warning" value="Create New"/>
            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

        </div>

        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->

<script>
    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }
    function format(d) {
        // `d` is the original data object for the row
        return '<table id="shipBody" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                '<thead>' +
                '<th></th>' +
                '<th>Document Reference</th>' +
                '<th>TO Number</th>' +
                '<th>Date Created</th>' +
                '<th>Created By</th>' +
                '<th>Last Updated By</th>' +
                '<th>Last Updated Date</th>' +
                '</thead>' +
                '</table>';
    }
    function detailFormat(d) {
        // `d` is the original data object for the row
        return '<table id="shipDetail" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                '<thead>' +
                '<th>Document Reference</th>' +
                '<th>Serial Reference</th>' +
                '<th>Part Number</th>' +
                '<th>Qty</th>' +
                '<th>Date Created</th>' +
                '<th>Created By</th>' +
                '<th>Last Updated By</th>' +
                '<th>Last Updated Date</th>' +
                '</thead>' +
                '</table>';
    }

    $(document).ready(function () {
        $('.date_time').mask('0000-00-00 00:00:00');
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>'


        var table = $('#example').DataTable({
            ajax: {"url": "../tableData/shippingHeaderTableData.php", "dataSrc": ""},
            columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bEdit' class='btn btn-warning' value='Edit'/>"
                }, {
                    targets: -2,
                    data: null,
                    orderable: false,
                    defaultContent: "<button type='button' class='btn btn-success' data-toggle='modal' data data-target='#pOpt'>Reports</button>"
                }

            ],
            buttons: [
                {extend: 'excel', filename: 'Shipping', title: 'Shipping'}
            ],
            columns: [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {data: "document_reference"},
                {data: "trailer_reference"},
                {data: "expected_delivery_time"},
                {data: "time_slot"},
                {data: "created_by"},
                {data: "date_created"},
                {data: "last_updated_date"},
                {data: "last_updated_by"},
                {data: ""},
                {data: ""}
            ],
            order: [[7, 'desc']]
        });
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });

        $('#bCreateNew').click(function () {
            $('#mNewShip').modal('show');
        });
        $('#example tbody').on('click', 'button', function () {
            data = table.row($(this).parents('tr')).data();
            document.getElementById('printHelp').value = data.document_reference;
            $('#sumReport').click(function () {
                window.open(report + "shipsummary.php?shippingreference=" + document.getElementById('printHelp').value);
                table.ajax.reload(null, false);
            });
            $('#detailReport').click(function () {
                window.open(report + "shipdetail.php?shippingreference=" + document.getElementById('printHelp').value);
                table.ajax.reload(null, false);
            });
            $('#excelReport').click(function () {
                window.open(report + "shipdetailxl.php?shippingreference=" + document.getElementById('printHelp').value);
                table.ajax.reload(null, false);
            });
            $('#manReport').click(function () {
                window.open(report + "manifestsummary.php?shippingreference=" + document.getElementById('printHelp').value);
                table.ajax.reload(null, false);
            });
        });
             $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });

        $('#newButton').click(function () {
            var trailerLocation = document.getElementById('nTraLoc').value
            
            if(trailerLocation.length <= 0 ) {
                alert('Trailer can not be blank');
                return;
            }

            $('#mNewShip').modal('hide');

            var obj = 'trailerLocation=' + trailerLocation + '|AND|userId=' + currentUser + '|AND|transactionTypeId=SHIP';

            var filter = obj;
              


            $.ajax({
                
                url: callPostService + filter +"&function=dispatch"+dispatch,
                type: 'POST',
                success: function (response,textstatus) {  //
                   
                    
                    
                    if (response.substring(0, 5) == 'FAIL-') {
                        alert('ERROR\n\ Unable to Create')
                        console.log(response)
                    }else if( response.substring(0,8) == 'OK  -Tag'){
                         alert('ERROR\n\ Unable to Create\n\ '+response.substring(5,36)+'\n\ ' + response.substr(response.length - 10));
                        console.log(response)
                    }else if( response.substring(0,6) == 'OK  -I'){
                         alert('ERROR\n\ Unable to Create\n\ '+response.substring(5,20)+'\n\ ' + response.substr(response.length - 10));
                        console.log(response)
                    } else {
                       $('#confCreate').modal('show');
                    }
                    
                }
            });
        });
        $('#example tbody').on('click', '#bEdit', function () {
            $('#mEdit').modal('show');
            var data = table.row($(this).parents('tr')).data();
            document.getElementById('eId').innerHTML = data.id;
            document.getElementById('eMan').innerHTML = data.document_reference;
            document.getElementById('eTrail').innerHTML = data.trailer_reference;
            document.getElementById('eTimeSlot').value = data.time_slot;
        });
        $('#mEdit').on('hidden.bs.modal', function (e) {
            $(this)
                    .find("input,textarea,select")
                    .val('')
                    .end()
                    .find("input[type=checkbox], input[type=radio]")
                    .prop("checked", "")
                    .end();
        });
        $("#bSaveEdit").on("click", function () {


            var manCode = document.getElementById('eMan').innerHTML;
            var timeSlot = document.getElementById('eTimeSlot').value;


            $('#mEdit').modal('hide');

            var obj = {
                "documentReference": manCode,
                "expectedDeliverTime": timeSlot,
                "updatedBy": currentUser
            };

            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            console.log(filter)


            $.ajax({
                
                url: callPostService + filter +"&function=editManifest"+ screen,
                type: 'GET',
                success: function (response, textstatus) {
                    $('#confEdit').modal('show');
                    console.log(response)
                }
            });
        });

        //this is the bit of logic that work the select a row 
        $('#example tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = table.row(tr).data();


            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                if (table.row('.shown').length) {
                    $('.details-control', table.row('.shown').node()).click();
                }

                var rowID = data.id;

                row.child(format(row.data())).show();
                tr.addClass('shown');
                bodyTable(rowID);

            }
        });


        function bodyTable(rowID) {

            var bodyTable = $('#shipBody').DataTable({
                ajax: {"url": "../tableData/shippingBodyData.php", "data": {rowID: rowID}, "dataSrc": ""},
                searching: false,
                select: {
                    style: 'os',
                    selector: 'td:not(:first-child)'

                },
                paging: false,
                info: false,
                columns: [
                    {
                        "className": 'table-controls',
                        "orderable": false,
                        "data": null,
                        "defaultContent": ''
                    },
                    {data: "document_reference"},
                    {data: "ran_order"},
                    {data: "date_created"},
                    {data: "created_by"},
                    {data: "last_updated_by"},
                    {data: "last_updated_date"}


                ],
                order: [[4, 'asc']]
            });

            $('#shipBody tbody').on('click', 'td.table-controls', function () {
                var tr = $(this).closest('tr');
                var row = bodyTable.row(tr);
                var data = bodyTable.row(tr).data();


                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    // Open this row
                    if (bodyTable.row('.shown').length) {
                        $('.table-controls', bodyTable.row('.shown').node()).click();
                    }

                    var rowID = data.id;

                    row.child(detailFormat(row.data())).show();
                    tr.addClass('shown');
                    detailTable(rowID);
                }
            });

        }

        function detailTable(rowID) {
            //alert(rowID)

            var detailTable = $('#shipDetail').DataTable({
                ajax: {"url": "../tableData/shippingDetailData.php", "data": {rowID: rowID}, "dataSrc": ""},
                searching: false,
                select: {
                    style: 'os',
                    selector: 'td:not(:first-child)'

                },
                paging: false,
                info: false,

                columns: [
                    {data: "document_reference"},
                    {data: "serial_reference"},
                    {data: "part_number"},
                    {data: "qtys"},
                    {data: "date_created"},
                    {data: "created_by"},
                    {data: "last_updated_by"},
                    {data: "last_updated_date"}


                ],
                order: [[5, 'asc']]
            });
        }

    });


</script>
</body>
</html>



