<?php
// Modified - 2019-12-19 - Added relative address for PHPValidate/validaterdt.php
header('Cache-Control: no-cache, no-store, must-revalidate');   
header('Expires: 0');
// Header to allow Cross-Server AJAX Connection
header('Access-Control-Allow-Origin: *'); 
header('Expires: 0');

include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Location Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>

        <div class="modal fade right" id="mCreateNew" role="dialog" aria-labelledby="viewEdit" aria-hidden="true" >
            <div class="modal-dialog modal-fluid" role="document" style="width: 1000px">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalPreviewLabel">New Location</h5>
                    </div>
                    <div class="modal-body">
                        <div class=" col-md-12">
                            <div id="nPage1">
                                <div class="row" align="center">
                                    <!-- Half of the modal-body div-->
                                    <div class="col-lg-3 col-lg-offset-3">
                                        <strong>From:</strong>
                                        <div class="control-group">

                                            <select class="form-control" type="text" id="lAisle">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
// Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.location_aisle;');
                                                echo "<option value='0'>Aisle</option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['aisle_code'] . '">' . $row['aisle_code'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                        <br>
                                        <div class="control-group">

                                            <select class="form-control" type="text" id="lRack">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
// Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.location_rack;');
                                                echo "<option value='0'>Rack</option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['rack_code'] . '">' . $row['rack_code'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                        <br>
                                        <div class="control-group">

                                            <select class="form-control" type="text" id="lColumn">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
// Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.location_column;');
                                                echo "<option value='0'>Column</option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['column_code'] . '">' . $row['column_code'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                        <br>
                                        <div class="control-group">

                                            <select class="form-control" type="text" id="lRow">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
// Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.location_row;');
                                                echo "<option value='0'>Row</option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['row_code'] . '">' . $row['row_code'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Other half of the modal-body div-->
                                    <div class="col-xs-3">
                                        <strong>To:</strong>
                                        <div class="control-group">

                                            <select class="form-control" type="text" id="rAisle">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
// Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.location_aisle;');
                                                echo "<option value='0'>Aisle</option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['aisle_code'] . '">' . $row['aisle_code'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                        <br>
                                        <div class="control-group">

                                            <select class="form-control" type="text" id="rRack">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
// Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.location_rack;');
                                                echo "<option value='0'>Rack</option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['rack_code'] . '">' . $row['rack_code'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                        <br>
                                        <div class="control-group">

                                            <select class="form-control" type="text" id="rColumn">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
// Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.location_column;');
                                                echo "<option value='0'>Column</option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['column_code'] . '">' . $row['column_code'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                        <br>
                                        <div class="control-group">

                                            <select class="form-control" type="text" id="rRow">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
// Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.location_row;');
                                                echo "<option value='0'>Row</option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['row_code'] . '">' . $row['row_code'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>     
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-6">
                                    <label class="input-group-text">Location Type:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="nLocationType">                                                                  
                                            <?php
                                            include ('../config/phpConfig.php');
// Check connection
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.location_type;');
                                            echo "<option value='0' selected></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['id'] . '">' . $row['location_type_code'] . ' - ' . $row['location_type_description'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-xs-6">
                                    <label class="input-group-text">Zone Type:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="nZoneType">                                                                  
                                            <?php
                                            include ('../config/phpConfig.php');
// Check connection
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.location_sub_type');
                                            echo "<option value='0' selected></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['id'] . '">' . $row['location_sub_type_code'] . ' -  ' . $row['location_sub_type_description'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <label class="input-group-text">Product Type:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="nProductType">                                                                  
                                            <?php
                                            include ('../config/phpConfig.php');
// Check connection
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.product_type');
                                            echo "<option value='0' selected></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['id'] . '">' . $row['product_type_code'] . ' - ' . $row['product_type_description'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-xs-6">
                                    <label class="input-group-text">Inventory Status:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="nInventoryStatus">                                                                  
                                            <?php
                                            include ('../config/phpConfig.php');
// Check connection
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.inventory_status');
                                            echo "<option value='0' selected></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['id'] . '">' . $row['inventory_status_code'] . ' - ' . $row['inventory_status_description'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>
                                    

                                </div>
                                 <div class="col-xs-6">
                                    <label class="input-group-text">Pick Group:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="nPickGroup">                                                                  
                                            <?php
                                            include ('../config/phpConfig.php');
// Check connection
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.pick_group order by pick_group_code desc;');
                                            echo "<option value='0' selected></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['id'] . '">' . $row['pick_group_code'] . ' - ' . $row['pick_group_description'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-xs-6">
                                    <label class="input-group-text">Part Number:</label>
                                    <div class="controls">
                                        <input type="text" name="partNumber" id="nPartNumber" class="form-control" >
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <label class="input-group-text">Max Qty:</label>
                                    <div class="controls">
                                        <input type="number" step="any" name="eMQty" id="nMQty" class="form-control" >
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <label class="input-group-text">Max weight:</label>
                                    <div class="controls">
                                        <input type="number" step="any" name="nWeight" id="nWeight" class="form-control" >
                                    </div>
                                </div>
                            </div>

                            <hr>
                            <div class="row" style="text-align: center ">
                                <!-- Half of the modal-body div-->
                                <div class="col-xs-12">
                                    <div class="col-xs-4">
                                        <div class="controls">
                                            <label class="input-group-text">Multi Part Location?:</label>  <input type="checkbox" name="nMultiPart" id="nMultiPart" class="input">
                                        </div>
                                    </div>

                                    <div class="col-xs-4">

                                        <div class="controls">
                                            <label class="input-group-text">Location Closed?:</label>   <input type="checkbox" name="nLocationClosed" id="nLocationClosed" class="input">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                <button type="button" id="bSaveNew" class="btn btn-success">Create</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confNew" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">New Location</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >New Location added</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton2" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade right" id="mEditLocation" role="dialog" aria-labelledby="viewEdit" aria-hidden="true" >
            <div class="modal-dialog modal-fluid" role="document" style="width: 1000px">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalPreviewLabel">New Location</h5>
                    </div>
                    <div class="modal-body">
                        <div class=" col-md-12">
                            <div id="nPage1">
                                <div class="row" align="center">
                                    <!-- Half of the modal-body div-->
                                    <p style="display: none;"><span id="eId"></span></p>
                                    <h2>Location code: <strong id="eLocationCode">#</strong></h2>
                                    <h2>Hash code: <strong id="eHashCode">#</strong></h2>
                                    <!-- Other half of the modal-body div-->
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-6">
                                    <label class="input-group-text">Location Type:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="eLocationType">                                                                  
                                            <?php
                                            include ('../config/phpConfig.php');
// Check connection
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.location_type;');
                                            echo "<option value='0' selected></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['id'] . '">' . $row['location_type_code'] . ' - ' . $row['location_type_description'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-xs-6">
                                    <label class="input-group-text">Zone Type:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="eZoneType">                                                                  
                                            <?php
                                            include ('../config/phpConfig.php');
// Check connection
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.location_sub_type order by location_sub_type_code asc');
                                            echo "<option value='0' selected></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['id'] . '">' . $row['location_sub_type_code'] . ' -  ' . $row['location_sub_type_description'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <label class="input-group-text">Product Type:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="eProductType">                                                                  
                                            <?php
                                            include ('../config/phpConfig.php');
// Check connection
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.product_type');
                                            echo "<option value='0' selected></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['id'] . '">' . $row['product_type_code'] . ' - ' . $row['product_type_description'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-xs-6">
                                    <label class="input-group-text">Inventory Status:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="eInventoryStatus">                                                                  
                                            <?php
                                            include ('../config/phpConfig.php');
// Check connection
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.inventory_status');
                                            echo "<option value='0' selected></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['id'] . '">' . $row['inventory_status_code'] . ' - ' . $row['inventory_status_description'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <label class="input-group-text">Filling Status:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="eFilling">                                                                  
                                            <?php
                                            include ('../config/phpConfig.php');
// Check connection
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.location_filling_status');
                                            echo "<option value='0' selected></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['id'] . '">' . $row['filling_code'] . ' - ' . $row['filling_description'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>

                                </div>
                                   <div class="col-xs-6">
                                    <label class="input-group-text">Pick Group:</label>
                                    <div class="controls">
                                        <select class="form-control" type="text" id="ePickGroup">                                                                  
                                            <?php
                                            include ('../config/phpConfig.php');
// Check connection
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.pick_group order by pick_group_code desc;');
                                            echo "<option value='0' selected></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['id'] . '">' . $row['pick_group_code'] . ' - ' . $row['pick_group_description'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-xs-6">
                                    <label class="input-group-text">Part Number:</label>
                                    <div class="controls">
                                        <input type="text" name="partNumber" id="ePartNumber" class="form-control" >
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <label class="input-group-text">Max Qty:</label>
                                    <div class="controls">
                                        <input type="number" step="any" name="eMQty" id="eMQty" class="form-control" >
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <label class="input-group-text">Max weight:</label>
                                    <div class="controls">
                                        <input type="number" step="any" name="eWeight" id="eWeight" class="form-control" >
                                    </div>
                                </div>

                            </div>


                            <hr>
                            <div class="row" >
                                <!-- Half of the modal-body div-->
                                <div class="col-xs-12" align="center">
                                    <div class="col-xs-4">
                                        <div class="controls">
                                            <label class="input-group-text">Multi Part Location?:</label>  <input type="checkbox" name="nMultiPart" id="eMultiPart" class="input">
                                        </div>
                                    </div>
                                    <!--                                    <div class="col-xs-4">
                                                                            <div class="controls">
                                                                                <label class="input-group-text">Replen When Empty:</label>   <input type="checkbox" name="nReplenEmpty" id="eReplenEmpty" class="input">
                                                                            </div>
                                                                        </div>-->
                                    <!--                                    <div class="col-xs-4">
                                    
                                                                            <div class="controls">
                                                                                <label class="input-group-text">Location Closed?:</label>   <input type="checkbox" name="nLocationClosed" id="eLocationClosed" class="input">
                                                                            </div>
                                    </div>-->
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                <button type="button" id="bEdit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Location</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong > Location Edited</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton2" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <?php
        include('../common/topNav.php');
        include('../common/sideBar.php');
        ?>
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th>Location Code</th>
                        <th>Hash Code</th>
                        <th>Location Type</th>
                        <th>Zone Type</th>
                        <th>Multi Part</th>
                        <th>Filling Status</th>
                        <th>Inventory Status</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th></th>
                </thead>
                <tfoot>
                    <tr>
                        <th>Location Code</th>
                        <th>Hash Code</th>
                        <th>Location Type</th>
                        <th>Zone Type</th>
                        <th>Multi Part</th>
                        <th>Filling Status</th>
                        <th>Inventory Status</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th></th>
                </tfoot>
            </table>

            <input type="Button" class="btn btn-warning" id="bCreateNew" value="Create New"/>
            <!--<input type="Button" id="exportExcel" class="btn btn-warning" onclick="locationReport()" value="Export To Excel"/>-->

        </div>

        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->

<script>
    
                function locationReport() {
                    $.ajax({
                        url: '../action/reports.php',
                        type: 'GET',
                        data: {
                            type: 'locationReport',
                            table: 'location',
                            filename: 'all_locations',
                            dFrom: '',
                            dTo: '',
                            shortCode: ''
                        },
                        complete: function () {
                            window.location = this.url;
                        }
                    });
                
            }


    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }

    $(document).ready(function () {
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>';

        var table = $('#example').DataTable({
            "processing": true,
            "serverSide": true,
            
            "ajax": {"url": "../tableData/locationTable.php"},
            
            "columnDefs": [{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bEdit' class='btn btn-warning' value='Edit'/>"
                },
                {"targets": [4, 5, 6, 7, 8], "searchable": false},
                {
                    render: function (data, type, row) {
                        return (data === "1") ? '<span data-order="1" style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color:red" data-order="0" class="glyphicon glyphicon-remove"></span>';
                    },
                    "targets": 4
                }
            ]

        });
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });

     $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });
         $('#example tfoot th').each(function (i) {
            if(i=== 0 ||i=== 1 || i=== 2 || i===3 ){
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Search ' + title + '" />');
            }
        });
        table.columns().every(function () {
            var that = this;
            $('input', this.footer()).on('keyup change clear', function () {
                if (that.search() !== this.value) {
                    that
                            .search(this.value)
                            .draw();
                }
            });
        });




        $("#bCreateNew").on("click", function () {
            $('#mCreateNew').modal('show');
        });

        $("#bSaveNew").on("click", function () {


            if (document.getElementById('lAisle').value == '0') {
                alert('Please Select From Aisle');
            } else if (document.getElementById('lRack').value == '0') {
                alert('Please Select From Rack');
            } else if (document.getElementById('lColumn').value == '0') {
                alert('Please Select From Coulmn');
            } else if (document.getElementById('lRow').value == '0') {
                alert('Please Select From Row');
            } else if (document.getElementById('rAisle').value == '0') {
                alert('Please Select To Aisle');
            } else if (document.getElementById('rRack').value == '0') {
                alert('Please Select To Rack');
            } else if (document.getElementById('rColumn').value == '0') {
                alert('Please Select To Column');
            } else if (document.getElementById('rRow').value == '0') {
                alert('Please Select To Row');
			} else if (document.getElementById('nLocationType').value == '0') {
                alert('Please Select Location Type');
			} else if (document.getElementById('nZoneType').value == '0') {
                alert('Please Select Zone Type');
			} else if (document.getElementById('nInventoryStatus').value == '0') {
                alert('Please Select Inventory Status');
            } else {
                $('#mCreateNew').modal('hide');
                var fromAisle = document.getElementById('lAisle').value;
                var fromRack = document.getElementById('lRack').value;
                var fromColumn = document.getElementById('lColumn').value;
                var fromRow = document.getElementById('lRow').value;
                var toAisle = document.getElementById('rAisle').value;
                var toRack = document.getElementById('rRack').value;
                var toColumn = document.getElementById('rColumn').value;
                var toRow = document.getElementById('rRow').value;
                var newLocationType = document.getElementById('nLocationType').value;
                var newZoneType = document.getElementById('nZoneType').value;
                var newPickGroup = document.getElementById('nPickGroup').value;
                var newPartNumber = document.getElementById('nPartNumber').value;
                var newMaxQty = document.getElementById('nMQty').value;
                var newMaxWeight = document.getElementById('nWeight').value;
                var newProductType = document.getElementById('nProductType').value;
                var newInventoryStatus = document.getElementById('nInventoryStatus').value;
                var newMultiPart;
                if ($('#nMultiPart').is(':checked')) {
                    newMultiPart = 1;
                } else {
                    newMultiPart = 0;
                }
                var locationFillingStatus;
                if ($('#nLocationClosed').is(':checked')) {
                    locationFillingStatus = 1;
                } else {
                    locationFillingStatus = 0;
                }

                var obj = {
                    'startAisle': fromAisle,
                    'endAisle': toAisle,
                    'startRack': fromRack,
                    'endRack': toRack,
                    'startColumn': fromColumn,
                    'endColumn': toColumn,
                    'startRow': fromRow.trim(),
                    'endRow': toRow.trim(),
                    'locationTypeId': newLocationType,
                    'zoneTypeId': newZoneType,
                    'productTypeId': newProductType,
                    'inventoryStatusId': newInventoryStatus,
                    'pickGroupId': newPickGroup,
                    'partNumber': newPartNumber,
                    'maxQty': newMaxQty,
                    'multiPart': newMultiPart,
                    'locationFillingStatus': locationFillingStatus,
                    'maxWeight': newMaxWeight
                };

                var newEditjson = JSON.stringify(obj);
                var filter = newEditjson;
		
                console.log(filter);

                $.ajax({
                    url: callPostService + filter + "&function=createLocation" + screen,
                    data: filter,
                    type: 'POST',
                    success: function (response, textstatus) {

                        if (response.startsWith("OK -")) {
                            $('#confNew').modal('show');
                        } else {
                            alert(response);
                        }

                    }
                });

            }
        });

        $('#example tbody').on('click', '#bEdit', function () {
            $('#mEditLocation').modal('show');
            var data = table.row($(this).parents('tr')).data();

            var locationCode = data['location_code'];

            $.ajax({
                url: '../tableData/locationHelp.php',
                type: 'GET',
                data: {'locationCode': locationCode},
                success: function (result) {
                    var data = JSON.parse(result);
                    console.log(data)
                    document.getElementById('eId').innerHTML = data[0].id;
                    document.getElementById('eLocationCode').innerHTML = data[0].location_code;
                    document.getElementById('eHashCode').innerHTML = data[0].location_hash;
                    document.getElementById('eLocationType').value = data[0].location_type_id;
                    document.getElementById('eZoneType').value = data[0].location_sub_type_id;
                    document.getElementById('eProductType').value = data[0].inventory_disposition_id;
                    document.getElementById('eInventoryStatus').value = data[0].inventory_status_id;
                    document.getElementById('eFilling').value = data[0].filling_status_id;
                    document.getElementById('ePickGroup').value = data[0].pick_group_id;
                    document.getElementById('eMQty').value = data[0].max_pick_face_qty;
                    document.getElementById('ePartNumber').value = data[0].part_number;
                    document.getElementById('eWeight').value = data[0].max_weight;
                    if (data[0].multi_part_location == '1') {
                        $('#eMultiPart').prop('checked', true)
                    } else {
                        $('#eMultiPart').prop('checked', false)
                    }
                }
            });
        });
        $("#bEdit").on("click", function () {

            var locationId = document.getElementById('eId').innerHTML;
            var newLocationType = document.getElementById('eLocationType').value;
            var newMaxWeight = document.getElementById('eWeight').value;
            var newZoneType = document.getElementById('eZoneType').value;
            var newProductType = document.getElementById('eProductType').value;
            var newInventoryStatus = document.getElementById('eInventoryStatus').value;
            var newPartNumber = document.getElementById('ePartNumber').value;
            var newMaxQty = document.getElementById('eMQty').value;
            var newPickGroup = document.getElementById('ePickGroup').value;
            var fillingStatus = document.getElementById('eFilling').value
            var newMultiPart;
            if ($('#eMultiPart').is(':checked')) {
                newMultiPart = true;
            } else {
                newMultiPart = false;
            }
            var newReplenWhenEmpty;
            if ($('#eReplenEmpty').is(':checked')) {
                newReplenWhenEmpty = true;
            } else {
                newReplenWhenEmpty = false;
            }

            $('#mEditLocation').modal('hide');

            var obj = {
                "id": locationId,
                "locationTypeId": newLocationType,
                "zoneTypeId": newZoneType,
                "productTypeId": newProductType,
                "inventoryStatusId": newInventoryStatus,
                "multiPart": newMultiPart,
                "locationFillingStatusId": fillingStatus,
                "partNumber": newPartNumber,
                "maxQty": newMaxQty,
                "pickGroupId": newPickGroup,
                "maxWeight": newMaxWeight,
                "updatedBy": currentUser
            };

            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
	    
            console.log(filter);
            $.ajax({
                url: callPostService + filter+ "&function=editLocation"  + screen,
                data: filter,
                type: 'POST',
                success: function (response, textstatus) {

                    if (response === 'OK - true') {
                        $('#confEdit').modal('show');
                    } else {
                        alert(response);
                    }

                }
            });
        });
    });
</script>
</body>
</html>
