    <?php

    include '../config/logCheck.php';

    ?>
<html>
    <head>
        <title>Shortage Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>
       <?php
      include('../common/topNav.php');
        include('../common/sideBar.php');
        
       ?>
        <!-- Page Content  -->
        <div id="content">
            <br>

                    <table id="example" class="compact stripe hover row-border" >
                        <thead>
                            <tr>
                                <th>Pick Reference</th>
                                <th>Order Reference</th>
                                <th>Part Number</th>
                                <th>Qty Expected</th>
                                <th>Qty Allocated</th>
                                <th>RCV Stock</th>
                                <th>QA Stock </th>
                                <th>QR Stock</th>
                                <th>Missing</th>
                        </thead>
                        <tfoot>
                            <tr>
                               <th>Pick Reference</th>
                                <th>Order Reference</th>
                                <th>Part Number</th>
                                <th>Qty Expected</th>
                                <th>Qty Allocated</th>
                                <th>RCV Stock</th>
                                <th>QA Stock </th>
                                <th>QR Stock</th>
                                <th>Missing</th>
                        </tfoot>
                    </table>
                    <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

                </div>

                <!--/span-->
            </div>
            <!--/row-->
        </div>
        <!--/span-->

        <script>
               function logOut() {
        
            var userID = <?php $_SESSION['userData']['username']?>
            $.ajax({
                url: '../action/userlogout.php',
                type: 'GET',
                data: {userID: userID },
                success: function (response, textstatus) {
                    alert("You have been logged out");
                    window.open('login.php','_self');
                }
            });
        }

            $(document).ready(function () {
                       var currentUser = '<?php print_r($_SESSION['userData']['username'])?>'
                                
                        var table = $('#example').DataTable({
                           ajax:{"url":"../tableData/shortageTable.php","dataSrc":""},
                           iDisplayLength: 25,
                            buttons: [
                                {extend: 'excel', filename: 'Shortage', title: 'Shortage'}
                            ],
                            columns: [
                                {data: "pick_reference"},
                                        {data: "order_reference"},
                                {data: "part_number"},
                                {data: "qty_expected"},
                                {data: "qty_allocated"},
                                {data: "rcv_qty"},
                                {data: "qa_qty"},
                                {data: "qr_qty"},
                                {data: "missing"}
                            ],
                            order: [[0, 'desc']]
                        });
                        $("#exportExcel").on("click", function () {
                            table.button('.buttons-excel').trigger();
                        });
                             $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });

            });


        </script>
    </body>
</html>



