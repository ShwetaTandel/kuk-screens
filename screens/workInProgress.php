<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>wip Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="modal fade bd-example-modal-sm" id="confDelete" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete Allocation: <strong><span id="delMessage2">*</span></strong></h5>
                    </div>
                    <br>
                    <div align="center">
                        <input type="text" name="eID" id="eID" class="form-control" style="display: none;">
                        <h4>Are you sure you want to delete <strong><span id="delMessage">*</span></strong></h4>
                        <h3 style="color: red;">Please Ensure this is not being picked</h3>

                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="conDel">Yes</button>
                        <button type="button" class="btn btn-danger"  data-dismiss="modal" >No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confDeleted" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Done</h5>
                    </div>
                    <br>
                    <div align="center">

                        <strong> <h3 style="color: red;">Work in Progress Deleted</h3></strong>

                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-info"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false); ">Ok</button>
                    </div>
                </div>
            </div>
        </div>

<?php
include('../common/topNav.php');
include('../common/sideBar.php');
?>
        <!-- Page Content  -->
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th>Order/ Group</th>
                        <th>Part</th>
                        <th>Allocation Qty</th>
                        <th>Serial Reference</th>
                        <th>From Location</th>
                        <th>To Location</th>
                        <th>Date Created</th>
                        <th>Last Updated</th>
                        <th>Action</th>
                </thead>
                <tfoot>
                    <tr>
                        <th>Order/ Group</th>
                        <th>Part</th>
                        <th>Allocation Qty</th>
                        <th>Serial Reference</th>
                        <th>From Location</th>
                        <th>To Location</th>
                        <th>Date Created</th>
                        <th>Last Updated</th>
                        <th>Action</th>
                </tfoot>
            </table>

            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

        </div>

        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->

<script>
    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }

    $(document).ready(function () {
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>'


        var table = $('#example').DataTable({
            ajax: {"url": "../tableData/workInProgressTable.php", "dataSrc": ""},
            columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bDelete' class='btn btn-danger' value='Delete'/>"
                }, {
                    targets: 2,
                    render: function (data, type, row) {
                        return  parseInt(data) / 1000;
                    }
                }
            ],
            buttons: [
                {extend: 'excel', filename: 'wip', title: 'WIP'}
            ],
            columns: [
                {data: "third_party_reference"},
                {data: "part_number"},
                {data: "allocated_qty"},
                {data: "serial_reference"},
                {data: "from_location_code"},
                {data: "to_location_code"},
                {data: "date_created"},
                {data: "last_updated"},
                {data: ""}
            ],
           
            order: [[0, 'asc']]
        });
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });

        
        $('#example tbody').on('click', '#bDelete', function () {
            var data = table.row($(this).parents('tr')).data();
            

            $('#confDelete').modal('show');
            document.getElementById('eID').value = data.id;
            document.getElementById('delMessage').innerHTML = data.third_party_reference;
            document.getElementById("delMessage2").innerHTML = data.third_party_reference

        });
             $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });

        $('#conDel').on('click', function () {
           
            
            var wipId = document.getElementById('eID').value;
            $('#confDelete').modal('hide');
            var filter = 'allocationDetailId=' + wipId + '|AND|userId=' + currentUser;
            console.log(filter);
            $.ajax({
                url: callGetService + filter +"&function=deleteAllocation"  + pick,
                type: 'GET',
                success: function (response, textstatus) {
                    if (response === 'true') {
                        $('#confDeleted').modal('show');

                    } else {
                        alert('Unable To Delete');
                    }


                }
            });
            
            
            

        });


    });

</script>
</body>
</html>



