<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>RTS Tasks</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>

        <div class="modal fade bd-example-modal-lg" id="newRTSModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New RTS</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" align="center">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-2">Part Number</label> 
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" id="newPartNumber" onchange="partHelp()" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Piece Qty</label> 
                                <div class="col-sm-10">
                                    <input class="form-control" type="number" id="newPieceQty" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">No of Boxes</label> 
                                <div class="col-sm-10">
                                    <input class="form-control" type="number" id="newNoOfBoxes" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Reason Code</label> 
                                <div class="col-sm-10">
                                    <select class="form-control" type="text" id="reasonCode">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
// Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.reason_code where reason_code_type like "RTS%";');
                                        echo "<option value=''></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['reason_code'] . '">' . $row['reason_code'] . "  -  " . $row['reason_code_description'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div align="center">

                                <label class="input-group-text">Notify KUK?:</label>
                                <div class="controls">
                                    <input type="checkbox" name="notifyKUK" id="notifyKUK" class="input">
                                </div>

                            </div>
                        </form>    
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="newRTSButton">Yes</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>       
        <?php
        include('../common/topNav.php');
        include('../common/sideBar.php');
        ?>
        <!-- Page Content  -->
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>Order Reference</th>
                        <th class="yes">Order Type</th>

                        <th>Part Number</th>
                        <th>Quantity</th>
                        <th>Delivery Date</th>
                        <th>BAAN Location</th>
                        <th>BAAN Serial</th>
                        <th>Machine Model</th>

                       <!-- <th class="yes">Reason code</th>-->

                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th>Options</th>
                </thead>
                <tfoot>
                    <tr>
                        <th></th>
                        <th>Order Reference</th>
                        <th>Order Type</th>

                        <th>Part Number</th>
                        <th>Quantity</th>

                        <th>Delivery Date</th>
                        <th>BAAN Location</th>
                        <th>BAAN Serial</th>
                        <th>Machine Model</th>

                        <!--<th>Reason code</th>-->

                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th>Mark Processed</th>
                </tfoot>
            </table>

            <input type="Button" id="bCreateNew" class="btn btn-warning" value="Unplanned RTS"/>
            <input type="Button" id="bNotReturned" class="btn btn-danger processButton" value="Not Returned" />
            <input type="Button" id="bProcessSelected" class="btn btn-primary processButton" value="Process" />
            <input type="Button" id="exportExcel" class="btn btn-info" value="Export To Excel"/>

        </div>

        <!--/span-->
    </body>
    <!--/span-->

    <script>
        function logOut() {

            var userID = <?php $_SESSION['userData']['username'] ?>
            $.ajax({
                url: '../action/userlogout.php',
                type: 'GET',
                data: {userID: userID},
                success: function (response, textstatus) {
                    alert("You have been logged out");
                    window.open('login.php', '_self');
                }
            });
        }
        function partHelp() {
            var partNumber = document.getElementById('newPartNumber').value;
            $.ajax({
                url: '../action/partHelp.php',
                type: 'GET',
                data: {partNumber: partNumber},
                success: function (response, textstatus) {
                    var myObj = JSON.parse(response);
                    var num = myObj[0].num;
                    if (num === '0') {
                        alert('Part Does Not Exist');
                        document.getElementById('newPartNumber').value = "";
                        return false;
                    } else {
                        return true;
                    }
                }
            });
        }
//the table set up for the Body
    function formatBody(d) {
        // `d` is the original data object for the row
        return '<table id="pBody" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                '<thead>' +
                '<th></th>' +
                '<th>Part Number</th>' +
                
                '<th>Recived Qty</th>' +
                '<th>Baan Location</th>' +
                '<th>Last Updated By</th>' +
                '<th>Last Updated Date</th>' +
                '</thead>' +
                '</table>';
    }
    //table set up for detail
    function formatDetail(d) {
        // `d` is the original data object for the row
        return '<table id="pDetail" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                '<thead>' +
                '<th>Part Id</th>' +
                '<th>Serial Reference</th>' +
                
                '<th>Received Location</th>' +
                '<th>Received Qty</th>' +
                '<th>Last Updated By</th>' +
                '<th>Last Updated Date</th>' +
                '<th></th>' +
                '</thead>' +
                '</table>';
    }
        $(document).ready(function () {
            var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>';
            var selected = [];
            //simple function (not sure is needed here)... 
            // this will on clse of any modal will clear the data from the modal
            $('.modal').on('hidden.bs.modal', function (e) {
                $(this).removeData();
            });
            var table = $('#example').DataTable({
                ajax: {"url": "../tableData/rtsTasks.php"}, //"dataSrc": ""
                iDisplayLength: 25,
                dom: 'Blfrtipb',
                "processing": true, // to show progress bar
                "serverSide": true,
                columnDefs: [{
                        targets: -1,
                        data: null,
                        defaultContent: "<input type='checkbox' id='bProcess' class='btn btn-danger' value=''/>"
                    }, {
                        targets: [1,2,3,4, 5,6,7,8,10,11],
                        orderable: false

                    }

                ],
                "createdRow": function (row, data, dataIndex) {
                if (data.order_type.indexOf('EMERG') !== -1) {
                    $(row).css('background-color', 'red');
                } 
                },
                initComplete: function () {
                    this.api().columns().every(function () {

                        var column = this;
                        if ($(column.header()).hasClass('yes')) {
                            var select = $('<select><option id="ssearch" value=""></option></select>')
                                    .appendTo($(column.header()))
                                    .on('change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                                $(this).val()
                                                );
                                        column
                                                .search(val ? val : '', true, false)
                                                .draw();
                                    });
                            column.data().unique().sort().each(function (d, j) {
                                select.append('<option value="' + d + '">' + d + '</option>');
                            });
                        }
                    });
                },
                buttons: [
                    {extend: 'excel', filename: 'RTS', title: 'RTS'}
                ],
                columns: [
                    {
                        "className": 'details-control',
                        "orderable": false,
                        "data": null,
                        "defaultContent": ''
                    },
                    {data: "order_reference"},
                    {data: "order_type"},
                    {data: "part_number"},
                    {data: "qty_received"},
                    {data: "date_time"},
                    {data: "baan_location"},
                    {data: "baan_serial"},
                    {data: "machine_model"},
                   // {data: "reason_code"},
                    {data: "last_updated"},
                    {data: "last_updated_by"},
                    {data: ""}
                ],
                order: [[9, 'desc']]
            });

            $("#exportExcel").on("click", function () {
                table.button('.buttons-excel').trigger();
            });

            $('#example tbody').on('click', '#bProcess', function () {
                var data = table.row($(this).parents('tr')).data();

                var id = data.id+ '-'+data.part_number;
                var index = $.inArray(id, selected);

                if (index === -1) {
                    selected.push(id);
                } else {
                    selected.splice(index, 1);
                }
            });
            $('#example_filter label input').on("focus", function (event) {
                //console.log('Focus')
                $('#example').DataTable().ajax.reload(null, false);

            });
            $('#bCreateNew').on('click', function () {
                document.getElementById("newPartNumber").value = '';
                document.getElementById("newPieceQty").value = '';
                document.getElementById("newNoOfBoxes").value = '';
                document.getElementById("reasonCode").value = '';
                document.getElementById("notifyKUK").checked = false;
                $('#newRTSModal').modal('show');

            });

            $('.processButton').on('click', function () {
                var notReturned = false;
                if($(this).attr('id') === 'bNotReturned'){
                    notReturned = true;
                }
                if (selected.length === 0) {
                    alert("Please select some rows for processing.")
                    return;
                } else {
                    if (confirm("Are you sure you want to mark the selected rows as processed?")) {
                        var filter = "headerIds=" + selected.join() + "|AND|updatedBy=" + currentUser+ "|AND|notReturned=" + notReturned;
                        console.log(filter)
                        $.ajax({
                            
                            url: callGetService + filter + "&function=processRTS"+recieving,
                            type: 'GET',
                            success: function (response, textstatus) {
                                if (response === 'true') {
                                    alert("The RTS tasks have been markeed as processed.");
                                    $('#example').DataTable().ajax.reload();
                                } else {

                                    alert("Something went wrong... Please try again.")
                                }
                            }
                        });

                    }
                }

            });
         
            $('#newRTSButton').on('click', function () {

                var partNumber = document.getElementById("newPartNumber").value;
                var pieceQty = document.getElementById("newPieceQty").value;
                var noOfBoxes = document.getElementById("newNoOfBoxes").value;
                var reasonCode = document.getElementById("reasonCode").value;
                var notifyKUK = document.getElementById("notifyKUK").value === 'on' ? true : false;
                if (partNumber === '' || pieceQty === '' || noOfBoxes === '' || reasonCode === '') {

                    alert("Please enter all the fields.");
                    return;
                }

                var rtsReq = {"partNumber": partNumber, "pieceQty": pieceQty, "noOfBoxes": noOfBoxes, "reasonCode": reasonCode, "notifyKUK": notifyKUK, "user": currentUser};
                var jsonReq = JSON.stringify(rtsReq);
                console.log(jsonReq);
                $.ajax({
                   
                    url: callPostService + jsonReq + "&function=addUnplannedRTS" + "&connection=" + recieving,
                    type: 'GET',
                    crossDomain: true,
                    contentType: "application/json; charset=utf-8",
                    success: function (response) {
                        if (response.trim() === "OK - true") {
                            alert("The new RTS has been added and processed successfully.");
                            $('#newRTSModal').modal('hide');
                            $('#example').DataTable().ajax.reload();
                        } else {
                            alert("Something went wrong. Please check with the administrator.");
                        }
                    }
                });
            });



            $('#example tbody').on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = table.row(tr);
                var data = table.row(tr).data();


                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    // Open this row
                    if (table.row('.shown').length) {
                        $('.details-control', table.row('.shown').node()).click();
                    }

                    var rowID = data.id;
                    row.child(formatBody(row.data())).show();
                    tr.addClass('shown');
                    rtsBodyTable(rowID);
                }
            });

            function rtsBodyTable(rowID) {
                var table = $('#pBody').DataTable({
                    ajax: {"url": "../tableData/rtsBodyTable.php", "data": {rowID: rowID}, "dataSrc": ""},
                    searching: false,
                    select: {
                        style: 'os',
                        selector: 'td:not(:first-child)'

                    },
                    paging: false,
                    info: false,
                    columns: [
                        {
                            "className": 'table-controls',
                            "orderable": false,
                            "data": null,
                            "defaultContent": ''
                        },
                        {data: "part_number"},
                        {data: "qty_received"},
                        {data: "baan_location"},
                        {data: "last_updated_by"},
                        {data: "last_updated"}


                    ],
                    order: [[1, 'asc']]
                });


                $('#pBody tbody').on('click', 'td.table-controls', function () {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);
                    var data = table.row(tr).data();


                    if (row.child.isShown()) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    }
                    else {
                        // Open this row
                        if (table.row('.shown').length) {
                            $('.table-controls', table.row('.shown').node()).click();
                        }

                        var rowID = data.id;
                        console.log(rowID)
                        row.child(formatDetail(row.data())).show();
                        tr.addClass('shown');
                        rtsDetailTable(rowID);
                    }
                });
            }
            function rtsDetailTable(rowID) {
                var table = $('#pDetail').DataTable({
                    ajax: {"url": "../tableData/rtsDetailTable.php", "data": {rowID: rowID}, "dataSrc": ""},
                    searching: false,
                    select: {
                        style: 'os',
                        selector: 'td:not(:first-child)'

                    },
                    paging: false,
                    info: false,
                    "createdRow": function (row, data, dataIndex) {
                        if (data.blocked != null && data.blocked == 1) {
                            $(row).css('background-color', '#CD5C5C');
                        }

                    },
                    columns: [
                        {data: "part_number"},
                        {data: "serial_reference"},
                        {data: "location_code"},
                        {data: "received_qty"},
                        {data: "last_updated_by"},
                        {data: "last_updated"}


                    ],
                    order: [[0, 'asc']]
                });
            }

        });
        
         

    </script>
</body>
</html>



