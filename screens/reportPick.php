<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Reports</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/fontawesome-free-5.9.0-web/css/all.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../js/libs/mask/jquery.mask.js" type="text/javascript"></script>
    </head>

    <?php
    include('../common/topNav.php');
    include('../common/sideBar.php');
    ?>
    <body>

        <div class="modal fade" id="mDates" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><span id="modalTitle"></span></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">From Date:</label>
                                    <div class="controls">
                                        <input type="date" name="fromDate" id="fromDate" class="form-control"  >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">To Date:</label>
                                    <div class="controls">
                                        <input type="date" name="toDate" id="toDate" class="form-control"  >
                                    </div>
                                </div>
                                <div id="hideCode">
                                    <div class="control-group">
                                        <label class="input-group-text">Short Code:</label>
                                        <select class="form-control" type="text" id="shortCode">                                                                  
                                            <?php
                                            include ('../config/phpConfig.php');
// Check connection
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT distinct(short_code) FROM ' . $mDbName . '.transaction_history');
                                            echo "<option value=''></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['short_code'] . '">' . $row['short_code'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" data-dismiss="modal" id="bCreate">Create</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="mDate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><span id="modalTitled"></span></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Date:</label>
                                    <div class="controls">
                                        <input type="text" name="reqDate" id="reqDate" class="form-control date_date" placeholder="YYYY-MM-DD" >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" data-dismiss="modal" id="bCreated">Create</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>








        <div id="content">
            <br>

            <div class="col-sm-3">
                <div class="panel panel-danger">
                    <div class="panel-heading"><i class="fas fa-calendar-alt fa-3x"></i> Table Reports</div>
                    <?php
                    $path = '<div class="panel-body"><a href="http://' . $mSystem . '/tableexcel/tables.html">View Report</a></div>';
                    echo($path);
                    ?>
                </div>
                <div class="panel panel-success ">
                    <div class="panel-heading"><i class="fas fa-file-excel fa-3x"></i>General Reports</div>
                    <?php
                    $path = '<div class="panel-body"><a href="http://' . $mSystem . '/tableexcel/reports.html">View Report</a></div>';
                    echo($path);
                    ?>
                </div>


                <!--  <div class="panel panel-info ">
                      <div class="panel-heading"><i class="far fa-calendar-alt fa-3x"></i>    End Of Month Report </div>
                      <div class="panel-body"><a onclick="endOfMonth()">Create Report</a></div>
                  </div>
  
                  <div class="panel panel-success">
                      <div class="panel-heading"><i class="far fa-clock fa-3x"></i>     Work in Progress Report</div>
                      <div class="panel-body"><a href="http://172.16.1.62/wip.html">View Report</a></div>
                  </div>      -->

                <!--  <div class="panel panel-warning">
                      <div class="panel-heading"><i class="fas fa-file-alt fa-3x"></i> Customer Receipts</div>
                      <div class="panel-body"><a onclick="custReceipts()">Create Report</a></div>
                  </div>
  
                  <div class="panel panel-danger">
                      <div class="panel-heading"><i class="fab fa-buromobelexperte fa-3x"></i> Location Upload</div>
                      <div class="panel-body"><a href="http://172.16.1.63/partupload/locationupload.html">View Upload</a></div>
                  </div>-->
            </div> 

            <!--   <div class="col-sm-3">
   
                   <div class="panel panel-success ">
                       <div class="panel-heading"><i class="fas fa-bullseye fa-3x"></i> Transaction History Report</div>
                       <div class="panel-body"><a onclick="transactionHistory()">Create Report</a></div>
                   </div>
   
                   <div class="panel panel-warning">
                       <div class="panel-heading"><i class="fas fa-grip-vertical fa-3x"></i> Inventory Report</div>
                       <div class="panel-body"><a href="http://172.16.1.62/inventory.html">View Report</a></div>
                   </div>      
   
                   <div class="panel panel-danger">
                       <div class="panel-heading"><i class="fas fa-receipt fa-3x"></i> Receipt Report</div>
                       <div class="panel-body"><a onclick="receiptReport()">View Report</a></div>
                   </div>
   
                   <div class="panel panel-info ">
                       <div class="panel-heading"><i class="fas fa-angle-double-up fa-3x"></i> Part Upload</div>
                       <div class="panel-body"><a href="http://172.16.1.63/partupload.html">View Report</a></div>
                   </div>
               </div> -->

            <!-- <div class="col-sm-3">

                <div class="panel panel-warning ">
                    <div class="panel-heading"><i class="fas fa-adjust fa-3x"></i> Serials Consumed Report</div>
                    <div class="panel-body"><a onclick="serialConsumed()">Create Report</a></div>
                </div>

                <div class="panel panel-danger">
                    <div class="panel-heading"><i class="fas fa-cube fa-3x"></i> F6 Report</div>
                    <div class="panel-body"><a onclick="sixReport()">Create Report</a></div>
                </div>      

                <div class="panel panel-danger">
                    <div class="panel-heading"><i class="far fa-file-excel fa-3x"></i> F2 Report</div>
                    <div class="panel-body"><a onclick="twoReport()">Create Report</a></div>
                </div>

                <div class="panel panel-info ">
                    <div class="panel-heading"><i class="fas fa-print fa-3x"></i> Location Label Print</div>
                    <div class="panel-body">Open Print</div>
                </div>
                <div class="panel panel-success ">
                    <div class="panel-heading"><i class="fas fa-print fa-3x"></i> Pallet Label Print</div>
                    <div class="panel-body"><a>Open Print</a></div>
                </div>
            </div>  -->

            <!--<div class="col-sm-3">
 
                 <div class="panel panel-danger ">
                     <div class="panel-heading"><i class="fas fa-cubes fa-3x"></i>Location Summary</div>
                     <div class="panel-body"><a href="http://172.16.1.63/locationSummary.html">View Summary</a></div>
                 </div>
 
                 <div class="panel panel-info">
                     <div class="panel-heading"><i class="fas fa-book fa-3x"></i> RRMC Orders</div>
                     <div class="panel-body"><a onclick="rrmcOrder()">Create Report</a></div>
                 </div>      
 
                 <div class="panel panel-success">
                     <div class="panel-heading"><i class="far fa-hand-point-up fa-3x"></i> Pick Summary</div>
                     <div class="panel-body"><a href="http://172.16.1.62/picksummary.html">View Summary</a></div>
                 </div>
 
 
             </div> -->


        </div>


        <script>
            function logOut() {

                var userID = <?php $_SESSION['userData']['username'] ?>
                $.ajax({
                    url: '../action/userlogout.php',
                    type: 'GET',
                    data: {userID: userID},
                    success: function (response, textstatus) {
                        alert("You have been logged out");
                        window.open('login.php', '_self');
                    }
                });
            }


            $('.date_time').mask('0000-00-00 00:00:00');
            $('.date_date').mask('0000-00-00');
            var url = '../action/reports.php';

            $('#mDates').on('hidden.bs.modal', function (e) {
                $(this)
                        .find("input,textarea,select")
                        .val('')
                        .end()
                        .find("input[type=checkbox], input[type=radio]")
                        .prop("checked", "")
                        .end();
            });
            $('#mDate').on('hidden.bs.modal', function (e) {
                $(this)
                        .find("input,textarea,select")
                        .val('')
                        .end()
                        .find("input[type=checkbox], input[type=radio]")
                        .prop("checked", "")
                        .end();
            });

            function endOfMonth() {
                $('#mDates').modal('show');
                document.getElementById('hideCode').style.display = 'none';
                document.getElementById('modalTitle').innerHTML = 'End Of Month Report';

                $('#bCreate').unbind().on('click', function () {
                    var dFrom = document.getElementById('fromDate').value;
                    var dTo = document.getElementById('toDate').value;
                    $.ajax({
                        url: url,
                        type: 'GET',
                        data: {
                            type: 'endOfMonth',
                            table: 'transaction_history',
                            filename: 'end_of_month',
                            dFrom: dFrom,
                            dTo: dTo,
                            shortCode: ''
                        },
                        complete: function () {
                            window.location = this.url;
                        }
                    });
                });
            }

            function custReceipts() {
                $('#mDate').modal('show');
                document.getElementById('modalTitled').innerHTML = 'Customer Receipts';

                $('#bCreated').unbind().on('click', function () {
                    var dFrom = document.getElementById('reqDate').value;

                    $.ajax({
                        url: url,
                        type: 'GET',
                        data: {
                            type: 'custReceipt',
                            table: 'document_header',
                            filename: 'customer_receipts',
                            dFrom: dFrom,
                            dTo: '',
                            shortCode: ''
                        },
                        complete: function () {
                            window.location = this.url;
                        }
                    });
                });
            }
            function transactionHistory() {
                $('#mDates').modal('show');
                document.getElementById('hideCode').style.display = 'block';
                document.getElementById('modalTitle').innerHTML = 'Transaction History Report';

                $('#bCreate').unbind().on('click', function () {
                    var dFrom = document.getElementById('fromDate').value;
                    var dTo = document.getElementById('toDate').value;
                    var shortCode = document.getElementById('shortCode').value;

                    $.ajax({
                        url: url,
                        type: 'GET',
                        data: {
                            type: 'transactionHistory',
                            table: 'transaction_history',
                            filename: 'transaction_history_' + shortCode,
                            dFrom: dFrom,
                            dTo: dTo,
                            shortCode: shortCode
                        },
                        complete: function () {
                            window.location = this.url;
                        }
                    });
                });
            }

            function receiptReport() {
                $('#mDate').modal('show');
                document.getElementById('modalTitle').innerHTML = 'Receipts Reports';
                $('#bCreated').unbind().on('click', function () {
                    var dFrom = document.getElementById('reqDate').value;
                    $.ajax({
                        url: url,
                        type: 'GET',
                        data: {
                            type: 'receiptReport',
                            table: 'document_header',
                            filename: 'receipts_report',
                            dFrom: dFrom,
                            dTo: '',
                            shortCode: ''
                        },
                        complete: function () {
                            window.location = this.url;
                        }
                    });
                });
            }

            function serialConsumed() {
                $('#mDates').modal('show');
                document.getElementById('hideCode').style.display = 'none';
                document.getElementById('modalTitle').innerHTML = 'Serial Consumed Report';

                $('#bCreate').unbind().on('click', function () {
                    var dFrom = document.getElementById('fromDate').value;
                    var dTo = document.getElementById('toDate').value;
                    $.ajax({
                        url: url,
                        type: 'GET',
                        data: {
                            type: 'serialConsumed',
                            table: 'transaction_history',
                            filename: 'serial_consumed',
                            dFrom: dFrom,
                            dTo: dTo,
                            shortCode: ''
                        },
                        complete: function () {
                            window.location = this.url;
                        }
                    });
                });
            }

            function sixReport() {
                $('#mDates').modal('show');
                document.getElementById('hideCode').style.display = 'none';
                document.getElementById('modalTitle').innerHTML = 'F6 Report';

                $('#bCreate').unbind().on('click', function () {
                    var dFrom = document.getElementById('fromDate').value;
                    var dTo = document.getElementById('toDate').value;

                    $.ajax({
                        url: url,
                        type: 'GET',
                        data: {
                            type: 'sixReport',
                            table: 'transaction_history',
                            filename: 'F6_Report',
                            dFrom: dFrom,
                            dTo: dTo,
                            shortCode: ''
                        },
                        complete: function () {
                            window.location = this.url;
                        }
                    });
                });
            }
            function twoReport() {
                $('#mDate').modal('show');
                document.getElementById('modalTitled').innerHTML = 'F2 Report';

                $('#bCreated').unbind().on('click', function () {
                    var dFrom = document.getElementById('reqDate').value;
                    $.ajax({
                        url: url,
                        type: 'GET',
                        data: {
                            type: 'twoReport',
                            table: 'allocation_detail',
                            filename: 'F2_Report',
                            dFrom: dFrom,
                            dTo: '',
                            shortCode: ''
                        },
                        complete: function () {
                            window.location = this.url;
                        }
                    });
                });
            }
            function rrmcOrder() {
                $('#mDate').modal('show');
                document.getElementById('modalTitle').innerHTML = 'RRMC Orders';

                $('#bCreated').unbind().on('click', function () {
                    var dFrom = document.getElementById('reqDate').value;
                    $.ajax({
                        url: url,
                        type: 'GET',
                        data: {
                            type: 'rrmcOrders',
                            table: 'document_header',
                            filename: 'rrmc_orders',
                            dFrom: dFrom,
                            dTo: '',
                            shortCode: ''
                        },
                        complete: function () {
                            window.location = this.url;
                        }
                    });
                });

            }




        </script>

    </body>
</html>