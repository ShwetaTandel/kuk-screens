<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Transaction History Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>


        <?php
        include('../common/topNav.php');
        include('../common/sideBar.php');
        ?>

        <div class="modal fade" id="mSearch" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Custom Search</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-md-2">Part Number:</label> 
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" id="sPart" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Serial Reference:</label> 
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" id="sSerial"  />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Short Code:</label> 
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" id="sShort"  />
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="button" id="bSearch" class="btn btn-success">Search</button>
                    </div>
                </div>
            </div>
        </div>






        <!-- Page Content  -->
        <div id="content">
            <br>

            <table id="example" class="compact stripe" style="width:100%">
                <thead>
                    <tr>
                        <th>Ran / Order</th>
                        <th>Part Number</th>
                        <th>Serial Reference</th>
                        <th>Short Code</th>
                        <th>Scanned QTY</th>
                        <th>From Location Code</th>
                        <th>To Location Code</th>
                        <th>Document Reference</th>
                        <th>Date Created</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                </thead>
                <tfoot>
                    <tr>
                        <th>Ran / Order</th>
                        <th>Part Number</th>
                        <th>Serial Reference</th>
                        <th>Short Code</th>
                        <th>Scanned QTY</th>
                        <th>From Location Code</th>
                        <th>To Location Code</th>
                        <th>Document Reference</th>
                        <th>Date Created</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>

                </tfoot>
            </table>
            <input align="left" type="Button" class="btn btn-warning" id='customSearch' value="Custom Search"/>
            <input align="left" type="Button" class="btn btn-info" id='customSearch' onclick="location.reload()" value="View ALL"/>

<!--            <input align="right" type="Button" id="next" class="btn btn-warning" value="NEXT"/>
            <input align="right" type="Button" id="previous" class="btn btn-warning" value="previous"/>-->

        </div>

        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->

<script>
    var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>'
    var tabStart = 0;
    var limit = tabStart;
    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }

    $(document).ready(function () {


        var table = $('#example').DataTable({
            ajax: {"url": "../tableData/testSpeed.php", data: {limit: limit}, "dataSrc": ""},
            searching:false,
            columnDefs: [
            ],
            buttons: [
                {extend: 'excel', filename: 'Transaction_history', title: 'Transaction_history'}
            ],
            columns: [
                {data: "ran_or_order"},
                {data: "part_number"},
                {data: "serial_reference"},
                {data: "short_code"},
                {data: "txn_qty"},
                {data: "from_location_code"},
                {data: "to_location_code"},
                {data: "associated_document_reference"},
                {data: "date_created"},
                {data: "last_updated"},
                {data: "last_updated_by"}
            ],
            order: [[0, 'asc']]
        });
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });

        $("#customSearch").unbind().on("click", function () {
            $('#mSearch').modal('show');


            $('#sPart').keyup(function () {

                if ($('#sPart').val().length > 0) {
                    $("#sSerial").prop("disabled", true);
                    $("#sShort").prop("disabled", true);
                }
                if ($('#sPart').val().length === 0) {
                    $("#sShort").prop("disabled", false);
                    $("#sSerial").prop("disabled", false);
                }
            });
            $('#sSerial').keyup(function () {

                if ($('#sSerial').val().length > 0) {
                    $("#sPart").prop("disabled", true);
                     $("#sShort").prop("disabled", true);
                }
                if ($('#sSerial').val().length === 0) {
                     $("#sShort").prop("disabled", false);
                    $("#sPart").prop("disabled", false);
                }
            });
            $('#sshort').keyup(function () {

                if ($('#sShort').val().length > 0) {
                    $("#sSerial").prop("disabled", true);
                    $("#sPart").prop("disabled", true);
                }
                if ($('#sShort').val().length === 0) {

                    $("#sSerial").prop("disabled", false);
                     $("#sPart").prop("disabled", false);
                }
            });


        });

        $('#bSearch').unbind().on("click", function () {
            $('#mSearch').modal('hide');
            if ($('#sPart').val().length > 0) {
                var partNum = $('#sPart').val();
                $('#example').DataTable().destroy();
                $('#example').DataTable({
                    ajax: {"url": "../tableData/transactionHistoryPart.php", data: {partNum: partNum}, "dataSrc": ""},
                    columnDefs: [
                    ],
                    buttons: [
                        {extend: 'excel', filename: 'Transaction_history', title: 'Transaction_history'}
                    ],
                    columns: [
                        {data: "ran_or_order"},
                        {data: "part_number"},
                        {data: "serial_reference"},
                        {data: "short_code"},
                        {data: "txn_qty"},
                        {data: "from_location_code"},
                        {data: "to_location_code"},
                        {data: "associated_document_reference"},
                        {data: "date_created"},
                        {data: "last_updated"},
                        {data: "last_updated_by"}
                    ],
                    order: [[0, 'asc']]
                });
            }
            



            if ($('#sSerial').val().length > 0) {
                var serialNum = $('#sSerial').val();
                $('#example').DataTable().destroy();
                $('#example').DataTable({
                    ajax: {"url": "../tableData/transactionHistorySerial.php", data: {serialNum: serialNum}, "dataSrc": ""},
                    columnDefs: [
                    ],
                    buttons: [
                        {extend: 'excel', filename: 'Transaction_history', title: 'Transaction_history'}
                    ],
                    columns: [
                        {data: "ran_or_order"},
                        {data: "part_number"},
                        {data: "serial_reference"},
                        {data: "short_code"},
                        {data: "txn_qty"},
                        {data: "from_location_code"},
                        {data: "to_location_code"},
                        {data: "associated_document_reference"},
                        {data: "date_created"},
                        {data: "last_updated"},
                        {data: "last_updated_by"}
                    ],
                    order: [[0, 'asc']]
                });
            }
                        if ($('#sShort').val().length > 0) {
                var sCode = $('#sShort').val();
                $('#example').DataTable().destroy();
                $('#example').DataTable({
                    ajax: {"url": "../tableData/transactionHistoryShort.php", data: {sCode: sCode}, "dataSrc": ""},
                    columnDefs: [
                    ],
                    buttons: [
                        {extend: 'excel', filename: 'Transaction_history', title: 'Transaction_history'}
                    ],
                    columns: [
                        {data: "ran_or_order"},
                        {data: "part_number"},
                        {data: "serial_reference"},
                        {data: "short_code"},
                        {data: "txn_qty"},
                        {data: "from_location_code"},
                        {data: "to_location_code"},
                        {data: "associated_document_reference"},
                        {data: "date_created"},
                        {data: "last_updated"},
                        {data: "last_updated_by"}
                    ],
                    order: [[0, 'asc']]
                });
            }

        });

        $('#mSearch').on('hidden.bs.modal', function (e) {
             $("#sSerial").prop("disabled", false);
             $("#sPart").prop("disabled", false);
            $(this)
                    .find("input,textarea,select")
                    .val('')
                    .end()
                    .find("input[type=checkbox], input[type=radio]")
                    .prop("checked", "")
                    .end();
        });



        $('#next').on('click', function () {

            var table = $('#example').DataTable();
            table.page('next').draw('page');


            var info = table.page.info();
            console.log(info);
            console.log(info.page);

            if (info.page === info.pages - 1) {
                console.log('last PAGE');
                tabStart = tabStart + 3000;

                limit = tabStart;

                $('#example').DataTable().destroy();
                $('#example').DataTable({
                    searching:false,
                    ajax: {"url": "../tableData/testSpeed.php", data: {limit: limit}, "dataSrc": ""},
                    columnDefs: [
                    ],
                    buttons: [
                        {extend: 'excel', filename: 'Transaction_history', title: 'Transaction_history'}
                    ],
                    columns: [
                        {data: "ran_or_order"},
                        {data: "part_number"},
                        {data: "serial_reference"},
                        {data: "short_code"},
                        {data: "txn_qty"},
                        {data: "from_location_code"},
                        {data: "to_location_code"},
                        {data: "associated_document_reference"},
                        {data: "date_created"},
                        {data: "last_updated"},
                        {data: "last_updated_by"}
                    ],
                    order: [[0, 'asc']]
                });


                table.page(0);
            }
        });

        $('#previous').on('click', function () {

            var table = $('#example').DataTable();
            table.page('previous').draw('page');


            var info = table.page.info();
            console.log(info);
            console.log(info.page);

            if (info.page === 0 && tabStart !== 0) {
                console.log('last PAGE');
                tabStart = tabStart - 3000;

                limit = tabStart;

                $('#example').DataTable().destroy();
                table = $('#example').DataTable({
                    searching:false,
                    ajax: {"url": "../tableData/testSpeed.php", data: {limit: limit}, "dataSrc": ""},
                    columnDefs: [
                    ],
                    buttons: [
                        {extend: 'excel', filename: 'Transaction_history', title: 'Transaction_history'}
                    ],
                    columns: [
                        {data: "ran_or_order"},
                        {data: "part_number"},
                        {data: "serial_reference"},
                        {data: "short_code"},
                        {data: "txn_qty"},
                        {data: "from_location_code"},
                        {data: "to_location_code"},
                        {data: "associated_document_reference"},
                        {data: "date_created"},
                        {data: "last_updated"},
                        {data: "last_updated_by"}
                    ],
                    order: [[0, 'asc']]

                }).on('postCreate', function (e, json) {
                    $("#example").DataTable().page('last').draw('page');
                });


            }

        });



    });

</script>
</body>
</html>



