<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>PI - Location Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="modal fade" id="pOpt" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Print Options</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" align="center">
                        <input type="text" id="printHelp" style="display: none;">
                        <input type="text" id="printHelp2" style="display: none;">
                        <input type="text" id="printHelp3" style="display: none;">
                        <button style="width: 200px" class='btn btn-success' id="printPiHead" type='button'>PI Header</button><br><br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="printClose" class="btn btn-secondary" data-dismiss="modal" >Done</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="mCreateNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group" id="ooLoc">
                                    <label class="input-group-text">Location Code:</label>
                                    <div class="controls">
                                        <input type="text" name="nLocationCode" id="nLocationCode" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Reason Code:</label>
                                    <select class="form-control" type="text" id="nReasonCode">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
// Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.pi_reason_code;');
                                        echo "<option value='0'></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['id'] . '">' . $row['short_code'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="newPiButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="mEditPi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group" id="eoLoc">
                                    <p style="display: none;"><span id="eId"></span></p>
                                    <label class="input-group-text">Location Code:</label>
                                    <div class="controls">
                                        <input type="text" name="eLocationCode" id="eLocationCode" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Reason Code:</label>
                                    <select class="form-control" type="text" id="eReasonCode">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
// Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.pi_reason_code;');
                                        echo "<option value='0'></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['id'] . '">' . $row['short_code'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="editPiButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confCreate" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">New PI Header</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >New PI Header added</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton2" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">PI Header</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >PI Header Edited</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton2" data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>



        <?php
        include('../common/topNav.php');
        include('../common/sideBar.php');
        ?>
        <!-- Page Content  -->
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>Location Code</th>
                        <th>Expected Case Qty</th>
                        <th>Actual Case Qty</th>
                        
                        <th>Difference</th>
                        <th class="yes">Reason Code</th>
                        <th>Serial Exists?</th>
                        <th>Incorrect Location?</th>
                        <th class="yes">Status</th>
                        <th>Date Created</th>
                        <th>Created By</th>
                        <th>Last Updated Date</th>
                        <th>last Updated By</th>
                        <th>Print Options</th>
                        <th>Actions</th>
                </thead>
                <tfoot>
                    <tr>
                        <th></th>
                        <th>Location Code</th>
                        <th>Expected Case Qty</th>
                        <th>Actual Case Qty</th>
                        <th>Difference</th>
                        <th>Reason Code</th>
                        <th>Serial Exists?</th>
                        <th>Incorrect Location?</th>
                        <th>Status</th>
                        <th>Date Created</th>
                        <th>Created By</th>
                        <th>Last Updated Date</th>
                        <th>last Updated By</th>
                        <th>Print Options</th>
                        <th>Actions</th>
                </tfoot>
            </table>

            <input type="Button" id="bCreateNew" class="btn btn-warning" value="Create New" />
            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

        </div>

        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->

<script>
    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }
    function myPi() {
        if (document.getElementById("piTable").innerHTML === "PI - Location") {
            document.getElementById("piTable").innerHTML = "3.14159265359";
        } else {
            document.getElementById("piTable").innerHTML = "PI - Location";
        }

    }
    function formatBody(d) {
        // `d` is the original data object for the row
        return '<table id="piBody" class="compact" border="0" style="padding-left:20px;">' +
                '<thead>' +
                '<th></th>' +
                '<th>Part Number</th>' +
                '<th>Location Code</th>' +
                '<th>Expected Qty</th>' +
                '<th>Actual Qty</th>' +
                '<th>Difference</th>' +
                '<th>Last Updated</th>' +
                '<th>Last Updated By</th>' +
                '</thead>' +
                '</table>';
    }

    function formatDetail(d) {
        return '<table id="piDetail" class="compact" border="0" style="padding-left:20px;">' +
                '<thead>' +
                '<th>Serial Refernece</th>' +
                '<th>Expected Qty</th>' +
                '<th>Actual Qty</th>' +
                '<th>Difference</th>' +
                '<th>Last Updated</th>' +
                '<th>Last Updated By</th>' +
                '</thead>' +
                '</table>';

    }

    $(document).ready(function () {
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>';

        var table = $('#example').DataTable({
            ajax: {"url": "../tableData/piTableData.php", "dataSrc": ""},
            columnDefs: [{
                            targets: -1,
                            data: null,
                            defaultContent: "<input type='button' id='bEdit' class='btn btn-warning' value='Edit'/>"
                        },{
                            targets: [8,5],
                            orderable: false
                        },{
                            targets: -2,
                            data: null,
                            orderable: false,
                            defaultContent: "<button type='button' class='btn btn-success' data-toggle='modal' data data-target='#pOpt'>Options</button>"
                        }
                    ],
            buttons: [
                {extend: 'excel',
                    filename: 'pi_table',
                    title: 'PI'}
            ],
            initComplete: function () {
                this.api().columns().every(function () {

                    var column = this;
                    if ($(column.header()).hasClass('yes')) {
                        var select = $('<select><option id="ssearch" value=""></option></select>')
                                .appendTo($(column.header()))
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                            );
                                    column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                            .draw();
                                });
                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                    }
                });
            },
            columns: [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {data: "location_code"},
                {data: "expected_case_qty"},
                {data: "actual_case_qty"},
                {data: "difference"},
                {data: "short_code"},
                {data: "serial_not_exist"},
                {data: "incorrect_location"},
                {data: "document_status_code"},
                {data: "date_created"},
                {data: "created_by"},
                {data: "last_updated_date"},
                {data: "last_updated_by"},
                {data: ""},
                {data: ""}
            ],
            order: [[11, 'desc']]
        });
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });
        $('#mCreateNew').on('hidden.bs.modal', function (e) {
            $(this)
                    .find("input,textarea,select")
                    .val('')
                    .end()
                    .find("input[type=checkbox], input[type=radio]")
                    .prop("checked", "")
                    .end();
        });

        $('#example tbody').on('click', 'button', function () {
            data = table.row($(this).parents('tr')).data();
            document.getElementById('printHelp').value = data.location_code;
            document.getElementById('printHelp2').value = data.id;
            document.getElementById('printHelp3').value = data.location_code;
            $('#printPiHead').click(function () {
                window.open(report + "piheader.php?locationcode=" + document.getElementById('printHelp').value + "&piid=" + document.getElementById('printHelp2').value);
                table.ajax.reload(null, false);
            });
        });


        $("#bCreateNew").on("click", function () {
            $('#mCreateNew').modal('show');
            document.getElementById("nReasonCode").value = "0";
        });

     $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });

        $("#newPiButton").on("click", function () {
            if (document.getElementById("nLocationCode").value === "") {
                alert('Please Enter Location Code');
                return;
            }
            if (document.getElementById("nReasonCode").value === "0" || document.getElementById("nReasonCode").value === "0") {
                alert('Please Enter a Reason Code');
                return;
            }
            if (document.getElementById("nLocationCode").value.length >= 1 && document.getElementById("nReasonCode").value !== "0") {

                var newLoc = document.getElementById('nLocationCode').value;
                var newReasonCode = document.getElementById('nReasonCode').value;

                var obj = {"locationCode": newLoc, "piReasonCodeId": newReasonCode, "currentUser": currentUser};
                var newPijson = JSON.stringify(obj);
                var filter = newPijson;
                console.log(filter);

                $.ajax({
                    url: callPostService+ filter + "&function=createEditPILocationHeader"  + pi,
                    type: 'GET',
                    success: function (response, textstatus) {
                        if (response === 'OK - true') {
                            $('#mCreateNew').modal('hide');
                            $('#confCreate').modal('show');
                        } else {
                            alert('An Error Has Occured');
                        }
                    }
                });
            }
        });

        $('#example tbody').on('click', '#bEdit', function () {
            $('#mEditPi').modal('show');
            var data = table.row($(this).parents('tr')).data();

            document.getElementById("eId").value = data.id;
            document.getElementById("eReasonCode").value = data.pi_reason_code_id;
            document.getElementById("eLocationCode").value = data.location_code;

        });

        $("#editPiButton").on("click", function () {
            var newID = document.getElementById('eId').value;
            var newLoc = document.getElementById('eLocationCode').value;
            var newReas = document.getElementById('eReasonCode').value;

            var obj = {"piHeaderId": newID, "locationCode": newLoc, "piReasonCodeId": newReas, "currentUser": currentUser};
            
            var newPijson = JSON.stringify(obj);
            var filter = newPijson;
            console.log(filter);

            $.ajax({
                url: callPostService+ filter + "&function=createEditPILocationHeader"  + pi,
                type: 'GET',
                success: function (response, textstatus) {
                    if (response === 'OK - true') {
                        $('#mEditPi').modal('hide');
                        $('#confEdit').modal('show');
                    } else {
                        alert('An Error Has Occured');
                    }
                }
            });
        });


        $('#example tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = table.row(tr).data();

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                //close any open rows first
                if (table.row('.shown').length) {
                    $('.details-control', table.row('.shown').node()).click();
                }

                var rowID = data.id;
                row.child(formatBody(row.data())).show();
                tr.addClass('shown');
                piBodyTable(rowID);
            }
        });

        function piBodyTable(rowID) {
            var table = $('#piBody').DataTable({
                ajax: {"url": "../tableData/piBodyTable.php", "data": {rowID: rowID}, "dataSrc": ""},
                searching: false,
                select: {
                    style: 'os',
                    selector: 'td:not(:first-child)'

                },
                paging: false,
                info: false,
                columns: [
                    {
                        "className": 'table-controls',
                        "orderable": false,
                        "data": null,
                        "defaultContent": ''
                    },
                    {data: "part_number"},
                    {data: "location_code"},
                    {data: "expected_qty"},
                    {data: "actual_qty"},
                    {data: "difference"},
                    {data: "last_updated_date"},
                    {data: "last_updated_by"}
                ],
                order: [[1, 'asc']]
            });

            $('#piBody tbody').on('click', 'td.table-controls', function () {
                var tr = $(this).closest('tr');
                var row = table.row(tr);
                var data = table.row(tr).data();

                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    // Open this row
                    if (table.row('.shown').length) {
                        $('.table-controls', table.row('.shown').node()).click();
                    }

                    var rowID = data.id;
                    row.child(formatDetail(row.data())).show();
                    tr.addClass('shown');
                    piDetailTable(rowID);
                }
            });
        }

        function piDetailTable(rowID) {
            $('#piDetail').DataTable({
                ajax: {"url": "../tableData/piDetailTable.php", "data": {rowID: rowID}, "dataSrc": ""},
                searching: false,
                paging: false,
                info: false,
                columns: [
                    {data: "serial_reference"},
                    {data: "exp_qty"},
                    {data: "act_qty"},
                    {data: "diff"},
                    {data: "last_updated_date"},
                    {data: "last_updated_by"}
                ],
                order: [[1, 'asc']]
            });
        }
    });
</script>
</body>
</html>


