<?php
include '..\config\logCheck.php';
?>
<html>
    <head>
        <title>Parts Master Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <script src="../js/libs/jquery-ui-1.12.1.custom/jquery-ui.js" type="text/javascript"></script>
        <link href="../js/libs/jquery-ui-1.12.1.custom/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../js/libs/mask/jquery.mask.js" type="text/javascript"></script>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="modal fade right" id="mCreateNew" role="dialog" aria-labelledby="viewEdit" aria-hidden="true" >
            <div class="modal-dialog modal-fluid" role="document" style="width: 1000px">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalPreviewLabel">Part Master</h5>
                    </div>
                    <div class="modal-body">
                        <div class=" col-md-12">
                            <div id="nPage1">
                                <div class="row">
                                    <!-- Half of the modal-body div-->
                                    <div class="col-xs-6">
                                        <div class="control-group">
                                            <label class="input-group-text">Part Number:</label>
                                            <div class="controls">
                                                <input type="text"  style="text-transform:uppercase" name="nPartNumber" id="nPartNumber" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Part Description:</label>
                                            <div class="controls">
                                                <input type="text" name="nPartDescription" id="nPartDescription" class="form-control">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Vendor:</label>
                                            <select class="form-control" type="text" id="nVendorCode">                                                                  
<?php
include ('../config/phpConfig.php');
// Check connection
if (mysqli_connect_errno()) {
    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
}
$result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.vendor;');
echo "<option value='0' selected></option>";
while ($row = mysqli_fetch_array($result)) {
    echo '<option value="' . $row['id'] . '">' . $row['vendor_name'] . '</option>';
}
echo '';
mysqli_close($con);
?>
                                            </select>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Effective From:</label>
                                            <div class="controls">
                                                <input type="text" name="nEffectiveFrom" id="nEffectiveFrom" class="form-control date_time">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Effective To:</label>
                                            <div class="controls">
                                                <input type="text" name="nEffectiveTo" id="nEffectiveTo" class="form-control date_time">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Zone Type:</label>
                                            <div class="controls">
                                                <select class="form-control" type="text" id="nZoneType">                                                                  
<?php
include ('../config/phpConfig.php');
// Check connection
if (mysqli_connect_errno()) {
    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
}
$result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.location_zone_type;');
echo "<option value='0' selected></option>";
while ($row = mysqli_fetch_array($result)) {
    echo '<option value="' . $row['id'] . '">' . $row['zone_type_code'] . '</option>';
}
echo '';
mysqli_close($con);
?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Other half of the modal-body div-->
                                    <div class="col-xs-6">
                                        <div class="control-group">
                                            <label class="input-group-text">Pack Type:</label>
                                            <select class="form-control" type="text" id="nPackType">                                                                  
<?php
include ('../config/phpConfig.php');
// Check connection
if (mysqli_connect_errno()) {
    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
}
$result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.pack_type;');
echo "<option value=\"$value\"></option>";
while ($row = mysqli_fetch_array($result)) {
    echo '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
}
echo '';
mysqli_close($con);
?>
                                            </select>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Vendor Part:</label>
                                            <div class="controls">
                                                <input type="text" name="nVendorPart" id="nVendorPart" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">WI Code:</label>
                                            <div class="controls">
                                                <input type="text" name="nWiCode" id="nWiCode" class="form-control">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Fixed Location:</label>
                                            <div class="controls">
                                                <input type="text" name="nFixedLocation" id="nFixedLocation" class="form-control">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Location Type:</label>
                                            <div class="controls">
                                                <select class="form-control" type="text" id="nLocationType">                                                                  
<?php
include ('../config/phpConfig.php');
// Check connection
if (mysqli_connect_errno()) {
    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
}
$result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.location_type;');
echo "<option value=\"$value\"></option>";
while ($row = mysqli_fetch_array($result)) {
    echo '<option value="' . $row['id'] . '">' . $row['location_type_code'] . '</option>';
}
echo '';
mysqli_close($con);
?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Weight:</label>
                                            <div class="controls">
                                                <input type="number" name="nWeight" id="nWeight" class="form-control" >
                                            </div>
                                        </div>


                                    </div>



                                </div>
                                <hr>
                                <div class="row">
                                    <!-- Half of the modal-body div-->
                                    <div class="col-xs-16">
                                        <div class="col-xs-2">
                                            <label class="input-group-text">Count on Receipt:</label>
                                            <div class="controls">
                                                <input type="checkbox" name="nCountOnRcpt" id="nCountOnRcpt" class="input">
                                            </div>
                                        </div>

                                        <div class="col-xs-2">
                                            <label class="input-group-text">Decant</label>
                                            <div class="controls">
                                                <input type="checkbox" name="nDecant" id="nDecant" class="input">
                                            </div>
                                        </div>
                                        <div class="col-xs-2">
                                            <label class="input-group-text">Inspect</label>
                                            <div class="controls">
                                                <input type="checkbox" name="nInspect" id="nInspect" class="input">
                                            </div>
                                        </div>
                                        <div class="col-xs-2">
                                            <label class="input-group-text">Important A:</label>
                                            <div class="controls">
                                                <input type="checkbox" name="nImportant" id="nImportant" class="input">
                                            </div>
                                        </div>
                                        <div class="col-xs-2">
                                            <label class="input-group-text">High Value</label>
                                            <div class="controls">
                                                <input type="checkbox" name="nHighValue" id="nHighValue" class="input">
                                            </div>
                                        </div>
                                        <div class="col-xs-2">
                                            <label class="input-group-text">Auto Receive</label>
                                            <div class="controls">
                                                <input type="checkbox" name="nAutoRecieve" id="nAutoRecieve" class="input">
                                            </div>
                                        </div>
                                        <div class="col-xs-2">
                                            <label class="input-group-text">Auto Pick</label>
                                            <div class="controls">
                                                <input type="checkbox" name="nAutoPick" id="nAutoPick" class="input">
                                            </div>
                                        </div>
                                        <div class="col-xs-5">
                                            <label class="input-group-text">Update Serial With Count</label>
                                            <div class="controls">
                                                <input type="checkbox" name="nUpdateWithSerial" id="nUpdateWithSerial" class="input">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <!-- Half of the modal-body div-->
                                    <div class="col-xs-6">
                                        <div class="control-group">
                                            <label class="input-group-text">Min Storage:</label>
                                            <div class="controls">
                                                <input type="number" name="nMinStorage" id="nMinStorage" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Shelf Life:</label>
                                            <div class="controls">
                                                <input type="number" name="nShelfLife" id="nShelfLife" class="form-control">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">RDT Message:</label>
                                            <div class="controls">
                                                <input type="text" name="nRdtMessage" id="nRdtMessage" class="form-control">
                                            </div>
                                        </div>

                                    </div>


                                    <div class="col-xs-6">
                                        <div class="control-group">
                                            <label class="input-group-text">Part Family:</label>
                                            <div class="controls">
                                                <input type="text" name="nPartFamily" id="nPartFamily" class="form-control">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Loose PCS Equal To:</label>
                                            <div class="controls">
                                                <input type="number" name="nLoosePcs" id="nLoosePcs" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div id="nPage2" style="display: none;">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Decant Charge:</label>
                                        <div class="controls">
                                            <select class="form-control" type="text" id="nDecantCharge">                                                                  
<?php
include ('../config/phpConfig.php');
// Check connection
if (mysqli_connect_errno()) {
    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
}
$result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.charge_master where charge_type_id = "1";');
echo "<option value='0' selected></option>";
while ($row = mysqli_fetch_array($result)) {
    echo '<option value="' . $row['id'] . '">' . $row['charge_name'] . '</option>';
}
echo '';
mysqli_close($con);
?>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Despatch Charge:</label>
                                        <div class="controls">
                                            <select class="form-control" type="text" id="nDespCharge">                                                                  
<?php
include ('../config/phpConfig.php');
// Check connection
if (mysqli_connect_errno()) {
    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
}
$result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.charge_master where charge_type_id = "4";');
echo "<option value='0' selected></option>";
while ($row = mysqli_fetch_array($result)) {
    echo '<option value="' . $row['id'] . '">' . $row['charge_name'] . '</option>';
}
echo '';
mysqli_close($con);
?>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Inspect Charge:</label>
                                        <div class="controls">
                                            <select class="form-control" type="text" id="nInspCharge">                                                                  
<?php
include ('../config/phpConfig.php');
// Check connection
if (mysqli_connect_errno()) {
    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
}
$result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.charge_master where charge_type_id = "2";');
echo "<option value='0' selected></option>";
while ($row = mysqli_fetch_array($result)) {
    echo '<option value="' . $row['id'] . '">' . $row['charge_name'] . '</option>';
}
echo '';
mysqli_close($con);
?>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Receipt Charge:</label>
                                        <div class="controls">
                                            <select class="form-control" type="text" id="nReceCharge">                                                                  
<?php
include ('../config/phpConfig.php');
// Check connection
if (mysqli_connect_errno()) {
    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
}
$result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.charge_master where charge_type_id = "4";');
echo "<option value='0' selected></option>";
while ($row = mysqli_fetch_array($result)) {
    echo '<option value="' . $row['id'] . '">' . $row['charge_name'] . '</option>';
}
echo '';
mysqli_close($con);
?>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Storage Charge:</label>
                                        <div class="controls">
                                            <select class="form-control" type="text" id="nStorCharge">                                                                  
<?php
include ('../config/phpConfig.php');
// Check connection
if (mysqli_connect_errno()) {
    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
}
$result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.charge_master where charge_type_id = "4";');
echo "<option value='0' selected></option>";
while ($row = mysqli_fetch_array($result)) {
    echo '<option value="' . $row['id'] . '">' . $row['charge_name'] . '</option>';
}
echo '';
mysqli_close($con);
?>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Transport Charge:</label>
                                        <div class="controls">
                                            <select class="form-control" type="text" id="nTrnspCharge">                                                                  
<?php
include ('../config/phpConfig.php');
// Check connection
if (mysqli_connect_errno()) {
    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
}
$result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.charge_master where charge_type_id = "3";');
echo "<option value='0' selected'></option>";
while ($row = mysqli_fetch_array($result)) {
    echo '<option value="' . $row['id'] . '">' . $row['charge_name'] . '</option>';
}
echo '';
mysqli_close($con);
?>
                                            </select>
                                        </div>

                                    </div>
                                    <br>
                                    <br>
                                </div>

                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            <button type="button" id="newModalBack" class="btn btn-primary">Back</button>
                            <button type="button" id="newModalNext" class="btn btn-primary">Next</button>
                            <button type="button" id="saveNew" class="btn btn-success">Create</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- conf new Part -->
        <div class="modal fade bd-example-modal-sm" id="confNewPart" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">New Part</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >New Part Added</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" data-dismiss="modal">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- conf Edit Part -->
        <div class="modal fade bd-example-modal-sm" id="confEditPart" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Part</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Part Edited</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" data-dismiss="modal">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade right" id="mEditPart" role="dialog" aria-labelledby="viewEdit" aria-hidden="true" >
            <div class="modal-dialog modal-fluid" role="document" style="width: 1000px">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalPreviewLabel">Part Master</h5>
                    </div>
                    <div class="modal-body">
                        <div class=" col-md-12">
                            <div id="ePage1">
                                <div class="row">
                                    <!-- Half of the modal-body div-->
                                    <div class="col-xs-6">
                                        <div class="control-group">
                                            <input type="text" id="Eid" style="display: none">
                                            <label class="input-group-text">Part Number:</label>
                                            <div class="controls">
                                                <input type="text" name="ePartNumber" id="ePartNumber" class="form-control" disabled>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Part Description:</label>
                                            <div class="controls">
                                                <input type="text" name="ePartDescription" id="ePartDescription" class="form-control">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Vendor:</label>
                                            <select class="form-control" type="text" id="eVendorCode">                                                                  
<?php
include ('../config/phpConfig.php');
// Check connection
if (mysqli_connect_errno()) {
    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
}
$result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.vendor;');
echo "<option value='0' selected></option>";
while ($row = mysqli_fetch_array($result)) {
    echo '<option value="' . $row['id'] . '">' . $row['vendor_name'] . '</option>';
}
echo '';
mysqli_close($con);
?>
                                            </select>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Effective From:</label>
                                            <div class="controls">
                                                <input type="text" name="eEffectiveFrom" id="eEffectiveFrom" class="form-control date_time">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Effective To:</label>
                                            <div class="controls">
                                                <input type="text" name="eEffectiveTo" id="eEffectiveTo" class="form-control date_time">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Zone Type:</label>
                                            <div class="controls">
                                                <select class="form-control" type="text" id="eZoneType">                                                                  
<?php
include ('../config/phpConfig.php');
// Check connection
if (mysqli_connect_errno()) {
    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
}
$result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.location_zone_type;');
echo "<option value='0' selected></option>";
while ($row = mysqli_fetch_array($result)) {
    echo '<option value="' . $row['id'] . '">' . $row['zone_type_code'] . '</option>';
}
echo '';
mysqli_close($con);
?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Other half of the modal-body div-->
                                    <div class="col-xs-6">
                                        <div class="control-group">
                                            <label class="input-group-text">Pack Type:</label>
                                            <select class="form-control" type="text" id="ePackType">                                                                  
<?php
include ('../config/phpConfig.php');
// Check connection
if (mysqli_connect_errno()) {
    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
}
$result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.pack_type;');
echo "<option value=\"$value\"></option>";
while ($row = mysqli_fetch_array($result)) {
    echo '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
}
echo '';
mysqli_close($con);
?>
                                            </select>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Vendor Part:</label>
                                            <div class="controls">
                                                <input type="text" name="eVendorPart" id="eVendorPart" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">WI Code:</label>
                                            <div class="controls">
                                                <input type="text" name="eWiCode" id="eWiCode" class="form-control">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Fixed Location:</label>
                                            <div class="controls">
                                                <input type="text" name="eFixedLocation" id="eFixedLocation" class="form-control">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Location Type:</label>
                                            <div class="controls">
                                                <select class="form-control" type="text" id="eLocationType">                                                                  
<?php
include ('../config/phpConfig.php');
// Check connection
if (mysqli_connect_errno()) {
    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
}
$result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.location_type;');
echo "<option value=\"$value\"></option>";
while ($row = mysqli_fetch_array($result)) {
    echo '<option value="' . $row['id'] . '">' . $row['location_type_code'] . '</option>';
}
echo '';
mysqli_close($con);
?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Weight:</label>
                                            <div class="controls">
                                                <input type="number" name="eWeight" id="eWeight" class="form-control" >
                                            </div>
                                        </div>


                                    </div>



                                </div>
                                <hr>
                                <div class="row">
                                    <!-- Half of the modal-body div-->
                                    <div class="col-xs-16">
                                        <div class="col-xs-2">
                                            <label class="input-group-text">Count on Receipt:</label>
                                            <div class="controls">
                                                <input type="checkbox" name="eCountOnRcpt" id="eCountOnRcpt" class="input">
                                            </div>
                                        </div>

                                        <div class="col-xs-2">
                                            <label class="input-group-text">Decant</label>
                                            <div class="controls">
                                                <input type="checkbox" name="eDecant" id="eDecant" class="input">
                                            </div>
                                        </div>
                                        <div class="col-xs-2">
                                            <label class="input-group-text">Inspect</label>
                                            <div class="controls">
                                                <input type="checkbox" name="eInspect" id="eInspect" class="input">
                                            </div>
                                        </div>
                                        <div class="col-xs-2">
                                            <label class="input-group-text">Important A:</label>
                                            <div class="controls">
                                                <input type="checkbox" name="eImportant" id="eImportant" class="input">
                                            </div>
                                        </div>
                                        <div class="col-xs-2">
                                            <label class="input-group-text">High Value</label>
                                            <div class="controls">
                                                <input type="checkbox" name="eHighValue" id="eHighValue" class="input">
                                            </div>
                                        </div>
                                        <div class="col-xs-2">
                                            <label class="input-group-text">Auto Receive</label>
                                            <div class="controls">
                                                <input type="checkbox" name="eAutoRecieve" id="eAutoRecieve" class="input">
                                            </div>
                                        </div>
                                        <div class="col-xs-2">
                                            <label class="input-group-text">Auto Pick</label>
                                            <div class="controls">
                                                <input type="checkbox" name="eAutoPick" id="eAutoPick" class="input">
                                            </div>
                                        </div>
                                        <div class="col-xs-5">
                                            <label class="input-group-text">Update Serial With Count</label>
                                            <div class="controls">
                                                <input type="checkbox" name="eUpdateWithSerial" id="eUpdateWithSerial" class="input">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <!-- Half of the modal-body div-->
                                    <div class="col-xs-6">
                                        <div class="control-group">
                                            <label class="input-group-text">Min Storage:</label>
                                            <div class="controls">
                                                <input type="number" name="eMinStorage" id="eMinStorage" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Shelf Life:</label>
                                            <div class="controls">
                                                <input type="number" name="eShelfLife" id="eShelfLife" class="form-control">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">RDT Message:</label>
                                            <div class="controls">
                                                <input type="text" name="eRdtMessage" id="eRdtMessage" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="control-group">
                                            <label class="input-group-text">Part Family:</label>
                                            <div class="controls">
                                                <input type="text" name="ePartFamily" id="ePartFamily" class="form-control">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Loose PCS Equal To:</label>
                                            <div class="controls">
                                                <input type="number" name="eLoosePcs" id="eLoosePcs" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="ePage2" style="display: none;">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Decant Charge:</label>
                                        <div class="controls">
                                            <select class="form-control" type="text" id="eDecantCharge">                                                                  
<?php
include ('../config/phpConfig.php');
// Check connection
if (mysqli_connect_errno()) {
    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
}
$result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.charge_master where charge_type_id = "1";');
echo "<option value='0' selected></option>";
while ($row = mysqli_fetch_array($result)) {
    echo '<option value="' . $row['id'] . '">' . $row['charge_name'] . '</option>';
}
echo '';
mysqli_close($con);
?>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Despatch Charge:</label>
                                        <div class="controls">
                                            <select class="form-control" type="text" id="eDespCharge">                                                                  
<?php
include ('../config/phpConfig.php');
// Check connection
if (mysqli_connect_errno()) {
    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
}
$result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.charge_master where charge_type_id = "4";');
echo "<option value='0' selected></option>";
while ($row = mysqli_fetch_array($result)) {
    echo '<option value="' . $row['id'] . '">' . $row['charge_name'] . '</option>';
}
echo '';
mysqli_close($con);
?>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Inspect Charge:</label>
                                        <div class="controls">
                                            <select class="form-control" type="text" id="eInspCharge">                                                                  
<?php
include ('../config/phpConfig.php');
// Check connection
if (mysqli_connect_errno()) {
    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
}
$result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.charge_master where charge_type_id = "2";');
echo "<option value='0' selected></option>";
while ($row = mysqli_fetch_array($result)) {
    echo '<option value="' . $row['id'] . '">' . $row['charge_name'] . '</option>';
}
echo '';
mysqli_close($con);
?>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Receipt Charge:</label>
                                        <div class="controls">
                                            <select class="form-control" type="text" id="eReceCharge">                                                                  
<?php
include ('../config/phpConfig.php');
// Check connection
if (mysqli_connect_errno()) {
    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
}
$result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.charge_master where charge_type_id = "4";');
echo "<option value='0' selected></option>";
while ($row = mysqli_fetch_array($result)) {
    echo '<option value="' . $row['id'] . '">' . $row['charge_name'] . '</option>';
}
echo '';
mysqli_close($con);
?>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Storage Charge:</label>
                                        <div class="controls">
                                            <select class="form-control" type="text" id="eStorCharge">                                                                  
<?php
include ('../config/phpConfig.php');
// Check connection
if (mysqli_connect_errno()) {
    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
}
$result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.charge_master where charge_type_id = "4";');
echo "<option value='0' selected></option>";
while ($row = mysqli_fetch_array($result)) {
    echo '<option value="' . $row['id'] . '">' . $row['charge_name'] . '</option>';
}
echo '';
mysqli_close($con);
?>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Transport Charge:</label>
                                        <div class="controls">
                                            <select class="form-control" type="text" id="eTrnspCharge">                                                                  
<?php
include ('../config/phpConfig.php');
// Check connection
if (mysqli_connect_errno()) {
    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
}
$result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.charge_master where charge_type_id = "3";');
echo "<option value='0' selected'></option>";
while ($row = mysqli_fetch_array($result)) {
    echo '<option value="' . $row['id'] . '">' . $row['charge_name'] . '</option>';
}
echo '';
mysqli_close($con);
?>
                                            </select>
                                        </div>

                                    </div>
                                    <br>
                                    <br>
                                </div>

                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            <button type="button" id="editModalBack" class="btn btn-primary">Back</button>
                            <button type="button" id="editModalNext" class="btn btn-primary">Next</button>
                            <button type="button" id="saveEdit" class="btn btn-success">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>











<?php
include('../common/topNav.php');
include('../common/sideBar.php');
?>
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th>Part Number</th>
                        <th>Description</th>
                        <th>Vendor Part</th>                     
                        <th>Location Type</th>
                        <th>location Sub Type</th>
                        <th>Inspect</th>
                        <th>Action</th>
                </thead>
                <tfoot>
                    <tr>
                        <th>Part Number</th>
                        <th>Description</th>
                        <th>Vendor Part</th>
                        <th>Location Type</th>
                        <th>location Sub Type</th>
                        <th>Inspect</th>
                        <th>Action</th>
                </tfoot>
            </table>

            <input type="Button" id="bCreateNew" class="btn btn-warning" value="Create New"/>
            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

        </div>

        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->



<script>
    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }
    $(document).ready(function () {
        $('.date_time').mask('0000-00-00 00:00:00');
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>'

        var table = $('#example').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {"url": "../action/serverSide.php"},
            "columnDefs": [{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bEdit' class='btn btn-warning' value='Edit'/>"
                },
                {
                    render: function (data, type, row) {
                        return (data === true) ? '<span data-order="1" style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color:red" data-order="0" class="glyphicon glyphicon-remove"></span>';
                    },
                    "targets": 5
                }
            ]
            
        });
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });


        $("#bCreateNew").on("click", function () {
            $('#mCreateNew').modal('show');
            document.getElementById('nPage1').style.display = "block";
            document.getElementById('nPage2').style.display = "none";
            document.getElementById('newModalNext').style.display = "inline";
            document.getElementById('newModalBack').style.display = "none";
        });
        $("#newModalNext").on("click", function () {
            document.getElementById('nPage1').style.display = "none";
            document.getElementById('nPage2').style.display = "block";
            document.getElementById('newModalNext').style.display = "none";
            document.getElementById('newModalBack').style.display = "inline";
        });
        $("#newModalBack").on("click", function () {
            document.getElementById('nPage1').style.display = "block";
            document.getElementById('nPage2').style.display = "none";
            document.getElementById('newModalNext').style.display = "inline";
            document.getElementById('newModalBack').style.display = "none";
        });
        $('#mCreateNew').on('hidden.bs.modal', function (e) {
            $(this)
                    .find("input,textarea,select")
                    .val('')
                    .end()
                    .find("input[type=checkbox], input[type=radio]")
                    .prop("checked", "")
                    .end();
        })


        //On change for the Vendor to pre populate when you slect the vendor 
        //do tis as a jquery onchange
        $("#nVendorCode").on("change", function () {
            //   document.getElementById('nEffectiveFrom').value = ""
            //   document.getElementById('nEffectiveTo').value = ""
            var venDrop = document.getElementById('nVendorCode').value;
            if (venDrop == "" || null) {
                alert('something')

            } else {
                // alert(venDrop)
                $.ajax({
                    url: "../tableData/partVendorHelp.php",
                    data: {'venDrop': venDrop},
                    type: "GET",
                    dataSrc: "",
                    success: function (result) {
                        var data = JSON.parse(result);
                        //console.log(data)
                        document.getElementById('nEffectiveFrom').value = data[0].effective_from;
                        document.getElementById('nEffectiveTo').value = data[0].effective_to;
                        document.getElementById('nDecantCharge').value = data[0].decant_charge_id;
                        document.getElementById('nDespCharge').value = data[0].despatch_charge_id;
                        document.getElementById('nInspCharge').value = data[0].inspect_charge_id;
                        document.getElementById('nTrnspCharge').value = data[0].transport_charge_id;
                        // document.getElementById('nRepCharge').value = data[0].report_charge_ids;
                        document.getElementById('nStorCharge').value = data[0].storage_charge_id;
                        document.getElementById('nReceCharge').value = data[0].receipt_charge_id;
                        if (data[0].auto_receive === '1') {
                            $('#nAutoRecieve').prop('checked', true);
                        } else {
                            $('#nAutoRecieve').prop('checked', false);
                        }
                        if (data[0].auto_pick === '1') {
                            $('#nAutoPick').prop('checked', true);
                        } else {
                            $('#nAutoPick').prop('checked', false);
                        }
                        if (data[0].decant === '1') {
                            $('#nDecant').prop('checked', true);
                        } else {
                            $('#nDecant').prop('checked', false);
                        }
                        if (data[0].inspect === '1') {
                            $('#nInspect').prop('checked', true);
                        } else {
                            $('#nInspect').prop('checked', false);
                        }
                    }
                });
            }

        });
        $("#saveNew").on("click", function () {
            var newPartNum = document.getElementById('nPartNumber').value;
            var newWeight = document.getElementById('nWeight').value;
            var newPartDesc = document.getElementById('nPartDescription').value;
            var newVendorCode = parseInt(document.getElementById('nVendorCode').value);
            var newEffectFrom = document.getElementById('nEffectiveFrom').value;
            var newEffectTo = document.getElementById('nEffectiveTo').value;
            var newZoneType = parseInt(document.getElementById('nZoneType').value);
            var newPackType = parseInt(document.getElementById('nPackType').value);
            var newVendorPart = document.getElementById('nVendorPart').value;
            var newWiCode = document.getElementById('nWiCode').value;
            var newFixLocation = document.getElementById('nFixedLocation').value;
            var newLocationType = document.getElementById('nLocationType').value;
            var newAutoReceive = $('#nAutoRecieve');
            var newAutoPick = $('#nAutoPick');
            var newDecant = $('#nDecant');
            var newInspect = $('#nInspect');
            var newCountOn = $('#nCountOnRecpt');
            var newImportant = $('#nImportant');
            var newHighValue = $('#nHighValue');
            var newUpdateWith = $('#nUpdateWithSerial');
            var newMinStor = document.getElementById('nMinStorage').value;
            var newShelfLife = document.getElementById('nShelfLife').value;
            var newRdtMess = document.getElementById('nRdtMessage').value;
            var newPartFamily = document.getElementById('nPartFamily').value;
            var newLoosePcs = document.getElementById('nLoosePcs').value;
            var newDecantCh = document.getElementById('nDecantCharge').value;
            var newDespatch = document.getElementById('nDespCharge').value;
            var newInspectCh = document.getElementById('nInspCharge').value;
            var newReceive = document.getElementById('nReceCharge').value;
            var newStorage = document.getElementById('nStorCharge').value;
            var newTransport = document.getElementById('nTrnspCharge').value;

            // var newReports = document.getElementById('nRepCharge').value;
            if (newAutoReceive.is(':checked')) {
                newAutoReceive = 1;
            } else {
                newAutoReceive = 0;
            }
            if (newAutoPick.is(':checked')) {
                newAutoPick = 1;
            } else {
                newAutoPick = 0;
            }
            if (newDecant.is(':checked')) {
                newDecant = 1;
            } else {
                newDecant = 0;
            }
            if (newInspect.is(':checked')) {
                newInspect = 1;
            } else {
                newInspect = 0;
            }
            if (newCountOn.is(':checked')) {
                newCountOn = 1;
            } else {
                newCountOn = 0;
            }
            if (newImportant.is(':checked')) {
                newImportant = 1;
            } else {
                newImportant = 0;
            }
            if (newHighValue.is(':checked')) {
                newHighValue = 1;
            } else {
                newHighValue = 0;
            }
            if (newUpdateWith.is(':checked')) {
                newUpdateWith = 1;
            } else {
                newUpdateWith = 0;

            }

            if (newPartNum === "") {
                alert('Please Enter a Part Number');
                return
            }
            if (newLocationType === "") {
                alert('Location type can not be left empty')
                return;
            }

            if (newVendorCode === 0) {
                alert('Vendor Code can not be left empty')
                return;
            }

            if (newZoneType === 0) {
                alert('Zone type can not be left empty')
                return;
            }
            var obj = {"partNumber": newPartNum, "effectiveFrom": newEffectFrom, "weight": newWeight, "effectiveTo": newEffectTo, "packTypeId": newPackType,
                "partDescription": newPartDesc, "vendorId": newVendorCode, "vendorPartCode": newVendorPart, "wiCode": newWiCode,
                "fixedLocationCode": newFixLocation, "locationTypeId": newLocationType, "zoneTypeId": newZoneType, "countOnRcpt": newCountOn,
                "updateSerialWithCount": newUpdateWith, "decant": newDecant, "inspect": newInspect, "miniStock": newMinStor, "shelfLife": newShelfLife,
                "importantA": newImportant, "highValue": newHighValue, "autoReceive": newAutoReceive, "autoPick": newAutoPick, "rdtMessage": newRdtMess,
                "partFamily": newPartFamily, "loosePcsEqualTo1": newLoosePcs, "storageChargeId": newStorage, "receiptChargeId": newReceive,
                "despatchChargeId": newDespatch, "decantChargeId": newDecantCh, "inspectChargeId": newInspectCh, "transportChargeId": newTransport,
                "createdBy": currentUser, "updatedBy": currentUser
            };

            var newPartjson = JSON.stringify(obj);
            var filter = newPartjson;
            console.log(filter);

            $.ajax({
                url: callPostService + filter+ "&function=createPart"  + screen,
                type: 'POST',
                success: function (response, textstatus) {

                    if (response === 'OK - true') {
                        $('#example').DataTable().ajax.reload(null, false);
                        $('#confNewPart').modal('show');
                        $('#mCreateNew').modal('hide');
                    } else {
                        alert(response);
                    }

                }

            });

        });

        $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });

        //Start of EDIT
        $('#mEditPart').on('hidden.bs.modal', function (e) {
            $(this)
                    .find("input,textarea,select")
                    .val('')
                    .end()
                    .find("input[type=checkbox], input[type=radio]")
                    .prop("checked", "")
                    .end();
        });
        $("#editModalNext").on("click", function () {
            document.getElementById('ePage1').style.display = "none";
            document.getElementById('ePage2').style.display = "block";
            document.getElementById('editModalNext').style.display = "none";
            document.getElementById('editModalBack').style.display = "inline";
        });
        $("#editModalBack").on("click", function () {
            document.getElementById('ePage1').style.display = "block";
            document.getElementById('ePage2').style.display = "none";
            document.getElementById('editModalNext').style.display = "inline";
            document.getElementById('editModalBack').style.display = "none";
        });
        $('#example tbody').on('click', '#bEdit', function () {
            console.log(table.row($(this).data()));
            var data = table.row($(this).parents('tr')).data();
            $('#mEditPart').modal('show');
            document.getElementById('ePage1').style.display = "block";
            document.getElementById('ePage2').style.display = "none";
            document.getElementById('editModalNext').style.display = "inline";
            document.getElementById('editModalBack').style.display = "none";
            document.getElementById('ePartNumber').value = data[0];
            document.getElementById('Eid').value = data.id;
            document.getElementById('ePartDescription').value = data[1];
            document.getElementById('eWeight').value = data.weight;
            document.getElementById('eVendorCode').value = data.vendor_id;
            document.getElementById('eEffectiveFrom').value = data.effective_from;
            document.getElementById('eEffectiveTo').value = data.effective_to;
            document.getElementById('eZoneType').value = data.zone_type_id;
            document.getElementById('ePackType').value = data.pack_type_id;
            document.getElementById('eVendorPart').value = data.vendor_part_code;
            document.getElementById('eWiCode').value = data.wi_code;
            document.getElementById('eFixedLocation').value = data.fixed_location_code;
            document.getElementById('eLocationType').value = data.location_type_id;
            document.getElementById('eMinStorage').value = data.mini_stock;
            document.getElementById('eShelfLife').value = data.shelf_life;
            document.getElementById('eRdtMessage').value = data.rdt_message;
            document.getElementById('ePartFamily').value = data.part_family;
            document.getElementById('eLoosePcs').value = data.loose_pcs_equal_to1;
            document.getElementById('eDecantCharge').value = data.decant_charge_id;
            document.getElementById('eDespCharge').value = data.despatch_charge_id;
            document.getElementById('eInspCharge').value = data.inspect_charge_id;
            document.getElementById('eReceCharge').value = data.receipt_charge_id;
            document.getElementById('eStorCharge').value = data.storage_charge_id;
            document.getElementById('eTrnspCharge').value = data.transport_charge_id;


            if (data.auto_receive === '1') {
                $('#eAutoRecieve').prop('checked', true);
            } else {
                $('#eAutoRecieve').prop('checked', false);
            }
            if (data.auto_pick === '1') {
                $('#eAutoPick').prop('checked', true);
            } else {
                $('#eAutoPick').prop('checked', false);
            }
            if (data.decant === '1') {
                $('#eDecant').prop('checked', true);
            } else {
                $('#eDecant').prop('checked', false);
            }
            if (data.inspect === '1') {
                $('#eInspect').prop('checked', true);
            } else {
                $('#eInspect').prop('checked', false);
            }
            if (data.count_on_rcpt === '1') {
                $('#eCountOnRecpt').prop('checked', true);
            } else {
                $('#eCountOnRecpt').prop('checked', false);
            }
            if (data.improtanta === '1') {
                $('#eImportant').prop('checked', true);
            } else {
                $('#eImportant').prop('checked', false);
            }
            if (data.high_value === '1') {
                $('#eHighValue').prop('checked', true);
            } else {
                $('#eHighValue').prop('checked', false);
            }
            if (data.update_serial_with_count === '1') {
                $('#eUpdateWithSerial').prop('checked', true);
            } else {
                $('#eUpdateWithSerial').prop('checked', false);
            }

        });
        $("#saveEdit").on("click", function () {
            var newPartNum = document.getElementById('ePartNumber').value;
            var newWeight = document.getElementById('eWeight').value;
            var eID = document.getElementById('Eid').value;
            var newPartDesc = document.getElementById('ePartDescription').value;
            var newVendorCode = parseInt(document.getElementById('eVendorCode').value);
            var newEffectFrom = document.getElementById('eEffectiveFrom').value;
            var newEffectTo = document.getElementById('eEffectiveTo').value;
            var newZoneType = parseInt(document.getElementById('eZoneType').value);
            var newPackType = parseInt(document.getElementById('ePackType').value);
            var newVendorPart = document.getElementById('eVendorPart').value;
            var newWiCode = document.getElementById('eWiCode').value;
            var newFixLocation = document.getElementById('eFixedLocation').value;
            var newLocationType = document.getElementById('eLocationType').value;
            var newAutoReceive = $('#eAutoRecieve');
            var newAutoPick = $('#eAutoPick');
            var newDecant = $('#eDecant');
            var newInspect = $('#eInspect');
            var newCountOn = $('#eCountOnRecpt');
            var newImportant = $('#eImportant');
            var newHighValue = $('#eHighValue');
            var newUpdateWith = $('#eUpdateWithSerial');
            var newMinStor = document.getElementById('eMinStorage').value;
            var newShelfLife = document.getElementById('eShelfLife').value;
            var newRdtMess = document.getElementById('eRdtMessage').value;
            var newPartFamily = document.getElementById('ePartFamily').value;
            var newLoosePcs = document.getElementById('eLoosePcs').value;
            var newDecantCh = document.getElementById('eDecantCharge').value;
            var newDespatch = document.getElementById('eDespCharge').value;
            var newInspectCh = document.getElementById('eInspCharge').value;
            var newReceive = document.getElementById('eReceCharge').value;
            var newStorage = document.getElementById('eStorCharge').value;
            var newTransport = document.getElementById('eTrnspCharge').value;
            // var newReports = document.getElementById('nRepCharge').value;

            if (newLocationType === "") {
                alert('Location type can not be left empty')
                return;
            }
            if (newVendorCode === 0) {
                alert('Vendor Code can not be left empty')
                return;
            }


            if (newZoneType === 0) {
                alert('Zone type can not be left empty')
                return;
            }
            if (newAutoReceive.is(':checked')) {
                newAutoReceive = 1;
            } else {
                newAutoReceive = 0;
            }
            if (newAutoPick.is(':checked')) {
                newAutoPick = 1;
            } else {
                newAutoPick = 0;
            }
            if (newDecant.is(':checked')) {
                newDecant = 1;
            } else {
                newDecant = 0;
            }
            if (newInspect.is(':checked')) {
                newInspect = 1;
            } else {
                newInspect = 0;
            }
            if (newCountOn.is(':checked')) {
                newCountOn = 1;
            } else {
                newCountOn = 0;
            }
            if (newImportant.is(':checked')) {
                newImportant = 1;
            } else {
                newImportant = 0;
            }
            if (newHighValue.is(':checked')) {
                newHighValue = 1;
            } else {
                newHighValue = 0;
            }
            if (newUpdateWith.is(':checked')) {
                newUpdateWith = 1;
            } else {
                newUpdateWith = 0;

            }

            var obj = {"id": eID, "partNumber": newPartNum, "weight": newWeight, "effectiveFrom": newEffectFrom, "effectiveTo": newEffectTo, "packTypeId": newPackType,
                "partDescription": newPartDesc, "vendorId": newVendorCode, "vendorPartCode": newVendorPart, "wiCode": newWiCode,
                "fixedLocationCode": newFixLocation, "locationTypeId": newLocationType, "zoneTypeId": newZoneType, "countOnRcpt": newCountOn,
                "updateSerialWithCount": newUpdateWith, "decant": newDecant, "inspect": newInspect, "miniStock": newMinStor, "shelfLife": newShelfLife,
                "importantA": newImportant, "highValue": newHighValue, "autoReceive": newAutoReceive, "autoPick": newAutoPick, "rdtMessage": newRdtMess,
                "partFamily": newPartFamily, "loosePcsEqualTo1": newLoosePcs, "storageChargeId": newStorage, "receiptChargeId": newReceive,
                "despatchChargeId": newDespatch, "decantChargeId": newDecantCh, "inspectChargeId": newInspectCh, "transportChargeId": newTransport,
                "createdBy": currentUser, "updatedBy": currentUser
            };

            var newPartjson = JSON.stringify(obj);
            var filter = newPartjson;
            console.log(filter);

            $.ajax({
                url: callPostServivce+filter + "&function=editPart"  + screen,
                type: 'POST',
                success: function (response, textstatus) {

                    if (response === 'OK  -') {
                        $('#example').DataTable().ajax.reload(null, false);
                        $('#confEditPart').modal('show');
                        $('#mEditPart').modal('hide');
                    } else {
                        alert(response);
                    }

                }

            });

        });


        $(".datepicker").datepicker({dateFormat: 'yy-mm-dd hh:mm:ss'});
    });

</script>
</body>
</html>



