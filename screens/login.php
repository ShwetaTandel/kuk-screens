<?php
session_start();

include '../config/ChromePhp.php';
?>

<head>
    <title>KUK-VIMS-Login</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
    <script src="../js/libs/bootstrap.bundle.min.js" type="text/javascript"></script>
    <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../css/mainCss.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="../config/screenConfig.js" type="text/javascript"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Overpass:wght@100;400;500;700;900&display=swap" rel="stylesheet">

    <style>
        .men>ul>li:hover {
            background: #0096d64f;
            padding: 0px 0 0 2px;

        }

        .men>ul {
            background: #f1f1f1;
            padding: 8px 5px 8px 5px;
            min-width: 200px;

        }

        .men>li {
            padding: 10px 0;
            text-align: center;
            line-height: 14px !important;
            height: 14px;

        }

        .men>ul>li>a:hover {
            text-decoration: none;

        }

        .men>ul>li>a {
            color: #000000;
            font-weight: 400;
            font-size: 21px !important;
        }

        .greyscale {
            filter: grayscale();
        }


        .f175 {
            font-size: 1.75rem !important;
            line-height: 32.5px;
        }

        .link-dec {
            text-decoration: none;
            color: #f80503 !important;
            font-weight: 900;
        }

        .bg-grey {
            background: #9e9797 !important;
        }
        html {
            box-sizing: border-box;
        }

        body {
            padding: 0px;
            font-family: 'Overpass', sans-serif;
            background-color: #f4f4f4;
        }

        .button-scheme {
            background-color: #111111 !important;
            border: 3px solid #f80503 !important;
        }

        .listh {
            height: 20px;
            line-height: 22px;
        }

        .form-container{
            display: flex;
            align-items: center;
            max-width: 500px;
            margin: 10px auto;
        }

        @media only screen and (max-width: 1400px) {
            .men>ul {
                background: #f1f1f1;
                padding: 8px 5px 8px 5px;
                min-width: 200px;
                max-height: 160px;
                overflow-y: scroll;

            }

        }
    </style>
</head>

<body>

    <header>
        <nav class="navbar navbar-light bg-light">
            <div class="container-fluid">
                <div style="display: flex; align-items:flex-start;">
                    <a class="navbar-brand" href="mainMenu.php">
                        <img src="../images/v-logo.png" alt="" width="116" height="50" class="d-inline-block align-text-top">
                    </a>
                    <h3 style="line-height: 42.5px; display:flex; font-weight:900; align-self:end; margin:0px;"> VIMS KUK</h3>
                </div>



                <!-- <img src="../images/k-logo.png" alt="" width="160" height="50" class="d-inline-block align-text-top"> -->
            </div>

        </nav>
    </header>
    
    <div style="display:flex; width: 100%; background-color: #d1d1d1; height:1px;"></div>
    
    <div class="jumbotron jumbotron-fluid" style="background-color: #f1f1f1; margin-bottom:0px; padding:1.5rem 1rem;">

    <h1 class="display-5 text-center">Welcome to VIMS</h1>
    <p class="lead text-center">(Vantec Inventory Management System)</p>

</div>

    <div class="form-container">
        <form role="form" class="p-5 w-100" style="background-color:#e7e7e7;" onsubmit="login()">
        <h4>Login to Vims</h4>
            <div class="form-group">
                <label for="username"> <i class="icon-user px-2"></i></span> Username</label>
                <input type="text" class="form-control" id="username" placeholder="Enter Username">
            </div>
            <div class="form-group">
                <label for="password"><i class="icon-key px-2"></i> </span> Password</label>
                <input type="password" class="form-control" id="password" placeholder="Enter password">
            </div>
            <input type="button" class="btn button-scheme btn-block my-4 p-2" style="color:white"; id="EnterKey" value="Log In" onclick="login()" />
        </form>
    </div>
 

    <footer style="width:100%; background:#000000; height: 25px; display:flex; justify-content:center; align-items:center; margin-top:60px;">
        <p style="margin:0; color:white;">
         Vantec Europe Limited <?php echo date("Y") ?>
        </p>
    </footer>

    <script>
        //ENTER KEY handling
        $(document).keypress(function(e) {
            var keycode = (e.keyCode ? e.keyCode : e.which);
            if (keycode == '13') {
                document.getElementById("EnterKey").click();
            }
        });

        //Login - call user.php  
        function login() {
            var userID = document.getElementById('username').value;
            var pass = document.getElementById('password').value;
            var check = 'no';
            $.ajax({
                url: '..\\action\\user.php',
                type: 'GET',
                data: {
                    userID: userID,
                    pass: pass,
                    check: check
                },
                success: function(response, textstatus) {
                    if (response.substring(0, 4) === 'OKUS') {
                        window.open("mainMenu.php", "_self");
                    } else {
                        alert('Wrong Username or Password');
                    }
                }
            });

        }
    </script>

</body>

</html>