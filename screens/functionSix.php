    <?php

    include '../config/logCheck.php';

    ?>
<html>
    <head>
        <title>F6 Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>
       
       <?php
            include('../common/topNav.php');
            include('../common/sideBar.php');
       ?>
        <!-- Page Content  -->
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th>Location Code</th>
                        <th>Part Number</th>
                        <th>Serial</th>
                        <th>Qty</th>
                        <th>Status</th>
                        <th>Date Created</th>
                        <th>Created By</th>
                </thead>
                <tfoot>
                    <tr>
                        <th>Location Code</th>
                        <th>Part Number</th>
                        <th>Serial</th>
                        <th>Qty</th>
                        <th>Status</th>
                        <th>Date Created</th>
                        <th>Created By</th>
                </tfoot>
            </table>

            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

        </div>

        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->

<script>
            function logOut() {
        
            var userID = <?php $_SESSION['userData']['username']?>
            $.ajax({
                url: '../action/userlogout.php',
                type: 'GET',
                data: {userID: userID },
                success: function (response, textstatus) {
                    alert("You have been logged out");
                    window.open('login.php','_self');
                }
            });
        }

    $(document).ready(function () {
       var currentUser = '<?php print_r($_SESSION['userData']['username'])?>'

        var table = $('#example').DataTable({
            ajax: {"url": "../tableData/functionSixTable.php", "dataSrc": ""},
            buttons: [
                {extend: 'excel', filename: 'f6', title: 'F6 Report'}
            ],
            columns: [
                {data: "location_code"},
                {data: "part_number"},
                {data: "serial"},
                {data: "qtys"},
                {data: "inventory_status_code"},
                {data: "date_created"},
                {data: "created_by"}
             
            ],
            order: [[5, 'desc']]
        });
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });
     $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });
                $('#example tfoot th').each(function (i) {
            if(i=== 0 || i===1 || i===2 || i===3  || i===4 || i===5 || i===6){
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Search ' + title + '" />');
            }
        });
        table.columns().every(function () {
            var that = this;
            $('input', this.footer()).on('keyup change clear', function () {
                if (that.search() !== this.value) {
                    that
                            .search(this.value)
                            .draw();
                }
            });
        });


    });

</script>
</body>
</html>



