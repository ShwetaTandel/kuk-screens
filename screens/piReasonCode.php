<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>PI Reason Code Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>

        <div class="modal fade" id="mCreateNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New PI Reason Code</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Short Code:</label>
                                    <div class="controls">
                                        <input type="text" name="nShortCode" id="nShortCode" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Description:</label>
                                    <div class="controls">
                                        <input type="text" name="nDescription" id="nDescription" class="form-control" >
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">

                        <button type="button" class="btn btn-success" id="newPiButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confCreate" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Reason Code Added</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >New Reason Code Added</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="mEditCode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit PI Reason Code</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <input type="text" name="eID" id="eID" class="form-control" style="display: none;">
                                    <label class="input-group-text">Short Code:</label>
                                    <div class="controls">
                                        <input type="text" name="eShortCode" id="eShortCode" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Description:</label>
                                    <div class="controls">
                                        <input type="text" name="eDescription" id="eDescription" class="form-control" >
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-danger"  style="float: left;" id="bDelete">Delete</button>
                        <button type="button" class="btn btn-success" id="editReasonCode">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-sm" id="confEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Reason Code</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Reason Code Edited</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="editConfButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>

<?php
include('../common/topNav.php');
include('../common/sideBar.php');
?>
        <!-- Page Content  -->
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th>Short Code</th>
                        <th>Description</th>
                        <th>Date Created</th>
                        <th>Created By</th>
                        <th>Last Updated Date</th>
                        <th>last Updated By</th>
                        <th></th>
                </thead>
                <tfoot>
                    <tr>
                        <th>Short Code</th>
                        <th>Description</th>
                        <th>Date Created</th>
                        <th>Created By</th>
                        <th>Last Updated Date</th>
                        <th>last Updated By</th>
                        <th></th>
                </tfoot>
            </table>

            <input type="Button" id="bCreateNew" class="btn btn-warning" value="Create New"/>
            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

        </div>

        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->

<script>

    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }

    $(document).ready(function () {
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>';


        var table = $('#example').DataTable({
            ajax: {"url": "../tableData/piReasonCodeTableData.php", "dataSrc": ""},
            columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bEdit' class='btn btn-warning' value='Edit'/>"
                }
            ],
            buttons: [
                {extend: 'excel',
                    filename: 'pi_table',
                    title: 'PI'}
            ],
            columns: [
                {data: "short_code"},
                {data: "description"},
                {data: "date_created"},
                {data: "created_by"},
                {data: "last_updated_date"},
                {data: "last_updated_by"},
                {data: ""}
            ],
            order: [[4, 'asc']]
        });
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });


        $("#bCreateNew").on("click", function () {
            $('#mCreateNew').modal('show');
        });
        $("#newPiButton").on("click", function () {


            var newCode = document.getElementById('nShortCode').value;
            var newDesc = document.getElementById('nDescription').value;
            ;

            $('#mCreateNew').modal('hide');

            var obj = 'shortCode=' + newCode + '|ANDs|desc=' + newDesc + '|ANDs|createdBy=' + currentUser;

            var filter = obj;
            console.log(filter);


            $.ajax({
                url: callGetService + filter + "&function=createPIReasonCode" +  pi,
                type: 'GET',
                success: function (response, textstatus) {
                    $('#confCreate').modal('show');
                }
            });
        });

        $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });

        $('#example tbody').on('click', '#bEdit', function () {

            $('#mEditCode').modal('show');
            var data = table.row($(this).parents('tr')).data();
            document.getElementById('eID').value = data.id;
            document.getElementById('eShortCode').value = data.short_code;
            document.getElementById('eDescription').value = data.description;
        });


        $("#editReasonCode").on("click", function () {

            var newID = parseInt(document.getElementById('eID').value);
            var newCode = document.getElementById('eShortCode').value;
            var newDesc = document.getElementById('eDescription').value;

            $('#mEditCode').modal('hide');

            var obj = 'id=' + newID + '|ANDs|shortCode=' + newCode + '|ANDs|desc=' + newDesc + '|ANDs|createdBy=' + currentUser;

            var filter = obj;
            console.log(filter);


            $.ajax({
                
                url: callGetService+ filter + "&function=UpdatePIReasonCode"  + pi,
                type: 'GET',
                success: function (response, textstatus) {
                    if (response === 'true') {
                        $('#confEdit').modal('show');
                    } else {
                        alert(response);
                    }
                }
            });
            });
        });

</script>
</body>
</html>


