    <?php

    include '../config/logCheck.php';

    ?>
<html>
    <head>
        <title>Charge Master Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>

        <div class="modal fade" id="mCreateNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New Charge</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Charge Type:</label>
                                    <select class="form-control" type="text" id="nChargeType">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
                                        // Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.charge_type;');
                                        echo "<option value=\"$value\"></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['id'] . '">' . $row['charge_name'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Charge Name:</label>
                                    <div class="controls">
                                        <input type="text" name="nChargeName" id="nChargeName" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">STD Charge:</label>
                                    <div class="controls">
                                        <input type="text" name="nStdCharge" id="nStdCharge" class="form-control">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="newChargeButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="mEditCharge" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New Charge</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <input type="text" name="eID" id="eID" class="form-control" style="display: none;">
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Charge Type:</label>
                                    <select class="form-control" type="text" id="eChargeType">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
                                        // Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.charge_type;');
                                        echo "<option value=\"$value\"></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['id'] . '">' . $row['charge_name'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Charge Name:</label>
                                    <div class="controls">
                                        <input type="text" name="eChargeName" id="eChargeName" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">STD Charge:</label>
                                    <div class="controls">
                                        <input type="text" name="eStdCharge" id="eStdCharge" class="form-control">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="editChargeButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-sm" id="confCreate" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Charge Added</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >New Charge Added</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-sm" id="confEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Charge Edited</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Charge Edited</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>






          <?php
      include('../common/topNav.php');
        include('../common/sideBar.php');
        
       ?>
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th class="yes">Charge Type</th>
                        <th>Charge Name</th>
                        <th>STD Charge</th>
                        <th>Created By</th>
                        <th>Date Created</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th>Action</th>
                </thead>
                <tfoot>
                    <tr>
                        <th>Charge Type</th>
                        <th>Charge Name</th>
                        <th>STD Charge</th>
                        <th>Created By</th>
                        <th>Date Created</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th>Action</th>
                </tfoot>
            </table>

            <input type="Button" id="bCreateNew" class="btn btn-warning" value="Create New"/>
            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

        </div>
        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->



<script>
            function logOut() {
        
            var userID = <?php $_SESSION['userData']['username']?>
            $.ajax({
                url: '../action/userlogout.php',
                type: 'GET',
                data: {userID: userID },
                success: function (response, textstatus) {
                    alert("You have been logged out");
                    window.open('login.php','_self');
                }
            });
        }

    $(document).ready(function () {
          var currentUser = '<?php print_r($_SESSION['userData']['username'])?>'


        var table = $('#example').DataTable({
            ajax: {"url": "../tableData/chargeMasterTable.php", "dataSrc": ""},
            columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bEdit' class='btn btn-warning' value='Edit'/>"
                }, {
                    targets: 0,
                    orderable: false

                }

            ],
            buttons: [
                {extend: 'excel', filename: 'Charge_Master', title: 'Charge Master'}
            ],
            initComplete: function () {
                this.api().columns().every(function () {

                    var column = this;
                    if ($(column.header()).hasClass('yes')) {
                        var select = $('<select><option id="ssearch" value=""></option></select>')
                                .appendTo($(column.header()))
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                            );
                                    column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                            .draw();
                                });
                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                    }
                });
            },
            columns: [
                {data: "charge_type"},
                {data: "charge_name"},
                {data: "std_charge"},
                {data: "created_by"},
                {data: "date_created"},
                {data: "last_updated_date"},
                {data: "last_updated_by"},
                {data: ""}

            ],
            order: [[1, 'asc']]
        });

        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });

     $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });


        $("#bCreateNew").on("click", function () {
            $('#mCreateNew').modal('show');
        });


        $('#example tbody').on('click', '#bEdit', function () {
            $('#mEditCharge').modal('show');
            var data = table.row($(this).parents('tr')).data();
            document.getElementById('eID').value = data.id;
            document.getElementById('eChargeType').value = data.charge_type_id;
            document.getElementById('eChargeName').value = data.charge_name;
            document.getElementById('eStdCharge').value = data.std_charge;

        });


        $("#editChargeButton").on("click", function () {
            var editId = document.getElementById('eID').value;
            var newType = document.getElementById('eChargeType').value;
            var newName = document.getElementById('eChargeName').value;
            var newStd = document.getElementById('eStdCharge').value;

            $('#mEditCharge').modal('hide');

            var obj = {"id": editId, "chargeTypeId": newType, "chargeName": newName, "stdCharge": newStd, "createdBy": currentUser, "updatedBy": currentUser};
            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            console.log(filter)


            $.ajax({
                url: callPostService +filter+ "&function=editCharge"  + screen,
                type: 'GET',
                success: function (response, textstatus) {
                    $('#confEdit').modal('show');
                }
            });
        });

        $("#newChargeButton").on("click", function () {

            var newType = document.getElementById('nChargeType').value;
            var newName = document.getElementById('nChargeName').value;
            var newStd = document.getElementById('nStdCharge').value;

            $('#mCreateNew').modal('hide');

            var obj = {"chargeTypeId": newType, "chargeName": newName, "stdCharge": newStd, "createdBy": currentUser, "updatedBy": currentUser};
            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            console.log(filter)


            $.ajax({
                url: callPostService +filter+ "&function=createCharge="  + screen,
                type: 'GET',
                success: function (response, textstatus) {
                    $('#confCreate').modal('show');
                }
            });
        });
    });

</script>
</body>
</html>



