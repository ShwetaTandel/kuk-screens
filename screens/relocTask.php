<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>RELOC Tasks</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>

        <div class="modal fade bd-example-modal-lg" id="missingSerialModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Reloc F8</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" align="center">
                        <form class="form-horizontal">
                          
                            <div class="form-group">
                                <label class="control-label col-sm-2">Reloc Tasks</label> 
                                <div class="col-sm-10">
                                    <select class="form-control" type="text" id="relocTask">                                                                  
                                    </select>
                                </div>
                            </div>

                            <div align="center">

                                <label style="color: red;">Pressing Yes would send a notification to KUK.</label>
                                
                            </div>
                        </form>    
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="relocF8Yes">Yes</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>       
        <?php
        include('../common/topNav.php');
        include('../common/sideBar.php');
        ?>
        <!-- Page Content  -->
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>Document Reference</th>
                        <th>GRN Reference Type</th>
                        <th>Line Number</th>
                        <th>Quantity</th>
                        
                        <th>From Location </th>
                        <th>To Location </th>
                        <th  class="yes">Processed </th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>

                </thead>
                <tfoot>
                    <tr>
                        <th></th>
                        <th><input type="text" placeholder="Search Document Reference" /></th>
                        <th><input type="text" placeholder="GRN Reference" /></th>
                        <th>Line Number</th>
                        <th>Quantity</th>
                        
                        <th><input type="text" placeholder="From Location" /> </th>
                        <th><input type="text" placeholder="To Location" />  </th>
                        <th>Processed </th>
                        <th>Last Updated</th>
                        <th><input type="text" placeholder="Last Updated By" /> </th>
                </tfoot>
            </table>

            <input type="Button" id="exportExcel" class="btn btn-info" value="Export To Excel"/>
            <input type="Button" id="relocF8" class="btn btn-warning" value="Reloc F8" onclick="showF8Tasks()"/>

        </div>

        <!--/span-->
    </body>
    <!--/span-->

    <script>
        function logOut() {

            var userID = <?php $_SESSION['userData']['username'] ?>
            $.ajax({
                url: '../action/userlogout.php',
                type: 'GET',
                data: {userID: userID},
                success: function (response, textstatus) {
                    alert("You have been logged out");
                    window.open('login.php', '_self');
                }
            });
        }
        //the table set up for the Body
         function formatBody(d) {
        // `d` is the original data object for the row
        return '<table id="pBody" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                '<thead>' +
                '<th>Serial Number</th>' +
                '<th>Qty</th>' +
                '<th>Processed</th>' +
                '</thead>' +
                '</table>';
     }
     function showF8Tasks(){
         $('#missingSerialModal').modal('show');
     }
    

        $(document).ready(function () {
            var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>';
            var selected = [];
            //simple function (not sure is needed here)... 
            // this will on clse of any modal will clear the data from the modal
            $('.modal').on('hidden.bs.modal', function (e) {
                $(this).removeData();
            });
            var table = $('#example').DataTable({
                ajax: {"url": "../tableData/relocHeaderData.php"}, //"dataSrc": ""
                iDisplayLength: 15,
                dom: 'Blfrtipb',
                "processing": true, // to show progress bar
                "serverSide": true,
                columnDefs: [{
                        targets: [1,2,3,4, 5,6,7,9],
                        orderable: false

                    }

                ],
                buttons: [
                    {extend: 'excel', filename: 'RTS', title: 'RTS'}
                ],
                initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    if ($(column.header()).hasClass('yes')) {
                         var select = $('<select><option id="processedSeelect" value=""></option></select>')
                                        .appendTo($(column.header()))
                                        .on('keyup change', function () {
                                        column.search($(this).val())
                                            .draw();
                                        });
                                        select.append("<option value = 0>Not Processed</option>");
                                        select.append("<option value = 1>Processed</option>");
                                        return;
                    }
                });
            },
                columns: [
                     {
                        "className": 'details-control',
                        "orderable": false,
                        "data": null,
                        "defaultContent": ''
                    },
                    {data: "document_reference"},
                    {data: "customer_reference"},
                    {data: "line_no"},
                    {data: "quantity"},
                    
                    {data: "from_location"},
                    {data: "to_location"},
                    {data: "processed",
                    render: function (data, type, row) {
                        return (data === '1') ? '<span style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color: red;" class="glyphicon glyphicon-remove"></span>';
                    }
                    },
                    {data: "last_updated_date"},
                    {data: "last_updated_by"}
                    
                ],
                order: [[8, 'desc']]
            });
              table.columns().every(function() {
            var that = this;
            $('input', this.footer()).on('keyup change', function(e) {
                if (that.search() !== this.value) that.search(this.value);
                // if serverside draw() only on enter
                if (e.keyCode === 13 ) that.draw();
            });
        });
            $.ajax({
               url: '../tableData/missingSerialsForRelocTable.php',
                type: 'GET',
                success: function (response) {
                 var jsonData = JSON.parse(response);
                 document.getElementById('relocF8').value = "Reloc F8("+jsonData.length+")";
                 var selectList = document.getElementById("relocTask");
                    selectList.options.length = 0;
                       var newOption = document.createElement('option');
                            newOption.setAttribute("value", "");
                            newOption.innerHTML = "";
                            selectList.add(newOption);
                    for (var i = 0; i < jsonData.length; i++) {
                            var newOption = document.createElement('option');
                            newOption.setAttribute("value", jsonData[i].id);
                            newOption.innerHTML = jsonData[i].document_reference;
                            selectList.add(newOption);
                    }
                }
            });
            $("#exportExcel").on("click", function () {
                table.button('.buttons-excel').trigger();
            });

            $("#relocF8Yes").on("click", function () {
                var filter = "documentReference=" + $('#relocTask :selected').text() + "|AND|updatedBy=" + currentUser;
                console.log(filter)
                $.ajax({
                    url: callGetService + filter + "&function=writeRelocReturnMessage"+recieving,
                    type: 'GET',
                    success: function (response, textstatus) {
                    if (response === 'true') {
                        alert("Notification sent successfully.");
                        $('#missingSerialModal').modal('hide');
                        location.reload();
                    } else {
                        alert("Something went wrong. Please check with the administrator.");
                    }
                    }
                });
                
            });

            $('#example tbody').on('click', '#bProcess', function () {
                var data = table.row($(this).parents('tr')).data();

                var id = data.id+ '-'+data.part_number;
                var index = $.inArray(id, selected);

                if (index === -1) {
                    selected.push(id);
                } else {
                    selected.splice(index, 1);
                }
            });
            $('#example_filter label input').on("focus", function (event) {
                //console.log('Focus')
                $('#example').DataTable().ajax.reload(null, false);

            });
          $('#example tbody').on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = table.row(tr);
                var data = table.row(tr).data();


                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    // Open this row
                    if (table.row('.shown').length) {
                        $('.details-control', table.row('.shown').node()).click();
                    }

                    var rowID = data.id;
                    row.child(formatBody(row.data())).show();
                    tr.addClass('shown');
                    relocBodyTable(rowID);
                }
            });
            function relocBodyTable(rowID) {
                var table = $('#pBody').DataTable({
                    ajax: {"url": "../tableData/relocBodyTable.php", "data": {rowID: rowID}, "dataSrc": ""},
                    searching: false,
                    select: {
                        style: 'os',
                        selector: 'td:not(:first-child)'

                    },
                    paging: false,
                    info: false,
                    columns: [
                       
                        {data: "serial_reference"},
                        {data: "quantity"},
                        {data: "processed"}
                  

                    ],
                    order: [[1, 'asc']]
                });
            }
        
        });

    </script>
</body>
</html>



