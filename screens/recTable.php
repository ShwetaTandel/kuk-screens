<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Receipt Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
        <!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <?php
    include ('../config/phpConfig.php')
    ?>
    <body>
        <div class="modal fade bd-example-modal-sm" id="mConfUpload" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Confirm Upload</h5>
                    </div>
                    <br>
                    <div align="center">
                        Are you sure you would like to upload:<strong > <span id="conFile">#</span></strong></br>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="bYes"  data-dismiss="modal">yes</button>
                        <button type="button" class="btn btn-danger" id="bNo"  data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="mPrinterModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="reopenModalTitle">Choose a printer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-2 col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Select Printer<span style="color: red">*</span></label>
                            <select class="form-control" id="printer" >
                                <?php
                                include ('../config/phpConfig.php');
                                if (mysqli_connect_errno()) {
                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                }
                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.printer_mapping');
                                echo "<option value='-1'></option>";
                                while ($row = mysqli_fetch_array($result)) {
                                    echo '<option value="' . $row['printer_code'] . '">' . $row['printer_code'] . '</option>';
                                }
                                echo '';
                                mysqli_close($con);
                                ?>
                            </select>
                        </div>

                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-green" id="mPrinterSubmit" onclick="setPrinter()">SUBMIT</button>
                <button type="button" class="btn btn-gray" id="mCancel" data-dismiss="modal" >CANCEL</button>
            </div>
        </div>

    </div>
</div>
        <div class="modal fade bd-example-modal-sm" id="mFileDone" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">File Uploaded</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h3>File successfully uploaded</h3>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" data-dismiss="modal" onclick="window.location.reload()">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="confDel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete Document Header</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" align="center">
                        <strong id="sureMessage">Are you sure you want to delete?</strong>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="bYesDelete">Yes</button>
                        <button type="button" class="btn btn-secondary" id="bDelClose" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="confDelDet" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete Document Detail</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" align="center">
                        <strong id="sureMessages">Are you sure you want to delete?</strong>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="dYesDelete">Yes</button>
                        <button type="button" class="btn btn-secondary" id="dDelClose" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="pOpt" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Print Options</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" align="center">
                        <input type="text" id="printHelpcRef" style="display: none;">
                        <input type="text" id="printHelp" style="display: none;">
                        <input type="text" id="printHelp2" style="display: none;">
                        <input type="text" id="printStatus" style="display: none;">
                        <button style="width: 200px" class='btn btn-danger' id="printHaulReport" type='button'>GRN Barcode</button><br><br>
                        <button style="width: 200px"  class='btn btn-success' id="sumReport" type='button'>Scan Report Summary</button><br><br>
                        <button style="width: 200px" class='btn btn-warning' id="detailReport" type='button'>Scan Report Detail</button><br><br>

                        <button style="width: 200px" class='btn btn-info' id="shortageRep" type='button'>Shortage Receipt Report </button><br><br>
                        <button style="width: 200px" class='btn btn-primary' id="highValueParts" type='button'>HV Parts Receipt Report </button><br><br>
                       <!-- <button style="width: 200px" class='btn btn-danger' id="odetteLabel" type='button'>Print Odette Label </button>-->
                        <div class="col-md-12">
                                <div class="btn-group">
                                    <button type="button" style="width: 200px"  class="btn-danger btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" >
                                        Print Odette Label
                                    </button>
                                    <div class="dropdown-menu" style="width: 340px;">
                                       
                                         
                                <?php
                                include ('../config/phpConfig.php');
                                if (mysqli_connect_errno()) {
                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                }
                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.printer_mapping where printer_code like "receiving%"');
                               
                                while ($row = mysqli_fetch_array($result)) {
                                    echo '  <button class="dropdown-item btn-success" name="odetteprint" id="'.$row['printer_code'].'" >'.$row['printer_code'].'</button>';
                                }
                                echo '';
                                mysqli_close($con);
                                ?>
                          
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="printClose" class="btn btn-secondary" data-dismiss="modal" >Done</button>
                    </div>
                </div>
            </div>
        </div>
        <!--
                
            END
        
            OF 
            
            MODALS
        
        -->

        <?php
        include('../common/topNav.php');
        include('../common/sideBar.php');
        ?>
        <!-- Page Content  -->
        <div id="content">
            <br>



            <div id="headTable">  

                <table id="example" class="compact stripe hover row-border" style="width:100%">
                    <thead>

                    <th></th>

                    <th>GRN Reference</th>
                    <th>Expected Delivery Date</th> 
                    <th>Document Reference</th>
                    <th class="yes">GRN Type</th>
                    <th>Shortage</th>
                    <th>High Value</th>
                    <th>Vendor Code</th>
                    <th>Last Update Date</th>
                    <th>Last Updated By</th>
                    <th class="yes">Status</th>
                    <th>Print Options</th>
                    <th>Admin</th>
                    </thead>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th><input type="text" placeholder="Search GRN" /></th>
                            <th>EDD</th> 
                            <th><input type="text" placeholder="Search Document Reference" /></th>
                            <th><input type="text" placeholder="Search GRN Type" /></th>
                           <th>Shortage</th>
                            <th>High Value</th>
                            <th>Vendor Code <input type="text" placeholder="Search Vendor Code" /></th>
                            <th>Last Update Date</th>
                            <th><input type="text" placeholder="Search Updated By" /></th>
                            <th><input type="text" placeholder="Search Status" /></th>
                            <th>Print Options</th>
                            <th>Admin</th>

                    </tfoot>
                </table>


                <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>



            </div>
        </div>
        <!--/span-->
 

<script>
    function logOut() {

        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }

//the table set up for the Body
    function format(d) {
        // `d` is the original data object for the row
        return '<table id="rBody" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                '<thead>' +
                '<th></th>' +
                '<th>Part Number</th>' +
                '<th>Case Reference</th>' +
                '<th>Advised Qty</th>' +
                '<th>Received Qty</th>' +
                '<th>Difference</th>' +
                '<th>Line No</th>' +
                '<th>Quarantined Qty</th>' +
                '<th>Created By</th>' +
                '<th>Date Created</th>' +
                '<th>Last Updated By</th>' +
                '<th>Last Updated Date</th>' +
                '</thead>' +
                '</table>';
    }
    //table set up for detail
    function formatDetail(d) {
        // `d` is the original data object for the row
        return '<table id="rDetail" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                '<thead>' +
                '<th>Part Id</th>' +
                '<th>Ran / Order</th>' +
                '<th>Serial Reference</th>' +
                '<th>Advised Qty</th>' +
                '<th>Received Qty</th>' +
                '<th>cts Qty</th>' +
                '<th>Diffrence</th>' +
                '<th>Product Type</th>' +
                '<th>Created By</th>' +
                '<th>Date Created</th>' +
                '<th>Last Updated By</th>' +
                '<th>Last Updated Date</th>' +
                '<th></th>' +
                '</thead>' +
                '</table>';
    }

    $(document).ready(function () {
        //set the current user var
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>';
        //on load set the header table to hidden
        //   document.getElementById('headTable').style.display = "none";


        //simple function (not sure is needed here)... 
        // this will on clse of any modal will clear the data from the modal
        $('.modal').on('hidden.bs.modal', function (e) {
            $(this).removeData();
        });


        // ths is the set up for the header table 
        //more notes through out to explain diffrent sections...

        var table = $('#example').DataTable({
            //simple ajax to  php to grab data
            ajax: {"url": "../tableData/rHeadTable.php"}, //"dataSrc": ""
            //setting the default page length 
            iDisplayLength: 25,
            "processing": true,
            "serverSide": true,

            buttons: [
                {extend: 'excel', filename: 'Receipts_Table', title: 'Receipts'}

            ],
//             select: {
//                    style: 'multi',
//                    selector: 'td:not(:first-child)'
//
//                },
            // setting some rules for some colums this
            //these rules are for the end 3 colums pick with the 'targets' 1-(last),(-2),(-3)
            columnDefs: [{
                    //what column is being used
                    targets: -1,
                    //data to be in column
                    data: null,
                    //is this column orderable
                    orderable: false,
                    //what to but in the column by default here 
                    // there are 3 buttons in the column wrote in html surrounded by quotes 

                    defaultContent: "<input style='width: 70px' type='Button' id='bClose' class='btn btn-warning' value='Close'/><input style='width: 80px' type='Button' id='bRelease' class='btn btn-info' value='Release'/>"
                }, {
                    targets: -2,
                    data: null,
                    orderable: false,
                    defaultContent: "<input type='button' style='width: 75px' class='btn btn-success' id='pOptButton' value='Options'/>"
                }, 
                {
                    targets: [0,1,2,3,4,5,6,7,9,10,11,12],
                    orderable: false

                },
                {
                    targets: [0,2,5,6,8,11,12],
                    searchable: false

                }

            ],
            //what data will be in each column these are done by selected the same of the data
            //from the php data that has been asked for by thr ajax
            columns: [
                // this first column is the column we uses to click for the drop to the body 
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                //{data: "haulier_reference"},
                {data: "customer_reference"},
                {data: "expected_delivery_date"},
                {data: "document_reference"},
                {data: "grn_type"},
                //{data: "delivery_note"},
                // {data: "jis_ind",
                //   render: function (data, type, row) {
                //     return (data === 'D') ? 'XD' : data;
                //}
                //},
                {data: "shortages",
                    render: function (data, type, row) {
                        return (data > 0) ? '<span style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color: red;" class="glyphicon glyphicon-remove"></span>';
                    }},
                {data: "high_values",
                    render: function (data, type, row) {
                        return (data>0) ? '<span style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color: red;" class="glyphicon glyphicon-remove"></span>';
                    }},
                {data: "vendor_code"},
                {data: "last_updated_date"},
                {data: "last_updated_by"},
                {data: "status"},
                {data: ""},
                {data: ""}
            ],
            //set order by selecting the column by index here we are sleceting the last updated date
            order: [[8, 'desc'], [3, 'desc']]

        });
        table.columns().every(function() {
            var that = this;
            $('input', this.footer()).on('keyup change', function(e) {
                if (that.search() !== this.value) that.search(this.value);
                // if serverside draw() only on enter
                if (e.keyCode === 13 ) that.draw();
            });
        });

        //start of the file upload this will get the file name
        $("#my-file-selector").change(function (event) {
            var tmppath = URL.createObjectURL(event.target.files[0]);
            var fileName = document.getElementById('upload-file-info').value;
            $('#mConfUpload').modal('show');
            document.getElementById('conFile').innerHTML = fileName;

        });
        // if the correct file is selected they will press yes and start this function 
        $("#bYes").on('click', function () {
            var file_data = $('#my-file-selector').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);

            $.ajax({
                url: '../action/uploadASNfile.php',
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function (response) {
                    if (response.substr(response.length - 13) == 'File Uploaded') {
                        $('#mFileDone').modal('show');
                    } else {
                        alert(response);
                    }
                }
            });
        });

        //the extend of the excel button so we can use this to trigger making the excel sheet
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });





        $('#example tbody').on('click', '#bRelease', function () {
            var data = table.row($(this).parents('tr')).data();
            if (data.status !== 'QUARANTINE') {
                alert("The Receipt you have selected is not Quarantined.");
                return;
            }
            var yes = confirm("Are you sure you want to Realease the Document Receipt - " + data.document_reference);
            if (yes) {
                var filter = "documentReference=" + data.document_reference + "|AND|updatedBy=" + currentUser;
                console.log(filter)
                $.ajax({
                    url: callGetService + filter + "&function=releaseReceipt" + recieving,
                    type: 'GET',
                    success: function (response, textstatus) {
                        if (response === 'true') {
                            alert("The receipt is now open for scanning.");
                            $('#example').DataTable().ajax.reload();
                        } else {

                            alert("Something went wrong... Please try again.")
                        }
                    }
                });
            }



        });


        // function to close single headers
        $('#example tbody').on('click', '#bClose', function () {
            var data = table.row($(this).parents('tr')).data();
            //json for the header
            var obj = {"documentReference": data.document_reference, "userId": currentUser};
            var closeReceipt = JSON.stringify(obj);
            var filter = closeReceipt;
            // check to make sure the status is complete
            if (data.status === 'COMPLETE') {
                if (confirm("Are you sure you want to close Receipt " + data.document_reference + " ?")) {
                    $.ajax({
                        url: callPostService + filter + "&function=closeReceipt" + "&connection=" + recieving,
                        //url: "http://172.16.1.70:8081/vantec/closeReceipt?documentReference="+filter,
                        type: 'POST',
                        success: function (response, textstatus) {
                            var test = response;

                            if (test === 'OK - true') {
                                alert("Receipt is now closed.")
                                $('#example').DataTable().ajax.reload();

                            } else {
                                alert('Something went wrong... Please try again.')
                            }
                        }
                    });
                }
            } else {
                alert('The receipt you selected is not Complete.')
            }
            ;
        });


        //when the user click on the options button in the header it will open a pop up with 3 more buttons on them
        //these are for the print out options 
        $('#example tbody').on('click', '#pOptButton', function () {
            $('#pOpt').modal('show');
            data = table.row($(this).parents('tr')).data();
            document.getElementById('printHelpcRef').value = data.customer_reference;
            document.getElementById('printHelp').value = data.haulier_reference;
            document.getElementById('printHelp2').value = data.document_reference;
            document.getElementById('printStatus').value = data.status;
            // document.getElementById('printUser').value = data.
            //this is the print haulier barcode done by sending the reference to an external program written by Geoff
            $('#printHaulReport').click(function () {
                window.open(report + "customerref.php?customerreference=" + document.getElementById('printHelpcRef').value + "&documentreference=" + document.getElementById('printHelp2').value);
                table.ajax.reload(null, false);
            });

            //this is for a summar report of the full receipt by sending the reference to an external program written by Geoff
            $('#sumReport').click(function () {
                window.open(report + "receiptsummaryxl.php?documentreference=" + document.getElementById('printHelp2').value);
                table.ajax.reload(null, false);
            });
            //this on for a full detail report of the receipt by sending the reference to an external program written by Geoff
            $('#detailReport').click(function () {
                window.open(report + "receiptdetailxl.php?&documentreference=" + document.getElementById('printHelp2').value);
                table.ajax.reload(null, false);
            });

            $('#shortageRep').click(function () {
                window.open(report + "receiptShortageSummaryxl.php?custreference=" + document.getElementById('printHelpcRef').value) + connections;
                table.ajax.reload(null, false);
            });


            $('#highValueParts').click(function () {
                window.open(report + "receiptHVSummaryxl.php?custreference=" + document.getElementById('printHelpcRef').value) + connections;
                table.ajax.reload(null, false);
            });


        });
         $('button[name="odetteprint"]').click(function () {
             var printer = this.id;
             if (document.getElementById('printStatus').value !== "OPEN") {
                alert("The receipt should be in OPEN status, in order to print Odette labels.");

                return;
            }
            var filter = "grnReference=" + document.getElementById('printHelpcRef').value + "|AND|updatedBy=" + currentUser;
            console.log(filter)
            $.ajax({
                url: callGetService + filter + "&function=addDetailsForOdetteLabel" + recieving,
                type: 'GET',
                success: function (response, textstatus) {
                    if (response === 'true') {
                        $.ajax({
                            url: "../action/printLabels.php?function=reclabelpdf&filter=grn=" + document.getElementById('printHelpcRef').value  + "|AND|printer="+printer,
                            type: 'GET',
                            success: function (response, textstatus) {
                                if (response === 'OK  -recLabelPdf') {
                                    alert("The odette labels are successfully printed.");
                                        $('#pOpt').modal('hide');
                                } else {
                                    alert(response);
                                }
                            }
                        });
                        table.ajax.reload(null, false);
                    } else {
                        alert(response);
                    }
                }
            });
                     
         });
        $('#odetteLabel').click(function () {
           
          

        });

        //function to open uo the header to show the corrisponding body
        $('#example tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = table.row(tr).data();


            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                //close other open row before opening  a new row
                if (table.row('.shown').length) {
                    $('.details-control', table.row('.shown').node()).click();
                }
                var rowID = data.id;
                row.child(format(row.data())).show();
                tr.addClass('shown');
                recBodyTable(rowID);
            }
        });







        function recBodyTable(rowID) {
            var tableb = $('#rBody').DataTable({
                ajax: {"url": "../tableData/rBodyTable.php", "data": {rowID: rowID}, "dataSrc": ""},
                searching: false,
                paging: false,
                info: false,
                columns: [
                    {
                        "className": 'table-controls',
                        "orderable": false,
                        "data": null,
                        "defaultContent": ''
                    },
                    {data: "part_number"},
                    {data: "case_reference"},
                    {data: "advised_qty"},
                    {data: "received_qty"},
                    {data: "difference"},
                    {data: "line_no"},
                    {data: "quarantined_qty"},
                    {data: "created_by"},
                    {data: "date_created"},
                    {data: "last_updated_by"},
                    {data: "last_updated_date"}


                ],
                "createdRow": function (row, data, dataIndex) {


                    if (data.quarantined_qty > 0) {
                        $(row).css('background-color', '#e06e58');
                    }
                },
                order: [[0, 'asc']]
            });
            // function to open the body fields to show detail
            $('#rBody tbody').on('click', 'td.table-controls', function () {
                var tr = $(this).closest('tr');
                var row = tableb.row(tr);
                var data = tableb.row(tr).data();


                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    // Open this row
                    var rowID = data.id;
                    if (tableb.row('.shown').length) {
                        $('.table-controls', tableb.row('.shown').node()).click();
                    }

                    row.child(formatDetail(row.data())).show();
                    tr.addClass('shown');
                    recDetailTable(rowID);
                }
            });
        }



        function recDetailTable(rowID) {
            //alert(rowID)
            //create the table for the detail
            var detailTable = $('#rDetail').DataTable({
                ajax: {"url": "../tableData/rDetailTable.php", "data": {rowID: rowID}, "dataSrc": ""},
                searching: false,
                select: false,
                paging: false,
                info: false,
                columnDefs: [{
                        targets: -1,
                        data: null,
                        orderable: false,
                        defaultContent: "<input type='Button' id='dDelete' class='btn btn-danger' value='Delete'/>"
                    }

                ],
                columns: [
                    {data: "part_number"},
                    {data: "ran_order"},
                    {data: "serial_reference"},
                    {data: "advised_qty"},
                    {data: "received_qty"},
                    {data: "cts_qty"},
                    {data: "difference"},
                    {data: "product_type_code"},
                    {data: "created_by"},
                    {data: "date_created"},
                    {data: "last_updated_by"},
                    {data: "last_updated_date"},
                    {data: ""}

                ]

            });
            // function to delete fro the RCP detail 
            $('#rDetail tbody').on('click', '#dDelete', function () {
                var detDocRef = '';
                var detPartNum = '';
                var detSeriRef = '';



                // create the JSON string
                var obj = '';


                $('#confDelDet').modal('show');
                //pop up to confirm that theu want to delete 
                document.getElementById('sureMessages').innerHTML = "Are you sure you want to delete?";
                document.getElementById('dDelClose').innerHTML = "No";
                document.getElementById('dYesDelete').style.display = "inline";

                var table = $('#rDetail').DataTable();

                var data = table.row($(this).parents('tr')).data();
                // alert(data.part_number);
                detDocRef = data.document_reference;
                detPartNum = data.part_number;
                detSeriRef = data.serial_reference;



                // create the JSON string
                obj = {"documentReference": detDocRef, "partNumber": detPartNum, "serialReference": detSeriRef, 'currentUser': currentUser};
                var deleteDetail = JSON.stringify(obj);
                var filter = deleteDetail;
                //if they have confirmed that they want to delte run the ajax to delete
                $('#dYesDelete').unbind().click(function () {
                    //console.log(filter)
                    $.ajax({
                        url: callPostService + filter + "&function=deleteDocumentDetail" + recieving,
                        type: 'POST',
                        success: function (response, textstatus) {

                            if (response === 'OK - true') {
                                //if successfull pop up changes to confirm the deletion 
                                document.getElementById('sureMessages').innerHTML = "Detail Deleted";
                                document.getElementById('dDelClose').innerHTML = "Close";
                                document.getElementById('dYesDelete').style.display = "none";
                                $('#rDetail').DataTable().ajax.reload();
                                $('#rBody').DataTable().ajax.reload();
                            } else {
                                //if fail it will display the server error
                                alert('Unable to Delete');
                                $('#confDelDet').modal('hide');
                            }
                        }
                    });
                });
            });
        }


    });

</script>
</body>
</html>



