<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Column Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="modal fade" id="mCreateNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New Aisle</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Column Code:</label>
                                    <div class="controls">
                                        <input type="text" name="nColumnCode" id="nColumnCode" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Column Description:</label>
                                    <div class="controls">
                                        <input type="text" name="nColumnDescription" id="nColumnDescription" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Filling Priority:</label>
                                    <div class="controls">
                                        <input type="text" name="nFillingPriority" id="nFillingPriority" class="form-control" >
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="newColumnButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confCreate" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Column Added</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >New Column Added</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal">Ok</button>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="mEditColumn" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New Aisle</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <input type="text" name="eID" id="eID" class="form-control" style="display: none">
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Column Code:</label>
                                    <div class="controls">
                                        <input type="text" name="nColumnCode" id="eColumnCode" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Column Description:</label>
                                    <div class="controls">
                                        <input type="text" name="nColumnDescription" id="eColumnDescription" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Filling Priority:</label>
                                    <div class="controls">
                                        <input type="text" name="nFillingPriority" id="eFillingPriority" class="form-control" >
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-danger"  style="float: left;" id="bDelete">Delete</button>
                        <button type="button" class="btn btn-success" id="editColumnButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Column Edited</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Column Edited</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" >Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confDelete" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete: <strong><span id="delMessage2">*</span></strong></h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Are you sure you want to delete <strong><span id="delMessage">*</span></strong></h4>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="conDel">Yes</button>
                        <button type="button" class="btn btn-danger"  data-dismiss="modal" >No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="doneDel" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Column Deleted</h4>
                    </div>

                    <div class="modal-footer" >
                        `                  <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="errorDel" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Column unable to be deleted</h4>
                    </div>

                    <div class="modal-footer" >
                        `                  <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>











<?php
include('../common/topNav.php');
include('../common/sideBar.php');
?>
        <!-- Page Content  -->
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th>Column Code</th>
                        <th>Description</th>
                        <th>Filling Priority</th>
                        <th>Created By</th>
                        <th>Date Created</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th></th>
                </thead>
                <tfoot>
                    <tr>
                        <th>Column Code</th>
                        <th>Description</th>
                        <th>Filling Priority</th>
                        <th>Created By</th>
                        <th>Date Created</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th></th>
                </tfoot>
            </table>

            <input type="Button" id="bCreateNew" class="btn btn-warning" value="Create New"/>
            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

        </div>

        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->

<script>


    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }

    $(document).ready(function () {
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>'


        var table = $('#example').DataTable({
            ajax: {"url": "../tableData/columnTable.php", "dataSrc": ""},
            columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bEdit' class='btn btn-warning' value='Edit'/>"
                }
            ],
            buttons: [
                {extend: 'excel', filename: 'Column', title: 'Column'}
            ],
            columns: [
                {data: "column_code"},
                {data: "column_description"},
                {data: "filling_priority"},
                {data: "created_by"},
                {data: "date_created"},
                {data: "last_updated_date"},
                {data: "last_updated_by"},
                {data: ""}
            ],
            order: [[0, 'asc']]
        });
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });



        $("#bCreateNew").on("click", function () {
            $('#mCreateNew').modal('show');
        });

     $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });

        $("#newColumnButton").on("click", function () {

            var newCode = document.getElementById('nColumnCode').value;
            var newDesc = document.getElementById('nColumnDescription').value;
            var newFill = document.getElementById('nFillingPriority').value;

            if (newCode.length === 0) {
                alert('Column Code Cannot Be Blank');
                return;
            }
            if (newFill.length === 0) {
                alert('Filling Priority Cannot Be Blank');
                return;
            }

            $('#mCreateNew').modal('hide');

            var obj = {"columnCode": newCode, "columnDescription": newDesc, "fillingPriority": newFill, "createdBy": currentUser, "updatedBy": currentUser};
            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            console.log(filter)


            $.ajax({
                url: callPostService +filter + "&function=createColumn" + screen,
                type: 'GET',
                success: function (response, textstatus) {
                    $('#confCreate').modal('show');
                    table.ajax.reload(null, false);
                }
            });
        });

        $('#example tbody').on('click', '#bEdit', function () {

            $('#mEditColumn').modal('show');
            var data = table.row($(this).parents('tr')).data();
            document.getElementById('eID').value = data.id;
            document.getElementById('eColumnCode').value = data.column_code
            document.getElementById('eColumnDescription').value = data.column_description;
            document.getElementById('eFillingPriority').value = data.filling_priority;
        });

        $("#editColumnButton").on("click", function () {

            var newID = document.getElementById('eID').value;
            var newCode = document.getElementById('eColumnCode').value;
            var newDesc = document.getElementById('eColumnDescription').value;
            var newFill = document.getElementById('eFillingPriority').value;

            if (newCode.length === 0) {
                alert('Column Code Cannot Be Blank');
                return;
            }
            if (newFill.length === 0) {
                alert('Filling Priority Cannot Be Blank');
                return;
            }

            $('#mEditColumn').modal('hide');

            var obj = {"id": newID, "columnCode": newCode, "columnDescription": newDesc, "fillingPriority": newFill, "createdBy": currentUser, "updatedBy": currentUser};
            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            console.log(filter)


            $.ajax({
                url: callPostService +filter + "&function=editColumn" + screen,
                type: 'GET',
                success: function (response, textstatus) {
                    $('#confEdit').modal('show');
                    table.ajax.reload(null, false);
                }
            });
        });

        $("#bDelete").on("click", function () {

            $('#confDelete').modal('show');
            document.getElementById("delMessage").innerHTML = document.getElementById('eColumnCode').value
            document.getElementById("delMessage2").innerHTML = document.getElementById('eColumnCode').value



        });

        $("#conDel").on("click", function () {
            var columnID = document.getElementById('eID').value;
            //   alert('DELETE: ' + rowID);

            var filter = 'columnId=' + columnID;

            $.ajax({
                url: callGetService +filter+ "&function=deleteColumn" +  screen,
                type: 'GET',
                success: function (response, textstatus) {
                    if (response.substring(0, 5) == 'FAIL-') {
                        $('#mEditColumn').modal('hide');
                        $('#confDelete').modal('hide');
                        $('#errorDel').modal('show');
                    } else {
                        $('#mEditColumn').modal('hide');
                        $('#confDelete').modal('hide');
                        $('#doneDel').modal('show');
                    }


                }
            });
        });



    });

</script>
</body>
</html>



