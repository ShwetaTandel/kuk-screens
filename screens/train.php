<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Trains Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="modal fade" id="mCreateNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New Train</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Train Code:</label>
                                    <div class="controls">
                                        <input type="text" name="nTrainCode" id="nTrainCode" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Train Description:</label>
                                    <div class="controls">
                                        <input type="text" name="nTrainDescription" id="nTrainDescription" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                            <label class="input-group-text">Order Type:</label>
                                            <select class="form-control" type="text" id="nOrderType">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
// Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT distinct (order_type_code) FROM ' . $mDbName . '.order_type where order_type_code not in ("INSPECT")');
                                                echo "<option value='-1'></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['order_type_code'] . '">' . $row['order_type_code'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>

                            </div>
                           
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="newTrainButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confCreate" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Train Added</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >New Train Added</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="mEditTrain" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Train</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <input type="text" name="eID" id="eID" class="form-control" style="display: none;">
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Train Code:</label>
                                    <div class="controls">
                                        <input type="text" name="eTrainCode" id="eTrainCode" class="form-control" >
                                    </div>
                                </div>
                                    <div class="control-group">
                                    <label class="input-group-text">Train Description:</label>
                                    <div class="controls">
                                        <input type="text" name="eTrainDescription" id="eTrainDescription" class="form-control" >
                                    </div>
                                </div>
                              <div class="control-group">
                                            <label class="input-group-text">Order Type:</label>
                                            <select class="form-control" type="text" id="eOrderType">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
// Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT distinct (order_type_code) FROM ' . $mDbName . '.order_type where order_type_code not in ("INSPECT")');
                                                echo "<option value='-1'></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['order_type_code'] . '">' . $row['order_type_code'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                              
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-danger"  style="float: left;" id="bDelete">Delete</button>
                        <button type="button" class="btn btn-success" id="editTrainButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Train Edited</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Train Edited</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confCreate" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Aisle Added</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >New Aisle Added</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-sm" id="confEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Aisle Edited</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Aisle Edited</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confDelete" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete: <strong><span id="delMessage2">*</span></strong></h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Are you sure you want to delete <strong><span id="delMessage">*</span></strong></h4>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="conDel">Yes</button>
                        <button type="button" class="btn btn-danger"  data-dismiss="modal" >No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="doneDel" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Train Deleted</h4>
                    </div>

                    <div class="modal-footer" >
                       <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="errorDel" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Train unable to be deleted</h4>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>

            <?php
            include('../common/topNav.php');
            include('../common/sideBar.php');
            ?>

        <!-- Page Content  -->
        <div id="content">
            <br>
            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>Train Code</th>
                        <th>Description</th>
                            <th>Order Type</th>
                        <th>Date Created</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th></th>
                </thead>
                <tfoot>
                    <tr>
                        <th></th>
                        <th>Train Code</th>
                        <th>Description</th>
                          <th>Order Type</th>
                        <th>Date Created</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th></th>
                </tfoot>
            </table>

            <input type="Button"  id="bCreateNew" class="btn btn-warning" value="Create New"/>
            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

        </div>
        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->

<script>


    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }
    function format(d) {
        // `d` is the original data object for the row
        return '<table id="rTrolley" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                '<thead>' +
                '<th></th>' +
                '<th>Trolley</th>' +
                '<th>Sequence</th>' +
                '<th></th>' +
                '</thead>' +
                '</table>';
    }
    //table set up for detail
    function formatDetail(d) {
        // `d` is the original data object for the row
        return '<table id="rZoneDest" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                '<thead>' +
                '<th>Zone Destination</th>' +
                '<th></th>' +
                '</thead>' +
                '</table>';
    }

    $(document).ready(function () {
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>';

        var table = $('#example').DataTable({
            ajax: {"url": "../tableData/trainTable.php", "dataSrc": ""},
            columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bEdit' class='btn btn-warning' value='Edit'/>"
                }
            ],
            buttons: [
                {extend: 'excel', filename: 'train_type', title: 'Train'}
            ],
            columns: [
            {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {data: "train"},
                {data: "description"},
                 {data: "order_type_code"},
                {data: "date_created"},
                {data: "last_updated"},
                {data: "last_updated_by"},
                {data: ""}
            ],
            order: [[0, 'asc']]
        });
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });

     $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });


        $("#bCreateNew").unbind().on("click", function () {
            $('#mCreateNew').modal('show');
        });
        $("#newTrainButton").unbind().on("click", function () {

            //  var newType = document.getElementById('nChargeType').value;
            var newCode = document.getElementById('nTrainCode').value;
            var newDesc = document.getElementById('nTrainDescription').value;
            var orderType = document.getElementById('nOrderType').value;

        // var newPlttrain = document.getElementById('nPlttrain').checked ? true:false;

            if(newCode.length === 0 ){
                alert('Train Code Cannot Be Blank');
                return;
            }
            
            $('#mCreateNew').modal('hide');

            var obj = {"trainCode": newCode, "orderType" : orderType,  "trainDescription": newDesc, "createdBy": currentUser, "updatedBy": currentUser};
            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            console.log(filter);


            $.ajax({
                url: callPostService + filter  + "&function=createTrain" + screen,
                type: 'GET',
                success: function (response, textstatus) {
                    if(response === 'OK - true'){
                    $('#confCreate').modal('show');
                }else{
                    alert('Unable to Add');
                }
                }
            });
        });
        $('#example tbody').unbind().on('click', '#bEdit', function () {

            $('#mEditTrain').modal('show');
            var data = table.row($(this).parents('tr')).data();
            document.getElementById('eID').value = data.id;
            document.getElementById('eTrainCode').value = data.train;
            document.getElementById('eTrainDescription').value = data.description;
             document.getElementById('eOrderType').value = data.order_type_code;
         
        });
 
        $("#editTrainButton").unbind().on("click", function () {

            var newID = document.getElementById('eID').value;
            var newCode = document.getElementById('eTrainCode').value;
            var newDesc = document.getElementById('eTrainDescription').value;
            
        var orderType = document.getElementById('eOrderType').value;
            
            if(newCode.length === 0 ){
                alert('Train Code Cannot Be Blank');
                return;
            }
            

            $('#mEditTrain').modal('hide');

            var obj = {"id": newID, "trainCode": newCode, "trainDescription": newDesc, "updatedBy": currentUser, "orderType":orderType};
            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            console.log(filter);


            $.ajax({
                url: callPostService + filter  + "&function=editTrain" + screen,
                type: 'GET',
                success: function (response, textstatus) {
                    if(response === 'OK - true'){
                    $('#confEdit').modal('show');
                }else{
                    alert('Unable to Edit');
                }
                }
                
            });
        });
        $("#bDelete").unbind().on("click", function () {

            $('#confDelete').modal('show');
            document.getElementById("delMessage").innerHTML = document.getElementById('eTrainCode').value;
            document.getElementById("delMessage2").innerHTML = document.getElementById('eTrainCode').value;

        });

        $("#conDel").unbind().on("click", function () {
            var trainID = document.getElementById('eID').value;
            //   alert('DELETE: ' + rowID);

            var filter = 'trainId=' + trainID;

            $.ajax({
                url: callGetService + filter + "&function=deleteTrain"  + screen,
                type: 'GET',
                success: function (response, textstatus) {
                    if (response === 'true') {
                        $('#mEditTrain').modal('hide');
                        $('#confDelete').modal('hide');
                        $('#doneDel').modal('show');
                        
                    } else {
                        $('#mEditTrain').modal('hide');
                        $('#confDelete').modal('hide');
                        $('#errorDel').modal('show');
                        
                    }
                }
            });
        });
        
        
        
          $('#example tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = table.row(tr).data();


            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                //close other open row before opening  a new row
                if (table.row('.shown').length) {
                    $('.details-control', table.row('.shown').node()).click();
                }
                var rowID = data.id;
                row.child(format(row.data())).show();
                tr.addClass('shown');
                trolleyTable(rowID);
            }
        });
        
        function trolleyTable(rowID) {
            var tableb = $('#rTrolley').DataTable({
                ajax: {"url": "../tableData/trolleyTable.php", "data": {rowID: rowID}, "dataSrc": ""},
                searching: false,
                paging: false,
                info: false,
                columnDefs: [{
                        targets: -1,
                        data: null,
                        orderable: false,
                        defaultContent: "<input type='Button' id='tEdit' class='btn btn-danger' value='Edit'/>"
                    }

                ],
                columns: [
                    {
                        "className": 'table-controls',
                        "orderable": false,
                        "data": null,
                        "defaultContent": ''
                    },
                    {data: "trolley"},
                    {data: "sequence"},
                     {data: ""}

                ],
                
                order: [[0, 'asc']]
            });
              $('#rTrolley tbody').on('click', 'td.table-controls', function () {
                var tr = $(this).closest('tr');
                var row = tableb.row(tr);
                var data = tableb.row(tr).data();


                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    // Open this row
                    var rowID = data.id;
                    if (tableb.row('.shown').length) {
                        $('.table-controls', tableb.row('.shown').node()).click();
                    }

                    row.child(formatDetail(row.data())).show();
                    tr.addClass('shown');
                    zoneDestTable(rowID);
                }
            });
        }
        
        function zoneDestTable(rowID) {
            //alert(rowID)
            //create the table for the detail
            var detailTable = $('#rZoneDest').DataTable({
                ajax: {"url": "../tableData/zoneDestinationTable.php", "data": {rowID: rowID}, "dataSrc": ""},
                searching: false,
                select: false,
                paging: false,
                info: false,
                columnDefs: [{
                        targets: -1,
                        data: null,
                        orderable: false,
                        defaultContent: "<input type='Button' id='zoneEdit' class='btn btn-danger' value='Edit'/>"
                    }

                ],
                columns: [
                    {data: "zone_destination"},
               
                    {data: ""}

                ]

            });
            }
    
    });

</script>
</body>
</html>



