<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>JIS Receipt Sacn Data</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>

        <?php
        include('../common/topNav.php');
        include('../common/sideBar.php');
        ?>
        <!-- Page Content  -->
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th>Pallet</th>
                        <th>Ran Order</th>
                        <th>Part Number</th>
                        <th>Serial Reference</th>
                        <th>Haulier Reference</th>
                        <th>Location</th>
                        <th>Scan Qty</th>
                        <th>Delivery Note</th>
                        <th>Created By</th>
                        <th>Date Created</th>
                        <th></th>
                </thead>
                <tfoot>
                    <tr>
                        <th>Pallet</th>
                        <th>Ran Order</th>
                        <th>Part Number</th>
                        <th>Serial Reference</th>
                        <th>Haulier Reference</th>
                        <th>Location</th>
                        <th>Scan Qty</th>
                        <th>Delivery Note</th>
                        <th>Created By</th>
                        <th>Date Created</th>
                        <th></th>
                </tfoot>
            </table>

            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

        </div>

        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->

<script>
    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }

    $(document).ready(function () {
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>'


        var table = $('#example').DataTable({
            ajax: {"url": "../tableData/scanDataTable.php", "dataSrc": ""},
            columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bDelete' class='btn btn-warning' value='Delete'/>"
                }
            ],
            buttons: [
                {extend: 'excel', filename: 'scanData', title: 'scanData'}
            ],
            columns: [
                {data: "plt_reference"},
                {data: "ran_order"},
                {data: "part_number"},
                {data: "serial_reference"},
                {data: "haulier_reference"},
                {data: "location_code"},
                {data: "scan_qty"},
                {data: "delivery_note"},
                {data: "created_by"},
                {data: "date_created"},
                {data: ""}
            ],
            order: [[0, 'desc']]
        });
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });
        $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });

        $('#example tbody').on('click', '#bDelete', function () {
            var r = confirm("Are You Sure?");
            if (r == true) {


                var tr = $(this).closest('tr');
                var id = table.row(tr).data().id;

                var filter = 'id=' + id;

                $.ajax({
                    url: callGetService + filter + "&function=deleteScanData" + screen,
                    type: 'GET',
                    success: function (response, textstatus) {
                        $('#example').DataTable().ajax.reload(null, false);


                    }
                });


            }
        });







    });

</script>
</body>
</html>



