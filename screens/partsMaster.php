<?php
// Modified - 2019-12-19 - Added relative address for PHPValidate/validaterdt.php
header('Cache-Control: no-cache, no-store, must-revalidate');   
header('Expires: 0');
// Header to allow Cross-Server AJAX Connection
header('Access-Control-Allow-Origin: *'); 
header('Expires: 0');

include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Parts Master Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <script src="../js/libs/jquery-ui-1.12.1.custom/jquery-ui.js" type="text/javascript"></script>
        <link href="../js/libs/jquery-ui-1.12.1.custom/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../js/libs/mask/jquery.mask.js" type="text/javascript"></script>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="modal fade right" id="mCreateNew" role="dialog" aria-labelledby="viewEdit" aria-hidden="true" >
            <div class="modal-dialog modal-fluid" role="document" style="width: 1000px">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalPreviewLabel">Part Master</h5>
                    </div>
                    <div class="modal-body">
                        <div class=" col-md-12">
                            <div id="nPage1">
                                <div class="row">
                                    <!-- Half of the modal-body div-->
                                    <div class="col-xs-6">
                                        <div class="control-group">
                                            <label class="input-group-text">Part Number:</label>
                                            <div class="controls">
                                                <input type="text"  style="text-transform:uppercase" name="nPartNumber" id="nPartNumber" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Part Description:</label>
                                            <div class="controls">
                                                <input type="text" name="nPartDescription" id="nPartDescription" class="form-control">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Vendor:</label>
                                            <select class="form-control" type="text" id="nVendorCode">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
                                                // Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.vendor;');
                                                echo "<option value='0' selected></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['vendor_name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Effective From:</label>
                                            <div class="controls">
                                                <input type="text" name="nEffectiveFrom" id="nEffectiveFrom" class="form-control date_time">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Effective To:</label>
                                            <div class="controls">
                                                <input type="text" name="nEffectiveTo" id="nEffectiveTo" class="form-control date_time">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Zone Type:</label>
                                            <div class="controls">
                                                <select class="form-control" type="text" id="nZoneType">                                                                  
                                                    <?php
                                                    include ('../config/phpConfig.php');
                                                    // Check connection
                                                    if (mysqli_connect_errno()) {
                                                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                    }
                                                    $result = mysqli_query($con, 'SELECT * FROM location_sub_type order by location_sub_type.location_sub_type_code asc;');
                                                    echo "<option value='0' selected></option>";
                                                    while ($row = mysqli_fetch_array($result)) {
                                                        echo '<option value="' . $row['id'] . '">' . $row['location_sub_type_code'] . '</option>';
                                                    }
                                                    echo '';
                                                    mysqli_close($con);
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Other half of the modal-body div-->
                                    <div class="col-xs-6">
                                        <div class="control-group">
                                            <label class="input-group-text">Pack Type:</label>
                                            <select class="form-control" type="text" id="nPackType">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
                                                // Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.pack_type;');
                                                echo "<option value=\"$value\"></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Jis Supply Group:</label>
                                            <div class="controls">
                                                <input type="text" name="nJisSupply" id="nJisSupply" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">WI Code:</label>
                                            <div class="controls">
                                                <input type="text" name="nWiCode" id="nWiCode" class="form-control" maxlength="10">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Fixed Location:</label>
                                            <div class="controls">
                                                <input type="text" name="nFixedLocation" id="nFixedLocation" class="form-control">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Location Type:</label>
                                            <div class="controls">
                                                <select class="form-control" type="text" id="nLocationType">                                                                  
                                                    <?php
                                                    include ('../config/phpConfig.php');
                                                    // Check connection
                                                    if (mysqli_connect_errno()) {
                                                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                    }
                                                    $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.location_type order by location_type_code asc;');
                                                    echo "<option value=\"$value\"></option>";
                                                    while ($row = mysqli_fetch_array($result)) {
                                                        echo '<option value="' . $row['id'] . '">' . $row['location_type_code'] . '</option>';
                                                    }
                                                    echo '';
                                                    mysqli_close($con);
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Weight:</label>
                                            <div class="controls">
                                                <input type="number" name="nWeight" id="nWeight" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Stack Height:</label>
                                            <div class="controls">
                                                <input type="text" name="nStackHeight" id="nStackHeight" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <!-- Half of the modal-body div-->
                                    <div class="col-xs-16">
                                        <div class="col-xs-3">
                                            <label class="input-group-text">Count on Receipt:</label>
                                            <div class="controls">
                                                <input type="checkbox" name="nCountOnRcpt" id="nCountOnRcpt" class="input">
                                            </div>
                                        </div>

                                        <div class="col-xs-3">
                                            <label class="input-group-text">Decant</label>
                                            <div class="controls">
                                                <input type="checkbox" name="nDecant" id="nDecant" class="input">
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="input-group-text">Inspect</label>
                                            <div class="controls">
                                                <input type="checkbox" name="nInspect" id="nInspect" class="input">
                                            </div>
                                        </div>
                                        <div id="nIall">
                                            <div class="col-xs-3" >
                                                <label class="input-group-text">Include current inventory?</label>
                                                <div class="controls">
                                                    <input type="checkbox" name="nInspectAll" id="nInspectAll" class="input">
                                                </div>
                                            </div>
                                        </div>
                                       
                                       
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <!-- Half of the modal-body div-->
                                    <div class="col-xs-16">
                                         <div class="col-xs-3">
                                            <label class="input-group-text">Is Full Box pick?</label>
                                            <div class="controls">
                                                <input type="checkbox" name="nIsFullBoxPick" id="nIsFullBoxPick" class="input">
                                            </div>
                                        </div>
                                         <div class="col-xs-3">
                                            <label class="input-group-text">High Value</label>
                                            <div class="controls">
                                                <input type="checkbox" name="nHighValue" id="nHighValue" class="input">
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="input-group-text">Auto Receive</label>
                                            <div class="controls">
                                                <input type="checkbox" name="nAutoRecieve" id="nAutoRecieve" class="input">
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="input-group-text">Auto Pick</label>
                                            <div class="controls">
                                                <input type="checkbox" name="nAutoPick" id="nAutoPick" class="input">
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <br/>
                                   <div class="row">
                                    <!-- Half of the modal-body div-->
                                    <div class="col-xs-16">
                                        
                                        <div class="col-xs-3">
                                            <label class="input-group-text">Update Serial With Count</label>
                                            <div class="controls">
                                                <input type="checkbox" name="nUpdateWithSerial" id="nUpdateWithSerial" class="input">
                                            </div>
                                        </div>
                                         <div class="col-xs-3">
                                            <label class="input-group-text">Is Modular?</label>
                                            <div class="controls">
                                                <input type="checkbox" name="nModular" id="nModular" class="input">
                                            </div>
                                        </div>
                                         <div class="col-xs-3">
                                            <label class="input-group-text">Is Big Part?</label>
                                            <div class="controls">
                                                <input type="checkbox" name="nBigPart" id="nBigPart" class="input">
                                            </div>
                                        </div>
                                         <div class="col-xs-3">
                                            <label class="input-group-text">Is AS400?</label>
                                            <div class="controls">
                                                <input type="checkbox" name="nAS400" id="nAS400" class="input">
                                            </div>
                                        </div>
                                    </div></div>
                                <hr>
                                <div class="row">
                                    <!-- Half of the modal-body div-->
                                    <div class="col-xs-6">
                                        <div class="control-group">
                                            <label class="input-group-text">Min Storage:</label>
                                            <div class="controls">
                                                <input type="number" name="nMinStorage" id="nMinStorage" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Shelf Life:</label>
                                            <div class="controls">
                                                <input type="number" name="nShelfLife" id="nShelfLife" class="form-control">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">RDT Message:</label>
                                            <div class="controls">
                                                <input type="text" name="nRdtMessage" id="nRdtMessage" class="form-control" maxlength="50">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="control-group">
                                            <label class="input-group-text">Part Family:</label>
                                            <div class="controls">
                                                <input type="text" name="nPartFamily" id="nPartFamily" class="form-control">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Loose PCS Equal To:</label>
                                            <div class="controls">
                                                <input type="number" name="nLoosePcs" id="nLoosePcs" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="nPage2" style="display: none;">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Decant Charge:</label>
                                        <div class="controls">
                                            <select class="form-control" type="text" id="nDecantCharge">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
                                                // Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.charge_master where charge_type_id = "1";');
                                                echo "<option value='0' selected></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['charge_name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Despatch Charge:</label>
                                        <div class="controls">
                                            <select class="form-control" type="text" id="nDespCharge">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
                                                // Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.charge_master where charge_type_id = "4";');
                                                echo "<option value='0' selected></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['charge_name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Inspect Charge:</label>
                                        <div class="controls">
                                            <select class="form-control" type="text" id="nInspCharge">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
                                                // Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.charge_master where charge_type_id = "2";');
                                                echo "<option value='0' selected></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['charge_name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Receipt Charge:</label>
                                        <div class="controls">
                                            <select class="form-control" type="text" id="nReceCharge">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
                                                // Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.charge_master where charge_type_id = "4";');
                                                echo "<option value='0' selected></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['charge_name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Storage Charge:</label>
                                        <div class="controls">
                                            <select class="form-control" type="text" id="nStorCharge">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
                                                // Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.charge_master where charge_type_id = "4";');
                                                echo "<option value='0' selected></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['charge_name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Transport Charge:</label>
                                        <div class="controls">
                                            <select class="form-control" type="text" id="nTrnspCharge">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
                                                // Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.charge_master where charge_type_id = "3";');
                                                echo "<option value='0' selected'></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['charge_name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            <button type="button" id="saveNew" class="btn btn-success">Create</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- conf new Part -->
        <div class="modal fade bd-example-modal-sm" id="confNewPart" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">New Part</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >New Part Added</strong>
                    </div>
                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" data-dismiss="modal">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- conf Edit Part -->
        <div class="modal fade bd-example-modal-sm" id="confEditPart" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Part</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Part Edited</strong>
                    </div>
                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" data-dismiss="modal">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade right" id="mEditPart" role="dialog" aria-labelledby="viewEdit" aria-hidden="true" >
            <div class="modal-dialog modal-fluid" role="document" style="width: 1000px">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalPreviewLabel">Part Master</h5>
                    </div>
                    <div class="modal-body">
                        <div class=" col-md-12">
                            <div id="ePage1">
                                <div class="row">
                                    <!-- Half of the modal-body div-->
                                    <div class="col-xs-6">
                                        <div class="control-group">
                                            <input type="text" id="Eid" style="display: none">
                                            <label class="input-group-text">Part Number:</label>
                                            <div class="controls">
                                                <input type="text" name="ePartNumber" id="ePartNumber" class="form-control" disabled>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Part Description:</label>
                                            <div class="controls">
                                                <input type="text" name="ePartDescription" id="ePartDescription" class="form-control">
                                            </div>
                                        </div>
                                       
                                        <div class="control-group">
                                            <label class="input-group-text">Vendor:</label>
                                            <select class="form-control" type="text" id="eVendorCode">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
                                                // Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.vendor;');
                                                echo "<option value='0' selected></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['vendor_name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Effective From:</label>
                                            <div class="controls">
                                                <input type="text" name="eEffectiveFrom" id="eEffectiveFrom" class="form-control date_time">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Effective To:</label>
                                            <div class="controls">
                                                <input type="text" name="eEffectiveTo" id="eEffectiveTo" class="form-control date_time">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Zone Type:</label>
                                            <div class="controls">
                                                <select class="form-control" type="text" id="eZoneType">                                                                  
                                                    <?php
                                                    include ('../config/phpConfig.php');
                                                    // Check connection
                                                    if (mysqli_connect_errno()) {
                                                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                    }
                                                    $result = mysqli_query($con, 'SELECT * FROM location_sub_type order by location_sub_type.location_sub_type_code asc;');
                                                    echo "<option value='0' selected></option>";
                                                    while ($row = mysqli_fetch_array($result)) {
                                                        echo '<option value="' . $row['id'] . '">' . $row['location_sub_type_code'] . '</option>';
                                                    }
                                                    echo '';
                                                    mysqli_close($con);
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                         <div class="control-group">
                                            <label class="input-group-text">Vendor Name:</label>
                                            <div class="controls">
                                                <input type="text" name="eVendorName" id="eVendorName" class="form-control" disabled="true">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Other half of the modal-body div-->
                                    <div class="col-xs-6">
                                        <div class="control-group">
                                            <label class="input-group-text">Pack Type:</label>
                                            <select class="form-control" type="text" id="ePackType">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
                                                // Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.pack_type;');
                                                echo "<option value=\"$value\"></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Jis Supply Group:</label>
                                            <div class="controls">
                                                <input type="text" name="eJisSupply" id="eJisSupply" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">WI Code:</label>
                                            <div class="controls">
                                                <input type="text" name="eWiCode" id="eWiCode" class="form-control" maxlength="10">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Fixed Location:</label>
                                            <div class="controls">
                                                <input type="text" name="eFixedLocation" id="eFixedLocation" class="form-control">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Location Type:</label>
                                            <div class="controls">
                                                <select class="form-control" type="text" id="eLocationType">                                                                  
                                                    <?php
                                                    include ('../config/phpConfig.php');
                                                    // Check connection
                                                    if (mysqli_connect_errno()) {
                                                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                    }
                                                    $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.location_type  order by location_type_code asc;');
                                                    echo "<option value=\"$value\"></option>";
                                                    while ($row = mysqli_fetch_array($result)) {
                                                        echo '<option value="' . $row['id'] . '">' . $row['location_type_code'] . '</option>';
                                                    }
                                                    echo '';
                                                    mysqli_close($con);
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Weight:</label>
                                            <div class="controls">
                                                <input type="number" name="eWeight" id="eWeight" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Stack Height: </label>
                                            <div class="controls">
                                                <input type="number" name="eStackHeight" id="eStackHeight" class="form-control" >
                                            </div>
                                        </div>


                                    </div>



                                </div>
                                <hr>
                                <div class="row">
                                    <!-- Half of the modal-body div-->
                                    <div class="col-xs-16">
                                        <div class="col-xs-3">
                                            <label class="input-group-text">Count on Receipt:</label>
                                            <div class="controls">
                                                <input type="checkbox" name="eCountOnRecpt" id="eCountOnRecpt" class="input">
                                            </div>
                                        </div>

                                        <div class="col-xs-3">
                                            <label class="input-group-text">Decant</label>
                                            <div class="controls">
                                                <input type="checkbox" name="eDecant" id="eDecant" class="input">
                                            </div>
                                        </div>

                                        <div class="col-xs-3">
                                            <label class="input-group-text">Inspect</label>
                                            <div class="controls">
                                                <input type="checkbox" name="eInspect" id="eInspect" class="input">
                                            </div>
                                        </div>
                                        <div id="iAll"> 
                                            <div class="col-xs-3">
                                                <label class="input-group-text">Include current inventory?</label>
                                                <div class="controls">
                                                    <input type="checkbox" name="eInspectAll" id="eInspectAll" class="input">
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <!-- Half of the modal-body div-->
                                    <div class="col-xs-16">
                                        <div class="col-xs-3">
                                            <label class="input-group-text">Is Full box pick?</label>
                                            <div class="controls">
                                                <input type="checkbox" name="eFullBoxPick" id="eFullBoxPick" class="input">
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="input-group-text">High Value</label>
                                            <div class="controls">
                                                <input type="checkbox" name="eHighValue" id="eHighValue" class="input">
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="input-group-text">Auto Receive</label>
                                            <div class="controls">
                                                <input type="checkbox" name="eAutoRecieve" id="eAutoRecieve" class="input">
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="input-group-text">Auto Pick</label>
                                            <div class="controls">
                                                <input type="checkbox" name="eAutoPick" id="eAutoPick" class="input">
                                            </div>
                                        </div>
                                       
                                    </div>
                                    </div>
                                <br/>
                                 <div class="row">
                                    <!-- Half of the modal-body div-->
                                    <div class="col-xs-16">
                                         <div class="col-xs-3">
                                            <label class="input-group-text">Update Serial With Count</label>
                                            <div class="controls">
                                                <input type="checkbox" name="eUpdateWithSerial" id="eUpdateWithSerial" class="input">
                                            </div>
                                        </div>
                                         <div class="col-xs-3">
                                            <label class="input-group-text">Is Modular?</label>
                                            <div class="controls">
                                                <input type="checkbox" name="eModular" id="eModular" class="input">
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="input-group-text">Is Big Part?</label>
                                            <div class="controls">
                                                <input type="checkbox" name="eBigPart" id="eBigPart" class="input">
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="input-group-text">Is AS400?</label>
                                            <div class="controls">
                                                <input type="checkbox" name="eAS400" id="eAS400" class="input">
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                <hr>
                                <div class="row">
                                    <!-- Half of the modal-body div-->
                                    <div class="col-xs-6">
                                        <div class="control-group">
                                            <label class="input-group-text">Min Storage:</label>
                                            <div class="controls">
                                                <input type="number" name="eMinStorage" id="eMinStorage" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Shelf Life:</label>
                                            <div class="controls">
                                                <input type="number" name="eShelfLife" id="eShelfLife" class="form-control">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">RDT Message:</label>
                                            <div class="controls">
                                                <input type="text" name="eRdtMessage" id="eRdtMessage" class="form-control" maxlength="50">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="control-group">
                                            <label class="input-group-text">Part Family:</label>
                                            <div class="controls">
                                                <input type="text" name="ePartFamily" id="ePartFamily" class="form-control">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="input-group-text">Loose PCS Equal To:</label>
                                            <div class="controls">
                                                <input type="number" name="eLoosePcs" id="eLoosePcs" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="ePage2" style="display: none;">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Decant Charge:</label>
                                        <div class="controls">
                                            <select class="form-control" type="text" id="eDecantCharge">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
                                                // Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.charge_master where charge_type_id = "1";');
                                                echo "<option value='0' selected></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['charge_name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Despatch Charge:</label>
                                        <div class="controls">
                                            <select class="form-control" type="text" id="eDespCharge">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
                                                // Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.charge_master where charge_type_id = "4";');
                                                echo "<option value='0' selected></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['charge_name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Inspect Charge:</label>
                                        <div class="controls">
                                            <select class="form-control" type="text" id="eInspCharge">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
                                                // Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.charge_master where charge_type_id = "2";');
                                                echo "<option value='0' selected></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['charge_name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Receipt Charge:</label>
                                        <div class="controls">
                                            <select class="form-control" type="text" id="eReceCharge">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
                                                // Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.charge_master where charge_type_id = "4";');
                                                echo "<option value='0' selected></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['charge_name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Storage Charge:</label>
                                        <div class="controls">
                                            <select class="form-control" type="text" id="eStorCharge">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
                                                // Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.charge_master where charge_type_id = "4";');
                                                echo "<option value='0' selected></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['charge_name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <label class="input-group-text">Transport Charge:</label>
                                        <div class="controls">
                                            <select class="form-control" type="text" id="eTrnspCharge">                                                                  
                                                <?php
                                                include ('../config/phpConfig.php');
                                                // Check connection
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.charge_master where charge_type_id = "3";');
                                                echo "<option value='0' selected'></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['charge_name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>

                                    </div>
                                    <br>
                                    <br>
                                </div>

                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>                  
                            <button type="button" id="saveEdit" class="btn btn-success">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<div class="modal fade" id="mPrintOdetteLabel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Print Odette Label</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Ran Order:</label>
                                    <div class="controls">
                                        <input type="text"  name="pRanOrder" id="pRanOrder" class="form-control" required>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Part Number:</label>
                                    <div class="controls">
                                        <input type="text" name="pPartNumber" id="pPartNumber" class="form-control" required>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">SNP:</label>
                                    <div class="controls">
                                        <input type="number" name="pSNP" id="pSNP" class="form-control" required>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">No Of Labels:</label>
                                    <div class="controls">
                                        <input type="number" name="pNoOfLbls" id="pNoOfLbls" class="form-control" required>
                                    </div>
                                </div>
                                 <div class="control-group">
                                    <label class="input-group-text">Printer:</label>
                                    <select class="form-control" type="text" id="printerIP">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
                                        // Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM printer_mapping where printer_code like "receiving%"');
                                        echo "<option value=\"$value\"></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['printer_code'] . '">' . $row['printer_code'] .'</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="printOdetteLabelButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
        include('../common/topNav.php');
        include('../common/sideBar.php');
        ?>
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th>Part Number</th>
                        <th>Description</th>
                        <th>Jis Supply Group</th>       
                        <th>Piece Part Number</th>   
                        <th>Location Type</th>
                        <th>location Sub Type</th>
                        <th>Inspect</th>
                        <th>Action</th>
                </thead>
                <tfoot>
                    <tr>
                        <th>Part Number</th>
                        <th>Description</th>
                        <th>Jis Supply Group</th>       
                        <th>Piece Part Number</th>   
                        <th>Location Type</th>
                        <th>location Sub Type</th>
                        <th>Inspect</th>
                        <th>Action</th>
                </tfoot>
            </table>
            <input type="Button" id="bCreateNew" class="btn btn-warning" value="Create New"/>
            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>
        </div>

        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->

<script>
    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }
    $(document).ready(function () {
        $('.date_time').mask('0000-00-00 00:00:00');
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>';

        var table = $('#example').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {"url": "../tableData/partsMasterTable.php"},
            initComplete: function () {
                this.api().columns().every(function () {

                    var column = this;
                    if ($(column.header()).hasClass('yes')) {
                        var select = $('<select><option id="ssearch" value=""></option></select>')
                                .appendTo($(column.header()))
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                            );
                                    column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                            .draw();
                                });
                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                    }
                });
            },
            "columnDefs": [{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bEdit' class='btn btn-warning' value='Edit'/><span> </span><input type='Button' id='bPrintLabels' class='btn btn-warning' value='Print Labels'/>"
                },
                       
                {"targets": [7], "searchable": false},
                {"targets": [7], "orderable": false},
                {
                    render: function (data, type, row) {
                        return (data === "1") ? '<span data-order="1" style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color:red" data-order="0" class="glyphicon glyphicon-remove"></span>';
                    },
                    "targets": 6
                }
            ]

        });
        $('#example tbody').on('click', '#bPrintLabels', function () {
            $('#mPrintOdetteLabel').modal('show');
            var data = table.row($(this).parents('tr')).data();
            document.getElementById('pPartNumber').value = data[0];
            
      
		      
	});
        $('#example tfoot th').each(function (i) {
            if(i=== 0 || i===1 || i===4 || i===5){
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Search ' + title + '" />');
            }
        });
        table.columns().every(function () {
            var that = this;
            $('input', this.footer()).on('keyup change clear', function () {
                if (that.search() !== this.value) {
                    that
                            .search(this.value)
                            .draw();
                }
            });
        });


        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });

        $("#bCreateNew").on("click", function () {

            $('#mCreateNew').modal('show');

        });

        $('#mCreateNew').on('hidden.bs.modal', function (e) {
            $(this)
                    .find("input,textarea,select")
                    .val('')
                    .end()
                    .find("input[type=checkbox], input[type=radio]")
                    .prop("checked", "")
                    .end();
        });



        //On change for the Vendor to pre populate when you slect the vendor 
        //do tis as a jquery onchange
        $("#nVendorCode").on("change", function () {
            //   document.getElementById('nEffectiveFrom').value = ""
            //   document.getElementById('nEffectiveTo').value = ""
            var venDrop = document.getElementById('nVendorCode').value;
            if (venDrop == "" || null) {
                alert('No Vendor Code');

            } else {
                // alert(venDrop)
                $.ajax({
                    url: "../tableData/partVendorHelp.php",
                    data: {'venDrop': venDrop},
                    type: "GET",
                    dataSrc: "",
                    success: function (result) {
                        var data = JSON.parse(result);
                        //console.log(data)
                        document.getElementById('nEffectiveFrom').value = data[0].effective_from;
                        document.getElementById('nEffectiveTo').value = data[0].effective_to;
                        if (data[0].auto_receive === '1') {
                            $('#nAutoRecieve').prop('checked', true);
                        } else {
                            $('#nAutoRecieve').prop('checked', false);
                        }
                        if (data[0].auto_pick === '1') {
                            $('#nAutoPick').prop('checked', true);
                        } else {
                            $('#nAutoPick').prop('checked', false);
                        }
                        if (data[0].decant === '1') {
                            $('#nDecant').prop('checked', true);
                        } else {
                            $('#nDecant').prop('checked', false);
                        }
                        if (data[0].inspect === '1') {
                            $('#nInspect').prop('checked', true);

                        } else {
                            $('#nInspect').prop('checked', false);

                        }
                    }
                });
            }




        });
        $("#saveNew").on("click", function () {
            var newPartNum = document.getElementById('nPartNumber').value;
            var newWeight = document.getElementById('nWeight').value;
            var newPartDesc = document.getElementById('nPartDescription').value;
            var newVendorCode = parseInt(document.getElementById('nVendorCode').value);
            var newEffectFrom = document.getElementById('nEffectiveFrom').value;
            var newEffectTo = document.getElementById('nEffectiveTo').value;
            var newZoneType = parseInt(document.getElementById('nZoneType').value);
            var newPackType = parseInt(document.getElementById('nPackType').value);
            var newJisSupply = document.getElementById('nJisSupply').value;
            var newWiCode = document.getElementById('nWiCode').value;
            var newFixLocation = document.getElementById('nFixedLocation').value;
            var newLocationType = document.getElementById('nLocationType').value;
            var newAutoReceive = $('#nAutoRecieve');
            var newAutoPick = $('#nAutoPick');
            var newDecant = $('#nDecant');
            var newInspect = $('#nInspect');
            var newCountOn = $('#nCountOnRecpt');
            var newFullBoxPick = $('#nIsFullBoxPick');
            var newHighValue = $('#nHighValue');
            var newUpdateWith = $('#nUpdateWithSerial');
            var newModular = $('#nModular');
            var newBigPart = $('#nBigPart');
            var newAS400 =  $('#nAS400');
            var newInspectAll = $('#ninspectAll');
            var newMinStor = document.getElementById('nMinStorage').value;
            var newShelfLife = document.getElementById('nShelfLife').value;
            var newRdtMess = document.getElementById('nRdtMessage').value;
            var newPartFamily = document.getElementById('nPartFamily').value;
            var newLoosePcs = document.getElementById('nLoosePcs').value;
            var newStackHeight = document.getElementById('nStackHeight').value;

            // var newReports = document.getElementById('nRepCharge').value;
            if (newAutoReceive.is(':checked')) {
                newAutoReceive = 1;
            } else {
                newAutoReceive = 0;
            }
            if (newAutoPick.is(':checked')) {
                newAutoPick = 1;
            } else {
                newAutoPick = 0;
            }
            if (newDecant.is(':checked')) {
                newDecant = 1;
            } else {
                newDecant = 0;
            }
             if (newModular.is(':checked')) {
                newModular = 1;
            } else {
                newModular = 0;
            }
            if (newBigPart.is(':checked')) {
                newBigPart = 1;
            } else {
                newBigPart = 0;
            }
            
            if (newAS400.is(':checked')) {
                newAS400 = 1;
            } else {
                newAS400 = 0;
            }
            if (newInspect.is(':checked')) {
                newInspect = 1;
            } else {
                newInspect = 0;
            }
            if (newInspectAll.is(':checked')) {
                newInspectAll = 1;
            } else {
                newInspectAll = 0;
            }
            if (newCountOn.is(':checked')) {
                newCountOn = 1;
            } else {
                newCountOn = 0;
            }
            if (newFullBoxPick.is(':checked')) {
                newFullBoxPick = 1;
            } else {
                newFullBoxPick = 0;
            }
            if (newHighValue.is(':checked')) {
                newHighValue = 1;
            } else {
                newHighValue = 0;
            }
            if (newUpdateWith.is(':checked')) {
                newUpdateWith = 1;
            } else {
                newUpdateWith = 0;

            }

            if (newPartNum === "") {
                alert('Please Enter a Part Number');
                return;
            }
            if (newLocationType === "") {
                alert('Location type can not be left empty');
                return;
            }

            if (newVendorCode === 0) {
                alert('Vendor Code can not be left empty');
                return;
            }


            if (newEffectFrom === "") {
                alert('Please Enter Effective From Date');
                return;
            }
            
            if (newEffectTo === "") {
                alert('Please Enter Effective To Date');
                return;
            }

            if (newZoneType === 0) {
                alert('Zone type can not be left empty');
                return;
            }
            var obj = {"partNumber": newPartNum, "effectiveFrom": newEffectFrom, "weight": newWeight, "effectiveTo": newEffectTo, "packTypeId": newPackType,
                "partDescription": newPartDesc, "vendorId": newVendorCode, "jisSupplyGroup": newJisSupply, "wiCode": newWiCode,
                "fixedLocationCode": newFixLocation, "locationTypeId": newLocationType, "zoneTypeId": newZoneType, "countOnRcpt": newCountOn,
                "updateSerialWithCount": newUpdateWith, "decant": newDecant, "inspect": newInspect, "includeInventory": newInspectAll, "miniStock": newMinStor, "shelfLife": newShelfLife,
                "fullBoxPick": newFullBoxPick,"modular": newModular,"bigPart":newBigPart,"isAS400":newAS400, "highValue": newHighValue, "autoReceive": newAutoReceive, "autoPick": newAutoPick, "rdtMessage": newRdtMess,
                "partFamily": newPartFamily, "loosePcsEqualTo1": newLoosePcs,
                "createdBy": currentUser, "updatedBy": currentUser, "stackHeight": newStackHeight
            };

            var newPartjson = JSON.stringify(obj);
            var filter = newPartjson;
	    
            console.log(filter);

            $.ajax({
              
                url: callPostService + filter + "&function=createPart" + "&connection=" + screen,
                type: 'POST',
                success: function (response, textstatus) {

                    if (response === 'OK - true') {
                        $('#example').DataTable().ajax.reload(null, false);
                        $('#confNewPart').modal('show');
                        $('#mCreateNew').modal('hide');
                    } else {
                        alert(response);
                    }
                }
            });
        });

        $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });

        //Start of EDIT
        $('#mEditPart').on('hidden.bs.modal', function (e) {
            $(this)
                    .find("input,textarea,select")
                    .val('')
                    .end()
                    .find("input[type=checkbox], input[type=radio]")
                    .prop("checked", "")
                    .end();
        });
         $('#mPrintOdetteLabel').on('hidden.bs.modal', function (e) {
            $(this)
                    .find("input,textarea,select")
                    .val('')
                    .end()
                    .find("input[type=checkbox], input[type=radio]")
                    .prop("checked", "")
                    .end();
        });
	$("#printOdetteLabelButton").on("click", function () {
			
            var ranOrder = document.getElementById('pRanOrder').value;
	    var partNumber = document.getElementById('pPartNumber').value;
            var snp = document.getElementById('pSNP').value;
            var noOfLabel = document.getElementById('pNoOfLbls').value;
           var printer = document.getElementById('printerIP').value;
            
            
            if(ranOrder === "" || partNumber==="" || snp==="" || noOfLabel === ""){
                alert("Please Fill all details");
                return;
            } else if(printer === ''){
                alert("Please select a printer");
                return;
            }else{
               
                $.ajax({
                            url: "../action/printLabels.php?function=partlabelpdf&filter=ranorder=" + ranOrder + "|AND|partnumber=" 
                                 + partNumber + "|AND|snp=" + snp + "|AND|number=" + noOfLabel +"|AND|printer=" +printer,
                            type: 'GET',
                            success: function (response, textstatus) {
                                if (response === 'OK  -partLabelPdf') {
                                    alert("The odette labels are successfully printed.");
                                    $('#mPrintOdetteLabel').modal('hide');                         
                                } else {
                                    alert(response);
                                }
                            }
                        });
		
            }
        });
        $('#example tbody').on('click', '#bEdit', function () {

            var data = table.row($(this).parents('tr')).data();
            console.log(table.row($(this).parents('tr')).data());
            $('#mEditPart').modal('show');

            var partNum = data[0];

            $.ajax({
                url: '../tableData/partMasterHelp.php',
                type: 'GET',
                data: {'partNum': partNum},
                success: function (result) {
                    var data = JSON.parse(result);
                    console.log(data);
                    document.getElementById('ePartNumber').value = data[0].part_number;
                    document.getElementById('Eid').value = data[0].id;
                    document.getElementById('ePartDescription').value = data[0].part_description;
                    document.getElementById('eWeight').value = data[0].weight;
                    document.getElementById('eVendorCode').value = data[0].vendor_id;
                    document.getElementById('eEffectiveFrom').value = data[0].effective_from;
                    document.getElementById('eEffectiveTo').value = data[0].effective_to;
                    document.getElementById('eZoneType').value = data[0].fixed_sub_type_id;
                    document.getElementById('eVendorName').value = data[0].vendor_name;
                    document.getElementById('ePackType').value = data[0].pack_type_id;
                    document.getElementById('eJisSupply').value = data[0].jis_supply_group;
                    document.getElementById('eWiCode').value = data[0].wi_code;
                    document.getElementById('eFixedLocation').value = data[0].location_code;
                    document.getElementById('eLocationType').value = data[0].fixed_location_type_id;
                    document.getElementById('eMinStorage').value = data[0].mini_stock;
                    document.getElementById('eShelfLife').value = data[0].shelf_life;
                    document.getElementById('eRdtMessage').value = data[0].rdt_prompt;
                    document.getElementById('ePartFamily').value = data[0].part_family;
                    document.getElementById('eLoosePcs').value = data[0].loose_part_qty;
                    document.getElementById('eStackHeight').value = data[0].stack_height;
                    
                    if (data[0].auto_receive === '1') {
                        $('#eAutoRecieve').prop('checked', true);
                    } else {
                        $('#eAutoRecieve').prop('checked', false);
                    }
                    if (data[0].auto_pick === '1') {
                        $('#eAutoPick').prop('checked', true);
                    } else {
                        $('#eAutoPick').prop('checked', false);
                    }
                    if (data[0].requires_decant === '1') {
                        $('#eDecant').prop('checked', true);
                    } else {
                        $('#eDecant').prop('checked', false);
                    }
                    if (data[0].requires_inspection === '1') {
                        $('#eInspect').prop('checked', true);

                    } else {
                        $('#eInspect').prop('checked', false);
                    }
                    if (data[0].requires_count === '1') {
                        $('#eCountOnRecpt').prop('checked', true);
                    } else {
                        $('#eCountOnRecpt').prop('checked', false);
                    }
                    if (data[0].full_box_pick === '1') {
                        $('#eFullBoxPick').prop('checked', true);
                    } else {
                        $('#eFullBoxPick').prop('checked', false);
                    }
                     if (data[0].modular === '1') {
                        $('#eModular').prop('checked', true);
                    } else {
                        $('#eModular').prop('checked', false);
                    }
                     if (data[0].is_big_part === '1') {
                        $('#eBigPart').prop('checked', true);
                    } else {
                        $('#eBigPart').prop('checked', false);
                    }
                     if (data[0].isAS400 === '1') {
                        $('#eAS400').prop('checked', true);
                    } else {
                        $('#eAS400').prop('checked', false);
                    }
                    if (data[0].high_value === '1') {
                        $('#eHighValue').prop('checked', true);
                    } else {
                        $('#eHighValue').prop('checked', false);
                    }
                    if (data[0].update_serial_with_count === '1') {
                        $('#eUpdateWithSerial').prop('checked', true);
                    } else {
                        $('#eUpdateWithSerial').prop('checked', false);
                    }
                }
            });

        });
        $("#saveEdit").on("click", function () {
            var newPartNum = document.getElementById('ePartNumber').value;
            var newWeight = document.getElementById('eWeight').value;
            var eID = document.getElementById('Eid').value;
            var newPartDesc = document.getElementById('ePartDescription').value;
            var newVendorCode = parseInt(document.getElementById('eVendorCode').value);
            var newEffectFrom = document.getElementById('eEffectiveFrom').value;
            var newEffectTo = document.getElementById('eEffectiveTo').value;
            var newZoneType = parseInt(document.getElementById('eZoneType').value);
            var newPackType = parseInt(document.getElementById('ePackType').value);
            var newJisSupply = document.getElementById('eJisSupply').value;
            var newWiCode = document.getElementById('eWiCode').value;
            var newFixLocation = document.getElementById('eFixedLocation').value;
            var newLocationType = document.getElementById('eLocationType').value;
            var newAutoReceive = $('#eAutoRecieve');
            var newAutoPick = $('#eAutoPick');
            var newDecant = $('#eDecant');
            var newInspect = $('#eInspect');
            var newInspectAll = $('#eInspectAll');
            var newCountOn = $('#eCountOnRecpt');
            var newFullBoxPick = $('#eFullBoxPick');
            var newHighValue = $('#eHighValue');
            var newUpdateWith = $('#eUpdateWithSerial');
            var newModular = $('#eModular');
            var newBigPart = $('#eBigPart');
             var newIsAS400 = $('#eAS400');
            var newMinStor = document.getElementById('eMinStorage').value;
            var newShelfLife = document.getElementById('eShelfLife').value;
            var newRdtMess = document.getElementById('eRdtMessage').value;
            var newPartFamily = document.getElementById('ePartFamily').value;
            var newLoosePcs = document.getElementById('eLoosePcs').value;
            var newStackHeight = document.getElementById('eStackHeight').value;

            if (newLocationType === "") {
                alert('Location type can not be left empty');
                return;
            }
            if (newVendorCode === 0) {
                alert('Vendor Code can not be left empty');
                return;
            }

            if (newZoneType === 0) {
                alert('Zone type can not be left empty');
                return;
            }
            if (newAutoReceive.is(':checked')) {
                newAutoReceive = 1;
            } else {
                newAutoReceive = 0;
            }
            if (newAutoPick.is(':checked')) {
                newAutoPick = 1;
            } else {
                newAutoPick = 0;
            }
            if (newDecant.is(':checked')) {
                newDecant = 1;
            } else {
                newDecant = 0;
            }
            if (newInspect.is(':checked')) {
                newInspect = 1;
            } else {
                newInspect = 0;
            }
            if (newModular.is(':checked')) {
                newModular = 1;
            } else {
                newModular = 0;
            }
               if (newBigPart.is(':checked')) {
                newBigPart = 1;
            } else {
                newBigPart = 0;
            }
              if (newIsAS400.is(':checked')) {
                newIsAS400 = 1;
            } else {
                newIsAS400 = 0;
            }
            if (newInspectAll.is(':checked')) {
                newInspectAll = 1;
            } else {
                newInspectAll = 0;
            }
            if (newCountOn.is(':checked')) {
                newCountOn = 1;
            } else {
                newCountOn = 0;
            }
            if (newFullBoxPick.is(':checked')) {
                newFullBoxPick = 1;
            } else {
                newFullBoxPick = 0;
            }
            if (newHighValue.is(':checked')) {
                newHighValue = 1;
            } else {
                newHighValue = 0;
            }
            if (newUpdateWith.is(':checked')) {
                newUpdateWith = 1;
            } else {
                newUpdateWith = 0;

            }

            var obj = {"id": eID, "partNumber": newPartNum, "weight": newWeight, "effectiveFrom": newEffectFrom, "effectiveTo": newEffectTo, "packTypeId": newPackType,
                "partDescription": newPartDesc, "vendorId": newVendorCode, "jisSupplyGroup": newJisSupply, "wiCode": newWiCode,
                "fixedLocationCode": newFixLocation, "locationTypeId": newLocationType, "zoneTypeId": newZoneType, "countOnRcpt": newCountOn,
                "updateSerialWithCount": newUpdateWith, "decant": newDecant, "inspect": newInspect, "includeInventory": newInspectAll, "miniStock": newMinStor, "shelfLife": newShelfLife,
                "fullBoxPick": newFullBoxPick,"modular": newModular, "bigPart":newBigPart,"isAS400":newIsAS400,"highValue": newHighValue, "autoReceive": newAutoReceive, "autoPick": newAutoPick, "rdtMessage": newRdtMess,
                "partFamily": newPartFamily, "loosePcsEqualTo1": newLoosePcs,
                "createdBy": currentUser, "updatedBy": currentUser, "stackHeight" : newStackHeight
            };

            var newPartjson = JSON.stringify(obj);
            var filter = newPartjson;
            
            console.log(filter);

            $.ajax({
               
               url: callPostService + filter + "&function=editPart" + "&connection=" + screen,
                type: 'POST',
                success: function (response, textstatus) {

                    if (response === 'OK - true') {
                        $('#example').DataTable().ajax.reload(null, false);
                        $('#confEditPart').modal('show');
                        $('#mEditPart').modal('hide');
                    } else {
                        alert(response);
                    }
                }
            });
        });
        $(".datepicker").datepicker({dateFormat: 'yy-mm-dd hh:mm:ss'});
    });

</script>
</body>
</html>
