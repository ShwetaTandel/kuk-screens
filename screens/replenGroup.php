<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Replen Group Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="modal fade" id="mCreateNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New Pick Group</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Replen Group:</label>
                                    <div class="controls">
                                        <input type="text" name="nPickCode" id="nPickGroup" class="form-control" maxlength="5">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Replen Group Description:</label>
                                    <div class="controls">
                                        <input type="text" name="nDesc" id="nDesc" class="form-control" >
                                    </div>
                                </div>
                                <div id="newHide">
                                <div class="control-group">
                                    <label class="input-group-text">Max Case Qty:</label>
                                    <div class="controls">
                                        <input type="number" name="nMaxC" id="nMaxC" class="form-control" >
                                    </div>
                                </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Alt Pick Group:</label>
                                    <select class="form-control" type="text" id="nAlt">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
// Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.pick_group;');
                                        echo "<option value='0'></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['pick_group_code'] . '">' . $row['pick_group_code'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
                               
                                <div class="col-md-7">
                                    <label class="input-group-text">Use Pallet:</label>

                                    <input type="checkbox" name="nUsePal" id="nUsePal" class="input">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-success" id="bSubmitNew">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="mEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Replen Group</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <input type="text" name="eID" id="eID" class="form-control" style="display: none;">
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">Replen Group:</label>
                                    <div class="controls">
                                        <input type="text" name="eReplenGroup" id="eReplenGroup" class="form-control" maxlength="5" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Replen Description:</label>
                                    <div class="controls">
                                        <input type="text" name="eDesc" id="eDesc" class="form-control" >
                                    </div>
                                </div>
                                <div id="editHide">
                                <div class="control-group">
                                    <label class="input-group-text">Max Case Qty:</label>
                                    <div class="controls">
                                        <input type="number" name="eMaxC" id="eMaxC" class="form-control" >
                                    </div>
                                </div> 
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Alt Pick Group:</label>
                                    <select class="form-control" type="text" id="eAlt">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
// Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.pick_group;');
                                        echo "<option value='0'></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['pick_group_code'] . '">' . $row['pick_group_code'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
                                
                                <div class="col-md-7">
                                    <label class="input-group-text">Use Pallet:</label>

                                    <input type="checkbox" name="eUsePal" id="eUsePal" class="input">

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
<!--                        <button type="button" class="btn btn-danger"  style="float: left;" id="bDelete">Delete</button>-->
                        <button type="button" class="btn btn-success" id="editButton">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confCreate" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Relpen Group Added</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >New Replen Group Added</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Group Edited</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Replen Group Edited</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
        include('../common/topNav.php');
        include('../common/sideBar.php');
        ?>
        <!-- Page Content  -->
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th>Replen Group</th>
                        <th>Description</th>
                        <th>Max Case Qty</th>
                        <th>Use PLT</th>
                        <th>Alt Replen Group</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th>Action</th>
                </thead>
                <tfoot>
                    <tr>
                        <th>Replen Group</th>
                        <th>Description</th>
                        <th>Max Case Qty</th>
                        <th>Use PLT</th>
                        <th>Alt Replen Group</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th>Action</th>
                </tfoot>
            </table>

            <input type="Button" id="bCreateNew" class="btn btn-warning" value="Create New"/>
            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

        </div>

        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->

<script>
    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }

    $(document).ready(function () {
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>'
        document.getElementById('newHide').style.display = 'none';
        document.getElementById('editHide').style.display = 'none';


        var table = $('#example').DataTable({
            ajax: {"url": "../tableData/replenGroupTable.php", "dataSrc": ""},
            columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<input type='Button' id='bEdit' class='btn btn-warning' value='Edit'/>"
                },
                {
                    render: function (data, type, row) {
                        return (data === "1") ? '<span data-order="1" style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color:red" data-order="0" class="glyphicon glyphicon-remove"></span>';
                    },
                    "targets": 3
                }
            ],
            buttons: [
                {extend: 'excel', filename: 'replenTask', title: 'ReplenTask'}
            ],
            columns: [
                {data: "pick_group_code"},
                {data: "pick_group_description"},
                {data: "max_case_qty"},
                {data: "pallet"},
                {data: "alt_pick_group_code"},
                {data: "last_updated"},
                {data: "last_updated_by"},
                {data: ""}
            ],
            order: [[0, 'asc']]
        });
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });


        $("#bCreateNew").on("click", function () {
            $('#mCreateNew').modal('show');
        });
             $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });
        $('#mCreateNew').on('hidden.bs.modal', function (e) {
            $(this)
                    .find("input,textarea,select")
                    .val('')
                    .end()
                    .find("input[type=checkbox], input[type=radio]")
                    .prop("checked", "")
                    .end();
        });



        $("#bSubmitNew").on("click", function () {
            var newGroup = document.getElementById('nPickGroup').value;
            var newDesc = document.getElementById('nDesc').value;
            var newMax = document.getElementById('nMaxC').value;
            var newAlt = document.getElementById('nAlt').value;
            var newUse = $('#nUsePal');
            if (newUse.is(':checked')) {
                newUse = 1;
            } else {
                newUse = 0;
            }

            if (newGroup.length === 0) {
                alert('Pick Group Cannot Be Blank');
                return;
            }
            if (newDesc.length === 0) {
                alert('Description Cannot Be Blank');
                return;
            }

            $('#mCreateNew').modal('hide');

            var obj = {
                "pickGroupCode": newGroup,
                "pickGroupDescription": newDesc,
                "maxCaseQty": newMax,
                "altPickGroup": newAlt,
                "pallet": newUse,
                "userId": currentUser
            };

            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            console.log(filter);

            $.ajax({
                url: callPostService+ filter + "&function=createPickGroup" +  screen,
                type: 'GET',
                success: function (response, textstatus) {
                    if (response === 'OK - true') {
                        $('#confCreate').modal('show');
                    } else {
                        alert('Unable to Add');
                    }


                }
            });

        });
        $("#nUsePal").on("change", function () {
           var newUse = $('#nUsePal');
            if (newUse.is(':checked')) {
                document.getElementById('newHide').style.display = 'block';
            } else {
                 document.getElementById('newHide').style.display = 'none';
            }
            
        })


        $('#example tbody').on('click', '#bEdit', function () {
            $('#mEdit').modal('show');
            var data = table.row($(this).parents('tr')).data();

            document.getElementById('eID').value = data.id;
            document.getElementById('eReplenGroup').value = data.pick_group_code;
            document.getElementById('eDesc').value = data.pick_group_description;
            document.getElementById('eMaxC').value = data.max_case_qty;
            document.getElementById('eAlt').value = data.alt_pick_group_code;
            
            if (data.pallet === '1') {
                document.getElementById('editHide').style.display = 'block';
                $('#eUsePal').prop('checked', true);
            } else {
                $('#eUsePal').prop('checked', false);
                document.getElementById('editHide').style.display = 'none';
            }
        });
         $("#eUsePal").on("change", function () {
           var newUse = $('#eUsePal');
            if (newUse.is(':checked')) {
                document.getElementById('editHide').style.display = 'block';
            } else {
                 document.getElementById('editHide').style.display = 'none';
            }
            
        })

         $("#editButton").on("click", function () {

            var eID = document.getElementById('eID').value;
            var replenGroup = document.getElementById('eReplenGroup').value;
            var replenDesc = document.getElementById('eDesc').value;
            var maxCase = document.getElementById('eMaxC').value;
             var newAlt = document.getElementById('eAlt').value;
            var newUse = $('#eUsePal');
            if (newUse.is(':checked')) {
                newUse = 1;
            } else {
                newUse = 0;
            }


            $('#mEdit').modal('hide');

            var obj = {
                "id": eID,
                "pickGroupCode": replenGroup,
                "pickGroupDescription": replenDesc,
                "maxCaseQty": maxCase,
                "altPickGroup": newAlt,
                "pallet": newUse,
                "userId": currentUser
            };


            var newEditjson = JSON.stringify(obj);
            var filter = newEditjson;
            console.log(filter);

            $.ajax({
                url: callPostService+ filter + "&function=editPickGroup" +  screen,
                type: 'GET',
                success: function (response, textstatus) {
                    if (response === 'OK - true') {
                        $('#confEdit').modal('show');
                    } else {
                        alert('Unable to Edit');
                    }


                }
            });


        });

    });

</script>
</body>
</html>



