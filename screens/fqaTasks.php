<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>FQA Task Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>

        <?php
        include('../common/topNav.php');
        include('../common/sideBar.php');
        ?>
        <!-- Page Content  -->
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th>Document Reference</th>
                        <th>Part Number</th>
                        <th>Serial Reference</th>
                        <th>Quantity</th>
                        <th >Processed</th>
                        <th>Date Created</th>
                        <th>Last Updated By</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Document Reference</th>
                        <th>Part Number</th>
                        <th>Serial Reference</th>
                        <th>Quantity</th>
                        <th>Processed</th>
                        <th>Date Created</th>
                        <th>Last Updated By</th>
                    </tr>
                </tfoot>
            </table>


            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

        </div>


        <script>
            function logOut() {

                var userID = <?php $_SESSION['userData']['username'] ?>
                $.ajax({
                    url: '../action/userlogout.php',
                    type: 'GET',
                    data: {userID: userID},
                    success: function (response, textstatus) {
                        alert("You have been logged out");
                        window.open('login.php', '_self');
                    }
                });
            }

            $(document).ready(function () {
                var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>';
                var table = $('#example').DataTable({
                    iDisplayLength: 25,
                    dom: 'Blfrtipb',
                    "processing": true, // to show progress bar
                    "serverSide": true,
                    ajax: {"url": "../tableData/fqaTasksTable.php"},
                    buttons: [
                        {extend: 'excel', filename: 'FQATask', title: 'FQATask'}
                    ],
                    columns: [
                        {data: "document_reference"},
                        {data: "part_number"},
                        {data: "serial_reference"},
                        {data: "quantity"},
                        {data: "processed",
                    render: function (data, type, row) {
                        return (data === '1') ? '<span style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color: red;" class="glyphicon glyphicon-remove"></span>';
                    }},
                        {data: "last_updated_date"},
                        {data: "last_updated_by"}
                    ],
                    order: [[5, 'desc']]
                });

                $("#exportExcel").on("click", function () {
                    table.button('.buttons-excel').trigger();
                });




            });

        </script>
    </body>
</html>



