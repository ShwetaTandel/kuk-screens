<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Replen Task Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="modal fade" id="mEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Replen Task</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="row" align="center">
                            <!-- Half of the modal-body div-->
                            <p style="display: none;"><span id="eId"></span></p>
                            <h2>From location code: <strong id="eFromLoc">#</strong></h2>
                            <h2>Part Number: <strong id="ePartNum">#</strong></h2>
                            <hr>
                            <h4>Suggested Serial: <strong id="eSugg">#</strong></h4>
                            <h4>Scanned Serial: <strong id="eScan">#</strong></h4>
                            <h4>Pallet Ref: <strong id="ePal">#</strong></h4>
                            <!-- Other half of the modal-body div-->
                        </div>
                        <br>
                        <hr>
                        <div class="row">
                            <!-- Half of the modal-body div-->
                            <div class="col-xs-12">
                                <div class="control-group">
                                    <label class="input-group-text">To Location:</label>
                                    <div class="controls">
                                        <input type="text" name="eToLoc" id="eToLoc" class="form-control" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Pick Group:</label> 

                                    <select class="form-control" type="text" id="ePickGroup">                                                                  
                                        <?php
                                        include ('../config/phpConfig.php');
                                        // Check connection
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.pick_group;');
                                        echo "<option value=''></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['id'] . '">' . $row['pick_group_code'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>

                                </div>
                                <div class="control-group">
                                    <label class="input-group-text">Priority:</label>
                                    <div class="controls">
                                        <input type="number" name="ePriority" id="ePriority" class="form-control" >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" align="center">
                        <button type="button" class="btn btn-danger"  style="float: left;" id="bDelete">Set To Processed</button>
                        <button type="button" class="btn btn-success"  id="bSaveEdit">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Task Edited</h5>
                    </div>
                    <br>
                    <div align="center">
                        <strong >Replen Task Edited Edited</strong>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="confDelete" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Process</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Are you sure you want to Process this Replen</h4>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="conDel">Yes</button>
                        <button type="button" class="btn btn-danger"  data-dismiss="modal" >No</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="doneDel" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Process</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Replen Processed</h4>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" id="confButton"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="errorDel" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>unable to Process</h4>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success"  data-dismiss="modal" onClick="$('#example').DataTable().ajax.reload(null, false);">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-sm" id="mPickGroup" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Pick Group</h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Pick Group does not match location</h4>
                        <h4>Are you sure you want to change this locations Pick Group</h4>
                        <h5><strong>(This will effect future replen in this location)</strong></h5>
                    </div>

                    <div class="modal-footer" >
                        <button type="button" class="btn btn-success" data-dismiss="modal" id="conChange">Yes</button>
                        <button type="button" class="btn btn-danger"  data-dismiss="modal" >No</button>
                    </div>
                </div>
            </div>
        </div>







        <?php
        include('../common/topNav.php');
        include('../common/sideBar.php');
        ?>
        <!-- Page Content  -->
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th>From Location</th>
                        <th>To Location</th>
                        <th>Part Number</th>
                        <th>Suggested Serial</th>
                        <th>Scanned Serial</th>
                        <th>Pallet</th>
                        <th class="yes">Pick Group</th>                      
                        <th class="yes">Processed</th>
                        <th>Priority</th>
                        <th>Date Created</th>
                        <th>Last Updated By</th>
                        <th></th>
                </thead>
                <tfoot>
                    <tr>
                        <th>From Location</th>
                        <th>To Location</th>
                        <th>Part Number</th>
                        <th>Suggested Serial</th>
                        <th>Scanned Serial</th>
                        <th>Pallet</th>
                        <th>Pick Group</th>                      
                        <th>Processed</th>
                        <th>Priority</th>
                        <th>Date Created</th>
                        <th>Last Updated By</th>
                        <th></th>
                </tfoot>
            </table>


            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

        </div>


        <script>
            function logOut() {

                var userID = <?php $_SESSION['userData']['username'] ?>
                $.ajax({
                    url: '../action/userlogout.php',
                    type: 'GET',
                    data: {userID: userID},
                    success: function (response, textstatus) {
                        alert("You have been logged out");
                        window.open('login.php', '_self');
                    }
                });
            }

            $(document).ready(function () {
                var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>';


                var table = $('#example').DataTable({
                  
                    
                    "serverSide": true,
                    ajax: {"url": "../tableData/replenTaskServerTable.php"},
                    columnDefs: [{
                            targets: -1,
                            data: null,
                            defaultContent: "<input type='Button' id='bEdit' class='btn btn-warning' value='Edit'/>"
                        },
                        {
                    targets: [6,7],
                    orderable: false

                }
                    ],
                    buttons: [
                        {extend: 'excel', filename: 'replenTask', title: 'ReplenTask'}
                    ],
                    initComplete: function () {
                        this.api().columns().every(function (index) {

                            var column = this;
                            if ($(column.header()).hasClass('yes')) {
                                 if (index === 7) {
                                        var select = $('<select><option id="processedSeelect" value=""></option></select>')
                                        .appendTo($(column.header()))
                                        .on('keyup change', function () {
                                        column.search($(this).val())
                                            .draw();
                                        });
                                        select.append("<option value = 0>Not Processed</option>");
                                        select.append("<option value = 1>Processed</option>");
                                        return;
                                }
                                if (index === 6) {
                                	 $.ajax({
                                    	 async:false,
                                         url: "../tableData/replenPickGroupData.php",
                                         type: 'GET',
                                         success: function (response, textstatus) {
                                             var json = JSON.parse(response);
                                        	 var select = $('<select><option id="processedSeelect" value=""></option></select>')
                                             .appendTo($(column.header()))
                                             .on('keyup change', function () {
                                             column.search($(this).val())
                                                 .draw();
                                             });
                                             for (var i=0;i<json.length;i++){
                                            	 select.append("<option value = "+json[i].pick_group_code+">"+json[i].pick_group_code+"</option>");
                                             }
                                         }
                                     });
                                 }
                            }
                        });
                    },
                    columns: [
                        {data: "from_location_code"},
                        {data: "to_location_code"},
                        {data: "part_number"},
                        {data: "suggested_serial"},
                        {data: "scanned_serial"},
                        {data: "pallet_reference"},
                        {data: "pick_group_code"},
                        {data: "processed",
                        render: function (data, type, row) {
                        return (data === '1') ? '<span style="color:green" class="glyphicon glyphicon-ok"></span>' : '<span style="color: red;" class="glyphicon glyphicon-remove"></span>';
                        }},
                        {data: "priority"},
                        {data: "date_created"},
                        {data: "last_updated_by"},
                        {data: ""}
                    ],
                    order: [[9, 'desc']]
                });
       
                $("#exportExcel").on("click", function () {
                    table.button('.buttons-excel').trigger();
                });
                
                
        $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });

                $('#example tbody').on('click', '#bEdit', function () {

                    $('#mEdit').modal('show');
                    var data = table.row($(this).parents('tr')).data();
                    document.getElementById('eId').innerHTML = data.id;
                    document.getElementById('eFromLoc').innerHTML = data.from_location_code
                    document.getElementById('ePartNum').innerHTML = data.part_number;
                    document.getElementById('eSugg').innerHTML = data.suggested_serial;
                    document.getElementById('eScan').innerHTML = data.scanned_serial
                    document.getElementById('ePal').innerHTML = data.pallet_reference;
                    document.getElementById('eToLoc').value = data.to_location_code;
                    document.getElementById('ePickGroup').value = data.pick_group_id;
                    document.getElementById('ePriority').value = data.priority;

                });
                $("#bDelete").on("click", function () {

                    $('#confDelete').modal('show');

                });

                $("#conDel").on("click", function () {
                    var replenId = document.getElementById('eId').innerHTML;
                    //   alert('DELETE: ' + rowID);

                    var obj =
                            {
                                "replenTaskId": replenId,
                                "userId": currentUser

                            };

                    var newEditjson = JSON.stringify(obj);
                    var filter = newEditjson;
                    console.log(filter);

                    $.ajax({
                        url: callPostService+ filter + "&function=deleteReplen"  + pick,
                        type: 'GET',
                        success: function (response, textstatus) {
                            if (response === 'OK - true') {
                                $('#mEdit').modal('hide');
                                $('#confDelete').modal('hide');
                                $('#doneDel').modal('show');
                            } else {
                                $('#mEdit').modal('hide');
                                $('#confDelete').modal('hide');
                                $('#errorDel').modal('show');

                            }


                        }
                    });
                });

                $("#bSaveEdit").on("click", function () {


                    var eId = document.getElementById('eId').innerHTML
                    var newToLoc = document.getElementById('eToLoc').value;
                    var newPickGroup = document.getElementById('ePickGroup').value;
                    var newPriority = document.getElementById('ePriority').value;

                    if (newToLoc.length === 0) {
                        alert('Row Code Cannot Be Blank');
                        return;
                    }
                    if (newPriority.length === 0) {
                        alert('Priority Cannot Be Blank');
                        return;
                    }
                    if (newPickGroup.length === 0) {
                        alert('Pick Group Cannot Be Blank');
                        return;
                    }

                    $.ajax({
                        url: '../action/checkLocation.php',
                        type: 'GET',
                        data: {toLoc: newToLoc},
                        success: function (result) {
                            var data = JSON.parse(result);

                            if (data[0] == null) {
                                alert('location is not replen')
                                return;
                            } else {
                                if (data[0].pick_group_id === newPickGroup) {

                                    $('#mEdit').modal('hide');

                                    var obj =
                                            {
                                                "replenTaskId": eId,
                                                "pickGroupId": newPickGroup,
                                                "priority": newPriority,
                                                "newLocationCode": newToLoc,
                                                "userId": currentUser

                                            };

                                    var newEditjson = JSON.stringify(obj);
                                    var filter = newEditjson;
                                    console.log(filter);

                                    $.ajax({
                                        url: callPostService+ filter + "&function=editReplen"  + pick,
                                        type: 'GET',
                                        success: function (response, textstatus) {
                                            if (response === 'OK - true') {
                                                $('#confEdit').modal('show');
                                            } else {
                                                alert('Unable to Edit');
                                            }

                                        }
                                    });

                                } else {

                                    $('#mPickGroup').modal('show');

                                    $('#conChange').on('click', function () {

                                        $('#mEdit').modal('hide');

                                        var obj =
                                                {
                                                    "replenTaskId": eId,
                                                    "pickGroupId": newPickGroup,
                                                    "priority": newPriority,
                                                    "newLocationCode": newToLoc,
                                                    "userId": currentUser

                                                };

                                        var newEditjson = JSON.stringify(obj);
                                        var filter = newEditjson;
                                        console.log(filter);

                                        $.ajax({
                                            url: callPostService+ filter + "&function=editReplen"  + pick,
                                            type: 'GET',
                                            success: function (response, textstatus) {
                                                if (response === 'OK - true') {
                                                    $('#confEdit').modal('show');
                                                } else {
                                                    alert('Unable to Edit');
                                                }

                                            }
                                        });

                                    });

                                }
                            }
                        }

                    });

                });

            });

        </script>
    </body>
</html>



