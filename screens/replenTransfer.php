<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Replen Transfer Table</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>

        <?php
        include('../common/topNav.php');
        include('../common/sideBar.php');
        ?>
        <!-- Page Content  -->
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" style="width:100%">
                <thead>
                    <tr>
                        <th>To Location Code</th>
                        <th>New Location</th>
                        <th>Part Number</th>
                        <th>From Group</th>
                        <th>To Group</th>
                        <th>Date Created</th>
                        <th>Created By</th>
                 
                </thead>
                <tfoot>
                    <tr>
                         <th>To Location Code</th>
                         <th>New Location</th>
                        <th>Part Number</th>
                        <th>From Group</th>
                        <th>To Group</th>
                        <th>Date Created</th>
                        <th>Created By</th>
              
                </tfoot>
            </table>

            <input type="Button" id="exportExcel" class="btn btn-warning" value="Export To Excel"/>

        </div>

        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->

<script>
    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }

    $(document).ready(function () {
        var currentUser = '<?php print_r($_SESSION['userData']['username']) ?>'


        var table = $('#example').DataTable({
            ajax: {"url": "../tableData/replenTransferTable.php", "dataSrc": ""},
            buttons: [
                {extend: 'excel', filename: 'replenTransfer', title: 'ReplenTransfer'}
            ],
            columns: [
                {data: "location_code"},
                {data: "new_location_code"},
                {data: "part_number"},
                {data: "from_group"},
                {data: "to_group"},
               
                {data: "date_created"},
                 {data: "created_by"}
            ],
            order: [[5, 'desc']]
        });
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });
     $('#example_filter label input').on("focus", function (event) {
            //console.log('Focus')
            $('#example').DataTable().ajax.reload(null, false);

        });



  

    

    });

</script>
</body>
</html>



