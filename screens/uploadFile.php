<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>File Upload</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>-->
        <link rel="stylesheet" type="text/css" href="../datatables/datatables.min.css"/>
        <script type="text/javascript" src="../datatables/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>
    </head>
    <body>
        <?php
        include('../common/topNav.php');
        include('../common/sideBar.php');
        ?>
        <!-- Page Content  -->
        <!-- Page Content  -->
        <br>
        <br>
        <div id="content">
            <br>

            <table id="example" class="compact stripe hover row-border" >
                <thead>
                    <tr>
                 
                        <th>Action</th>
                    </tr>
                </thead>
            </table>

            <!--/span-->
        </div>

        <!--/span-->
    </div>
    <!--/row-->
</div>
<!--/span-->


<script>
    function logOut() {

        var userID = <?php $_SESSION['userData']['username'] ?>
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out");
                window.open('login.php', '_self');
            }
        });
    }

    $(document).ready(function () {

        var table = $('#example').DataTable({
            ajax: {"url": "../tableData/uploadFileTable.php", "dataSrc": ""},
            buttons: [
                {extend: 'excel', filename: 'upload_data', title: 'Upload'}
            ],                columnDefs: [
                    {
                        targets:  0,
                        className: 'dt-left'
                    }
                ],
            columns: [
            
                {data: "data"}


            ],
            order: [[0, 'asc']]
        });
        $("#exportExcel").on("click", function () {
            table.button('.buttons-excel').trigger();
        });

    });

</script>
</body>
</html>



