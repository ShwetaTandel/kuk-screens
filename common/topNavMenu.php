<?php
include_once '../config/logCheck.php';
?>
	<div style="position:absolute; top:1%;left:1%; font-size:calc(1.5vw + 0.5vh + .05vmin); font-weight:bold;display:inline-block" href="#">Vantec</div>
	<div style="position:absolute; top:1%; left:40%; color:black; font-weight:bold;font-size:calc(1.0vw + 0.5vh + .05vmin); font-weight:bold;display:inline">
		<?php
			$fileTitle = ucfirst(pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME));
			$Words = preg_replace('/(?<!\ )[A-Z]/', ' $0', $fileTitle);
			echo($Words)
		?>
	</div>
	<BR>
	<div style="position:absolute; top:5%;left:1%;font-size: calc(0.5vw + 0.5vh + .05vmin)"> 
		<span><a href="../Screens/mainMenu.php">| Home </a></span>
		<span><a href="../Screens/dashboard.php">| Dashboard </a></span>
		<span class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown">| Pick Dial <span class="caret"></span></a>
			<ul class="dropdown-menu">
				<li><a style="color:black" href="../Screens/pickDial.php?data=">Pick Dial</a></li>
<!--				<li><a style="color:black" href="../Screens/pickDial.php?data=jis">Pick Dial JIS</a></li>  !-->
<!--				<li><a style="color:black" href="../Screens/pickDial.php?data=350">Pick Dial KANBAN</a></li> !-->
<!--				<li><a style="color:black" href="../Screens/pickDial.php?data=misc ">Pick Dial MISC</a></li> !-->
<!--				<li><a style="color:black" href="../Screens/pickDial.php?data=short">Pick Dial SHORT</a></li> !-->
			</ul>
		</span>
		<span><a href="../Screens/reportPick.php">| Reports </a></span>
				<span class="dropdown">
				<?php  if ($_SESSION['userData']['user_level_id'] === '1')
				{
					echo '<a class="dropdown-toggle" data-toggle="dropdown">| Static Data<span class="caret"></span></a>';
				}
				?>
					<ul class="dropdown-menu">
						<?php
							include ('../config/phpConfig.php');
							if (mysqli_connect_errno()) {
								echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
							}
							$result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.menu_detail WHERE menu_body_id=1 AND active =1');
							while ($row = mysqli_fetch_array($result)) {
								echo '<li><a style="color:black" href="' . $row['action'] . '">' . $row['description'] . '</a></li>';
							}							
						?>
					</ul>
				</span>
				<span class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown">| Processing
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<?php
							include ('../config/phpConfig.php');
							// Check connection
							if (mysqli_connect_errno()) {
								echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
							}
							$result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.menu_detail WHERE menu_body_id=2 AND active =1');
							while ($row = mysqli_fetch_array($result)) {
								echo '<li><a style="color:black" href="' . $row['action'] . '">' . $row['description'] . '</a></li>';
							}
						?>
					</ul>
				</span>
				<span class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown">| Enquiries 
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<?php
							include ('../config/phpConfig.php');

							// Check connection
							if (mysqli_connect_errno()) {
								echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
							}
							$result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.menu_detail WHERE menu_body_id=3 AND active =1');
							while ($row = mysqli_fetch_array($result)) {
								echo '<li><a style="color:black" href="' . $row['action'] . '">' . $row['description'] . '</a></li>';
							}
						?>
					</ul>
				</span>
			</ul>			
	</div>
	<div class="container-fluid-nav text-right" style="position:absolute; top:4%;left:90%"><a onclick="logOut()">Log Out - <?php print_r( $_SESSION['userData']['username'])?></a></div>
<br>
