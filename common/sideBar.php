  <!-- /.navbar -->
               <div class="wrapper">
            <nav id="sidebar">
                <ul class="list-unstyled fixed-bottom components">
                 <li>
                     
                      <?php  if ($_SESSION['userData']['user_level_id'] === '1'){
                          echo '<a href="#staticSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Static Data </a>';
                      } ?>  
                        <ul class="collapse list-unstyled" id="staticSubmenu">
                            <?php
                            include ('../config/phpConfig.php');
                           
                                // Check connection
                            if (mysqli_connect_errno()) {
                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                            }
                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.menu_detail WHERE menu_body_id=1 AND active =1');
                            while ($row = mysqli_fetch_array($result)) {
                                echo '<li><a href="' . $row['action'] . '">' . $row['description'] . '</a></li>';
                            }
                                
                            ?>
                        </ul>
                    </li>
                       <li>
                        <a href="#procSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Processing</a>
                        <ul class="collapse list-unstyled" id="procSubmenu">
                            <?php
                            include ('../config/phpConfig.php');

                            // Check connection
                            if (mysqli_connect_errno()) {
                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                            }
                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.menu_detail WHERE menu_body_id=2 AND active =1');
                            while ($row = mysqli_fetch_array($result)) {
                                echo '<li><a href="' . $row['action'] . '">' . $row['description'] . '</a></li>';
                            }
                            ?>
                    </li>
                </ul>
                </li>
                <li>
                    <a href="#EnqSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Enquiries</a>
                    <ul class="collapse list-unstyled" id="EnqSubmenu">
                        <?php
                        include ('../config/phpConfig.php');

                        // Check connection
                        if (mysqli_connect_errno()) {
                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                        }
                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.menu_detail WHERE menu_body_id=3 AND active =1');
                        while ($row = mysqli_fetch_array($result)) {
                            echo '<li><a href="' . $row['action'] . '">' . $row['description'] . '</a></li>';
                        }
                        ?>
                    </ul>
                </li>
                </ul>
            </nav>
        </div>