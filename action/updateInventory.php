<?php
// updateInventory.php
// VIMS inventory_master processing
// -------------------------------------------------------------------------------------------
// Modified -   2020-07-17 - Created
//              2021-04-25 - populate vendor_code & product_type_code on transaction_history
// -------------------------------------------------------------------------------------------
header('Cache-Control: no-cache, no-store, must-revalidate');   
header('Expires: 0');
// Header to allow Cross-Server AJAX Connection
header('Access-Control-Allow-Origin: *');
set_time_limit(0);
$root = $_SERVER['DOCUMENT_ROOT'];
$dir = (dirname(__FILE__));

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

// validaterdt - VIMS Server Validation

$function  			= $_GET['function'];
$Connect   			= $_GET['connection'];
$User				= $_GET['user'];
$Pwd				= $_GET['pword'];
$Host              	= $_GET['host'];
$DbName				= $_GET['dbase'];
$TableName 			= $_GET['table'];
$mReturnFields		= $_GET['returnfields'];

if ($Host == "")
{
//open connection to mysql db
	include_once ('../config/phpConfig.php');
	$Host = $mHost;
	$DbName = $mDbName;
	$User = $mDbUser;
	$Pwd = $mDbPassword;	
}

// mysql connection
	//$connection=mysqli_connect($Host,$User,$Pwd,$DbName) or
	//	exit("FAIL-Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());

// Get Input Parameters from POST or GET
if (!empty($_POST))
{
	$filter = "";
	foreach($_POST as $k1=>$v1)
	{
		if ($filter != "")
			$filter .= ",";
		$filter .= $k1.":".$v1;
	}
}
else
{
	$filter = $_GET['filter'];
}

// Reformat Filter
$filter = str_replace("|AND|","&",$filter);
$filter = str_replace("/dbase/",$mDbName,$filter);
$filter = str_replace("%20"," ",$filter);
$filter = str_replace("&",",",$filter);
$filter = str_replace("=",":",$filter);

$mSQLData = array();

$mErrMsg = "OK  -";
$mDataStr = "";	
		 
if (($function != null) && (strtolower($function) == "validateinventory"))
{
//	exit("FAIL-TESTVAL-".$filter);
	
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$filter);
// Create Associative Array by Exploding Array Elements on :
	for ($i = 0; $i < count($mArray1); $i++)
	{
		$s = 0;
		$e = 0;
		$k1 = $mArray1[$i];
		$s = strpos($k1,":");
		if ($s >= 0)
		{
			$k2 = substr($k1,0,$s);
			$v2 = substr($k1,$s+1);
		}		
		if ($k2 == "partNumber")
			$mPartNumber = trim($v2);
		elseif ($k2 == "serialReference")
			$mSerialReference = trim($v2);
		elseif ($k2 == "serialNumber")
			$mSerialReference = trim($v2);
		elseif ($k2 == "locationCode")
			$mLocationCode = trim($v2);
		elseif ($k2 == "expiryDate")
			$mExpiryDate = trim($v2);
		elseif ($k2 == "statusCode")
			$mStatusCode = trim($v2);
		elseif ($k2 == "decant")
			$mDecant = trim($v2);
		elseif ($k2 == "inspect")
			$mInspect = trim($v2);
		elseif ($k2 == "currentUser")
			$mCurrentUser = trim($v2);
		elseif ($k2 == "transactionType")
			$mTransactionTypeId = strtoupper(trim($v2));
	}
	echo ("OK  -validateInventory");
	return;
}
elseif (($function != null) && (strtolower($function) == "updateinventory"))
{
	$mStatusCode = "";
    $mProductTypeCode = "W1-PRD";
//	echo("FAIL-TEST1-".$filter);
//	return;
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$filter);
// Create Associative Array by Exploding Array Elements on :
	for ($i = 0; $i < count($mArray1); $i++)
	{
		$s = 0;
		$e = 0;
		$k1 = $mArray1[$i];
		$s = strpos($k1,":");
		if ($s >= 0)
		{
			$k2 = substr($k1,0,$s);
			$v2 = substr($k1,$s+1);
		}
		if ($k2 == "partNumber")
			$mPartNumber = trim($v2);
		elseif ($k2 == "id")
			$mInvId = trim($v2);
		elseif ($k2 == "serialReference")
			$mSerialReference = trim($v2);
		elseif ($k2 == "serialNumber")
			$mSerialReference = trim($v2);
		elseif ($k2 == "locationCode")
			$mLocationCode = trim($v2);
		elseif ($k2 == "expiryDate")
			$mExpiryDate = trim($v2);
		elseif ($k2 == "productType")
			$mProductType = trim($v2);
		elseif ($k2 == "statusCode")
			$mStatusCode = trim($v2);
		elseif ($k2 == "decant")
			$mDecant = trim($v2);
		elseif ($k2 == "inspect")
			$mInspect = trim($v2);
		elseif ($k2 == "currentUser")
			$mCurrentUser = trim($v2);
		elseif ($k2 == "transactionType")
			$mTransactionTypeId = strtoupper(trim($v2));
	}

// POST Operation

// Validate Location
	$mToLocId = "";
	$mMultiPart = 0;
	$mDataQuery = "SELECT location.id AS id, location.location_code AS location_code, location.multi_part_location AS multi_part_location, location_filling_status.filling_code AS filling_code  FROM location LEFT JOIN location_filling_status ON location.filling_status_id=location_filling_status.id WHERE location.location_code='".$mLocationCode."' LIMIT 1";
	if (!$mData = mysqli_query($connection,$mDataQuery))
		return "FAIL-".mysqli_error($connection).$mDataQuery;
	if (mysqli_num_rows($mData) == 0)
		return "FAIL-Invalid Location Code.";
	
	$mRow = mysqli_fetch_assoc($mData);
	$mToLocId = ($mRow['id']);
	$mToLocCode = ($mRow['location_code']);
	$mFillingCode = ($mRow['filling_code']);
	$mMultiPart = ($mRow['multi_part_location']);
	
	if (strtolower($mFillingCode) == "full" && ($mLocationCode != $mToLocCode))
	{
		echo("FAIL-location is Full");
		return;
	}
	if (strtolower($mFillingCode) == "closed")
	{
		echo("FAIL-location is Closed");
		return;
	}
	
	//Conversion Factor
	$mDataQuery = "SELECT part.id, part.conversion_factor,vendor.vendor_reference_code AS vendor_code FROM part LEFT JOIN vendor ON part.vendor_id=vendor.id WHERE part_number='".$mPartNumber."' LIMIT 1";
	if (!$mInvData = mysqli_query($connection,$mDataQuery))
		exit("FAIL-".mysqli_error($connection)."-".$mDataQuery);
	if (mysqli_num_rows($mInvData) == 0)
	{
		echo("FAIL-Invalid Part Number");
		return;
	}
	$mInvRow = mysqli_fetch_assoc($mInvData);
	$mPartId = $mInvRow['id'];
	$mConvFactor = $mInvRow['conversion_factor'];
    $mVendor = $mInvRow['vendor_code'];
	
	
	//Product Type
	$mDataQuery = "SELECT product_type_code FROM product_type WHERE id='".$mProductType."' LIMIT 1";
	if (!$mProductData = mysqli_query($connection,$mDataQuery))
		exit("FAIL-".mysqli_error($connection)."-".$mDataQuery);
	if (mysqli_num_rows($mProductData) != 0)
	{
		$mProductRow = mysqli_fetch_assoc($mProductData);
		$mProductCode = $mProductRow['product_type_code'];
	}
	
	
	
	if (($mLocationCode != $mToLocCode) && ($mMultiPart == 0))
	{
		$mDataQuery = "SELECT id FROM inventory_master WHERE current_location_id=".$mToLocId." AND part_number='".$mPartNumber."' LIMIT 1";
		if (!$mData = mysqli_query($connection,$mDataQuery))
			exit ("FAIL-".mysqli_error($connection).$mDataQuery);
		if (mysqli_num_rows($mData) > 0)
		{
			echo("FAIL-Mixed Parts are not allowed");
			return;
		}
	}
	
	$mDataQuery = "SELECT inventory_master.id AS id, inventory_master.inventory_qty AS inventory_qty,inventory_master.ran_or_order AS ran_or_order";
    $mDataQuery .=",tag.tag_reference AS tag_reference";
    $mDataQuery .=",location.location_code AS location_code";
    $mDataQuery .=",product_type.product_type_code AS product_type_code";
    $mDataQuery .=" FROM inventory_master";
    $mDataQuery .=" LEFT JOIN tag ON inventory_master.parent_tag_id=tag.id";
    $mDataQuery .=" LEFT JOIN location ON inventory_master.current_location_id=location.id";
    $mDataQuery .=" LEFT JOIN product_type ON inventory_master.product_type_id=product_type.id";
    $mDataQuery .=" WHERE inventory_master.id=".$mInvId." LIMIT 1";
	if (!$mData = mysqli_query($connection,$mDataQuery))
		return "FAIL-".mysqli_error($connection).$mDataQuery;
	while($mRow = mysqli_fetch_assoc($mData)) 
	{
		$mQty = $mRow['inventory_qty'];
		$mFromLocationCode = $mRow['location_code'];
		$mSourceTag = $mRow['tag_reference'];
		$mRanOrder = $mRow['ran_or_order'];
        $mProductTypeCode = $mRow['product_type_code'];
	}

	if (($mSerialReference == null) && ($mFromLocationCode != $mLocationCode))
	{
		echo("FAIL-cannot change location if Serial is Blank");
		return;
	}
	
	$mDataQuery = "UPDATE inventory_master SET last_updated=now(), last_updated_by='".$mCurrentUser."'";
	if ($mExpiryDate)
	{
		$mDataQuery .= ",expiry_date='".$mExpiryDate."'";
	}
	if ($mProductType)
	{
		$mDataQuery .= ",product_type_id='".$mProductType."'";
	}
	if ($mToLocId != "")
	{
		$mDataQuery .= ", current_location_id=".$mToLocId.",location_id=".$mToLocId.",location_code='".$mLocationCode."'";
	}
	if ($mDecant == 1)
	{
		$mDataQuery .= ",requires_decant=1";
	}
	else
	{
		$mDataQuery .= ",requires_decant=false";
	}
	if ($mInspect == 1)
	{
		$mDataQuery .= ",requires_inspection=1";
	}
	else
	{
		$mDataQuery .= ",requires_inspection=0";
	}
	if (($mStatusCode) &&($mStatusCode != ""))
	{
		$mDataQuery .= ",inventory_status_id=(SELECT id FROM inventory_status where inventory_status_code='".$mStatusCode."')";
	}
	$mDataQuery .= " WHERE inventory_master.id=".$mInvId." LIMIT 1";
	if (!mysqli_query($connection,$mDataQuery))
		exit ("FAIL-".mysqli_error($connection).$mDataQuery);

	$mDataQuery = "INSERT INTO transaction_history (id,short_code,part_number,serial_reference,ran_or_order,txn_qty,from_location_code,to_location_code,date_created,last_updated,created_by,last_updated_by,transaction_reference_code,requires_decant,requires_inspection,conversion_factor,vendor_code,product_type_code)";
	$mDataQuery .= " VALUES(DEFAULT,'STKMVS','".$mPartNumber."','".$mSerialReference."','".$mRanOrder."',".$mQty.",'".$mFromLocationCode."','".$mToLocCode."',now(),now(),'".$mCurrentUser."','".$mCurrentUser."','STKMVS',".$mDecant.",".$mInspect.",".$mConvFactor.",'".$mVendor."','".$mProductTypeCode."')";	
	//echo($mDataQuery);
	if (!mysqli_query($connection,$mDataQuery))
		return "FAIL-".mysqli_error($connection)."-".$mDataQuery;	

	if (($mFromLocationCode != $mToLocCode))
	{
		$mDataQuery = "SELECT id FROM inventory_master WHERE location_code='".$mFromLocationCode."' LIMIT 1";
		if (!$mData = mysqli_query($connection,$mDataQuery))
			exit ("FAIL-".mysqli_error($connection).$mDataQuery);
//		exit ("FAIL-".mysqli_error($connection).$mDataQuery." ".mysqli_num_rows($mData));
		if (mysqli_num_rows($mData) == 0)
		{
			$mDataQuery = "UPDATE location SET filling_status_id=(SELECT id FROM location_filling_status WHERE lower(filling_code)='empty') WHERE location_code='".$mFromLocationCode."' LIMIT 1";
		}
		else
		{
			$mDataQuery = "UPDATE location SET filling_status_id=(SELECT id FROM location_filling_status WHERE lower(filling_code)='filling') WHERE location_code='".$mFromLocationCode."' LIMIT 1";
		}
		if (!mysqli_query($connection,$mDataQuery))
			exit ("FAIL-".mysqli_error($connection)."-".$mDataQuery);	
//		exit ("FAIL-".mysqli_error($connection)."-".$mDataQuery." ".mysqli_num_rows($mData));	
		$mDataQuery = "UPDATE location SET filling_status_id=(SELECT id FROM location_filling_status WHERE lower(filling_code)='filling') WHERE location_code='".$mToLocCode."' limit 1";
		if (!mysqli_query($connection,$mDataQuery))
			return "FAIL-".mysqli_error($connection)."-".$mDataQuery;	
	}
	
//	echo ("FAIL-TEST3-".$mDataQuery."-".mysqli_error($connection));
	echo ("Ok  -updateInventory");
	return;
}

elseif (($function != null) && (strtolower($function) == "splitserial"))
{
	$mStatusCode = "";
//	echo("FAIL-TEST1-".$filter);
//	return;
	$mSerialReferenceOut = "";
    $mProductTypeCode = "W1-PRD";

// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$filter);
// Create Associative Array by Exploding Array Elements on :
	for ($i = 0; $i < count($mArray1); $i++)
	{
		$s = 0;
		$e = 0;
		$k1 = $mArray1[$i];
		$s = strpos($k1,":");
		if ($s >= 0)
		{
			$k2 = substr($k1,0,$s);
			$v2 = substr($k1,$s+1);
		}
		if ($k2 == "partNumber")
			$mPartNumber = trim($v2);
		elseif ($k2 == "id")
			$mInvId = $v2;
		elseif ($k2 == "serialReferenceIn")
			$mSerialReferenceIn = trim($v2);
		elseif ($k2 == "serialReferenceOut")
			$mSerialReferenceOut = trim($v2);
		elseif ($k2 == "locationCode")
			$mLocationCode = trim($v2);
		elseif ($k2 == "qty")
			$mQty = floatval($v2);
		elseif ($k2 == "currentUser")
			$mCurrentUser = trim($v2);
		elseif ($k2 == "transactionType")
			$mTransactionTypeId = strtoupper(trim($v2));
	}
//	echo("FAIL-TEST2-".$mInvId."-".$mArray1[0]."-".$filter);
//	return;

	$mDataQuery = "SELECT id AS id, current_location_id AS current_location_id FROM inventory_master WHERE id=".$mInvId." AND requires_inspection=1 LIMIT 1";
	if (!$mData = mysqli_query($connection,$mDataQuery))
		exit ("FAIL-".mysqli_error($connection)."-".$mDataQuery."-Filter-".$filter);
	if (mysqli_num_rows($mData) == 0)
		exit ("FAIL-This Serial in not marked for Inspection");
	else
	{
		$mRow = mysqli_fetch_assoc($mData);
// Validate Location Type
		$mDataQuery = "SELECT location.id AS id, location_type.location_type_code AS location_type_code FROM location JOIN location_type ON location.location_type_id=location_type.id WHERE location.id=".$mRow['current_location_id']." LIMIT 1";
		if (!$mData = mysqli_query($connection,$mDataQuery))
			return "FAIL-".mysqli_error($connection)."-".$mDataQuery;
		$mRow = mysqli_fetch_assoc($mData);
		if (mysqli_num_rows($mData) == 0)
			exit ("FAIL-This is not a QA Location");
		else
		{
			if (strtolower($mRow['location_type_code']) != "qa")
				exit("FAIL-This is not a QA location");
		}
	}

// Generate new Serial Reference	
	If ($mSerialReferenceOut == "")
	{
		$i = 0;
		$mSerialReferenceOut = $mSerialReferenceIn;
		for ($i=1;$i<9999;$i++)
		{
			$mDataQuery = "SELECT id FROM inventory_master WHERE part_number='".$mPartNumber."' AND serial_reference='".$mSerialReferenceOut."' AND inventory_qty>0 LIMIT 1";
			if (!$mData = mysqli_query($connection,$mDataQuery))
				exit ("FAIL-".mysqli_error($connection)."-".$mDataQuery);
			if (mysqli_num_rows($mData) == 0)
				break;
			$mSerialReferenceOut = $mSerialReferenceIn.$i;
		}
		echo ("OKSR-".$mSerialReferenceOut);
		return;
	}

// Split Inventory Serial
//	$mDataQuery = "SELECT * FROM inventory_master WHERE id =".$mInvId." LIMIT 1";
    $mDataQuery = "SELECT inventory_master.part_id AS part_id,inventory_master.part_number AS part_number,inventory_master.serial_reference AS serial_reference,inventory_master.ran_or_order AS ran_or_order,inventory_master.location_code AS location_code,inventory_master.current_location_id AS current_location_id,inventory_master.product_type_id AS product_type_id,inventory_master.inventory_status_id AS inventory_status_id";
    $mDataQuery .=",product_type.product_type_code AS product_type_code";
    $mDataQuery .=" FROM inventory_master";
    $mDataQuery .=" LEFT JOIN product_type ON inventory_master.product_type_id=product_type_id";
    $mDataQuery .=" WHERE inventory_master.id =".$mInvId." LIMIT 1";
	if (!$mData = mysqli_query($connection,$mDataQuery))
		exit ("FAIL-".mysqli_error($connection)."-".$mDataQuery);
	$mRow = mysqli_fetch_assoc($mData);
	if (mysqli_num_rows($mData) > 0)
	{
        $mProductTypeCode = $mRow['product_type_code'];
		$mDataQuery = "SELECT part.id AS id, part.conversion_factor AS conversion_factor";
        $mDataQuery .=",vendor.vendor_reference_code AS vendor_code";
        $mDataQuery .=" FROM part";
        $mDataQuery .=" JOIN vendor ON part.vendor_id=vendor.id";
        $mDataQuery .=" WHERE part.id=".$mRow['part_id']." LIMIT 1";
		if (!$mData = mysqli_query($connection,$mDataQuery))
			exit("FAIL-".mysqli_error($connection)."-".$mDataQuery);
		$mInvRow = mysqli_fetch_assoc($mData);
		$mConvFactor = $mInvRow['conversion_factor'];
        $mVendor = $mInvRow['vendor_code'];
		$mTxnQty = $mQty*$mConvFactor;
		$mDataQuery = "INSERT INTO inventory_master (id,version,allocated_qty,current_disposition_id,current_inventory_status_id,current_location_id,location_id,location_code,date_created,created_by,expiry_date,inventory_qty,available_qty,last_updated,last_updated_by,parent_tag_id,part_id,part_number,ran_or_order,recorded_missing,requires_decant,requires_inspection,serial_reference,originating_detail_id,conversion_factor,stock_date,active,inventory_status_id,product_type_id)";		
		$mDataQuery .= " VALUES(DEFAULT,0,0,null,null,".$mRow['current_location_id'].",".$mRow['current_location_id'].",'".$mRow['location_code']."',now(),'".$mCurrentUser."',null,".$mTxnQty.",".$mTxnQty.",now(),'".$mCurrentUser."',null,".$mRow['part_id'].",'".$mRow['part_number']."','".$mRow['ran_or_order']."',0,0,1,'".$mSerialReferenceOut."',null,".$mConvFactor.",now(),1,".$mRow['inventory_status_id'].",".$mRow['product_type_id'].")";		
		if (!mysqli_query($connection,$mDataQuery))
			exit("FAIL-".mysqli_error($connection)."-".$mDataQuery);
		
		$mDataQuery = "INSERT INTO transaction_history (id,short_code,part_number,serial_reference,ran_or_order,txn_qty,from_location_code,to_location_code,date_created,last_updated,created_by,last_updated_by,transaction_reference_code,requires_decant,requires_inspection,vendor_code,product_type_code)";
		$mDataQuery .= " VALUES(DEFAULT,'STKMVFS','".$mRow['part_number']."','".$mSerialReferenceOut."','".$mRow['ran_or_order']."',".$mTxnQty.",'".$mRow['location_code']."','".$mRow['location_code']."',now(),now(),'".$mCurrentUser."','".$mCurrentUser."','STKMVFS',0,1,'".$mVendor."','".$mProductTypeCode."')";	
		if (!mysqli_query($connection,$mDataQuery))
		return "FAIL-".mysqli_error($connection)."-".$mDataQuery;	
		
		$mDataQuery = "UPDATE inventory_master SET last_updated=now(),last_updated_by='".$mCurrentUser."',inventory_qty=inventory_qty-".$mTxnQty.",available_qty=inventory_qty-allocated_qty WHERE id=".$mInvId." LIMIT 1";
		if (!mysqli_query($connection,$mDataQuery))
			exit("FAIL-".mysqli_error($connection)."-".$mDataQuery);
		
		$mDataQuery = "INSERT INTO transaction_history (id,short_code,part_number,serial_reference,ran_or_order,txn_qty,from_location_code,to_location_code,date_created,last_updated,created_by,last_updated_by,transaction_reference_code,requires_decant,requires_inspection,vendor_code,product_type_code)";
		$mDataQuery .= " VALUES(DEFAULT,'SA-OUT','".$mRow['part_number']."','".$mSerialReferenceIn."','".$mRow['ran_or_order']."',".$mTxnQty.",'".$mRow['location_code']."','".$mRow['location_code']."',now(),now(),'".$mCurrentUser."','".$mCurrentUser."','SA-OUT',0,1,'".$mVendor."','".$mProductTypeCode."')";	
		if (!mysqli_query($connection,$mDataQuery))
		return "FAIL-".mysqli_error($connection)."-".$mDataQuery;	
		
	}
	echo ("OK  -splitInventory");
	return;
}

elseif (($function != null) && (strtolower($function) == "saout"))
{
	$mStatusCode = "";
	$mComment = "";
	$mReasonCode = "";
	$mConversionFactor = 1;
    $mProductType = "W1-PRD";
	//echo("FAIL-TEST1-".$filter);
//	return;

// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$filter);
// Create Associative Array by Exploding Array Elements on :
	for ($i = 0; $i < count($mArray1); $i++)
	{
		$s = 0;
		$e = 0;
		$k1 = $mArray1[$i];
		$s = strpos($k1,":");
		if ($s >= 0)
		{
			$k2 = substr($k1,0,$s);
			$v2 = substr($k1,$s+1);
		}
		if ($k2 == "partNumber")
			$mPartNumber = trim($v2);
		elseif ($k2 == "id")
			$mInvId = $v2;
		elseif ($k2 == "serialReference")
			$mSerialReference = trim($v2);
		elseif ($k2 == "locationCode")
			$mLocationCode = trim($v2);
		elseif ($k2 == "comment")
			$mComment = trim($v2);
		elseif ($k2 == "reasonCode")
			$mReasonCode = trim($v2);
		elseif ($k2 == "conversionFactor")
			$mConversionFactor = floatval($v2);
		elseif ($k2 == "qty")
			$mQty = floatval($v2);
		elseif ($k2 == "currentUser")
			$mCurrentUser = trim($v2);
		elseif ($k2 == "transactionType")
			$mTransactionTypeId = strtoupper(trim($v2));
	}
	

// Split Inventory Serial
//	$mDataQuery = "SELECT * FROM inventory_master WHERE id =".$mInvId." LIMIT 1";
    $mDataQuery = "SELECT inventory_master.part_id AS part_id,inventory_master.part_number AS part_number,inventory_master.serial_reference AS serial_reference,inventory_master.ran_or_order AS ran_or_order,inventory_master.location_code AS location_code,inventory_master.product_type_id AS product_type_id";
    $mDataQuery .=",product_type.product_type_code AS product_type_code";
    $mDataQuery .=" FROM inventory_master";
    $mDataQuery .=" LEFT JOIN product_type ON inventory_master.product_type_id=product_type_id";
    $mDataQuery .=" WHERE inventory_master.id =".$mInvId." LIMIT 1";
	if (!$mData = mysqli_query($connection,$mDataQuery))
		exit ("FAIL-".mysqli_error($connection)."-".$mDataQuery);
	$mRow = mysqli_fetch_assoc($mData);
	if (mysqli_num_rows($mData) > 0)
	{
		$mProductType = $mRow['product_type_code'];
		$mDataQuery = "SELECT  part.id,part.conversion_factor AS conversion_factor,vendor.vendor_reference_code as vendor_code,inventory_key,single_tier FROM part left join vendor on vendor.id=part.vendor_id WHERE part.id=".$mRow['part_id']." LIMIT 1";
		if (!$mData = mysqli_query($connection,$mDataQuery))
			exit("FAIL-".mysqli_error($connection)."-".$mDataQuery);
		$mInvRow = mysqli_fetch_assoc($mData);
		$mConvFactor = $mInvRow['conversion_factor'];
		$mVendor = $mInvRow['vendor_code'];
		$mSingleTier = $mInvRow['single_tier'];
		$mInventoryKey = $mInvRow['inventory_key'];
//		$mTxnQty = $mQty*$mConvFactor;
		$mTxnQty = $mQty;
		if ($mSingleTier==true && $mInventoryKey == null)
			{
				echo ("FAIL- Inventory Key is null");
				return;
			}
		if (($mRow['inventory_qty'] > $mTxnQty) && ($mRow['inventory_qty'] > 0))
		{
			$mDataQuery = "UPDATE inventory_master SET last_updated=now(),last_updated_by='".$mCurrentUser."',inventory_qty=inventory_qty-".$mTxnQty.",available_qty=inventory_qty-allocated_qty WHERE id=".$mInvId." LIMIT 1";
			if (!mysqli_query($connection,$mDataQuery))
				exit("FAIL-".mysqli_error($connection)."-".$mDataQuery);
		}
		else
		{	
			//include 'ChromePhp.php';
			//added this query to chcek if decant header is closed 
			//if close remove inventory is from decant body and delete inventory 
			$mDataQuery  = "select COUNT(*) as count from decant_header left join document_status on decant_header.document_status_id = document_status.id ";
			$mDataQuery .= " where decant_header.id in (select decant_header_id from decant_body where inventory_master_id=".$mInvId.") ";
			$mDataQuery .= " and document_status_code != 'CLOSED' ";
			//ChromePhp::log($mDataQuery);
			if (!$mDataToValidate = mysqli_query($connection,$mDataQuery))
			  exit ("FAIL-".mysqli_error($connection)."-".$mDataQuery);
			$mValidateRow = mysqli_fetch_assoc($mDataToValidate);
			//ChromePhp::log($mValidateRow['count']);
			if ($mValidateRow['count'] > 0)
			{
				echo("FAIL- Decant Header is open");
				return;
			}else{
			    $mDataQuery = "update decant_body set inventory_master_id = null where inventory_master_id=".$mInvId.";";
				    if (!mysqli_query($connection,$mDataQuery))
					return "FAIL-".mysqli_error($connection)."-".$mDataQuery;		
			}
			
			
			
		$mDataQuery = "SELECT id, conversion_factor FROM part WHERE part.id=".$mRow['part_id']." LIMIT 1";
			
			$mDataQuery = "DELETE FROM inventory_master WHERE id=".$mInvId." LIMIT 1";
			if (!mysqli_query($connection,$mDataQuery))
				exit("FAIL-".mysqli_error($connection)."-".$mDataQuery);
		}
		$mDataQuery = "INSERT INTO transaction_history (id,short_code,part_number,serial_reference,ran_or_order,txn_qty,from_location_code,date_created,last_updated,created_by,last_updated_by,transaction_reference_code,requires_decant,requires_inspection,comment,reason_code,conversion_factor,vendor_code,product_type_code)";
		$mDataQuery .= " VALUES(DEFAULT,'SA-OUT','".$mRow['part_number']."','".$mSerialReference."','".$mRow['ran_or_order']."',".$mTxnQty.",'".$mRow['location_code']."',now(),now(),'".$mCurrentUser."','".$mCurrentUser."','SA-OUT',0,1,'".$mComment."','".$mReasonCode."',".$mConversionFactor.",'".$mVendor."','".$mProductType."')";	
		if (!mysqli_query($connection,$mDataQuery))
		return "FAIL-".mysqli_error($connection)."-".$mDataQuery;	
	}
	echo ("OK  -saout");
	return;
}

elseif (($function != null) && (strtolower($function) == "sain"))
{
	$mStatusCode = "";
	$mComment = "";
	$mReasonCode = "";
	$mConversionFactor = 1;
//	echo("FAIL-TEST1-".$filter);
//	return;

// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$filter);
// Create Associative Array by Exploding Array Elements on :
	for ($i = 0; $i < count($mArray1); $i++)
	{
		$s = 0;
		$e = 0;
		$k1 = $mArray1[$i];
		$s = strpos($k1,":");
		if ($s >= 0)
		{
			$k2 = substr($k1,0,$s);
			$v2 = substr($k1,$s+1);
		}
		if ($k2 == "partNumber")
			$mPartNumber = strtoupper(trim($v2));
		elseif ($k2 == "serialReference")
			$mSerialReference = trim($v2);
		elseif ($k2 == "ranOrder")
			$mRanOrder = trim($v2);
		elseif ($k2 == "locationCode")
			$mLocationCode = strtoupper(trim($v2));
		elseif ($k2 == "comment")
			$mComment = trim($v2);
		elseif ($k2 == "reasonCode")
			$mReasonCode = trim($v2);
		elseif ($k2 == "qty")
			$mQty = floatval($v2);
		elseif ($k2 == "currentUser")
			$mCurrentUser = trim($v2);
		elseif ($k2 == "sendMessage")
			$mSndMsg = trim($v2);
		elseif ($k2 == "productTypeId")
			$mProductTypeId = trim($v2);
		elseif ($k2 == "transactionType")
			$mTransactionTypeId = strtoupper(trim($v2));
	}
	if (($mSerialReference == "") || (!$mSerialReference))
	{
		echo("FAIL-Invalid Serial Reference");
		return;
	}
	$mDataQuery = "SELECT part.id, conversion_factor ,requires_decant,vendor_reference_code,inventory_key,single_tier FROM part left join vendor on vendor.id=part.vendor_id WHERE part_number='".$mPartNumber."' LIMIT 1";
	if (!$mInvData = mysqli_query($connection,$mDataQuery))
		exit("FAIL-".mysqli_error($connection)."-".$mDataQuery);
	if (mysqli_num_rows($mInvData) == 0)
	{
		echo("FAIL-Invalid Part Number");
		return;
	}
	$mInvRow = mysqli_fetch_assoc($mInvData);
	$mPartId = $mInvRow['id'];
	$mConvFactor = $mInvRow['conversion_factor'];
	$mreqDecant = $mInvRow['requires_decant'];
	$mVendor = $mInvRow['vendor_reference_code'];
	$mSingleTier = $mInvRow['single_tier'];
	$mInventoryKey = $mInvRow['inventory_key'];
	$mTxnQty = $mQty*$mConvFactor;
//	$mTxnQty = $mQty;
	if ($mSingleTier==true && $mInventoryKey == null)
			{
				echo ("FAIL- Inventory Key is null");
				return;
			}
	$mDataQuery = "SELECT id, inventory_status_id FROM location WHERE location_code='".$mLocationCode."' LIMIT 1";
	if (!$mLocData = mysqli_query($connection,$mDataQuery))
		exit("FAIL-".mysqli_error($connection)."-".$mDataQuery);
	if (mysqli_num_rows($mLocData) == 0)
	{
		echo("FAIL-Invalid Location".$mDataQuery."-".$mPartNumber."-".$mSerialReference);
		return;
	}
	$mLocRow = mysqli_fetch_assoc($mLocData);
	$mLocId = $mLocRow['id'];
	$mInvStsId = $mLocRow['inventory_status_id'];
	
	$mDataQuery = "SELECT product_type_code FROM product_type WHERE id='".$mProductTypeId."' LIMIT 1";
	if (!$mProductData = mysqli_query($connection,$mDataQuery))
		exit("FAIL-".mysqli_error($connection)."-".$mDataQuery);
	if (mysqli_num_rows($mProductData) == 0)
	{
		echo("FAIL-Invalid Product".$mDataQuery."-".$mProductTypeId."-".$mProductTypeId);
		return;
	}
	$mProductRow = mysqli_fetch_assoc($mProductData);
	$mProductTypeCode = $mProductRow['product_type_code'];
	
	$mDataQuery = "SELECT inventory_master.id AS id, inventory_status.inventory_status_code AS inventory_status_code FROM inventory_master LEFT JOIN inventory_status ON inventory_master.inventory_status_id=inventory_status.id WHERE inventory_master.part_number ='".$mPartNumber."' AND inventory_master.serial_reference='".$mSerialReference."' LIMIT 1";
	if (!$mData = mysqli_query($connection,$mDataQuery))
		exit ("FAIL-".mysqli_error($connection)."-".$mDataQuery);
	$mRow = mysqli_fetch_assoc($mData);
	
	if (mysqli_num_rows($mData) > 0)
	{
		if (strtolower($mRow['inventory_status_code']) != "ok")
		{
			echo(" FAIL-this serial is unavailable");
			return;
		}
		$mUpdQuery = "UPDATE inventory_master SET inventory_qty=inventory_qty+".$mTxnQty.", Last_updated=now(), last_updated_by='".$mCurrentUser."' WHERE id=".$mRow['id']." LIMIT 1";
		if (!mysqli_query($connection,$mUpdQuery))
		exit("FAIL-".mysqli_error($connection)."-".$mUpdQuery);
	}
	else
	{
		$mUpdQuery = "INSERT INTO inventory_master (id,version,allocated_qty,current_disposition_id,current_inventory_status_id,current_location_id,location_id,location_code,date_created,created_by,expiry_date,inventory_qty,available_qty,last_updated,last_updated_by,parent_tag_id,part_id,part_number,ran_or_order,recorded_missing,requires_decant,requires_inspection,serial_reference,originating_detail_id,conversion_factor,stock_date,active,inventory_status_id,product_type_id)";		
		$mUpdQuery .= " VALUES(DEFAULT,0,0,null,null,".$mLocId.",".$mLocId.",'".$mLocationCode."',now(),'".$mCurrentUser."',null,".$mTxnQty.",".$mTxnQty.",now(),'".$mCurrentUser."',null,".$mPartId.",'".$mPartNumber."','".$mRanOrder."',0,".$mreqDecant.",0,'".$mSerialReference."',null,".$mConvFactor.",now(),1,".$mInvStsId.",".$mProductTypeId.")";		
		if (!mysqli_query($connection,$mUpdQuery))
			exit("FAIL-".mysqli_error($connection)."-".$mUpdQuery);
	}
	$mDataQuery = "INSERT INTO transaction_history (id,comment,short_code,part_number,serial_reference,ran_or_order,txn_qty,from_location_code,to_location_code,date_created,last_updated,created_by,last_updated_by,transaction_reference_code,requires_decant,requires_inspection,reason_code,product_type_code,vendor_code)";
	$mDataQuery .= " VALUES(DEFAULT,'".$mComment."','SA-IN','".$mPartNumber."','".$mSerialReference."','".$mRanOrder."',".$mTxnQty.",null,'".$mLocationCode."',now(),now(),'".$mCurrentUser."','".$mCurrentUser."','SA-IN',".$mreqDecant.",1,'".$mReasonCode."','".$mProductTypeCode."','".$mVendor."')";	
	if (!mysqli_query($connection,$mDataQuery))
		return "FAIL-".mysqli_error($connection)."-".$mDataQuery;	

// Test if Notification Message is needed
	if ($mSndMsg = 1)
	{
//		include_once ('createmessage.php');
	}
	
	echo ("OK  -sain");
	return;
}

echo ("FAIL-No Function-".$function."-".$filter);
return;
?>