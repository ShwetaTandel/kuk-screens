<?php
// report.php
// VIMS RDT Reports
// ----------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------
header('Cache-Control: no-cache, no-store, must-revalidate');   
header('Expires: 0');
// Header to allow Cross-Server AJAX Connection
header('Access-Control-Allow-Origin: *');

set_time_limit(0);

include ('../config/phpConfig.php');
include ('../config/ChromePhp.php');


// error_reporting(E_ALL);
// ini_set('display_errors', 1);

// report - VIMS RDT Report Controller

$mFunction  		= $_GET['function'];
$mConnect   		= $_GET['connection'];
$mUser				= $_GET['user'];
$mPwd				= $_GET['pword'];
$mHost              = $_GET['host'];
$mDbName			= $_GET['dbase'];
$mTableName 		= $_GET['table'];
$mFilter			= $_GET['filter'];
$mReturnFields		= $_GET['returnfields'];
ChromePhp::log("here first 1");

$mFilter = str_replace("|AND|","&",$mFilter);
$mFilter = str_replace("/dbase/",$mDbName,$mFilter);

$mSQLData = array();

$mErrMsg = "OK  -";

// Check for Session Time-Out
if (($mFunction != null) && ($mFunction == "sessiontimeout"))
{
	$mTimeOut = 900;
	$mFilter = str_replace("=",":",$mFilter);
	$mFilter = str_replace("&",",",$mFilter);
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$mFilter);
// Create Associative Array by Exploding Array Elements on :
	foreach($mArray1 as $k1=>$v1)
	{
		list($k2, $v2) = explode(":",$mArray1[$k1]);
		if ($k2 == "timeout")
			$mTimeOut = $v2*3600;
	}
	
	session_start();
	if(time() - $_SESSION['userData']['timestamp'] > $mTimeOut)  
	{ 
// subtract new timestamp from the old one
		echo("FALO-Session Timed-out");
		unset($_SESSION['userData']);
		exit;
	} 
	else 
	{
		$_SESSION['userData']['timestamp'] = time(); //set new timestamp
	}
}

// printer mapping
elseif (($mFunction != null) && ($mFunction=="getprinter"))
{
// Set default error message
	$mErrMsg = "OK  -";
// Explode Input Parm
// Replace Chars in Input String to emulate a JSON format instead of GET format
	$mFilter = str_replace("=",":",$mFilter);
	$mFilter = str_replace("&",",",$mFilter);
	$i=0;
	$mPage = "";
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$mFilter);
// Create Associative Array by Exploding Array Elements on :
	foreach($mArray1 as $k1=>$v1)
	{
		list($k2, $v2) = explode(":",$mArray1[$k1]);
		if ($k2 == "printer")
			$mPrinter = strtoupper($v2);
		elseif ($k2 == "currentUser")
			$mCurrentUser = strtoupper($v2);
		elseif ($k2 == "page")
			$mPage = strtoupper($v2);
	}
// mysql connection - Use Passed values...
	
			
//	exit("FAIL-TEST1-".$mFilter);
	$mPrinterAdd = "";
	$mDataQuery = "SELECT id AS id, printer_address AS printer_address, description AS printer_name FROM printer_mapping WHERE printer_code='".$mPrinter."' OR printer_address='".$mPrinter."' LIMIT 1";
//	exit("FAIL-TEST1-".$mFilter."-".$mDataQuery);
	if (!mysqli_query($connection,$mDataQuery))
		exit("FAIL-".mysqli_error($connection).$mDataQuery);
	$mData = (mysqli_query($connection,$mDataQuery));
//	exit ("FAIL-".mysqli_error($connection).$mDataQuery."-".mysqli_num_rows($mData));
	if (mysqli_num_rows($mData) == 0)
		exit("FAIL-Invalid Printer Code.");

	$mRow = mysqli_fetch_assoc($mData);
	$mPrinterId = ($mRow['id']);
	$mPrinterAdd = ($mRow['printer_address']);
	$mPrinterName = ($mRow['printer_name']);
	
	if (substr($mErrMsg,0,4) != "FAIL")
		$mErrMsg = "OK  -getprinter";
	
//	if ($mPrinterAdd == "")
//		$mPrinterAdd = "172.18.21.150";
	
	if ($mPage != "")
		exit("DATA:printercontent:".$mPrinterName.",printerinput:".$mPrinterAdd.":I:".$mPage);
	else
		exit("DATA:printercontent:".$mPrinterName.",printerinput:".$mPrinterAdd);
}

// Pallet Labels
elseif (($mFunction != null) && ($mFunction=="pltlabelpdf"))
{
// Set default error message
	$mErrMsg = "OK  -";
// Explode Input Parm
// Replace Chars in Input String to emulate a JSON format instead of GET format
	$mFilter = str_replace("=",":",$mFilter);
	$mFilter = str_replace("&",",",$mFilter);
	$i=0;
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$mFilter);
// Create Associative Array by Exploding Array Elements on :
	foreach($mArray1 as $k1=>$v1)
	{
		list($k2, $v2) = explode(":",$mArray1[$k1]);
		if ($k2 == "prefix")
			$mPrefix = strtoupper($v2);
		elseif ($k2 == "number")
			$mNumber = strtoupper($v2);
		elseif ($k2 == "printer")
			$mPrinter = strtoupper($v2);
		elseif ($k2 == "label")
			$mLabel = strtoupper($v2);
		elseif ($k2 == "currentUser")
			$mCurrentUser = strtoupper($v2);
	}
	
//	exit("FAIL-TEST1-".$mFilter);
	
	include_once('../reports/pltlabelpdf.php');
	$mStatus = labelPrint($mPrefix,$mNumber,$mPrinter,$mLabel,$mHost,$mUser,$mPwd,$mDbName,"n");
	if (substr($mStatus,0,4) != "FAIL")
		$mStatus = "OK  -pltlabelpdf";
}

// Location Labels
elseif (($mFunction != null) && ($mFunction=="loclabelpdf"))
{
// Set default error message
	$mErrMsg = "OK  -";
// Explode Input Parm
// Replace Chars in Input String to emulate a JSON format instead of GET format
	$mFilter = str_replace("=",":",$mFilter);
	$mFilter = str_replace("&",",",$mFilter);
	$i=0;
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$mFilter);
// Create Associative Array by Exploding Array Elements on :
	foreach($mArray1 as $k1=>$v1)
	{
		list($k2, $v2) = explode(":",$mArray1[$k1]);
		if ($k2 == "fromloc")
			$mFromLoc = strtoupper($v2);
		elseif ($k2 == "toloc")
			$mToLoc = strtoupper($v2);
		elseif ($k2 == "number")
			$mNumber = strtoupper($v2);
		elseif ($k2 == "printer")
			$mPrinter = strtoupper($v2);
		elseif ($k2 == "label")
			$mLabel = strtoupper($v2);
		elseif ($k2 == "currentUser")
			$mCurrentUser = strtoupper($v2);
	}
	
//	exit("FAIL-TEST1-".$mFilter);
	
	include_once('../reports/locationlabelpdf.php');
	$mStatus = labelPrint($mFromLoc,$mToLoc,$mNumber,$mPrinter,$mLabel,$mHost,$mUser,$mPwd,$mDbName,"n");
	if (substr($mStatus,0,4) != "FAIL")
		$mStatus = "OK  -loclabelpdf";
}

// Decant Labels
elseif (($mFunction != null) && ($mFunction=="decantlabelpdf"))
{
// Set default error message
	$mErrMsg = "";
// Explode Input Parm
// Replace Chars in Input String to emulate a JSON format instead of GET format
	$mFilter = str_replace("=",":",$mFilter);
	$mFilter = str_replace("&",",",$mFilter);
	$i=0;
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$mFilter);
// Create Associative Array by Exploding Array Elements on :
	foreach($mArray1 as $k1=>$v1)
	{
		list($k2, $v2) = explode(":",$mArray1[$k1]);
		if ($k2 == "decantid")
			$mDecantId = $v2;
		elseif ($k2 == "printer")
			$mPrinter = strtoupper($v2);
		elseif ($k2 == "label")
			$mLabel = strtoupper($v2);
		elseif ($k2 == "currentUser")
			$mCurrentUser = strtoupper($v2);
	}
	
//	exit("FAIL-TEST1-".$mFilter."-".$mDecantId);
	
	
	if ($mPrinter == "")
	{
		$mPrinter = getPrinter($mHost,$mUser,$mPwd,$mDbName,$mCurrentUser,'decant');
		if (substr($mPrinter,0,2)=="FA")
		{
			$mErrMsg = $mPrinter;
		}
	}
//	exit("FAIL-TEST1-".$mFilter."-".$mPrinter);
	if ($mErrMsg == "")
	{
		include_once('../reports/decantlabelpdf.php');
		$mStatus = labelPrint($mDecantId,$mPrinter,$mLabel,$mHost,$mUser,$mPwd,$mDbName,"n");
	}
//	if (substr($mStatus,0,4) != "FAIL")
//		$mStatus = "OK  -decantlabelpdf";
}

// Receipt Labels
elseif (($mFunction != null) && ($mFunction=="reclabelpdf"))
{
	
    
    ////htttestp://172.18.25.4/rdtcif/rdtvims/rdtphp/report.php?user:root,pword:root,host:172.18.25.4,dbase:cif,function:reclabelpdf,filter:haulier:SARAH
// Set default error messag
	$mErrMsg = "";
// Explode Input Parm
// Replace Chars in Input String to emulate a JSON format instead of GET format
	$mFilter = str_replace("=",":",$mFilter);
	$mFilter = str_replace("&",",",$mFilter);
	ChromePhp::log("here1");
	$i=0;
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$mFilter);
// Create Associative Array by Exploding Array Elements on :
	$mPrinter = "";
	
	foreach($mArray1 as $k1=>$v1)
	{
		list($k2, $v2) = explode(":",$mArray1[$k1]);
		if ($k2 == "grn")
			$grnRef = $v2;
		elseif ($k2 == "printer")
			$mPrinter = $v2;
		elseif ($k2 == "label")
			$mLabel = strtoupper($v2);
		elseif ($k2 == "currentUser")
			$mCurrentUser = strtoupper($v2);
	}
	ChromePhp::log("The printer code/GRN is ".$mPrinter.$grnRef);
	if ($mPrinter == "")
	{
            $mPrinter = getPrinter('receiving_pot');
        }else{
            $mPrinter = getPrinter($mPrinter);
            
            if (substr($mPrinter,0,2)=="FA")
            {
                    $mErrMsg = $mPrinter;
            }
	
        }
            
	
//	exit("FAIL-TEST1-".$mFilter."-".$mPrinter);
	if ($mErrMsg == "")
	{
		include_once('../reports/receiptOdetteLabelpdf.php');
		$mStatus = labelPrint($grnRef,$mPrinter,$mLabel,"n");
	}
}

// Receipt Labels
elseif (($mFunction != null) && ($mFunction=="partlabelpdf"))
{
// Set default error message
	$mErrMsg = "";
// Explode Input Parm
// Replace Chars in Input String to emulate a JSON format instead of GET format
	$mFilter = str_replace("=",":",$mFilter);
	$mFilter = str_replace("&",",",$mFilter);
	$i=0;
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$mFilter);
// Create Associative Array by Exploding Array Elements on :
	$mPrinter = "";
	$mSerialReference = "";
	$mNumber = 1;
	foreach($mArray1 as $k1=>$v1)
	{
		list($k2, $v2) = explode(":",$mArray1[$k1]);
		if ($k2 == "ranorder")
			$mRanOrOrder = $v2;
		elseif ($k2 == "ranororder")
			$mRanOrOrder = $v2;
		elseif ($k2 == "partnumber")
			$mPartNumber = $v2;
		elseif ($k2 == "snp")
			$mSnp = $v2;
		elseif ($k2 == "serialreference")
			$mSerialReference = $v2;
		elseif ($k2 == "number")
			$mNumber = $v2;
		elseif ($k2 == "printer")
			$mPrinter = strtoupper($v2);
		elseif ($k2 == "label")
			$mLabel = strtoupper($v2);
		elseif ($k2 == "currentUser")
			$mCurrentUser = strtoupper($v2);
	}
	
	if ($mPrinter == "")
	{
            $mPrinter = getPrinter('receiving_kuk');
        }else{
            $mPrinter = getPrinter($mPrinter);
            ChromePhp::log("The ip of printer is ".$mPrinter);
            if (substr($mPrinter,0,2)=="FA")
            {
                    $mErrMsg = $mPrinter;
            }
	
        }
//	exit("FAIL-TEST1-".$mFilter."-".$mPrinter);
	if ($mErrMsg == "")
	{
		include_once('../reports/partlabelpdf.php');
		$mStatus = labelPrint($mRanOrOrder,$mPartNumber,$mSnp,$mNumber,$mPrinter,$mLabel,$mHost,$mUser,$mPwd,$mDbName,"n",$mSerialReference);
	}
}

// Part Labels
elseif (($mFunction != null) && ($mFunction=="partlabelsmallpdf"))
{
// Set default error message
	$mErrMsg = "";
// Explode Input Parm
// Replace Chars in Input String to emulate a JSON format instead of GET format
	$mFilter = str_replace("=",":",$mFilter);
	$mFilter = str_replace("&",",",$mFilter);
	$i=0;
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$mFilter);
// Create Associative Array by Exploding Array Elements on :
	$mPrinter = "";
	$mSerialReference = "";
	$mNumber = 1;
	foreach($mArray1 as $k1=>$v1)
	{
		list($k2, $v2) = explode(":",$mArray1[$k1]);
		if ($k2 == "ranorder")
			$mRanOrOrder = $v2;
		elseif ($k2 == "ranororder")
			$mRanOrOrder = $v2;
		elseif ($k2 == "partnumber")
			$mPartNumber = $v2;
		elseif ($k2 == "snp")
			$mSnp = $v2;
		elseif ($k2 == "serialreference")
			$mSerialReference = $v2;
		elseif ($k2 == "number")
			$mNumber = $v2;
		elseif ($k2 == "printer")
			$mPrinter = strtoupper($v2);
		elseif ($k2 == "label")
			$mLabel = strtoupper($v2);
		elseif ($k2 == "currentUser")
			$mCurrentUser = strtoupper($v2);
	}
	
	if ($mPrinter == "")
	{
		$mPrinter = getPrinter($mHost,$mUser,$mPwd,$mDbName,$mCurrentUser,'partlabelsmall');
		if (substr($mPrinter,0,2)=="FA")
		{
			$mErrMsg = $mPrinter;
		}
	}
//	exit("FAIL-TEST1-".$mFilter."-".$mPrinter);
	if ($mErrMsg == "")
	{
		include_once('../reports/partlabelsmallpdf.php');
		$mStatus = labelPrint($mRanOrOrder,$mPartNumber,$mSnp,$mNumber,$mPrinter,$mLabel,$mHost,$mUser,$mPwd,$mDbName,"n",$mSerialReference);
	}
}

// Pick Labels
elseif (($mFunction != null) && ($mFunction=="picklabelsmallpdf"))
{
// Set default error message
	$mErrMsg = "";
// Explode Input Parm
// Replace Chars in Input String to emulate a JSON format instead of GET format
	$mFilter = str_replace("=",":",$mFilter);
	$mFilter = str_replace("&",",",$mFilter);
	$i=0;
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$mFilter);
// Create Associative Array by Exploding Array Elements on :
	$mPrinter = "";
	$mSerialReference = "";
	$mNumber = 1;
	foreach($mArray1 as $k1=>$v1)
	{
		list($k2, $v2) = explode(":",$mArray1[$k1]);
		if ($k2 == "documentreference")
			$mDocReference = $v2;
		elseif ($k2 == "printer")
			$mPrinter = strtoupper($v2);
		elseif ($k2 == "label")
			$mLabel = strtoupper($v2);
		elseif ($k2 == "currentUser")
			$mCurrentUser = strtoupper($v2);
	}
	
	if ($mPrinter == "")
	{
		$mPrinter = getPrinter('picklabelsmall');
		if (substr($mPrinter,0,2)=="FA")
		{
			$mErrMsg = $mPrinter;
		}
	}
	if ($mErrMsg == "")
	{
		include_once('../reports/picklabelsmallpdf.php');
		$mStatus = labelPrint($mDocReference,$mPrinter,$mLabel,$mHost,$mUser,$mPwd,$mDbName,"n");
	}
}elseif (($mFunction != null) && ($mFunction=="picklabelzebra"))
{
    ChromePhp::log("I am here");
// Set default error message
	$mErrMsg = "";
// Explode Input Parm
// Replace Chars in Input String to emulate a JSON format instead of GET format
	$mFilter = str_replace("=",":",$mFilter);
	$mFilter = str_replace("&",",",$mFilter);
	$i=0;
// Explode Input String Into Array - Separated by ,
	$mArray1 = explode(",",$mFilter);
// Create Associative Array by Exploding Array Elements on :
	$mPrinter = "";
	$mSerialReference = "";
	$mNumber = 1;
	foreach($mArray1 as $k1=>$v1)
	{
		list($k2, $v2) = explode(":",$mArray1[$k1]);
		if ($k2 == "pickdetailid")
			$mPickDetailId = $v2;
		elseif ($k2 == "pickreference")
			$mPickReference = strtoupper($v2);
		elseif ($k2 == "printer")
			$mPrinter = strtoupper($v2);
		elseif ($k2 == "label")
			$mLabel = strtoupper($v2);
		elseif ($k2 == "currentUser")
			$mCurrentUser = strtoupper($v2);
	}
	
	if ($mPrinter == "")
	{
		$mPrinter = getPrinter('ZEBK01');
		if (substr($mPrinter,0,2)=="FA")
		{
			$mErrMsg = $mPrinter;
		}
	}
	else
	{
		$mPrinter = getPrinter($mPrinter);
		if (substr($mPrinter,0,2)=="FA")
		{
			$mErrMsg = $mPrinter;
		}
	}
	if ($mErrMsg == "")
	{
		include_once('../reports/picklabelpdf.php');
		$mStatus = labelPrint($mPrinter,$mLabel,$mHost,$mUser,$mPwd,$mDbName,$mCurrentUser,"n",$mPickDetailId);
	}
}

if (($mStatus == "") && ($mErrMsg != ""))
	$mStatus = $mErrMsg;
if ((substr($mStatus,0,2) != "OK") && (substr($mStatus,0,2) != "FA") && (substr($mStatus,0,2) != "DA"))
	$mStatus = "FAIL-".$mStatus;
// echo json_encode(array_merge(array("status" => $mStatus),array("data" => $mDataStr),$mSQLData));
if ($mDataStr == "")
	echo $mStatus;
else if(substr($mDataStr,0,4) == "DATA")
	echo $mDataStr;
else
	echo $mStatus.$mDataStr;		
return;

function getPrinter($mPrinter)
{
        global $connection;
	$mPrinterAdd = "";
        
	$sql = "SELECT id AS id, printer_address AS printer_address, description AS printer_name FROM kuk.printer_mapping WHERE printer_code='".$mPrinter."' OR printer_address='".$mPrinter."' LIMIT 1";
        
	$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
	if (mysqli_num_rows($result) == 0)
		return("FAIL-Invalid Printer Code.");

	$mRow = mysqli_fetch_assoc($result);
	$mPrinterId = ($mRow['id']);
	$mPrinterAdd = ($mRow['printer_address']);
	$mPrinterName = ($mRow['printer_name']);
	
	return ($mPrinterAdd);
}

?>