      



<?php
 
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
$stuff = ''; 
$join = '';

// DB table to use
$table = 'transaction_history';
 
// Table's primary key
$primaryKey = 'id';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
  
    array( 'db' => 'ran_or_order', 'dt' => 0 ),
    array( 'db' => 'part_number',  'dt' => 1 ),
    array( 'db' => 'serial_reference',   'dt' => 2 ),
    array( 'db' => 'short_code',  'dt' => 3 ),
    array( 'db' => 'txn_qty',   'dt' => 4 ),
    array( 'db' => 'from_location_code',     'dt' => 5 ),
    array( 'db' => 'to_location_code', 'dt' => 6),
    array( 'db' => 'associated_document_reference', 'dt' => 7),
    array( 'db' => 'date_created', 'dt' => 8),
    array( 'db' => 'last_updated', 'dt' => 9),
    array( 'db' => 'last_updated_by', 'dt' => 10)
    
);


 
// SQL server connection information
$sql_details = array(
    'user' => 'root',
    'pass' => 'root',
    'db'   => 'kuk',
    'host' => 'van-kuk-test'
);
 
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require( '../action/ssp.class.php' );
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $stuff, $join )
);