<?php

include '../config/phpConfig.php';

$type = $_GET['type'];
$table = $_GET['table'];
$filename = $_GET['filename'];
$dFrom = $_GET['dFrom'];
$dTo = $_GET['dTo'];
$shortCode = $_GET['shortCode'];


if ($type === 'endOfMonth') {
    endOfMonth($table, $filename, $dFrom, $dTo, $con);
} else if ($type === 'custReceipt') {
    custReceipts($table, $filename, $dFrom, $con);
} else if ($type === 'transactionHistory') {
    transactionHistory($table, $filename, $dFrom, $dTo, $shortCode, $con);
} else if ($type === 'receiptReport') {
    receiptReport($table, $filename, $dFrom, $con);
} else if ($type === 'serialConsumed'){
    serialConsumed($table, $filename, $dFrom, $dTo, $con);
} else if ($type === 'sixReport'){
    sixReport($table, $filename, $dFrom, $dTo, $con);
} else if ($type === 'twoReport'){
    twoReport($table, $filename, $dFrom, $con);
} else if ($type === 'rrmcOrders'){
    rrmcOrders($table, $filename, $dFrom, $con);
}else if ($type === 'locationReport') {
    locationReport($table, $filename, $con);    
} 

function endOfMonth($table, $filename, $dFrom, $dTo, $con) {
     //from vantec.transaction_history WHERE date_created BETWEEN '"+startDate+" 00:00:01' AND '"+endDate+" 23:59:59' GROUP by short_code, ran_or_order ORDER by short_code, ran_or_order
    $sql = "select short_code,transaction_reference_code, ran_or_order, count(*) AS rec_count from $table WHERE date_created BETWEEN '".$dFrom." 00:00:01' AND '".$dTo." 23:59:59' GROUP by short_code, ran_or_order ORDER by short_code, ran_or_order;";
    $result = mysqli_query($con, $sql) or die("Couldn't execute query:<br>" . mysqli_error() . "<br>" . mysqli_errno());
    header("Content-Type: application/csv");
    header("Content-Disposition: attachment; filename=$filename.csv");
    header("Pragma: no-cache");
    header("Expires: 0");
    $lineStr =  "Row_Labels,Description,Total_Transactions,Total_TOs \r\n";
    $prvCode = null;
    $descCode = null;
    $prvTo = null;
    $totCount = 0;
    $toCount = 0;
    $sep = "\t";
  /*  $names = mysqli_fetch_fields($result);
    foreach ($names as $name) {
        print ($name->name . $sep);
    }
 $schema_insert = "";
        for ($j = 0; $j < mysqli_num_fields($result); $j++) {
            if (!isset($row[$j]))
                $schema_insert .= "NULL" . $sep;
            elseif ($row[$j] != "")
                $schema_insert .= "$row[$j]" . $sep;
            else
                $schema_insert .= "" . $sep;
        }
        $schema_insert = str_replace($sep . "$", "", $schema_insert);
        $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
        $schema_insert .= "\t";
        print(trim($schema_insert));
        print "\n";   */
    print($lineStr);
    print("\n");
    while ($row = mysqli_fetch_assoc($result)) {
        if (($prvCode != null) && ($prvCode != $row['short_code']))
                           {
                               print($prvCode.",".$descCode.",".$totCount.",".$toCount."\r\n");
                               $totCount=0;
                               $toCount = 0;
                               $prvTo = null;
                           }
                           if (($row['ran_or_order'] != null) && ($row['ran_or_order'] != ""))
                                          $toCount = $toCount+1;
                           $totCount =$totCount+$row['rec_count'];
                           $prvCode = $row['short_code'];
                           $descCode = $row['transaction_reference_code'];
    }
       print($prvCode.",".$descCode.",".$totCount.",".$toCount."\r\n");
}

function custReceipts($table, $filename, $dFrom, $con) {
    $sql = "Select document_header.document_reference_code, vendor.vendor_name ,document_header.expected_delivery,document_header.vehicle_reference,document_header.last_updated_by, substring(document_header.date_created,1,19)as date_created from $table join vendor on vendor.id = $table.vendor_id where document_header.date_created >= '$dFrom' and document_header.date_created <= '$dFrom 23:59:59' and document_type = 'Customer Receipt';";
    $result = mysqli_query($con, $sql) or die("Couldn't execute query:<br>" . mysqli_error() . "<br>" . mysqli_errno());
    $file_ending = "xls";
    header("Content-Type: application/xls");
    header("Content-Disposition: attachment; filename=$filename.xls");
    header("Pragma: no-cache");
    header("Expires: 0");
    $sep = "\t";
    $names = mysqli_fetch_fields($result);
    foreach ($names as $name) {
        print ($name->name . $sep);
    }
    print("\n");
    while ($row = mysqli_fetch_row($result)) {
        $schema_insert = "";
        for ($j = 0; $j < mysqli_num_fields($result); $j++) {
            if (!isset($row[$j]))
                $schema_insert .= "NULL" . $sep;
            elseif ($row[$j] != "")
                $schema_insert .= "$row[$j]" . $sep;
            else
                $schema_insert .= "" . $sep;
        }
        $schema_insert = str_replace($sep . "$", "", $schema_insert);
        $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
        $schema_insert .= "\t";
        print(trim($schema_insert));
        print "\n";
    }
}

function transactionHistory($table, $filename, $dFrom, $dTo, $shortCode, $con) {


    $sql = "Select * from $table WHERE date_created between '".$dFrom." 00:00:01' AND '".$dTo." 23:59:59' and short_code LIKE '$shortCode%';";
    $result = mysqli_query($con, $sql) or die("Couldn't execute query:<br>" . mysqli_error() . "<br>" . mysqli_errno());
 //   $file_ending = "xls";
    $file_ending = "csv";
 //   header("Content-Type: application/xls");
    header( "Content-Type: text/ms-excel;charset=utf-8" );
    header("Content-Disposition: attachment; filename=$filename.xls");
    header("Pragma: no-cache");
    header("Expires: 0");
    $sep = "\t";
    $names = mysqli_fetch_fields($result);
    foreach ($names as $name) {
        print ($name->name . $sep);
    }
    print("\n");
    while ($row = mysqli_fetch_row($result)) {
        $schema_insert = "";
        for ($j = 0; $j < mysqli_num_fields($result); $j++) {
            if (!isset($row[$j]))
                $schema_insert .= "NULL" . $sep;
            elseif ($row[$j] != "")
                $schema_insert .= "$row[$j]" . $sep;
            else
                $schema_insert .= "" . $sep;
        }
        $schema_insert = str_replace($sep . "$", "", $schema_insert);
        $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
        $schema_insert .= "\t";
        print(trim($schema_insert));
        print "\n";
    }
}

function receiptReport($table, $filename, $dFrom, $con) {
    
    $sql = "select e.document_reference_code as document_reference_code, m.document_reference_code as parent_receipt ,substring(e.date_created,1,30) as date_created,substring(e.last_updated,1,30) as last_updated from $table e left join $table m on e.parent_id = m.id where e.last_updated >= '$dFrom' and e.last_updated <= '$dFrom 23:59:59' AND e.transmitted = 1 and e.document_reference_code like 'RCP%';";
    $result = mysqli_query($con, $sql) or die("Couldn't execute query:<br>" . mysqli_error() . "<br>" . mysqli_errno());
    $file_ending = "xls";
    header("Content-Type: application/xls");
    header("Content-Disposition: attachment; filename=$filename.xls");
    header("Pragma: no-cache");
    header("Expires: 0");
    $sep = "\t";
    $names = mysqli_fetch_fields($result);
    foreach ($names as $name) {
        print ($name->name . $sep);
    }
    print("\n");
    while ($row = mysqli_fetch_row($result)) {
        $schema_insert = "";
        for ($j = 0; $j < mysqli_num_fields($result); $j++) {
            if (!isset($row[$j]))
                $schema_insert .= "NULL" . $sep;
            elseif ($row[$j] != "")
                $schema_insert .= "$row[$j]" . $sep;
            else
                $schema_insert .= "" . $sep;
        }
        $schema_insert = str_replace($sep . "$", "", $schema_insert);
        $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
        $schema_insert .= "\t";
        print(trim($schema_insert));
        print "\n";
    }
    
}

function serialConsumed($table, $filename, $dFrom, $dTo, $con){
    $sql = "SELECT part_number,serial_reference,from_location_code,short_code FROM $table where (short_code = 'GLTP' OR short_code = 'NPRDTO' OR short_code = 'AFTP' OR short_code = 'OBSPP' OR short_code = 'OBSPS' OR short_code = 'CSET' OR short_code = 'PMUN' OR short_code = 'PC_PICK' OR short_code = 'EMERG' OR short_code = 'NPRD' OR short_code = 'XDOCK') AND date_created >= '$dFrom' and date_created <= '$dTo'  AND serial_consumed = true;";
    $result = mysqli_query($con, $sql) or die("Couldn't execute query:<br>" . mysqli_error() . "<br>" . mysqli_errno());
    $file_ending = "xls";
    header("Content-Type: application/xls");
    header("Content-Disposition: attachment; filename=$filename.xls");
    header("Pragma: no-cache");
    header("Expires: 0");
    $sep = "\t";
    $names = mysqli_fetch_fields($result);
    foreach ($names as $name) {
        print ($name->name . $sep);
    }
    print("\n");
    while ($row = mysqli_fetch_row($result)) {
        $schema_insert = "";
        for ($j = 0; $j < mysqli_num_fields($result); $j++) {
            if (!isset($row[$j]))
                $schema_insert .= "NULL" . $sep;
            elseif ($row[$j] != "")
                $schema_insert .= "$row[$j]" . $sep;
            else
                $schema_insert .= "" . $sep;
        }
        $schema_insert = str_replace($sep . "$", "", $schema_insert);
        $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
        $schema_insert .= "\t";
        print(trim($schema_insert));
        print "\n";
    }
}

function sixReport($table, $filename, $dFrom, $dTo, $con){
    $sql = "select * from $table where date_created >= '$dFrom' And date_created <= '$dTo' AND transaction_reference_code = 'MISSING'";
    $result = mysqli_query($con, $sql) or die("Couldn't execute query:<br>" . mysqli_error() . "<br>" . mysqli_errno());
    $file_ending = "xls";
    header("Content-Type: application/xls");
    header("Content-Disposition: attachment; filename=$filename.xls");
    header("Pragma: no-cache");
    header("Expires: 0");
    $sep = "\t";
    $names = mysqli_fetch_fields($result);
    foreach ($names as $name) {
        print ($name->name . $sep);
    }
    print("\n");
    while ($row = mysqli_fetch_row($result)) {
        $schema_insert = "";
        for ($j = 0; $j < mysqli_num_fields($result); $j++) {
            if (!isset($row[$j]))
                $schema_insert .= "NULL" . $sep;
            elseif ($row[$j] != "")
                $schema_insert .= "$row[$j]" . $sep;
            else
                $schema_insert .= "" . $sep;
        }
        $schema_insert = str_replace($sep . "$", "", $schema_insert);
        $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
        $schema_insert .= "\t";
        print(trim($schema_insert));
        print "\n";
    }
}

function twoReport($table, $filename, $dFrom, $con){
       $sql = "SELECT allocation_detail.id,allocation_detail.allocated_qty,allocation_detail.line_no,substring(allocation_detail.date_created,1,30) as date_created,substring(allocation_detail.last_updated,1,30) as last_updated,allocation_detail.last_updated_by, inventory_master.part_number, inventory_master.serial_reference,location.location_code, allocation_detail.processed, allocation_detail.requirement_id, scheduled_work.third_party_reference, allocation_detail.originating_request_id, allocation_detail.active, allocation_detail.to_location_id, allocation_detail.replenishment,allocation_detail.priority FROM $table left join inventory_master on allocation_detail.inventory_master_id = inventory_master.id left join scheduled_work on allocation_detail.scheduled_work_id = scheduled_work.id left join location on inventory_master.current_location_id = location.id where allocation_detail.last_updated >= '$dFrom 00:00:00' AND allocation_detail.last_updated <= '$dFrom 23:59:59' and allocation_detail.priority > '1';";
    $result = mysqli_query($con, $sql) or die("Couldn't execute query:<br>" . mysqli_error() . "<br>" . mysqli_errno());
    $file_ending = "xls";
    header("Content-Type: application/xls");
    header("Content-Disposition: attachment; filename=$filename.xls");
    header("Pragma: no-cache");
    header("Expires: 0");
    $sep = "\t";
    $names = mysqli_fetch_fields($result);
    foreach ($names as $name) {
        print ($name->name . $sep);
    }
    print("\n");
    while ($row = mysqli_fetch_row($result)) {
        $schema_insert = "";
        for ($j = 0; $j < mysqli_num_fields($result); $j++) {
            if (!isset($row[$j]))
                $schema_insert .= "NULL" . $sep;
            elseif ($row[$j] != "")
                $schema_insert .= "$row[$j]" . $sep;
            else
                $schema_insert .= "" . $sep;
        }
        $schema_insert = str_replace($sep . "$", "", $schema_insert);
        $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
        $schema_insert .= "\t";
        print(trim($schema_insert));
        print "\n";
    }
    
}

function rrmcOrders($table, $filename, $dFrom, $con){
       $sql = "SELECT substring(date_created,1,19) as date_created, tanum, document_reference_code, bwlvs, document_type, substring(expected_delivery,1,19) as expected_delivery  FROM $table where date_created >= '$dFrom' and date_created <= '$dFrom 23:59:59' and document_type = 'RRM Order';";
    $result = mysqli_query($con, $sql) or die("Couldn't execute query:<br>" . mysqli_error() . "<br>" . mysqli_errno());
    $file_ending = "xls";
    header("Content-Type: application/xls");
    header("Content-Disposition: attachment; filename=$filename.xls");
    header("Pragma: no-cache");
    header("Expires: 0");
    $sep = "\t";
    $names = mysqli_fetch_fields($result);
    foreach ($names as $name) {
        print ($name->name . $sep);
    }
    print("\n");
    while ($row = mysqli_fetch_row($result)) {
        $schema_insert = "";
        for ($j = 0; $j < mysqli_num_fields($result); $j++) {
            if (!isset($row[$j]))
                $schema_insert .= "NULL" . $sep;
            elseif ($row[$j] != "")
                $schema_insert .= "$row[$j]" . $sep;
            else
                $schema_insert .= "" . $sep;
        }
        $schema_insert = str_replace($sep . "$", "", $schema_insert);
        $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
        $schema_insert .= "\t";
        print(trim($schema_insert));
        print "\n";
    }
    
}

function locationReport($table, $filename, $con){
       $sql = "SELECT location_code, location_code, location_type.location_type_code, location_sub_type.location_sub_type_code, IF(multi_part_location = 1, 'YES', 'NO') as multi_part, location_filling_status.filling_code, inventory_status.inventory_status_code, $table.last_updated, $table.last_updated_by  FROM $table left join location_type on location_type.id = location_type_id left join location_sub_type on location_sub_type.id = location_sub_type_id left join location_filling_status on location_filling_status.id = filling_status_id left join inventory_status on inventory_status.id = inventory_status_id";
    $result = mysqli_query($con, $sql) or die("Couldn't execute query:<br>" . mysqli_error() . "<br>" . mysqli_errno());
    $file_ending = "xls";
    header("Content-Type: application/xls");
    header("Content-Disposition: attachment; filename=$filename.xls");
    header("Pragma: no-cache");
    header("Expires: 0");
    $sep = "\t";
    $names = mysqli_fetch_fields($result);
    foreach ($names as $name) {
        print ($name->name . $sep);
    }
    print("\n");
    while ($row = mysqli_fetch_row($result)) {
        $schema_insert = "";
        for ($j = 0; $j < mysqli_num_fields($result); $j++) {
            if (!isset($row[$j]))
                $schema_insert .= "NULL" . $sep;
            elseif ($row[$j] != "")
                $schema_insert .= "$row[$j]" . $sep;
            else
                $schema_insert .= "" . $sep;
        }
        $schema_insert = str_replace($sep . "$", "", $schema_insert);
        $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
        $schema_insert .= "\t";
        print(trim($schema_insert));
        print "\n";
    }
    
}

?>
