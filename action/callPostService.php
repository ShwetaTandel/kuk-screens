<?php
        $mConnect   		= $_GET['connection']; // Service URL
        $mFilter	        = $_GET['filter']; // JSON DATA
        $mFunction              = $_GET['function']; // Service function name
	$mJsonObject = $mFilter;
// POST Operation
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mConnect.$mFunction);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mJsonObject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$mResult = curl_exec($ch);
		$mErrors = curl_error($ch);
		$mResponse = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		if ($mResponse != "200"){
			exit("FAIL-Message Error Code=".$mResult.":".$mResponse.":".$mErrors);
                }
                else{
                    echo("OK - ".$mResult);
                }
	}
	catch (Exception $e)
	{
		exit("FAIL-Connection Error at - ".$mConnect.$mFilter.":".$e);
	}
?>