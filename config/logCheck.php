<?php
    session_start();
    include '../config/ChromePhp.php';
    include '../config/phpConfig.php';
    
    if (!isset($_SESSION['userData'])) {
        echo '<h1>Please login. Go back to login page.</h1>  <form action="login.php"><input type="submit" value="Login" /></form>';
        die();
    }
    if ($_SESSION['userData']['user_level_id'] === '3'){
          echo '<h1>Access Denied!</h1>  <form action="login.php"><input type="submit" value="Login" /></form>';
        die();
    }
    $session_value = (isset($_SESSION['userData'])) ? $_SESSION['userData'] : 'No User';
    
  ?>
