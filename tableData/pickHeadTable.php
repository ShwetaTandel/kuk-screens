      
 <?php
    include ('../config/phpConfig.php');

    //fetch table rows from mysql db
    $sql = "SELECT pick_header.id, pick_header.document_reference, pick_header.customer_reference, pick_header.order_header_id,document_header.bwlvs, pick_header.time_slot, document_status.document_status_description as status, pick_header.date_created, pick_header.created_by, pick_header.last_updated_date, pick_header.last_updated_by, document_header.pick_type FROM pick_header left join document_status on pick_header.document_status_id = document_status.id left join document_header on document_header.id = pick_header.order_header_id;";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = $row;
    }

    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>

