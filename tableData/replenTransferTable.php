      
 <?php
    //open connection to mysql db
include ('../config/phpConfig.php');
    //fetch table rows from mysql db
    $sql = "SELECT replen_transfer_history.*, part.part_number, fp.pick_group_code as from_group, tp.pick_group_code as to_group FROM kuk.replen_transfer_history left join pick_group tp on tp.id = to_pick_group_id left join pick_group fp on fp.id = from_pick_group_id left join inventory_master on inventory_master.id = inventory_master_id left join part on part.id = replen_transfer_history.part_id;";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = $row;
    }

    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>