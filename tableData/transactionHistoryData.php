<?php

error_reporting(E_ALL & ~E_NOTICE);
// ini_set('display_errors', 1);

    //open connection to mysql db
include ('../config/phpConfig.php');
    //fetch table rows from mysql db
	
//create an array
    $dataArray = array();
	
	$table = "transaction_history";
	$offset="";
	$records="";
	$filter="";
	$orderBy = "";
	$offset = $_POST['start'];
	$records = $_POST['length'];
	$filter = $_POST['filter'];
	$orderBy = $_POST['sort'];
	$orderIndex = $_POST['order'][0]['column'];
	$columnName = $_POST['columns'][$orderIndex]['data']; // Column name
	$orderDirection = $_POST['order'][0]['dir']; // asc or desc
	$iFilteredTotal = 0;
	$iTotal = 0;
	
	$orderColumn = array('id','ran_or_order','part_number','serial_reference','to_plt','short_code','txn_qty','from_location_code','to_location_code','customer_reference','last_updated','last_updated_by');
	
	// Total data set length
	$sql = "select TABLE_ROWS from information_schema.TABLES where TABLE_SCHEMA = 'vantec' AND table_name='".$table."'";
//	$sql = "SELECT count(id) FROM ".$table;
	$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
	$row = mysqli_fetch_array($result);
	$iTotal = $row[0];
//	$iTotal = mysqli_num_rows($result);
/*		
    	if ($filter != "")
	{
		$sql = "SELECT count(id) FROM ".$table;	
		$sql .= " WHERE ".$filter;
		$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
		$row = mysqli_fetch_array($result);
//		$iFilteredTotal = $row[0];
//		$iFilteredTotal = mysqli_num_rows($result);
	}
	$iFilteredTotal = $row[0];
*/

	$timeout = 10;  /* seconds for timeout */
	$realConnection = mysqli_init( );
	$realConnection->options( MYSQLI_OPT_CONNECT_TIMEOUT, $timeout ) ||
		die( 'mysqli_options failed: ' . $link->error );
	$realConnection->real_connect($db,$mDbUser,$mDbPassword,$mDbName) ||
		die( 'mysqli_real_connect failed: ' . $link->error );
	
	$sql = "SELECT transaction_history.id AS id,transaction_history.ran_or_order,transaction_history.part_number,transaction_history.serial_reference,transaction_history.to_plt,transaction_history.short_code,transaction_history.txn_qty,transaction_history.from_location_code,transaction_history.to_location_code,transaction_history.customer_reference,transaction_history.last_updated,transaction_history.last_updated_by,transaction_history.conversion_factor,transaction_history.vendor_code,transaction_history.product_type_code FROM transaction_history";
	if ($filter != "")
		$sql .= " WHERE ".$filter;
	if ($orderBy != "")
		$sql .= " ORDER BY ".$orderBy;
	elseif(isset($_POST['order']))
	{
		$sql .= " ORDER BY ".$columnName." ".$orderDirection." ";
	}
	else
	{
		$sql .= " ORDER BY date_created DESC";
	}	
		
	if ($records != "")
	{
		$limits = $records+1;
		$sql .= " LIMIT ".$limits;
	}
	else
		$sql .= " LIMIT 25";
	if ($offset != "")
		$sql .= " OFFSET ".$offset;
	else
		$sql .= " OFFSET 0";

    $result = mysqli_query($realConnection, $sql);
	$iFilteredTotal = mysqli_num_rows($result)+$offset;
	if (mysqli_num_rows($result)==0)
	{
//		$dataArray[]=array("id"=>"1","ran_or_order"=>$sql."-".mysqli_error($result));
		$dataArray = array();
	}
	else
	{
		$i = 0;
		while($row =mysqli_fetch_array($result))
		{
			$i++;
			if ($i <= $records)
				$dataArray[] = $row;
		}
	}
	$output = array(
		'draw'				=>	intval($_POST['draw']),
		'recordsTotal'		=>	$iTotal,
		'recordsFiltered'	=>	$iFilteredTotal,
		'data'				=>	$dataArray
	);
	
	echo json_encode($output);
    //close the db connection
    mysqli_close($connection);
?>