<?php
 include ('../config/phpConfig.php');
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

// DB table to use
$table = 'transaction_history';

// Table's primary key
$primaryKey = 'id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    array('db' => 'ran_or_order', 'dt' => 'ran_or_order', 'field' => 'ran_or_order'),
    array('db' => 'part_number', 'dt' => 'part_number', 'field' => 'part_number'),
    array('db' => 'serial_reference', 'dt' => 'serial_reference', 'field' => 'serial_reference'),
	array('db' => 'to_plt', 'dt' => 'to_plt', 'field' => 'to_plt'),
    array('db' => 'short_code', 'dt' => 'short_code', 'field' => 'short_code'),
    array('db' => 'txn_qty', 'dt' => 'txn_qty', 'field' => 'txn_qty'),
    array('db' => 'from_location_code', 'dt' => 'from_location_code', 'field' => 'from_location_code'),
    array( 'db' =>'to_location_code', 'dt' => 'to_location_code', 'field' => 'to_location_code'),
    array('db' => 'customer_reference', 'dt' => 'customer_reference', 'field' => 'customer_reference'),
    array('db' => 'date_created', 'dt' => 'date_created', 'field' => 'date_created'),
    array('db' => 'last_updated', 'dt' => 'last_updated', 'field' => 'last_updated'),
    array('db' => 'last_updated_by', 'dt' => 'last_updated_by', 'field' => 'last_updated_by'),
    array('db' => 'id', 'dt' => 'id', 'field' => 'last_updated_by'),
    array('db' => 'conversion_factor', 'dt' => 'conversion_factor', 'field' => 'conversion_factor')
    
);


// SQL server connection information
$sql_details = array(
    'user' => $mDbUser,
    'pass' => $mDbPassword,
    'db' => $mDbName,
    'host' => $mHost
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

// require( 'ssp.class.php' );
require( '../action/ssp.classAND.php' );

$joinQuery = "";
$extraWhere = "";
$groupBy = "";
$having = "";

echo json_encode(
        SSP::simpleQuery($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having)
);
