<?php
  include ('../config/phpConfig.php');
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
$stuff = 'location.*, inventory_disposition_code , location_type.location_type_code ,location_sub_type.location_sub_type_code, location_filling_status.filling_code, inventory_status.inventory_status_code'; 
$join = 'left join location_sub_type on location_sub_type.id = location_sub_type_id left join location_filling_status on location.filling_status_id = location_filling_status.id left join inventory_status on inventory_status.id = inventory_status_id left join location_type on location_type_id = location_type.id left join inventory_disposition on inventory_disposition.id = inventory_disposition_id';

// DB table to use
$table = 'location';
 
// Table's primary key
$primaryKey = 'id';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
  
    array( 'db' => 'location_code', 'dt' => 'location_code' ),
    array( 'db' => 'location_hash',  'dt' => 'location_hash' ),
    array( 'db' => 'location_type_code',   'dt' => 'location_type_code' ),
    array( 'db' => 'location_sub_type_code',  'dt' => 'location_sub_type_code' ),
    array( 'db' => 'multi_part_location',   'dt' => 'multi_part_location' ),
    array( 'db' => 'filling_code',     'dt' => 'filling_code' ),
    array( 'db' => 'inventory_status_code', 'dt' => 'inventory_status_code'),
    array( 'db' => 'last_updated', 'dt' => 'last_updated'),
    array( 'db' => 'last_updated_by', 'dt' => 'last_updated_by')
    
);


 
// SQL server connection information
$sql_details = array(
    'user' => $mDbUser,
    'pass' => $mDbPassword,
    'db'   => $mDbName,
    'host' => $mHost
);
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require( '../action/ssp.class.php' );
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $stuff, $join )
);