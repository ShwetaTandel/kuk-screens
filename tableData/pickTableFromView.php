<?php
 include ('../config/phpConfig.php');
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

// DB table to use
$table = 'picktable';

// Table's primary key
$primaryKey = 'id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    array('db' => 'document_reference', 'dt' => 'document_reference', 'field' => 'document_reference'),
    array('db' => 'status', 'dt' => 'status', 'field' => 'status'),
    array( 'db' =>'order_reference', 'dt' => 'order_reference', 'field' => 'order_reference'),
    array('db' => 'date_created', 'dt' => 'date_created', 'field' => 'date_created'),
    array('db' => 'last_updated_date', 'dt' => 'last_updated_date', 'field' => 'last_updated_date'),
    array('db' => 'last_updated_by', 'dt' => 'last_updated_by', 'field' => 'last_updated_by'),
    array('db' => 'id', 'dt' => 'id', 'field' => 'id'),
    array('db' => 'baan_serial', 'dt' => 'baan_serial', 'field' => 'baan_serial'),
    array('db' => 'machine_model', 'dt' => 'machine_model', 'field' => 'machine_model'),
    array('db' => 'pick_type', 'dt' => 'pick_type', 'field' => 'pick_type')
);


// SQL server connection information
$sql_details = array(
    'user' => $mDbUser,
    'pass' => $mDbPassword,
    'db' => $mDbName,
    'host' => $mHost
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

// require( 'ssp.class.php' );
require( '../action/ssp.classAND.php' );

$joinQuery = "";
$extraWhere = "";
$groupBy = "";
$having = "";

echo json_encode(
        SSP::simpleQuery($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having)
);
