      
 <?php
    include ('../config/phpConfig.php');

    //fetch table rows from mysql db
    $sql = "select pick_header.document_reference as pick_reference, order_header.customer_reference as order_reference, part_number, qty_expected, qty_allocated,qty_shortage from order_header left join pick_header on order_header.pick_header_id = pick_header.id left join pick_body on pick_header.id = pick_body.pick_header_id where  qty_expected >  qty_allocated";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $curr = $row;
        $partNumber = $row['part_number'];
        
        $rcvQuery = "select sum(available_qty+hold_qty) as rcv_qty from inventory_master join location on inventory_master.current_location_id = location.id join  location_type on location.location_type_id = location_type.id  where location_type_code = 'RCV' and (requires_inspection = true or is_pickable = 0) and inventory_master.part_number = '". $partNumber. "';";
        $rcvQtyResult = mysqli_query($connection, $rcvQuery);
        $rcvQty =0;
        while ($mInnerRow = mysqli_fetch_assoc($rcvQtyResult)) {
            if($mInnerRow['rcv_qty'] !== null){
                 $rcvQty = $mInnerRow['rcv_qty'];
            }
        }
        $curr['rcv_qty'] = $rcvQty;
        
        $qrQuery = "select sum(available_qty+hold_qty) as qr_qty from inventory_master join location on inventory_master.current_location_id = location.id join  location_type on location.location_type_id = location_type.id  where location_type_code = 'QTINE' and (requires_inspection = true or is_pickable = 0) and inventory_master.part_number = '". $partNumber. "';";
        $qrResult = mysqli_query($connection, $qrQuery);
        $qrQty =0;
        while ($mInnerRow = mysqli_fetch_assoc($qrResult)) {
            if($mInnerRow['qr_qty'] !== null){
                 $qrQty = $mInnerRow['qr_qty'];
            }
        }
        $curr['qr_qty'] = $qrQty;
        
         $qaQuery = "select sum(available_qty+hold_qty) as qa_qty from inventory_master join location on inventory_master.current_location_id = location.id join  location_type on location.location_type_id = location_type.id  where location_type_code = 'QA' and (requires_inspection = true or is_pickable = 0) and inventory_master.part_number = '". $partNumber. "';";
        $qaResult = mysqli_query($connection, $qaQuery);
        $qaQty =0;
        while ($mInnerRow = mysqli_fetch_assoc($qaResult)) {
            if($mInnerRow['qa_qty'] !== null){
                 $qaQty = $mInnerRow['qa_qty'];
            }
        }
        $curr['qa_qty'] = $qaQty;
        
        $missingQuery = "select sum(available_qty+hold_qty) as missing from inventory_master join location on inventory_master.current_location_id = location.id join  location_type on location.location_type_id = location_type.id  join inventory_status on inventory_master.inventory_status_id = inventory_status.id  where location_type_code = 'QA' and (requires_inspection = true or is_pickable = 0) and inventory_status_code = 'MISSING' and inventory_master.part_number = '". $partNumber. "';";
        $missingResult = mysqli_query($connection, $qaQuery);
        $missingQty =0;
        while ($mInnerRow = mysqli_fetch_assoc($missingResult)) {
            if($mInnerRow['missing'] !== null){
                 $missingQty = $mInnerRow['missing'];
            }
        }
        $curr['missing'] = $missingQty;
   
        array_walk_recursive($curr, function (&$item) {
            $item = mb_convert_encoding($item, "UTF-8");
        });
        $emparray[] = $curr;
    }
    echo json_encode($emparray);
    //close the db connection
    mysqli_close($connection);
?>