<?php
 include ('../config/phpConfig.php');
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

// DB table to use
$table = 'receipt_header';

// Table's primary key
$primaryKey = 'id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    array('db' => 'h.id', 'dt' => 'id', 'field' => 'id'),
    array('db' => 'h.customer_reference', 'dt' => 'customer_reference', 'field' => 'customer_reference'),
    array('db' => 'h.expected_delivery_date', 'dt' => 'expected_delivery_date', 'field' => 'expected_delivery_date'),
    array('db' => 'h.document_reference', 'dt' => 'document_reference', 'field' => 'document_reference'),
    array('db' => 'h.last_updated_date', 'dt' => 'last_updated_date', 'field' => 'last_updated_date'),
    array('db' => 'h.last_updated_by', 'dt' => 'last_updated_by', 'field' => 'last_updated_by'),
    array('db' => 'd.document_status_description', 'dt' => 'status', 'field' => 'document_status_description'),
    array('db' => 'h.grn_type', 'dt' => 'grn_type', 'field' => 'grn_type'),
    array('db' => 'count(if (shortage=true, shortage, null))', 'as' => 'shortages','dt' => 'shortages', 'field' => 'shortages'),
    array('db' => 'count(if (high_value=true, high_value, null))', 'as' => 'high_values','dt' => 'high_values', 'field' => 'high_values'),
    array('db' => 'v.vendor_code', 'dt' => 'vendor_code', 'field' => 'vendor_code')
    
);

// SQL server connection information
$sql_details = array(
    'user' => $mDbUser,
    'pass' => $mDbPassword,
    'db' =>   $mDbName,
    'host' => $mHost
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

// require( 'ssp.class.php' );
require( '../action/ssp.classAND.php');

$joinQuery = "FROM ".$mDbName.".receipt_header as h left join ".$mDbName.".receipt_body as v on v.receipt_header_id = h.id left join ".$mDbName.".part p on p.id = v.part_id left join ".$mDbName.".document_status as d on d.id = h.document_status_id";
$extraWhere = "d.document_status_code != 'DELETED'";
$groupBy = "h.document_reference";
$having = "";



echo json_encode(
       // SSP::simpleReceiptQuery($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having)
         SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having)
);



?>
