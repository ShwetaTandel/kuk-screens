<?php
 //open connection to mysql db
    include ('../config/phpConfig.php');
    //fetch table rows from mysql db
    $sql =  "SELECT * FROM reloc_header join reloc_body on reloc_header.id = reloc_body.reloc_header_id"
            ." join inventory_master on inventory_master.id = reloc_body.inventory_master_id "
            ." join inventory_status on  inventory_master.inventory_status_id = inventory_status.id and inventory_status_code = 'MISSING' and reloc_header.processed = false ";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = $row;
    }
    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>