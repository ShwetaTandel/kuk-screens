<?php

error_reporting(E_ALL & ~E_NOTICE);
// ini_set('display_errors', 1);
//open connection to mysql db
include ('../config/phpConfig.php');
include ('../config/ChromePhp.php');
//fetch table rows from mysql db
//create an array
$dataArray = array();

$table = "inventory_master";
$offset = "";
$records = "";
$filter = "";
$orderBy = "";
$offset = $_POST['start'];
$records = $_POST['length'];
$filter = $_POST['filter'];
$orderBy = $_POST['sort'];
$orderIndex = $_POST['order'][0]['column'];
$columnName = $_POST['columns'][$orderIndex]['data']; // Column name
$orderDirection = $_POST['order'][0]['dir']; // asc or desc
$iFilteredTotal = 0;
$iTotal = 0;

$orderColumn = array('id', 'part_number', 'serial_reference', 'ran_or_order', 'inventory_qty', 'allocated_qty',  'picked_qty','available_qty','inventory_status_code', 'location_code', 'tag_reference', 'conversion_factor', 'date_created');

// Total data set length
$sql = "select TABLE_ROWS from information_schema.TABLES where TABLE_SCHEMA = 'kuk' AND table_name='" . $table . "'";
//	$sql = "SELECT count(id) FROM ".$table;
$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
$row = mysqli_fetch_array($result);
$iTotal = $row[0];

$timeout = 10;  /* seconds for timeout */
$realConnection = mysqli_init();
$realConnection->options(MYSQLI_OPT_CONNECT_TIMEOUT, $timeout) ||
        die('mysqli_options failed: ' . $link->error);
$realConnection->real_connect($db, $mDbUser,$mDbPassword,  $mDbName) ||
        die('mysqli_real_connect failed: ' . $link->error);

$sql = "SELECT inventory_master.id AS id,inventory_master.ran_or_order AS ran_or_order,inventory_master.part_number AS part_number,inventory_master.serial_reference,inventory_master.tag_reference,inventory_master.inventory_qty,inventory_master.available_qty,inventory_master.picked_qty,inventory_master.allocated_qty,inventory_master.hold_qty,inventory_master.location_code,inventory_master.last_updated,inventory_master.date_created,inventory_master.last_updated_by,inventory_master.conversion_factor,inventory_master.requires_inspection,inventory_master.requires_decant,inventory_status.inventory_status_code";
$sql .=",vendor.vendor_reference_code AS vendor_reference_code";
$sql .=",product_type.product_type_code";
$sql .=" FROM inventory_master";
$sql .=" JOIN part ON inventory_master.part_id=part.id";
$sql .=" JOIN location ON inventory_master.current_location_id=location.id";
$sql .=" LEFT JOIN vendor ON part.vendor_id=vendor.id";
$sql .=" LEFT JOIN inventory_status ON inventory_master.inventory_status_id=inventory_status.id";
$sql .=" LEFT JOIN tag ON inventory_master.parent_tag_id=tag.id";
$sql .=" LEFT JOIN product_type ON inventory_master.product_type_id=product_type.id";
if ($filter != "")
    $sql .= " WHERE " . $filter;
if (isset($_POST['order'])) {
    $sql .= " ORDER BY " . $columnName . " " . $orderDirection . " ";
} elseif ($orderBy != "")
    $sql .= " ORDER BY " . $orderBy;
else {
    $sql .= " ORDER BY date_created desc";
}

if ($records != "") {
    $limits = $records + 1;
    $sql .= " LIMIT " . $limits;
} else
    $sql .= " LIMIT 25";
if ($offset != "")
    $sql .= " OFFSET " . $offset;
else
    $sql .= " OFFSET 0";
ChromePhp::log($sql);
$result = mysqli_query($realConnection, $sql);
$iFilteredTotal = mysqli_num_rows($result) + $offset;
if (mysqli_num_rows($result) == 0) {
    $dataArray = array();
//		$dataArray[]=array("id"=>"1","ran_or_order"=>$sql);
} else {
    $i = 0;
    while ($row = mysqli_fetch_array($result)) {
        $i++;
        if ($i <= $records)
            $dataArray[] = $row;
//			if ($i == 1)
//				$dataArray[]=array("id"=>"1","ran_or_order"=>$sql.mysqli_error($result));
    }
}

$output = array(
    'draw' => intval($_POST['draw']),
    'recordsTotal' => $iTotal,
    'recordsFiltered' => $iFilteredTotal,
    'data' => $dataArray
);

echo json_encode($output);
//close the db connection
mysqli_close($connection);
?>