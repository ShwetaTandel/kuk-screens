      
 <?php
    //open connection to mysql db
include ('../config/phpConfig.php');
    //fetch table rows from mysql db
    $sql = "SELECT replen_task.*,  pick_group.pick_group_code, part.part_number FROM replen_task left join pick_group on pick_group.id = replen_task.pick_group_id left join inventory_master on inventory_master.id = replen_task.inventory_master_id left join part on part.id = replen_task.part_id;";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = $row;
    }

    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>