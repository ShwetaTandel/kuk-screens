<?php
include ('../config/phpConfig.php');
include ('../config/ChromePhp.php');

$filter = $_GET['filter'];
//fetch table rows from mysql db
if ($filter === 'data') {
    $returnResult = array();
    $sql = "select distinct SUBSTRING_INDEX(location_type_code, '-',1) as location_type_code,pick_smv from location_type where lower(location_area) = 'storage' order by display_order";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $jsonArray = array();
    $locTime = array();
    while ($row = mysqli_fetch_assoc($result)) {
         $jsonArray[] = $row['location_type_code'];
         $locTime[$row['location_type_code']] = $row['pick_smv'];
         
    }
    array_unshift($jsonArray, 'Id','Run','Order Type','Description', 'Pick', 'Line on Date', 'Comments');
    array_push($jsonArray,"Total");
     $returnResult ['columns'] = $jsonArray;
    //$sql = "SELECT  pick_body.id as id, count(*) as cnt , comments,pick_header.document_reference, order_header.expected_delivery_time, SUBSTRING_INDEX(location_type_code, '-',1) as location_type_code , pick_body.part_number, concat(machine_model,' ', baan_serial) as description FROM kuk.pick_body left join pick_header on pick_header.id = pick_body.pick_header_id left join order_header on order_header.id = pick_header.order_header_id left join part on part.id = pick_body.part_id left join location_type on location_type.id = part.fixed_location_type_id where processed is false group by document_reference, location_type_code order by document_reference";
     //$sql = "SELECT  pick_body.id as id, order_header.id as orderHeaderId,count(*) as cnt , order_type,comments,pick_header.document_reference, order_header.expected_delivery_time, SUBSTRING_INDEX(location_type_code, '-',1) as location_type_code , pick_body.part_number, concat(machine_model,' ', baan_serial) as description FROM kuk.pick_body left join pick_header on pick_header.id = pick_body.pick_header_id left join order_header on order_header.pick_header_id = pick_header.id left join part on part.id = pick_body.part_id left join location_type on location_type.id = part.fixed_location_type_id where processed is false group by orderHeaderId,document_reference, location_type_code order by document_reference";
    $sql= "SELECT  pick_body.id as id,count(*) as cnt,comments,pick_header.document_reference,pick_header.id as pickHeaderId,SUBSTRING_INDEX(location_type_code, '-',1) as location_type_code , pick_body.part_number, pick_type "
         ." FROM kuk.pick_body left join pick_header on pick_header.id = pick_body.pick_header_id left join order_header on order_header.pick_header_id = pick_header.id "
         ."left join part on part.id = pick_body.part_id left join location_type on location_type.id = part.fixed_location_type_id"
         ." where processed is false ";
     if (isset($_GET['toDate']) && isset($_GET['fromDate']) ) {
          $sql.= " and expected_delivery_time between '".$_GET['fromDate']."' and '".$_GET['toDate']."' "."group by document_reference, location_type_code order by document_reference";
     }else{
          $sql.="group by document_reference, location_type_code order by document_reference";
     }
    ChromePhp::log($sql);
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $returnResultTemp = array();
    $resultArray = array();
    $colTotalArray = array();
    $docRef = '';
    $total = 0;
    while ($row = mysqli_fetch_assoc($result)) {
        if($docRef !== $row['document_reference']){
            $docRef = $row['document_reference'];
            if(!empty($resultArray)){
                $returnResultTemp [] = $resultArray;
            }
            $resultArray = array();
            $total = 0;
            $sqlInner = "SELECT order_header.*, offset, order_type.description as description, order_type_code from order_header inner join order_type on SUBSTRING_INDEX(order_header.order_type, '-',1) = order_type.order_type_code where pick_header_id =".$row['pickHeaderId'];
           
            $machine = array();
            $desc ="";
            $resultInner = mysqli_query($connection, $sqlInner) or die("Error in Selecting " . mysqli_error($connection));
             while ($rowInner = mysqli_fetch_assoc($resultInner)) {
                 $offset = $rowInner['offset'];
                 $offsetStr = "PT".intdiv($offset,60)."H".($offset%60)."M";// 2hr 10 mins - PT2H10M
               
                 $pickTime = new DateTime($rowInner['expected_delivery_time']);
                 $lodTime = new DateTime($rowInner['expected_delivery_time']);
                 $tosub = new DateInterval($offsetStr); 
                 $pickTime->sub($tosub);
                 $row['pick_time']  = $pickTime->format("d-m-Y H:i");
                 $row['expected_delivery_time'] = $lodTime->format("d-m-Y H:i");
                 if($rowInner['order_type_code'] === 'SPARES'){
                       $desc= $rowInner['description'];
                    
                 }else if($rowInner['machine_model']!== ''){
                     if(array_key_exists($rowInner['machine_model'])){
                        $machine[$rowInner['machine_model']] = $machine[$rowInner['machine_model']] +1;
                     }else{
                          $machine[$rowInner['machine_model']] = $machine[$rowInner['machine_model']] +1;
                     }
                    
                 }
                // $row['order_type']  = $rowInner['order_type'];
             }    
        }
        foreach ($jsonArray as $column) {
            if($column === 'Run'){
                $resultArray[$column] = $row['document_reference'];
            }else if ($column === 'Description'){
                if(!array_key_exists($column, $resultArray) || $resultArray[$column] === ''){
                    if(!empty($machine)){
                        $resultArray[$column] = str_replace('=', ' X ', http_build_query($machine, null, ','));
                    }else{
                        $resultArray[$column] = $desc;
                    }
                   
                }
            }else if ($column === 'Pick'){
                if(!array_key_exists($column, $resultArray) || $resultArray[$column] === ''){
                    $resultArray[$column] = $row['pick_time'];
                }
                
            }else if ($column === 'Line on Date'){
                if(!array_key_exists($column, $resultArray) || $resultArray[$column] === ''){
                    $resultArray[$column] = $row['expected_delivery_time'];
                }
            }else if ($column === 'Order Type'){
                /* if(array_key_exists($column, $resultArray) && $resultArray[$column] !== null && $resultArray[$column]!== $row['order_type'] ){
                     $resultArray[$column] = "MIXED";
                 }else{
                      $resultArray[$column] = $row['order_type'];
                 }*/
                if(!array_key_exists($column, $resultArray) || $resultArray[$column] === ''){
                      $resultArray[$column] = $row['pick_type'];
                }
                
            }else if ($column === $row['location_type_code']){
                $resultArray[$column] = $row['cnt'];
                $total+=$row['cnt'];
                $colTotalArray[$column]['colTotal']+=$row['cnt'];
            }else if ($column === 'Comments'){
                if(array_key_exists($column, $resultArray) && $resultArray[$column] === null){
                        $resultArray[$column] = $row['comments'];
                }else if(!array_key_exists($column, $resultArray)){
                        $resultArray[$column] = $row['comments'];
                }
            }else if ($column === 'Id'){
                $resultArray[$column] = $row['id'];
            }else if ($column === 'Total'){
                $resultArray[$column] = $total;
              
            }else {
                if(!array_key_exists($column, $resultArray)){
                    $resultArray[$column] = '0';
                    $colTotalArray[$column]['colTotal'] += 0;
                }
            }

        }
        
        //
    }
    $colTotalRow = $resultArray;
    if (!empty($resultArray)) {
        $returnResultTemp [] = $resultArray;
    }else{
        //If its empty result fill up the locations with 0 values
         foreach ($locTime as $key => $value) {
             $colTotalRow[$key] = 0;
         }
    }
  //  if (!empty($resultArray)) {
    //Get the Colum total rows 
   
    $totTotal =0;
    foreach ($colTotalArray as $key => $value) {
        $colTotalRow[$key] = $value['colTotal'];
        $totTotal+=$value['colTotal'];
    }
    $colTotalRow['Total'] = $totTotal;
    $colTotalRow['Id'] = '';
    $colTotalRow['Run'] = '';
    $colTotalRow['Pick'] = '';
    $colTotalRow['Order Type'] = '';
     $colTotalRow['Description'] = '';
    $colTotalRow['Line on Date'] = '';
    $colTotalRow['Comments'] = 'Tasks By Area';
    $returnResultTemp [] = $colTotalRow;
    
    $timeTotalRow = $resultArray;
    $hrTotal =0;
    foreach ($locTime as $key => $value) {
        $timeTotalRow[$key] = round(($value * $colTotalRow[$key]) / 3600, 2);
        $hrTotal +=round(($value * $colTotalRow[$key]) / 3600, 2);
    }
    
    $timeTotalRow['Total'] = number_format($hrTotal, 2, '.', ',');
    $timeTotalRow['Id'] = '';
    $timeTotalRow['Run'] = '';
    $timeTotalRow['Pick'] = '';
    $timeTotalRow['Order Type'] = '';
     $timeTotalRow['Description'] = '';
    $timeTotalRow['Line on Date'] = '';
    $timeTotalRow['Comments'] = 'Time By Area(hours)';
    $returnResultTemp [] = $timeTotalRow;
    
    //}
    //add everything in data
    $returnResult['data'] = $returnResultTemp;
    echo json_encode($returnResult);
    mysqli_close($connection);
}else if ($filter === 'comments'){
    $comment = $_GET['comments'];
    
    $pickBodyId = $_GET['id'];
    $sql= "UPDATE ".$mDbName. ".pick_body set comments =' ".$comment."'  where document_reference='".$pickBodyId."';";
     $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
        if($result === TRUE){
            echo "OK";
        }
            mysqli_close($connection);
}else if ($filter === 'dateRangeReport'){
    
     $from = $_GET['from'];
      $to = $_GET['to'];
    
}
?>