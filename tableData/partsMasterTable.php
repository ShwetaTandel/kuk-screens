<?php
 include ('../config/phpConfig.php');
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

// DB table to use
$table = 'part';

// Table's primary key
$primaryKey = 'id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes

$columns = array(
  
    array( 'db' => 'p.part_number', 'dt' => 0, 'field' => 'part_number' ),
    array( 'db' => 'p.part_description',  'dt' => 1, 'field' => 'part_description' ),
    array( 'db' => 'p.jis_supply_group',   'dt' => 2, 'field' => 'jis_supply_group' ),
    array( 'db' => 'p.piece_part_number',   'dt' => 3, 'field' => 'piece_part_number' ),
    array( 'db' => 'l.location_type_code',  'dt' => 4, 'field' => 'location_type_code' ),
    array( 'db' => 's.location_sub_type_code',   'dt' => 5, 'field' => 'location_sub_type_code' ),
    array( 'db' => 'p.requires_inspection',     'dt' => 6, 'field' => 'requires_inspection' )
);



// SQL server connection information
$sql_details = array(
    'user' => $mDbUser,
    'pass' => $mDbPassword,
    'db' => $mDbName,
    'host' => $mHost
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

// require( 'ssp.class.php' );
require( '../action/ssp.classAND.php' );


$joinQuery = "FROM part as p left join location_type as l on p.fixed_location_type_id = l.id left join location_sub_type as s on s.id = p.fixed_sub_type_id";
$extraWhere = "";
$groupBy = "";
$having = "";

echo json_encode(
        SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having)
);



