<?php
  include ('../config/phpConfig.php');
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
//$stuff = 'location.*, inventory_disposition_code , location_type.location_type_code ,location_sub_type.location_sub_type_code, location_filling_status.filling_code, inventory_status.inventory_status_code'; 
$join = 'from location as l left join location_sub_type as ls on ls.id = l.location_sub_type_id left join location_filling_status as f on l.filling_status_id = f.id left join inventory_status as i on i.id = l.inventory_status_id left join location_type as lt on l.location_type_id = lt.id left join inventory_disposition as ids on ids.id = l.inventory_disposition_id';
$extraWhere = "";
$groupBy = "";
$having = "";

// DB table to use
$table = 'location';
 
// Table's primary key
$primaryKey = 'id';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
  
    array( 'db' => 'l.location_code', 'dt' => 0, 'field' => 'location_code' ),
    array( 'db' => 'l.location_hash',  'dt' => 1, 'field' => 'location_hash' ),
    array( 'db' => 'lt.location_type_code',   'dt' => 2, 'field' => 'location_type_code' ),
    array( 'db' => 'ls.location_sub_type_code',  'dt' => 3,'field' => 'location_sub_type_code' ),
    array( 'db' => 'l.multi_part_location',   'dt' => 4 ,'field' => 'multi_part_location' ),
    array( 'db' => 'f.filling_code',     'dt' => 5,'field' => 'filling_code' ),
    array( 'db' => 'i.inventory_status_code', 'dt' => 6,'field' => 'inventory_status_code'),
    array( 'db' => 'l.last_updated', 'dt' => 7,'field' => 'last_updated'),
    array( 'db' => 'l.last_updated_by', 'dt' =>8,'field' => 'last_updated_by'),
     array( 'db' => 'lt.pick_smv', 'dt' => 9,'field' => 'pick_smv')
    
    
);


 
// SQL server connection information
$sql_details = array(
    'user' => $mDbUser,
    'pass' => $mDbPassword,
    'db'   => $mDbName,
    'host' => $mHost
);
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
//require( '../action/ssp.class.php' );
require( '../action/ssp.classAND.php' );
 
echo json_encode(
    //SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $stuff, $join )
         SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $join, $extraWhere, $groupBy, $having)
);