<?php
include ('../config/phpConfig.php');
include ('../config/ChromePhp.php');

$data = $_GET['data'];
ChromePhp::log($data);
//fetch table rows from mysql db
if ($data === 'notConfirmed') {
    $sql = "SELECT distinct(receipt_header.customer_reference) AS customerReference, TIMESTAMPDIFF(MINUTE,receipt_header.date_created,now()) as timeDiff FROM kuk.receipt_header LEFT JOIN kuk.receipt_body on receipt_header.id=receipt_body.receipt_header_id and receipt_header.document_status_id = 1;";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $jsonArray = array();
    while ($row = mysqli_fetch_assoc($result)) {
          $jsonArray[] = $row;
    }
    echo json_encode($jsonArray);
    //close the db connection
    mysqli_close($connection);
} else if ($data === 'notCleared') {
    $sql = "SELECT customer_reference, receipt_detail.serial_reference as serial_reference, available_qty, receipt_detail.part_number as part_number,TIMESTAMPDIFF(MINUTE,inventory_master.last_updated,now()) as timeDiff FROM kuk.inventory_master JOIN kuk.receipt_detail on inventory_master.serial_reference = receipt_detail.serial_reference JOIN receipt_header on receipt_header.document_reference = receipt_detail.document_reference where  inventory_status_id = 1 and current_location_id in (select id from location where location_type_id = 1) and available_qty > 0 and receipt_detail.document_status_id = 3";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $resultArray = array();
    $jsonArray = array();
    while ($row = mysqli_fetch_assoc($result)) {
          $jsonArray[] = $row;
        }
    echo json_encode($jsonArray);
} elseif ($data === 'notPicked350') {
    $sql = "select document_header.id, DATE_FORMAT(sysdate(),'%j') as today,DATE_FORMAT(document_header.date_created,'%j') as digitDate,DATE_FORMAT(document_header.date_created,'%H:%i') as digit,benum as carset,tanum as tonumber, document_status_id as status, qty_expected, qty_transacted from " . $mDbName . ".document_header," . $mDbName . ".document_body where document_header.id = document_body.document_header_id and order_type in (350) and picked= 0 and  document_header.date_created > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 24 HOUR) order by document_header.date_created ASC;";
    
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $jsonArray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        if ($row['status'] != "8") {
            $jsonArray[] = array(
                'digit' => $row ['digit'],
                'carset' => $row ['carset'],
                'tonumber' => $row['tonumber'],
                'picks' => ($row['qty_expected']/1000) ."/".($row['qty_transacted']/1000),
                'today' => $row['today'],
                'digitDate' => $row['digitDate']
            );
        }
    }
    echo json_encode($jsonArray);
    //close the db connection
    mysqli_close($connection);
} else if ($data === 'marshalled350') {
    //$sql = "select DATE_FORMAT(sysdate(),'%j') as today,DATE_FORMAT(expected_delivery,'%j') as digitDate,DATE_FORMAT(expected_delivery,'%H:%i') as digit,benum as carset,tanum as tonumber,document_reference_code as doc from " . $mDbName . ".document_header where order_type in (350) and transmitted= 1  and document_reference is null and expected_delivery > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 24 HOUR) order by expected_delivery ASC;";
    $sql = "select DATE_FORMAT(sysdate(),'%j') as today,DATE_FORMAT(document_header.date_created,'%j') as digitDate,DATE_FORMAT(document_header.date_created,'%H:%i') as digit,benum as carset,tanum as tonumber,document_reference_code as doc, tag_reference as tag from " . $mDbName . ".inventory_master, " . $mDbName . ".document_header where  inventory_master.ran_or_order = document_header.tanum and location_id = 38885 and  order_type in (350) and document_reference is null and document_header.date_created > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 24 HOUR) order by document_header.date_created ASC;";
   // $sql = "select distinctrow * from marshalled";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $resultArray = array();
    $jsonArray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        if ($row['carset'] != "LSJ_Z6RST1" && substr($row['carset'],0,1) != "S") {
            $jsonArray[] = array(
                'digit' => $row['digit'],
                'carset' => $row['carset'],
                'tonumber' => $row['tonumber'],
                'today' => $row['today'],
                'digitDate' => $row['digitDate'],
                'tag' => $row['tag'],
                'doc' => $row ['doc'] 
            );
        }
    }
    echo json_encode($jsonArray);
} else if ($data === 'trailer') {
    $sql = "select shipping_header.document_reference as document_reference_code, trailer_reference, ran_order from " . $mDbName . ".shipping_body, " . $mDbName . ".shipping_header where  shipping_body.shipping_header_id = shipping_header.id and  shipping_header.last_updated_date > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) order by expected_delivery_time ASC";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $resultArray = array();
    $jsonArray = array();
    $ranArray = array();
    $ranTrailerArray = array();
    while ($row = mysqli_fetch_assoc($result)) {
            $resultArray[] = array(
                'document_reference_code' => $row['document_reference_code'],
                'trailer_reference' => $row['trailer_reference'],
                'ran_order'=>$row['ran_order']    
                
            );
            array_push($ranArray, $row['ran_order']);
            $ranTrailerArray[$row['ran_order']]=$row['trailer_reference'];
        
    }
        $trailerSql = "select DATE_FORMAT(expected_delivery,'%H:%i') as digit,benum as carset,tanum as tonumber from " . $mDbName . ".document_header where tanum in ('" . implode("','", $ranArray). "') and last_updated > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) and order_type in (945, 943) order by expected_delivery ASC";
        $result = mysqli_query($connection, $trailerSql) or die("Error in Selecting " . mysqli_error($connection));
        while ($row = mysqli_fetch_assoc($result)) {
            if ($row['carset'] != "LSJ_Z6RST1" && substr($row['carset'],0,1) != "S") {
            $jsonArray[] = array(
                'digit' => $row['digit'],
                'carset' => $row['carset'],
                'tonumber' => $row['tonumber'],
                'trailer' => $ranTrailerArray[$row['tonumber']]
            );
            }
        }
    echo json_encode($jsonArray);
}else if ($data === 'trailer350') {
    //$sql = "select document_reference, trailer_reference from " . $mDbName . ".shipping_header last_updated_date > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) order by expected_delivery_time ASC;";
    $sql = "select shipping_header.document_reference as document_reference_code, trailer_reference, ran_order from " . $mDbName . ".shipping_body, " . $mDbName . ".shipping_header where  shipping_body.shipping_header_id = shipping_header.id and  shipping_header.last_updated_date > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) order by expected_delivery_time ASC";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $resultArray = array();
    $jsonArray = array();
        $ranArray = array();
    $ranTrailerArray = array();

    while ($row = mysqli_fetch_assoc($result)) {
            $resultArray[] = array(
                'document_reference_code' => $row['document_reference_code'],
                'trailer_reference' => $row['trailer_reference'],
                'ran_order'=>$row['ran_order']    
            );
              array_push($ranArray, $row['ran_order']);
            $ranTrailerArray[$row['ran_order']]=$row['trailer_reference'];
        
    }
        $trailerSql = "select DATE_FORMAT(document_header.date_created,'%H:%i') as digit,benum as carset,tanum as tonumber from " . $mDbName . ".document_header where tanum in ('" . implode("','", $ranArray). "') and last_updated > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) and order_type in (350) order by document_header.date_created ASC";
        $result = mysqli_query($connection, $trailerSql) or die("Error in Selecting " . mysqli_error($connection));
        while ($row = mysqli_fetch_assoc($result)) {
            if ($row['carset'] != "LSJ_Z6RST1" && substr($row['carset'],0,1) != "S") {
            $jsonArray[] = array(
                'digit' => $row['digit'],
                'carset' => $row['carset'],
                'tonumber' => $row['tonumber'],
               'trailer' => $ranTrailerArray[$row['tonumber']]
            );
            }
        }
        
  //  }
    echo json_encode($jsonArray);
} else if ($data === 'trailerMisc') {
    //$sql = "select document_reference, trailer_reference from " . $mDbName . ".shipping_header last_updated_date > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) order by expected_delivery_time ASC;";
    $sql = "select shipping_header.document_reference as document_reference_code, trailer_reference, ran_order from " . $mDbName . ".shipping_body, " . $mDbName . ".shipping_header where  shipping_body.shipping_header_id = shipping_header.id and  shipping_header.last_updated_date > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) order by expected_delivery_time ASC";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $resultArray = array();
    $jsonArray = array();
            $ranArray = array();
    $ranTrailerArray = array();

    while ($row = mysqli_fetch_assoc($result)) {
            $resultArray[] = array(
                'document_reference_code' => $row['document_reference_code'],
                'trailer_reference' => $row['trailer_reference'],
                'ran_order'=>$row['ran_order']    
            );
                          array_push($ranArray, $row['ran_order']);
            $ranTrailerArray[$row['ran_order']]=$row['trailer_reference'];

        
    }
        $trailerSql = "select DATE_FORMAT(document_header.date_created,'%H:%i') as digit,benum as carset,tanum as tonumber from " . $mDbName . ".document_header where tanum in ('" . implode("','", $ranArray). "')  and last_updated > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 2 HOUR) and order_type in (988,993) order by document_header.date_created ASC";
        $result = mysqli_query($connection, $trailerSql) or die("Error in Selecting " . mysqli_error($connection));
        while ($row = mysqli_fetch_assoc($result)) {
            if ($row['carset'] != "LSJ_Z6RST1" && substr($row['carset'],0,1) != "S") {
            $jsonArray[] = array(
                'digit' => $row['digit'],
                'carset' => $row['carset'],
                'tonumber' => $row['tonumber'],
              'trailer' => $ranTrailerArray[$row['tonumber']]
            );
            }
        }
        
  
    echo json_encode($jsonArray);
}else if ($data === 'notPickedJIS') {
   
     $sql = "select * from notpicked_jis order by digitDate, digit";
    
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $jsonArray = array();
    
    while ($row = mysqli_fetch_assoc($result)) {
        ChromePhp::log($row['tonumber']);
            $total = $row['total'];
             $outstanding = $row['outstanding'];
            //if ($total != 0) {
                ChromePhp::log($row['tonumber'].'total greater than 0');
                $jsonArray[] = array(
                    'id' => $row['id'],
                    'picktype' => $row['picktype'],
                    'digit' => $row ['digit'],
                    'carset' => $row ['carset'],
                    'tonumber' => $row['tonumber'],
                    'picks' => $total . "/" . $outstanding,
                    'today' => $row ['today'],
                    'digitDate' => $row['digitDate']
                );
            //}
        
    }
    echo json_encode($jsonArray);
    //close the db connection
    mysqli_close($connection);
}else if ($data === 'marshalledJIS') {
     $sql = "select distinctrow * from marshalled_jis order by digitDate, digit";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $resultArray = array();
    $jsonArray = array();
    while ($row = mysqli_fetch_assoc($result)) {
       if(substr($row['carset'],0,1) == 'J' || substr($row['carset'],0,1) == 'S' || $row['carset'] == "LSJ_Z6RST1"){
            $jsonArray[] = $row;
        }
    }
    echo json_encode($jsonArray);
} else if ($data === 'trailerJIS') {
    $sql = "select * from trailer_jis order by digitDate, digit";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $resultArray = array();
    $jsonArray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        if(substr($row['carset'],0,1) == 'J' || substr($row['carset'],0,1) == 'S' || $row['carset'] == "LSJ_Z6RST1"){
            $jsonArray[] = array(
                'digit' => $row['digit'],
                'carset' => $row['carset'],
                'tonumber' => $row['tonumber'],
                'trailer' => $row['trailer']
            );
        }
        
    }
    echo json_encode($jsonArray);
}elseif ($data === 'notPickedMisc') {
    $sql = "select document_header.id, DATE_FORMAT(sysdate(),'%j') as today,DATE_FORMAT(document_header.date_created,'%j') as digitDate,DATE_FORMAT(document_header.date_created,'%H:%i') as digit,benum as carset,tanum as tonumber, document_status_id as status, qty_expected, qty_transacted from " . $mDbName . ".document_header," . $mDbName . ".document_body where document_header.id = document_body.document_header_id and order_type in (988,993) and picked= 0 and  document_header.date_created > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 24 HOUR) group by document_header.id order by document_header.date_created ASC;";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $jsonArray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        if ($row['status'] != "8") {
            $jsonArray[] = array(
                'digit' => $row ['digit'],
                'carset' => $row ['carset'],
                'tonumber' => $row['tonumber'],
                'picks' => ($row['qty_expected']/1000) ."/".($row['qty_transacted']/1000),
                'today' => $row['today'],
                'digitDate' => $row['digitDate']
            );
        }
    }
    echo json_encode($jsonArray);
    //close the db connection
    mysqli_close($connection);
} else if ($data === 'marshalledMisc') {
    //$sql = "select DATE_FORMAT(sysdate(),'%j') as today,DATE_FORMAT(expected_delivery,'%j') as digitDate,DATE_FORMAT(expected_delivery,'%H:%i') as digit,benum as carset,tanum as tonumber,document_reference_code as doc from " . $mDbName . ".document_header where order_type in (350) and transmitted= 1  and document_reference is null and expected_delivery > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 24 HOUR) order by expected_delivery ASC;";
    $sql = "select DATE_FORMAT(sysdate(),'%j') as today,DATE_FORMAT(document_header.date_created,'%j') as digitDate,DATE_FORMAT(document_header.date_created,'%H:%i') as digit,benum as carset,tanum as tonumber,document_reference_code as doc, tag_reference as tag from " . $mDbName . ".inventory_master, " . $mDbName . ".document_header where  inventory_master.ran_or_order = document_header.tanum and location_id = 38885 and  order_type in (988,993) and transmitted= 1 and document_reference is null and document_header.date_created > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 24 HOUR) order by document_header.date_created ASC;";
   // $sql = "select distinctrow * from marshalled order by digitDate, digit";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $resultArray = array();
    $jsonArray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        if ($row['carset'] != "LSJ_Z6RST1" && substr($row['carset'],0,1) != "S") {
            $jsonArray[] = array(
                'digit' => $row['digit'],
                'carset' => $row['carset'],
                'tonumber' => $row['tonumber'],
                'today' => $row['today'],
                'digitDate' => $row['digitDate'],
                'tag' => $row['tag'],
                'doc' => $row ['doc'] 
            );
        }
    }
    echo json_encode($jsonArray);
}
?>