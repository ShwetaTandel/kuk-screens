<?php
 include ('../config/phpConfig.php');
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

// DB table to use
$table = 'rts_header';

// Table's primary key
$primaryKey = 'id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    array('db' => 'h.id', 'dt' => 'id', 'field' => 'id'),
    array('db' => 'h.order_reference', 'dt' => 'order_reference', 'field' => 'order_reference'),
    array('db' => 'b.part_number', 'dt' => 'part_number', 'field' => 'part_number'),
    array('db' => 'h.position', 'dt' => 'position', 'field' => 'position'),
    array('db' => 'b.qty_received', 'dt' => 'qty_received', 'field' => 'qty_received'),
    array('db' => 'b.baan_location', 'dt' => 'baan_location', 'field' => 'baan_location'),
    array('db' => 'h.machine_model', 'dt' => 'machine_model', 'field' => 'machine_model'),
    array('db' => 'h.date_time', 'dt' => 'date_time', 'field' => 'date_time'),
    array('db' => 'h.reason_code', 'dt' => 'reason_code', 'field' => 'reason_code'),
    array('db' => 'h.model_variant', 'dt' => 'model_variant', 'field' => 'model_variant'),
	array('db' => 'h.is_processed', 'dt' => 'is_processed', 'field' => 'is_processed'),
	array('db' => 'h.order_type', 'dt' => 'order_type', 'field' => 'order_type'),
    array('db' => 'h.date_created', 'dt' => 'date_created', 'field' => 'date_created'),
    array('db' => 'h.last_updated', 'dt' => 'last_updated', 'field' => 'last_updated'),
    array('db' => 'h.last_updated_by', 'dt' => 'last_updated_by', 'field' => 'last_updated_by'),
	array('db' => 'b.baan_serial', 'dt' => 'baan_serial', 'field' => 'baan_serial')
    
    
);

// SQL server connection information
$sql_details = array(
    'user' => $mDbUser,
    'pass' => $mDbPassword,
    'db' =>   $mDbName,
    'host' => $mHost
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

// require( 'ssp.class.php' );
require( '../action/ssp.classAND.php');

$joinQuery = "FROM ".$mDbName.".rts_header as h left join ".$mDbName.".rts_body as b on b.rts_header_id = h.id";
$extraWhere = "is_processed is false";
$groupBy = "";
$having = "";



echo json_encode(
       // SSP::simpleReceiptQuery($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having)
         SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having)
);



?>
