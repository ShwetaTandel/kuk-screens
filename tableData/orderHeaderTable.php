<?php
 include ('../config/phpConfig.php');
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

// DB table to use
$table = 'order_header';

// Table's primary key
$primaryKey = 'id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes

$columns = array(
  
    array( 'db' => 'order_header.document_reference','as'=>' order_reference', 'dt' => 'order_reference', 'field' => 'order_reference' ),
    array( 'db' => 'order_header.customer_reference',  'dt' => 'customer_reference', 'field' => 'customer_reference' ),
    array( 'db' => 'expected_delivery_time',   'dt' => 'expected_delivery_time', 'field' => 'expected_delivery_time' ),
    array( 'db' => 'order_type',   'dt' => 'order_type', 'field' => 'order_type' ),
    array( 'db' => 'document_status.document_status_code', 'dt' => 'document_status_code', 'field' => 'document_status_code'),
    array( 'db' => 'order_header.document_status_id', 'dt' => 'document_status_id', 'field' => 'document_status_id'),
    array( 'db' => 'pick_header.document_reference','as'=>' pick_reference',     'dt' => 'pick_reference', 'field' => 'pick_reference' ),
    array( 'db' => 'order_header.machine_model',     'dt' => 'machine_model', 'field' => 'machine_model' ),
    array( 'db' => 'order_header.baan_serial',     'dt' => 'baan_serial', 'field' => 'baan_serial' ),
    array( 'db' => 'order_header.date_created',  'dt' => 'date_created', 'field' => 'date_created' ),
    array( 'db' => 'order_header.last_updated_date',   'dt' => 'last_updated_date', 'field' => 'last_updated_date' ),
    array( 'db' => 'order_header.last_updated_by',     'dt' =>'last_updated_by', 'field' => 'last_updated_by' ),
    array( 'db' => 'order_header.id',     'dt' =>'id', 'field' => 'id' ),
    
    
    
    
    
);


// SQL server connection information
$sql_details = array(
    'user' => $mDbUser,
    'pass' => $mDbPassword,
    'db' => $mDbName,
    'host' => $mHost
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

// require( 'ssp.class.php' );
require( '../action/ssp.classAND.php' );
//document_header
$joinQuery = "FROM order_header left join document_status on (document_status_id = document_status.id) left join pick_header on (pick_header.id=pick_header_id) ";
$extraWhere = "document_status.document_status_code != 'DELETED' ";//"class in('com.vantec.documents.orders.CustomerOrderHeader', 'com.vantec.documents.orders.RROrderHeader')";
$groupBy = "";
$having = "";

echo json_encode(
        SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having)
);


