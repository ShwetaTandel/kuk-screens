      
 <?php
    //open connection to mysql db
include ('../config/phpConfig.php');
    //fetch table rows from mysql db
    $sql = "SELECT allocation_detail.*, scheduled_work.third_party_reference, part.part_number, inventory_master.serial_reference, l1.location_code AS from_location_code, l2.location_code AS to_location_code FROM kuk.allocation_detail LEFT JOIN part ON part.id = allocation_detail.part_id LEFT JOIN inventory_master ON inventory_master.id = allocation_detail.inventory_master_id LEFT JOIN location l1 ON inventory_master.current_location_id = l1.id LEFT JOIN location l2 ON l2.id = allocation_detail.to_location_id LEFT JOIN scheduled_work ON scheduled_work.id = allocation_detail.scheduled_work_id WHERE allocation_detail.allocated_qty != 0 AND allocation_detail.processed = 0;";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = $row;
    }

    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>