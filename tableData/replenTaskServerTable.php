<?php
 include ('../config/phpConfig.php');
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

// DB table to use
$table = 'replen_task';

// Table's primary key
$primaryKey = 'id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes

$columns = array(
    
	array( 'db' => 'r.id', 'dt' => 'id', 'field' => 'id' ),
    array( 'db' => 'r.from_location_code', 'dt' => 'from_location_code', 'field' => 'from_location_code' ),
    array( 'db' => 'r.to_location_code',  'dt' => 'to_location_code', 'field' => 'to_location_code' ),
    array( 'db' => 'p.part_number',   'dt' => 'part_number', 'field' => 'part_number' ),
    array( 'db' => 'r.suggested_serial',   'dt' =>'suggested_serial', 'field' => 'suggested_serial' ),
    array( 'db' => 'r.scanned_serial',  'dt' => 'scanned_serial', 'field' => 'scanned_serial' ),
	array( 'db' => 'r.pallet_reference',  'dt' => 'pallet_reference', 'field' => 'pallet_reference' ),
    array( 'db' => 'pi.pick_group_code',   'dt' => 'pick_group_code', 'field' => 'pick_group_code' ),
	array( 'db' => 'r.pick_group_id',   'dt' => 'pick_group_id', 'field' => 'pick_group_id' ),
    array( 'db' => 'r.processed',     'dt' => 'processed', 'field' => 'processed' ),
	array( 'db' => 'r.priority',     'dt' => 'priority', 'field' => 'priority' ),
	array( 'db' => 'r.date_created',     'dt' => 'date_created', 'field' => 'date_created' ),
	array( 'db' => 'r.last_updated_by',     'dt' => 'last_updated_by', 'field' => 'last_updated_by' )
);



// SQL server connection information
$sql_details = array(
    'user' => $mDbUser,
    'pass' => $mDbPassword,
    'db' => $mDbName,
    'host' => $mHost
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

// require( 'ssp.class.php' );
require( '../action/ssp.classAND.php' );


$joinQuery = "FROM replen_task as r left join pick_group as pi on pi.id = r.pick_group_id  left join part as p on p.id = r.part_id ";
$extraWhere = "";
$groupBy = "";
$having = "";

echo json_encode(
        SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having)
);



