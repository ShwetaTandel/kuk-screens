      
 <?php
include ('../config/phpConfig.php');

    //fetch table rows from mysql db
    $sql = "SELECT charge_master.id, charge_type.charge_name as charge_type, charge_master.charge_name,charge_type_id ,charge_master.std_charge, charge_master.created_by,charge_master.date_created,charge_master.last_updated_date,charge_master.last_updated_by FROM ".$mDbName.".charge_master left join ".$mDbName.".charge_type on charge_master.charge_type_id = charge_type.id;";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = $row;
    }
    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>