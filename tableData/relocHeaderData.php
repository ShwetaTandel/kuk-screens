<?php
 include ('../config/phpConfig.php');
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

// DB table to use
$table = 'reloc_header';

// Table's primary key
$primaryKey = 'id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    array('db' => 'h.id', 'dt' => 'id', 'field' => 'id'),
    array('db' => 'h.document_reference', 'dt' => 'document_reference', 'field' => 'document_reference'),
    array('db' => 'b.customer_reference', 'dt' => 'customer_reference', 'field' => 'customer_reference'),
    array('db' => 'h.quantity', 'dt' => 'quantity', 'field' => 'quantity'),
    array('db' => 'h.line_no', 'dt' => 'line_no', 'field' => 'line_no'),
    array('db' => 'h.from_location', 'dt' => 'from_location', 'field' => 'from_location'),
    array('db' => 'h.to_location', 'dt' => 'to_location', 'field' => 'to_location'),
    array('db' => 'h.processed', 'dt' => 'processed', 'field' => 'processed'),
    array('db' => 'h.last_updated_date', 'dt' => 'last_updated_date', 'field' => 'last_updated_date'),
    array('db' => 'h.last_updated_by', 'dt' => 'last_updated_by', 'field' => 'last_updated_by')
    
    
);

// SQL server connection information
$sql_details = array(
    'user' => $mDbUser,
    'pass' => $mDbPassword,
    'db' =>   $mDbName,
    'host' => $mHost
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

// require( 'ssp.class.php' );
require( '../action/ssp.classAND.php');

$joinQuery = "FROM ".$mDbName.".reloc_header as h left join ".$mDbName.".receipt_header as b on b.id = h.receipt_header_id";
$extraWhere = "";
$groupBy = "";
$having = "";



echo json_encode(
       // SSP::simpleReceiptQuery($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having)
         SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having)
);



?>
