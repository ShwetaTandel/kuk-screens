 <?php
include ('../config/phpConfig.php');
    //fetch table rows from mysql db
    $sql = "SELECT decant_header.id as id,document_reference,part_number,document_status_code,decant_header.last_updated_date,decant_header.last_updated_by FROM ".$mDbName.".decant_header join ".$mDbName.".document_status where document_status_id = document_status.id  order by decant_header.last_updated_date desc;";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = $row;
    }

    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>