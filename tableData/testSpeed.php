<?php
  include ('../config/phpConfig.php');
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
$stuff = ' inventory_master.*, inventory_status_code, location.location_code, tag.tag_reference'; 
$join = 'left join kuk.location on inventory_master.current_location_id = location.id left join tag on tag.id = inventory_master.parent_tag_id left join inventory_status on inventory_master.inventory_status_id = inventory_status.id';

// DB table to use
$table = 'inventory_master';
 
// Table's primary key
$primaryKey = 'id';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
  
    array( 'db' => 'part_number', 'dt' => 'part_number' ),
    array( 'db' => 'serial_reference',  'dt' => 'serial_reference' ),
    array( 'db' => 'ran_or_order',   'dt' => 'ran_or_order' ),
    array( 'db' => 'inventory_qty',  'dt' => 'inventory_qty' ),
    array( 'db' => 'allocated_qty',   'dt' => 'allocated_qty' ),
  //  array( 'db' => 'inventory_status_code',     'dt' => 'inventory_status_code' ),
    array( 'db' => 'location_code', 'dt' => 'location_code'),
    array( 'db' => 'tag_reference', 'dt' => 'tag_reference'),
    array( 'db' => 'requires_inspection', 'dt' => 'requires_inspection'),
    array( 'db' => 'date_created', 'dt' => 'date_created')
    
);


 
// SQL server connection information
$sql_details = array(
    'user' => $mDbUser,
    'pass' => $mDbPassword,
    'db'   => $mDbName,
    'host' => $mHost
);
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require( '../action/ssp.class.php' );
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $stuff, $join )
);