<?php
header('Cache-Control: no-cache, no-store, must-revalidate');   
header('Expires: 0');

// error_reporting(E_ALL);
// ini_set("display_errors", 1);

 include ('../config/phpConfig.php');
  include ('../config/ChromePhp.php');

// User
// Company
// Menu Header
// Menu Body

// Menu Detail
if (($mFunction != null) && ($mFunction == "menudetail"))
{
// Get Menu Header by Menu name
	If ($mMenu != "")				
		$mDataQuery = "SELECT id AS id FROM menu_header WHERE menu_name = '".$mMenu."' LIMIT 1";
	else
		$mDataQuery = "SELECT id AS id FROM menu_header LIMIT 1";
			
	$mReturnData = array();
	$i=0;
	$mId = "";
	$mData = mysqli_query($connection,$mDataQuery);
	while($mRow = mysqli_fetch_assoc($mData)) 
	{
		$mId = ($mRow['id']);
		$i++;
	}

// If Menu Exists Get Matching Body Records (categories) 
	if ($i > 0)
	{
               
		$i = 0;
		$mDataQuery = "SELECT id AS id, category_name AS category_name FROM menu_body WHERE menu_header_id=".$mId;				
		$mData = mysqli_query($connection,$mDataQuery) or
			die("FAIL-MySQL Error :".mysqli_error($connection));
                 
		while($mRow = mysqli_fetch_assoc($mData)) 
		{
			$mBodyId = ($mRow['id']);
			$mCategory = ($mRow['category_name']);
// For Each Body Get the Matching Details (Menu Items) into an array
			$mDetailQuery = "SELECT menu_detail.description AS name, menu_detail.main_Sequence AS main_sequence, menu_detail.sub_sequence AS sub_sequence, menu_detail.menu_code AS menu_code, menu_detail.action AS url FROM menu_detail WHERE active = 1 AND menu_body_id=".$mBodyId ;
			$mDetailData = mysqli_query($connection,$mDetailQuery);
			while($mRow = mysqli_fetch_assoc($mDetailData))
			{
				$mReturnData[] = $mRow;
			}
// Combine the Body Items & Detail Array into a JSON Array Element
			$i++;
//			$mJsonData[] =(array_merge(array($mCategory.":"),$mReturnData));
			$mJsonData[] = array($mCategory,$mReturnData);
			$mJson = json_encode($mJsonData);
			$mJson = str_replace(",[",":[",$mJson);
			$mJson = str_replace("]:[",",",$mJson);
			$mJson = str_replace("[[","{",$mJson);
			$mJson = str_replace("]]]","]}",$mJson);
                        unset($mReturnData);
                        
		}				
	}
					
	mysqli_close($connection);

// Encode the JSON Array into a JSON Object & echo this to the Calling Program
//	echo json_encode($mJsonData);
	echo($mJson);
}

?>