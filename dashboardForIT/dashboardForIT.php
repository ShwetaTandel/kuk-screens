<!DOCTYPE html>
<?php
include '../config/logCheck.php';
?>
<html>
    <head>
        <title>Dashboard</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <link href="../js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <link href="../css/dashboardStyle.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js" type="text/javascript"></script>  
        <script src="../dashboard/dashAjax.js" type="text/javascript"></script>
        <style>

        </style>

    </head>

    <body>



        <div id="div1" align="left" >
                    <h4 align="left" style="font-weight:bold">Location Area(Work or null)</h2>
							<div class="col-sm-2">
								<div class="well">

									<table id="locationAreaTable" class="compact stripe hover row-border" style="width:50%">
										<thead>
											<tr>
												<th>Location Code</th>
												<th>Location Area</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
        </div>
		<div id="div2" align="left" >
                    <h4 align="left" style="font-weight:bold">Filling Status(null)</h2>
							<div class="col-sm-1">
								<div class="well">

									<table id="fillingStatusTable" class="compact stripe hover row-border" style="width:50%">
										<thead>
											<tr>
												<th>Location Code</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
        </div>
		<div id="div3" align="left" >
                    <h4 align="left" style="font-weight:bold">Inventory(-ve Available Qty)</h2>
							<div class="col-sm-3">
								<div class="well">

									<table id="invWithNegativeAvalQtyTable" class="compact stripe hover row-border" style="width:50%">
										<thead>
											<tr>
												<th>Part Number</th>
												<th>Serial Reference</th>
												<th>Available Qty</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
        </div>
		<div id="div4" align="left" >
                    <h4 align="left" style="font-weight:bold">Inventory (Allocation but no open pick detail) </h2>
							<div class="col-sm-2">
								<div class="well">

									<table id="invAllocatedButNoOpenPickDetailTable" class="compact stripe hover row-border" style="width:50%">
										<thead>
											<tr>
												<th>Part Number</th>
												<th>Serial Reference</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
        </div>
		<div id="div5" align="left" >
			<h4 align="left" style="font-weight:bold">Blocked Tanums</h2>
					<div class="col-sm-2">
						<div class="well">

							<table id="blockedTanumTable" class="compact stripe hover row-border" style="width:50%">
								<thead>
									<tr>
										<th>Tanum</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
        </div>
				<div id="div6" align="right" >
                    <h4 align="left" style="font-weight:bold">Inventory(No Status)</h2>
							<div class="col-sm-2">
								<div class="well">

									<table id="invWithStatusIdBlankTable" class="compact stripe hover row-border" style="width:50%">
										<thead>
											<tr>
												<th>Part Number</th>
												<th>Serial Reference</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
        </div>



    <script>

        function openReplen() {
            window.open('replenTask.php', '_self')
        }


        $(document).ready(function () {
            var locationAreaTable = $('#locationAreaTable').DataTable({
                ajax: {"url": "../dashboardForIT/locationAreaTable.php", "dataSrc": ""},
                paging: false,
                searching: false,
                bInfo: false,
                ordering: false,
                columns: [
                    {data: "location_type_code"},
                    {data: "location_area"},
                ],
                order: [[0, 'asc']]
            });
			
			var fillingStatusTable = $('#fillingStatusTable').DataTable({
                ajax: {"url": "../dashboardForIT/fillingStatusTable.php", "dataSrc": ""},
                paging: false,
                searching: false,
                bInfo: false,
                ordering: false,
                columns: [
                    {data: "location_code"}
                ],
                order: [[0, 'asc']]
            });
			
			var invWithNegativeAvalQtyTable = $('#invWithNegativeAvalQtyTable').DataTable({
                ajax: {"url": "../dashboardForIT/invWithNegativeAvalQtyTable.php", "dataSrc": ""},
                paging: false,
                searching: false,
                bInfo: false,
                ordering: false,
                columns: [
                    {data: "part_number"},
					{data: "serial_reference"},
					{data: "available_qty"}
                ],
                order: [[0, 'asc']]
            });
			var invAllocatedButNoOpenPickDetailTable = $('#invAllocatedButNoOpenPickDetailTable').DataTable({
                ajax: {"url": "../dashboardForIT/invAllocatedButNoOpenPickDetailTable.php", "dataSrc": ""},
                paging: false,
                searching: false,
                bInfo: false,
                ordering: false,
                columns: [
                    {data: "part_number"},
					{data: "serial_reference"}
                ],
                order: [[0, 'asc']]
            });
			var blockedTanumTable = $('#blockedTanumTable').DataTable({
                ajax: {"url": "../dashboardForIT/blockedTanumTable.php", "dataSrc": ""},
                paging: false,
                searching: false,
                bInfo: false,
                ordering: false,
                columns: [
                    {data: "customer_reference"}
                ],
                order: [[0, 'asc']]
            });
			var invWithStatusIdBlankTable = $('#invWithStatusIdBlankTable').DataTable({
                ajax: {"url": "../dashboardForIT/invWithStatusIdBlankTable.php", "dataSrc": ""},
                paging: false,
                searching: false,
                bInfo: false,
                ordering: false,
                columns: [
                    {data: "part_number"},
					{data: "serial_reference"}
                ],
                order: [[0, 'asc']]
            });

        });



    </script>



</body>
</html>