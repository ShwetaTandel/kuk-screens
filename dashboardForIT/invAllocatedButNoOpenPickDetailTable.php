      
 <?php
include ('../config/phpConfig.php');
    //fetch table rows from mysql db
    $sql = "select part_number,serial_reference from inventory_master where id not in 
			(select inventory_master_id from pick_detail where inventory_master_id is not null and processed=false)
			and part_id not in(select id from part where jis_supply_group is not null and jis_supply_group != '')
			and allocated_qty > 0";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = $row;
    }

    echo json_encode($emparray);

    //close the db connection
    mysqli_close($connection);
?>