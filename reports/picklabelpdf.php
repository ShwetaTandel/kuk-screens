<?php
header('Cache-Control: no-cache, no-store, must-revalidate');   
header('Expires: 0');

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

// RTS Zebra Label

include('../barcode/php-barcode.php');
require('../barcode/fpdf.php');
include ('../config/phpConfig.php');
// require(dirname(__FILE__) . '/../../vimsconfig/phpConfig.php');

class PDF extends FPDF
{
// Page header
	function Header()
	{
// Logo
//		$this->Image('../images/vel_logo_large.jpg',10,6,50);
// Arial bold 15
		$this->SetFont('Arial','B',15);
// Move to the right
		$this->Cell(100);
// Title
		$this->Cell(50,10,'Part',0,0,'C');
// Line break
		$this->Ln(20);
	}

// Page footer
	function Footer()
	{
// Position at 1.5 cm from bottom
		$this->SetY(-15);
// Arial italic 8
		$this->SetFont('Arial','I',8);
// date / time
		$date = date('d/m/Y h:i:s a', time());
// Page number
		$this->Cell(0,10,'Page '.$this->PageNo().'   Date '.$date,0,0,'C');
	}
	
	function TextWithRotation($x, $y, $txt, $txt_angle, $font_angle=0)
    {
        $font_angle+=90+$txt_angle;
        $txt_angle*=M_PI/180;
        $font_angle*=M_PI/180;
    
        $txt_dx=cos($txt_angle);
        $txt_dy=sin($txt_angle);
        $font_dx=cos($font_angle);
        $font_dy=sin($font_angle);
    
        $s=sprintf('BT %.2F %.2F %.2F %.2F %.2F %.2F Tm (%s) Tj ET',$txt_dx,$txt_dy,$font_dx,$font_dy,$x*$this->k,($this->h-$y)*$this->k,$this->_escape($txt));
        if ($this->ColorFlag)
            $s='q '.$this->TextColor.' '.$s.' Q';
        $this->_out($s);
    }
// Barcode table
	function BarcodeTablePdf($header, $data)
	{
// date / time
		$date = date('d/m/Y h:i:s a', time());
// barcode settings
		$fontSize = 8;
		$marge    = 5;   // between barcode and hri in pixel
		$x        = 30;  // barcode center
		$y        = 70;  // barcode center
		$height   = 10;   // barcode height in 1D ; module size in 2D
		$width    = 0.25;    // barcode width in 1D ; not use in 2D
		$angle    = 0;   // rotation in degrees  
		$code     = 'VANTEC'; // barcode text;)
		$type     = 'code39';
		$black    = '000000'; // color in hex
		$ty = 82;
// Headings
		$this->AddPage();
		$this->Text(10,50,"Report Date: ".$date);
		
		$this->SetFont('Arial','',$fontSize);
		$this->SetTextColor(0, 0, 0);
// Initial Line
		$this->ln(30);
// Column widths
		$w = array(40, 0);
// Header
		$i = 0;
		foreach($header as $col)
		{
			$this->Cell($w[$i],7,$col,0);
			$i++;
		}
		$this->Ln();
// Data
		$l = 0;
		foreach($data as $row)
		{
			$c = 0;
			$x = 30;
			foreach($row as $key=>$col)
			{
				$barCol = $col;
				if (($key == "FLD03"))
				{
					$barcode = Barcode::fpdf($this, $black, $x, $y, $angle, $type,$barCol, $width, $height);
					$this->TextWithRotation($x, $y+8, $col, $angle);
				}
				else
				{
					if (strlen($col) > 0)
					{
						if ($key == "FLD07")
							$this->TextWithRotation(30, $ty, "Loc.", $angle);
						elseif ($key == "FLD01")
							$this->TextWithRotation(30, $ty, "Ref.", $angle);
						elseif ($key == "FLD06")
							$this->TextWithRotation(30, $ty, "Model", $angle);
						elseif ($key == "FLD02")
							$this->TextWithRotation(30, $ty, "Part", $angle);
						else
							$this->TextWithRotation(30, $ty, $key, $angle);
						$this->TextWithRotation(40, $ty, $col, $angle);
						$ty=$ty+5;
					}
				}
				$c++;
				
				$i++;
				if ($x == 30)
					$x = $x+20;
				else
					$x = $x+30;
			}
			$y = $ty+8;
			$ty = $ty+20;
//			$this->Ln();
//			$l = $l+1;
//			if ($l > 9)
			if ($ty > 250)
			{
				$this->AddPage();
				$this->Text(10,50,"Report Date: ".$date);
// Header
				$i = 0;
				$this->ln(30);
				foreach($header as $col)
				{
					$this->Cell($w[$i],7,$col,0);
					$i++;
				}
				$this->Ln();
				$l = 0;
				$y = 70;
				$ty = 80;
			}
		}
	}	
}

// Load data
function LoadData($mHost, $mUser, $mPwd, $mDbName, $mPickDetailId)
{
	global $connection;
        if($mHost === null){
     echo "here";
            $mDbDefaults = defaultDbConnection();
            $mHost = $mDbDefaults[0];
            $mUser = $mDbDefaults[1];
            $mPwd = $mDbDefaults[2];
            $mDbName = $mDbDefaults[3];
      
        }
// date / time
	$date = date('d/m/Y h:i', time());
// Read file lines
	$data = array();
// mysql connection
	if($connection == null){
	$connection=mysqli_connect($mHost,$mUser,$mPwd,$mDbName) or
		exit("FAIL-Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());     
	}
    //$mDataQuery = "select group_concat(order_header.customer_reference) AS FLD01, pick_detail.part_number AS FLD02,  CASE WHEN pick_detail.serial_reference_scanned IS null THEN pick_detail.serial_reference ELSE pick_detail.serial_reference_scanned END AS FLD03, CASE WHEN pick_detail.qty_scanned IS null THEN pick_detail.qty_allocated ELSE pick_detail.qty_scanned END AS FLD04,part.part_description AS FLD05,group_concat(order_header.machine_model) AS FLD06, group_concat(order_header.baan_serial )AS FLD07, DATE_FORMAT(now(),'%d/%m/%Y')  AS FLD08,pick_detail.location_code AS FLD09 from pick_detail join part on part.id = pick_detail.part_id join pick_header on pick_header.document_reference = pick_detail.document_reference join order_header on order_header.pick_header_id = pick_header.id where pick_detail.id = ".$mPickDetailId;
	$mDataQuery = "select group_concat(order_header.customer_reference) AS FLD01, pick_detail.part_number AS FLD02,  CASE WHEN pick_detail.serial_reference_scanned IS null THEN pick_detail.serial_reference ELSE pick_detail.serial_reference_scanned END AS FLD03, CASE WHEN pick_detail.qty_scanned IS null or pick_detail.qty_scanned=0 THEN pick_detail.qty_allocated ELSE pick_detail.qty_scanned END AS FLD04,part.part_description AS FLD05,group_concat(order_header.machine_model) AS FLD06, group_concat(order_header.baan_serial )AS FLD07, DATE_FORMAT(now(),'%d/%m/%Y')  AS FLD08,pick_detail.location_code AS FLD09 from pick_detail join part on part.id = pick_detail.part_id join pick_order_link on pick_order_link.pick_detail_id = pick_detail.id join order_body on order_body.id = pick_order_link.order_body_id join pick_header on pick_header.document_reference = pick_detail.document_reference join order_header on order_header.id = order_body.order_header_id where pick_detail.id = ".$mPickDetailId;
	// mysql query construct
	if (!$mData = mysqli_query($connection,$mDataQuery))
		return "FAIL-".mysqli_error($connection).":".$mDataQuery;
	$i = 0;
	while ($mRow = mysqli_fetch_assoc($mData))		
	{
            $data[] =($mRow);
            $i++;
	}
	mysqli_close($connection);
	if ($i == 0)
		return "FAIL-No Matching records ".$mDataQuery;
	else
		return $data;	
}

// Print label
function label($mLabel, $data, $mPrinter,$mLabelsAcross=0)
{
//    ChromePhp::log("in label");
//    print_r ($mLabel."-".$mPrinter."-".$mLabelsAcross);
	$mLabelContent = "";
// Label Layout
/*
HEADER
"^XA"
LABEL1
^PR2
^FO30,60^A0,30^FDFLD08^FS
^FO30,90^A0,30^FDLocation: FLD07 \ FLD09^FS
^FO250,60^A0,40^FDRTS: FLD01^FS
^FO30,120^A0,30^FDModel:^FS
^FO150,120^A0,50^FDFLD06^FS
^FO30,160^A0,30^FDPart:^FS
^FO150,160^A0,50^FDFLD02^FS
^FO460,160^A0,30^FDQty:^FS
^FO505,160^A0,40^FDFLD04^FS
^FO40,210^A0,50^FDFLD05^FS
^FO40,260^BY2^B3N,N,90,N,N^FDSFLD03^FS
^FO200,360^A0,30^FDFLD03^FS
FOOTER
"^XZ"
*/

// Load Label Template
	$mPrintData = __DIR__."/".$mLabel;

	$mFh = fopen($mPrintData, 'r');
	$mLabel = fread($mFh, filesize($mPrintData));
	fclose($mFh);
// Add NewLine Characters
	$lines=explode(PHP_EOL,$mLabel);

// No, Labels to a row (0=single label, 1=double labels)	
//	$mLabelsAcross = 0;
    $mHeaders = array();
	$mContent1 = "";
	$mContent2 = "";
    $mFooters = array();
	$c = 0;
    $i = 0;
	
// Build label from template
	foreach($lines as $line)
	{
        if ($line == "HEADER")
        {
            $c = 0;
            continue;
        }
        if ($line == "LABEL1")
        {
            $c = 1;
            continue;
        }
        if ($line == "LABEL2")
        {
            $c = 2;
            continue;
        }
        if ($line == "FOOTER")
        {
            $c = 9;
            continue;
        }
        if ($c == 0)
            $mHeaders[] = $line;
        if ($c == 1)
            $mContent1 .= $line."\n";
        if ($c == 2)
            $mContent2 .= $line."\n";
        if ($c == 9)
            $mFooters[] = $line;
	}
// Label Data
	$mLabelContent1 = $mContent1;
	$mLabelContent2 = $mContent2;

// Populate Label with data	`
	$c = 0;
	foreach($data as $row)
	{
		$i = 0;
		foreach($row as $k=>$v)
		{
// Replace Template placeholder with Value
			if ($c == 0)
				$mLabelContent1 = str_replace($k,$v,$mLabelContent1);
			else
				$mLabelContent2 = str_replace($k,$v,$mLabelContent2);
			$i++;
		}
		$c++;
		if ($c > $mLabelsAcross)
		{
//            print_r($mLabelContent1." ".$mLabelContent2);
//            print_r($mFooter);
// Send Label to Printer
			$fp = fsockopen($mPrinter, 9100, $errno, $errstr, 30);
            foreach ($mHeaders as $mHeader)
            {
                fputs ($fp,$mHeader."\n");
//                print_r ($mHeader."\n");
            }
//          fputs ($fp,"\x02L\n");
//			fputs ($fp,"H08\n");
//			fputs ($fp,"D11\n");
			fprintf($fp,$mLabelContent1.$mLabelContent2);
//            print_r($mLabelContent1.$mLabelContent2);
            foreach ($mFooters as $mFooter)
            {
                fputs ($fp,$mFooter."\n");
//                print_r ($mFooter."\n"); 
            }
//            fputs ($fp,"E\n");

// Restore Print Layout for next labels
			$mLabelContent1 = $mContent1;
			$mLabelContent2 = $mContent2;
			$c = 0;
		}
	}
// Print Remaining label if Two across
	if ($c > 0)
	{
		$fp = fsockopen($mPrinter, 9100, $errno, $errstr, 30);
		foreach ($mHeaders as $mHeader)
            fputs ($fp,$mHeader."\n");
		fprintf($fp,$mLabelContent1);
		foreach ($mFooters as $mFooter)
            fputs ($fp,$mFooter."\n"); 
	}
	fclose($fp);
	
	if ($mLabelContent != "")
		$mLabelContent = "FAIL-".$mLabelContent;
	return $mLabelContent;
}

// Return Default mySql Connection
function defaultDbConnection()
{
// get default System Settings from xml file	
	$mSystem = "DEFAULT";
// Get System Definition XML from URL
	$mData = file_get_contents("../config/systemsettings.xml");
// Parse XML to XML Object Array
	$xml = simplexml_load_string($mData, 'SimpleXMLElement', LIBXML_NOCDATA);
// Split-out System Definitions from XML 'fields' Nodes
	$i = 0;

	for ($i = 0; $i <= 150; $i++) 
	{			
		$mSystemId = "";
		$mPath = "";
		$mDbHost = "";
		$mDbUser = "";
		$mDbPwd = "";
		$mDbase = "";
		$mMqServer = "";
		$mCompany = "";
		if (("".$xml->systems->system[$i]->systemid."" == "") || ("".$xml->systems->system[$i]->systemid."" == false) || ("".$xml->systems->system[$i]->systemid."" == false))
			break;
		
		$mSystemId = "".$xml->systems->system[$i]->systemid."";
		if (($mSystemId == "") || ($mSystemId == false))
			break;
		
// Store System Settings from "trandef/systemsettings.xml"		
		if (strToUpper($mSystem) == strToUpper($mSystemId))
		{
			$mPath = "".$xml->systems->system[$i]->path."";
			$mDbHost = "".$xml->systems->system[$i]->dbhost."";
			$mDbUser = "".$xml->systems->system[$i]->dbuser."";
			$mDbPwd = "".$xml->systems->system[$i]->dbpwd."";
			$mDbase = "".$xml->systems->system[$i]->dbase."";
			break;
		}
		if (($mSystemId == "") || ($mSystemId == false))
			break;
	}
	$mDbDefaults = array();
	$mDbDefaults[0] = $mDbHost;
	$mDbDefaults[1] = $mDbUser;
	$mDbDefaults[2] = $mDbPwd;
	$mDbDefaults[3] = $mDbase;
	$mDbDefaults[4] = $mPath;
	return $mDbDefaults;
}

// Print Labels
function labelPrint($mPrinter,$mLabel,$mHost,$mUser,$mPwd,$mDbName,$mCurrentUser,$mOutput,$mPickDetailId,$mLabelsAcross=0,$mSerialReference=null,$mQty=null)
{
// date / time
	$date = date('d/m/Y h:i:s a', time());
	
	if (($mPrinter == null) || ($mPrinter == "") || (!$mPrinter))
		$mPrinter = "172.16.1.42";

	if (($mLabel == null) || ($mLabel == "") || (!$mLabel))
		$mLabel = "pick_zebra.txt";
	
//	return "FAIL-TEST1-".$mPartNumber."-".$mPrinter."-".$mLabel."-".$mSerialReference;
	
	$data = LoadData($mHost, $mUser, $mPwd, $mDbName, $mPickDetailId);
	if (substr($data,0,2)=="FA")
		return $data;
    if($mSerialReference != null)
        $data[0]['FLD03'] = $mSerialReference;
    if($mQty != null)
        $data[0]['FLD04'] = $mQty;
//	return LoadData($mHost, $mUser, $mPwd, $mDbName, $mFromLoc, $mToLoc);
	if ($mPrinter)
		$mResult = label($mLabel, $data, $mPrinter,$mLabelsAcross);
	if (substr($mResult,0,2)=="FA")
		return $mResult;

//            ChromePhp::log("sf".$mResult);
	$pdf = new PDF();
// Column headings
	$header = "Part,Desc,vendor,Serial,Ran,Qty";
	$pdf->SetFont('Arial','',12);

	$pdf->BarcodeTablePdf($header,$data);
	$pdf->Ln(5);
/*
	if ($mOutput == "")
		$pdf->Output();
	elseif($mOutput == "f")
	{
		$mPdf = "../../vimsreports/picklabel".$mCurrentUser.".pdf";
		$pdf->Output($mPdf,$mOutput);
	}
	else
	{
		$mPdf = "../../vimsreports/picklabel".$mCurrentUser.".pdf";
		$pdf->Output($mPdf,'f');
	}
*/
	return "OKMS-pickLabelPdf - Printer ".$mPrinter;
}

/*
// Data loading
$mFunction			= $_GET['function'];
$mConnect   		= $_GET['connection'];
$mUser				= $_GET['user'];
$mPwd				= $_GET['pword'];
$mHost              = $_GET['host'];
$mDbName			= $_GET['dbase'];
$mCurrentUser       = $_GET['currentuser'];
$mLabel      		= $_GET['label'];
$mOutput			= $_GET['output'];
$mPrinter			= $_GET['printer'];

$mFilter = str_replace("|AND|","&",$mFilter);
$mFilter = str_replace("/dbase/",$mDbName,$mFilter);
*/
?>