<?php
header('Cache-Control: no-cache, no-store, must-revalidate');   
header('Expires: 0');

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

// Manifest Summary

$pickreference 		= $_GET['pickreference'];
$mConnect   		= "";
$mUser				= "";
$mPwd				= "";
$mHost              = "";
$mDbName			= "";
$mCurrentUser       = "";
$mOutput			= "";

include('../barcode/php-barcode.php');
require('../barcode/fpdf.php');

class PDF extends FPDF
{




// Page header
	function Header()
	{
// Arial bold 15
		$this->SetFont('Arial','B',15);
// Move to the right
		$this->Cell(99);
// Title
		$this->Cell(1,10,'Pick Summary Sheet',0,0,'C');
// Line break
        $this-> Line(10, 20, 200, 20);
	}

// Page footer
	function Footer()
	{
// Position at 1.5 cm from bottom
		$this->SetY(-15);
// Arial italic 8
		$this->SetFont('Arial','I',8);
// date / time
		$date = date('d');
// Page number
		$this->Cell(0,10,'Page '.$this->PageNo().'   Date '.$date,0,0,'C');
	}  

// Text formatting for Barcode	
	function TextWithRotation($x, $y, $txt, $txt_angle, $font_angle=0)
    {
        $font_angle+=90+$txt_angle;
        $txt_angle*=M_PI/180;
        $font_angle*=M_PI/180;
    
        $txt_dx=cos($txt_angle);
        $txt_dy=sin($txt_angle);
        $font_dx=cos($font_angle);
        $font_dy=sin($font_angle);
    
        $s=sprintf('BT %.2F %.2F %.2F %.2F %.2F %.2F Tm (%s) Tj ET',$txt_dx,$txt_dy,$font_dx,$font_dy,$x*$this->k,($this->h-$y)*$this->k,$this->_escape($txt));
        if ($this->ColorFlag)
            $s='q '.$this->TextColor.' '.$s.' Q';
        $this->_out($s);
    }	
	
// Load data
	function LoadData($mConnection, $pickreference)
	{
	
      // Read file lines
		$data = array();		
		$mDataQuery = "SELECT order_header.id order_header_id,
        order_header.baan_serial,
        order_header.machine_model,
        pick_header.id pick_header_id,
        pick_header.document_reference,
        tl1.train_location_hash,
        count(distinct order_header.baan_serial) BAAN_Count,
        count(distinct order_header.machine_model) Machine_Count,
        substring(tl1.train_location_code,1,10) short_train,
        substring(tl1.train_location_code,12,4) trolley_train,
		substring(tl1.train_location_code,13,20) trol,
        substring(tl1.train_location_code,17,4) end_train,
		trolley.trolley troll_troll,
        trolley.id trolley_id,
        zone_destination.zone_destination zone_destination
		FROM pick_header
		join order_header on order_header.pick_header_id=pick_header.id
		join train_locations tl1 on substring(tl1.train_location_code,1,10)=pick_header.document_reference
        join trolley on trolley.trolley=substring(tl1.train_location_code,13,20)
        join zone_destination on zone_destination.trolley_id=trolley.id
		WHERE pick_header.document_reference='$pickreference' 
		group by train_location_hash, machine_model, baan_serial;";
        if (!mysqli_query($mConnection,$mDataQuery))
		{
			print_r("FAIL-".mysql_error($mConnection)."-".$mDataQuery);
		}
		// mysql query construct
		$mData = mysqli_query($mConnection,$mDataQuery);
		
		$i = 0;
		while($mRow = mysqli_fetch_assoc($mData)) 
		{
			$data[] =($mRow);
			$i++;
		}
				
		mysqli_close($mConnection);
		return $data;
		return $trains;
	}

	function getTrains($data){
		$trains = [];

		foreach($data as $value) {
			if (!in_array($value['train_location_hash'], $trains))
			array_push($trains, $value['train_location_hash']);
		};

		return $trains;
	}
	
	
	// Barcode table
	function BarcodeTable($header, $data, $shippingRef,$prefix, $trains)
	{
		//loop through each of the train location hash to create a pdf for each one.
		foreach ($trains as $train) {

			$baan_nums = [];
			$baan_counts = [];
			$baan_countlist = [];

			$machine_nums = [];
			$machine_counts = [];
			$machine_countlist = [];
			
			$train_nums = [];
			$train_counts = [];
			$train_countlist = [];
		
			$troll_nums = [];
			$troll_counts = [];
			$troll_countlist = [];
			
			$hash_codes = [];

			$zones = [];

			// $jdata = json_encode($data);

			foreach($data as $value) {
				//if the hash value is equal to the hash for this loop then add machine model to array
				if ($value['train_location_hash'] == $train){
					array_push($machine_nums, $value['machine_model']);
				}
				//get the hash value for this particular look
				if (!in_array($value['train_location_hash'], $hash_codes)){
					$hash_codes[$value['train_location_hash']] = $value['train_location_hash'];

				}
			};
			
			foreach($data as $value) {
				if ($value['train_location_hash'] == $train && !in_array($value['zone_destination'], $zones))
				array_push($zones, $value['zone_destination']);
			};
			
			foreach($data as $value) {
				if ($value['train_location_hash'] == $train && !in_array($value['baan_serial'], $baan_nums))
				array_push($baan_nums, $value['baan_serial']);
			};

			foreach($data as $value) {
				if ($value['train_location_hash'] == $train && !in_array($value['end_train'], $train_nums))
				array_push($train_nums, $value['end_train']);
			};
		
			foreach($data as $value) {
				if ($value['train_location_hash'] == $train && !in_array($value['trolley_train'], $troll_nums))
				array_push($troll_nums, $value['trolley_train']);
			};

			
			/*get the count for the machine numbers */
			$machine_counts = array_count_values($machine_nums);
			$baan_counts = array_count_values($baan_nums);

			// create the list so we can implode it to get string to display on pdf
			foreach($machine_counts as $key => $value){
				array_push($machine_countlist, $key . " x" . $value);
			}
		
			foreach($baan_counts as $key => $value){
				array_push($baan_countlist, $key . " x" . $value);
			}
				
			//get strings of what we need to display on the bardcode
			$machine_list = implode(", ", $machine_countlist);
			$baan_list = implode(", ", $baan_countlist);
			$zone_list = implode(" , ", $zones);
			$train_list = implode(" , ", $train_nums);
			$troll_list = implode(" , ", $troll_nums);


			$date = date('d/m/Y');
			// barcode settings
			   $fontSize = 12;
			   $marge    = 10;   // between barcode and hri in pixel
			   $x        = 105;  // barcode center
			   $y        = 161;  // barcode center
			   $height   = 20;   // barcode height in 1D ; module size in 2D
			   $width    = 0.75;    // barcode width in 1D ; not use in 2D
			   $angle    = 0;   // rotation in degrees  
			   $code     = ''; // barcode text;)
			   $type     = 'code39';
			   $black    = '000000'; // color in hex
			// Headings
	   
			
			   $this->SetFont('Arial','', 16);
			   $this->AddPage();
			   $this->setXY(10,20);  
			   $this->cell(190,12,"PRODUCTION ISSUE LOCATION",0,0,'C');
			//    $this-> Line(10, 31, 200, 31);
			//    $this-> Line(10, 50, 200, 50);
			$this->setXY(10,50);  
			$this-> Line(10, 47.5, 200, 47.5);
			$this->cell(190,11,"Model and Quantity",0,0,'C');
			$this-> Line(10, 77.5, 200, 77.5);
			$this->setXY(10,80);  
			   $this->cell(190,11,"Machine Number",0,0,'C');
			   $this->SetFont('Arial','BU', 24);
			   $this->setXY(10,31);  
			   $this->cell(190,19,$zone_list,0,0,'C');
			   $this->SetFont('Arial','BU', 11);
			   $this->setXY(10,61);  
			   $this->cell(190,19,$baan_list,0,0,'C');
			   $this->setXY(10,90);  
			   $this->cell(190,20,$machine_list,0,0,'C');
			   $this->SetFont('Arial','', 16);   
			   
			   $this->setXY(10,110);  
			   $this-> Line(10, 107.5, 200, 107.5);
			   $this->cell(190,10,"Run Number",0,0,'C');
			   $this->SetFont('Arial','BU', 14);   
			   $this->setXY(10,121);  
			   $this->cell(190,12,$data[0]['document_reference'],0,0,'C');
			   $this-> Line(10, 133.5, 200, 133.5);
			   $this->SetFont('Arial','', 16);   
			   
			   
			   $this-> Line(10, 10, 10, 235);
			   $this-> Line(200, 10, 200, 235);
			   $this->setXY(10,133.5);  
			   $this->cell(40,11.5,"Date",0,0,'C');
			   $this->setXY(80,133.5);  
			   $this->SetFont('Arial','B', 16);   
			   $this->cell(50,11.5,$date,0,0,'C');
			   $this-> Line(10, 145.5, 200, 145.5);
			   $this-> Line(50, 133.5, 50, 145.5);
			   $barcode = Barcode::fpdf($this, $black, $x, $y, $angle, $type, $hash_codes[$train], $width, $height);
			   $this->setXY(10,171.5);  
			   $this->SetFont('Arial','BU', 16);   
			   $this->cell(190,10, $troll_list . "-" . $train_list, 0 , 0, "C");
			   $this->setXY(10,185.5);  
			   $this->SetFont('Arial','', 16);   
			   $this->cell(190,12,"TROLLEY", 0 ,0, "C");
			   $this->setXY(10,196.5);  
			   $this->SetFont('Arial','BU', 16);   
			   $this->cell(190,10,$troll_list,0,0,'C');
			   //    $this-> Line(200, 200, 10, 200);
			   //    $this-> Line(200, 210, 10, 210);
			   $this->setXY(10,210.5);  
			   $this->SetFont('Arial','', 16);   
			   $this->cell(190,12,"TRAIN",0 ,0, "C");
			   $this->SetFont('Arial','BU', 16);   
			   $this->setXY(10,221.5);  
			   $this->cell(190,10,$train_list,0,0,'C');
			   
			   $this-> Line(10, 210, 200, 210);
			   $this-> Line(10, 184, 200, 184);
			   $this-> Line(10, 10, 200, 10);
			   $this-> Line(10, 235, 200, 235);
			   
			   
			   // $this->Text(100,50,"Report Date: ".$date);
			   
			   $this->SetFont('Arial','',$fontSize);
			   $this->SetTextColor(0, 0, 0);
			// Initial Line
			   $this->ln(30);
			// Column widths
			   $w = array(108, 50);
	   
			
		}
       // date / time
	
		
			
	
		}
		
	 // }
	}



function getPrefix($mHost, $mUser, $mPwd, $mDbName)
{
// Read Headers
	$prefix = array();
// mysql connection
	$mConnection=mysqli_connect($mHost,$mUser,$mPwd,$mDbName) or
		exit("FAIL-Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());
		
	$mDataQuery = "SELECT field_name AS fieldName, substr(prefix_name,1,1) AS prefix FROM default_setting";
// mysql query construct
	if(!mysqli_query($mConnection,$mDataQuery))
		exit("FAIL-".mysqli_error($mConnection));
	$mData = mysqli_query($mConnection,$mDataQuery);
	$i = 0;
	while($mRow = mysqli_fetch_assoc($mData)) 
	{
		$prefix[] =($mRow);
		$i++;
	}
	mysqli_close($mConnection);
	return $prefix;
//    print_r($data.":".$mDataQuery.":".$i);
}

// Main Process
function mainProcess($mHost, $mUser, $mPwd, $mDbName,$pickreference,$mOutput)
{
	$pdf = new PDF('P','mm', array(210,297));
// Column headings
	$header = array();

	if (!$mConnection=mysqli_connect($mHost,$mUser,$mPwd,$mDbName))
		$mConnection = $connection;

// Field Prefixes
	$prefix = getPrefix($mHost, $mUser, $mPwd, $mDbName);

// date / time
	$date = date('d/m/Y h:i:s a');

	$data = $pdf->LoadData($mConnection, $pickreference);
	$trains = $pdf->getTrains($data);
	$pdf->SetFont('Arial','',12);

	$pdf->BarcodeTable($header,$data,$pickreference,$prefix, $trains);
	$pdf->Ln(5);

	if ($mOutput == "")
		$pdf->Output();
	else
	{
		$mPdf = "../../../vimsreports/manifestsummary".$mCurrentUser.".pdf";
		$pdf->Output($mPdf,$mOutput);
	}
}



	include("../vimsconfig/phpConfig.php");
	$mConnection = defaultDbConnectionVal("DEFAULT");
	$mConnectionVals = explode(",",$mConnection);
	$mHost = $mConnectionVals[1];
	$mUser = $mConnectionVals[2];
	$mPwd = $mConnectionVals[3];
	$mDbName = $mConnectionVals[4];
	mainProcess($mHost, $mUser, $mPwd, $mDbName,$pickreference,$mOutput);
	
?>