<?php
header('Cache-Control: no-cache, no-store, must-revalidate');   
header('Expires: 0');

error_reporting(E_ALL);
ini_set('display_errors', 0);

// Manifest Summary

$pickreference 		= $_GET['pickreference'];
$mConnect   		= "";
$mUser				= "";
$mPwd				= "";
$mHost              = "";
$mDbName			= "";
$mCurrentUser       = "";
$mOutput			= "";

include('../barcode/php-barcode.php');
require('../barcode/fpdf.php');
include("../config/phpConfig.php");


class PDF extends FPDF
{

// Page header
	function Header()
	{
		$this->SetY(4);
// Arial bold 15
		$this->SetFont('Helvetica','BU',12);
		$this->Cell(120,5,"",0,0,'C');
		$this->Cell(40,8,"Pick Details",0,1,'L');
		$this->SetFont('Helvetica','B',8);
		$date = date("d/m/y H:i:s");

// Title
		$this->Cell(120,3,'Picking Ref: ' . $_GET['pickreference'],0,0,'L');
		$this->Cell(40,3,"Report Run Time: " . $date,0,1,'L');

// Line break
        
	}

// Page footer
	function Footer()
	{
// Position at 1.5 cm from bottom
		$this->SetY(-10);
// Arial italic 8
		$this->SetFont('Arial','I',7);
// date / time
        $date = date('d');
// Page number
		$this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
	}  

// Text formatting for Barcode	
	function TextWithRotation($x, $y, $txt, $txt_angle, $font_angle=0)
    {
        $font_angle+=90+$txt_angle;
        $txt_angle*=M_PI/180;
        $font_angle*=M_PI/180;
    
        $txt_dx=cos($txt_angle);
        $txt_dy=sin($txt_angle);
        $font_dx=cos($font_angle);
        $font_dy=sin($font_angle);
    
        $s=sprintf('BT %.2F %.2F %.2F %.2F %.2F %.2F Tm (%s) Tj ET',$txt_dx,$txt_dy,$font_dx,$font_dy,$x*$this->k,($this->h-$y)*$this->k,$this->_escape($txt));
        if ($this->ColorFlag)
            $s='q '.$this->TextColor.' '.$s.' Q';
        $this->_out($s);
    }	
	
// Load data
	function LoadData($mConnection, $pickreference)
	{
	
      // Read file lines
		$data = array();		
		$mDataQuery = "SELECT 
		order_header.id order_header_id,
		order_header.baan_serial,
		order_header.machine_model,
		order_header.order_type,
		order_body.order_header_id,
		order_body.customer_reference,
		order_body.zone_destination,
		order_body.line_no,
		order_body.part_number,
		order_body.qty_expected ord,
		order_body.qty_transacted pick,
		order_body.difference,
		pick_header.id pick_header_id,
		pick_header.document_reference,
		part.part_description,
		part.fixed_location_type_id,
		tr.trolley,
		tr.id trolleyid,
		zone_destination.zone_destination zone_destination,
		zone_destination.trolley_id,
		location_type.location_type_code
		FROM kuk.pick_header
		join order_header on order_header.pick_header_id=pick_header.id
		join order_body on order_body.order_header_id=order_header.id
		join zone_destination on order_body.zone_destination=zone_destination.zone_destination
		join trolley tr on zone_destination.trolley_id=tr.id
		JOIN part on order_body.part_number=part.part_number
		JOIN location_type on part.fixed_location_type_id=location_type.id
		WHERE pick_header.document_reference='$pickreference'
        order by trolleyid;";

	

        if (!mysqli_query($mConnection,$mDataQuery))
		{
			print_r("FAIL-".mysql_error($mConnection)."-".$mDataQuery);
		}
		// mysql query construct
		$mData = mysqli_query($mConnection,$mDataQuery);
		
		$i = 0;
		while($mRow = mysqli_fetch_assoc($mData)) 
		{
			$data[] =($mRow);
			$i++;
		}
				
		mysqli_close($mConnection);
		return $data;

	}
	
	
	// Barcode table
	function BarcodeTable($header, $data, $shippingRef,$prefix)
	{

			   $date = date('d/m/Y');
			// barcode settings
			   $fontSize = 10;
			   $marge    = 10;   // between barcode and hri in pixel
			   $x        = 105;  // barcode center
			   $y        = 161;  // barcode center
			   $height   = 20;   // barcode height in 1D ; module size in 2D
			   $width    = 0.75;    // barcode width in 1D ; not use in 2D
			   $angle    = 0;   // rotation in degrees  
			   $code     = ''; // barcode text;)
			   $type     = 'code39';
			   $black    = '000000'; // color in hex
			// Headings
	   
			
			   $this->SetFont('Arial','', 7);
			   $this->AddPage();
			 		//loop through each of the train location hash to create a pdf for each one.
			$count = count($data);

			for($i = 0; $i < $count; $i++){

				// // ADD IN THE LOGIC TO CREATE PAGE per Trolley //
				// if($i % 5 == 0){
				// 	$this->AddPage();
				// }
				if($i == 0) {
					$this->SetFont('Helvetica','B',8);
					$this->SetX(16.5);
					$this->Cell(18,7,"Train/Trolley: " . $data[$i]['trolley'],0,1,'C');
					$this->SetFont('Helvetica','',8);
					$this->Cell(18,5,'Line Loc.',0,0,'L');
			
					$this->Cell(18,5,'Pick Type.',0,0,'L');
			
					$this->Cell(30,5,'Part No.',0,0,'L');
			
					$this->Cell(35,5,'Part Description.',0,0,'L');
					$this->SetX(130);
			
					$this->Cell(20,5,'Order',0,0,'L');
			
					$this->Cell(12,5,'Pos',0,0,'C');
					$this->Cell(12,5,'Ord',0,0,'C');
					$this->Cell(12,5,'Pick',0,0,'C');
					$this->Cell(12,5,'Diff',0,0,'C');
					$this->SetX(220);
					$this->Cell(35,5,'Serial',0,0,'L');
					$this->Cell(35,5,'Model',0,1,'L');
					$this->SetFont('Arial','', 7);
				}
				if ($i > 1 && $data[$i -1]['trolley'] !== $data[$i]['trolley']){
					$this->AddPage();
					$this->SetFont('Helvetica','B',8);
					$this->SetX(16.5);
					$this->Cell(18,7,"Train/Trolley: " . $data[$i]['trolley'],0,1,'C');
					$this->SetFont('Helvetica','',8);
					$this->Cell(18,5,'Line Loc.',0,0,'L');
			
					$this->Cell(18,5,'Pick Type.',0,0,'L');
			
					$this->Cell(30,5,'Part No.',0,0,'L');
			
					$this->Cell(35,5,'Part Description.',0,0,'L');
					$this->SetX(130);
			
					$this->Cell(20,5,'Order',0,0,'L');
			
					$this->Cell(12,5,'Pos',0,0,'C');
					$this->Cell(12,5,'Ord',0,0,'C');
					$this->Cell(12,5,'Pick',0,0,'C');
					$this->Cell(12,5,'Diff',0,0,'C');
					$this->SetX(220);
					$this->Cell(35,5,'Serial',0,0,'L');
					$this->Cell(35,5,'Model',0,1,'L');
					$this->SetFont('Arial','', 7);
				}


				$this->SetX(11);
				$this->Cell(18, 4, $data[$i]['zone_destination'], 0, 0, 'L');
				$this->Cell(18, 4, $data[$i]['location_type_code'], 0, 0, 'L');
				$this->Cell(30,4,$data[$i]['part_number'],0,0,'L');
				$this->Cell(35,4,$data[$i]['part_description'],0,0,'L');
				$this->SetX(130);
				$this->Cell(20,4,$data[$i]['customer_reference'],0,0,'L');
				$this->Cell(12,4,$data[$i]['line_no'],0,0,'C');
				$this->Cell(12,4,$data[$i]['ord'],0,0,'C');
				$this->Cell(12,4,$data[$i]['pick'],0,0,'C');
				$this->Cell(12,4,$data[$i]['difference'],0,0,'C');
				$this->SetX(220);
				$this->Cell(35,4,$data[$i]['baan_serial'],0,0,'L');
				$this->Cell(35,4,$data[$i]['machine_model'],0,1,'L');
				
			}
		
			// Column widths
			   $w = array(108, 50);
	   	
		}
		
	 // }
	}



function getPrefix($db, $mDbUser, $mDbPassword, $mDbName)
{
// Read Headers
	$prefix = array();
// mysql connection
	$mConnection=mysqli_connect($db,$mDbUser,$mDbPassword,$mDbName) or
		exit("FAIL-Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());
		
	$mDataQuery = "SELECT field_name AS fieldName, substr(prefix_name,1,1) AS prefix FROM default_setting";
// mysql query construct
	if(!mysqli_query($mConnection,$mDataQuery))
		exit("FAIL-".mysqli_error($mConnection));
	$mData = mysqli_query($mConnection,$mDataQuery);
	$i = 0;
	while($mRow = mysqli_fetch_assoc($mData)) 
	{
		$prefix[] =($mRow);
		$i++;
	}
	mysqli_close($mConnection);
	return $prefix;
//    print_r($data.":".$mDataQuery.":".$i);
}

// Main Process
function mainProcess($db, $mDbUser, $mDbPassword, $mDbName,$pickreference,$mOutput)
{
	$pdf = new PDF('L','mm', array(210,297));
// Column headings
	$header = array();

	if (!$mConnection=mysqli_connect($db,$mDbUser,$mDbPassword,$mDbName))
		$mConnection = $connection;

// Field Prefixes
	$prefix = getPrefix($db, $mDbUser, $mDbPassword, $mDbName);

// date / time
	$date = date('d/m/Y h:i:s a');

	$data = $pdf->LoadData($mConnection, $pickreference);
	// $trains = $pdf->getTrains($data);
	$pdf->SetFont('Arial','',12);

	$pdf->BarcodeTable($header,$data,$pickreference,$prefix);
	$pdf->Ln(5);

	if ($mOutput == "")
		$pdf->Output();
	else
	{
		$mPdf = "../../../vimsreports/manifestsummary".$mCurrentUser.".pdf";
		$pdf->Output($mPdf,$mOutput);
	}
}

mainProcess($db, $mDbUser, $mDbPassword, $mDbName,$pickreference,$mOutput);
